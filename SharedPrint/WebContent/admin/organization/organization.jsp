<%@ page language="java" import="java.util.*" pageEncoding="utf-8" contentType="text/html; charset=utf-8"%>
<div class="panel">
  <div class="panel-heading">
    查询区域
  </div>
  <div class="panel-body">
    <div class="row">
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon">机构编码</span> <input type="text"
						id="OrgCode" name="OrgCode" class="form-control"
						placeholder="">
				</div>
			</div>
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon">机构名称</span> <input type="text"
						id="OrgNameLike" name="OrgNameLike"
						class="form-control" placeholder="">
				</div>
			</div>
			<div class="col-md-4">
				<button class="btn btn-success" type="button" id="searchBtn">
					<i class="icon-search"></i>搜索
				</button>
			</div>
		</div>
  </div>
</div>
<div class="datagrid">
  <div class="search-box">
  	<button class="btn btn-success " type="button"
  	  data-toggle="modal" data-target="#addModal" id="addBtn"><i class="icon-plus"></i>新增</button>
  	  &nbsp;&nbsp;&nbsp;&nbsp;
  	<button class="btn btn-info " data-toggle="modal" data-target="#updateModal"
  		 type="button" id="updateBtn"><i class="icon icon-edit"></i>修改</button>
    	&nbsp;&nbsp;&nbsp;&nbsp;
    <button class="btn btn-danger " type="button" id="deleteBtn"><i class="icon icon-trash"></i>删除</button>
    	&nbsp;&nbsp;&nbsp;&nbsp;
    <button class="btn " type="button" id="detailBtn" 
    	data-toggle="modal" data-target="#detailModal"><i class="icon icon-info"></i>详情</button>
  </div><hr>
</div>
<div id="orgListDataGrid" class="datagrid"></div>
 <div id="listPager" class="pager"  data-ride="pager" ></div>
 <div class="modal fade" id="addModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
        <h4 class="modal-title">新增菜单</h4>
      </div>
      <form id="addForm" name="addForm">
      <div class="modal-body">
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">机构编码</span>
						  <input type="text" class="form-control" id="addOrgCode" name="addOrgCode" placeholder="机构编码">
						</div>	
        			</div> 
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">机构名称</span>
						  <input type="text" class="form-control" id="addOrgName" name="addOrgName" placeholder="机构名称">
						</div>	
        			</div>       	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">上级机构名称</span>
						  <select class="form-control" id="addOrgParentCode" name="addOrgParentCode">
						    <option value="0">请选择</option>
						  </select>
						</div>	
        			</div> 
        			<div class="col-md-6">
	    				<div class="input-group">
						 <span class="input-group-addon">机构所属地区</span>
						 <input type="text" id="addOrgAreaBelong" name="addOrgAreaBelong" class="form-control" placeholder="">
						</div>
	    			</div>       			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">机构联系人</span>
						  <input type="text" class="form-control" id="addOrgConnPerson" name="addOrgConnPerson" placeholder="机构联系人">
						</div>	
        			</div> 
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">机构联系电话</span>
						  <input type="text" class="form-control" id="addOrgConnTelephone" name="addOrgConnTelephone" placeholder="机构联系电话">
						</div>	
        			</div>       	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-12">
        				<div class="input-group">
						  <span class="input-group-addon">机构地址</span>
						    <textarea  id="addOrgAddress" name="addOrgAddress" 
				  				cols="60" rows="5"class="form-control"></textarea>
						</div>	
        			</div>        			      	
        	</div>
      </div>
      <input type="hidden" id="action" name="action" value="add"/>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
        <button type="submit" class="btn btn-primary" id="addSaveBtn" name="addSaveBtn">保存</button>
      </div>
      </form>
    </div>
  </div>
</div>
 <div class="modal fade" id="updateModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
        <h4 class="modal-title">修改机构信息</h4>
      </div>
      <form id="updateForm" name="updateForm">
      <div class="modal-body">
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">机构编码</span>
						  <input type="text" class="form-control" id="updateOrgCode" name="updateOrgCode" placeholder="机构编码">
						</div>	
        			</div> 
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">机构名称</span>
						  <input type="text" class="form-control" id="updateOrgName" name="updateOrgName" placeholder="机构名称">
						</div>	
        			</div>       	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">上级机构名称</span>
						  <select class="form-control" id="updateOrgParentCode" name="updateOrgParentCode">
						    <option value="#">请选择</option>
						  </select>
						</div>	
        			</div> 
        			<div class="col-md-6">
	    				<div class="input-group">
						 <span class="input-group-addon">机构所属地区</span>
						 <input type="text" id="updateOrgAreaBelong" name="updateOrgAreaBelong" class="form-control" placeholder="">
						</div>
	    			</div>       			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">机构联系人</span>
						  <input type="text" class="form-control" id="updateOrgConnPerson" name="updateOrgConnPerson" placeholder="机构联系人">
						</div>	
        			</div> 
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">机构联系电话</span>
						  <input type="text" class="form-control" id="updateOrgConnTelephone" name="updateOrgConnTelephone" placeholder="机构联系电话">
						</div>	
        			</div>       	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-12">
        				<div class="input-group">
						  <span class="input-group-addon">机构地址</span>
						    <textarea  id="updateOrgAddress" name="updateOrgAddress" 
				  				cols="60" rows="5"class="form-control"></textarea>
						</div>	
        			</div>        			      	
        	</div>
      </div>
      <input type="hidden" id="action" name="action" value="update">
      <input type="hidden" id="updateOrgId" name="updateOrgId">
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
        <button type="submit" class="btn btn-primary" id="updateSaveBtn" name="updateSaveBtn">保存</button>
      </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="detailModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
        <h4 class="modal-title">机构详情</h4>
      </div>
      <form id="detailForm" name="detailForm">
      <div class="modal-body">
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">机构编码</span>
						  <input type="text" class="form-control" id="detailOrgCode" name="detailOrgCode" placeholder="机构编码">
						</div>	
        			</div> 
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">机构名称</span>
						  <input type="text" class="form-control" id="detailOrgName" name="detailOrgName" placeholder="机构名称">
						</div>	
        			</div>       	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">上级机构名称</span>
						  <input type="text" class="form-control" id="detailOrgParentName" name="detailOrgParentName" placeholder="上级机构名称">
						</div>	
        			</div> 
        			<div class="col-md-6">
	    				<div class="input-group">
						 <span class="input-group-addon">机构所属地区</span>
						 <input type="text" id="detailOrgAreaBelong" name="detailOrgAreaBelong" class="form-control" placeholder="">
						</div>
	    			</div>       			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">机构联系人</span>
						  <input type="text" class="form-control" id="detailOrgConnPerson" name="detailOrgConnPerson" placeholder="机构联系人">
						</div>	
        			</div> 
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">机构联系电话</span>
						  <input type="text" class="form-control" id="detailOrgConnTelephone" name="detailOrgConnTelephone" placeholder="机构联系电话">
						</div>	
        			</div>       	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-12">
        				<div class="input-group">
						  <span class="input-group-addon">机构地址</span>
						    <textarea  id="detailOrgAddress" name="detailOrgAddress" 
				  				cols="60" rows="5"class="form-control"></textarea>
						</div>	
        			</div>        			      	
        	</div>
      </div>
      <input type="hidden" id="detailOrgId" name="detailOrgId"/>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
      </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/organization.js?ver=1"></script>
<!--  <script type="text/javascript" src="${pageContext.request.contextPath}/js/menu.js?version=33"></script>-->