<%@ page language="java" pageEncoding="UTF-8"%>
<div class="container-fluid">
	<hr>
	<input type="text" class="form-control" readonly="readonly" value="本年度商户增长表">
	<div class="row">
		<div class="col-md-12">
			<canvas id="myLineChart" width="1000" height="400"></canvas>
		</div>
	</div>
	<hr>
	<input type="text" class="form-control"  readonly="readonly" value="本年度客户增长表">
	<div class="row">
		<div class="col-md-12">
			<canvas id="myBarChart" width="1000" height="400"></canvas>
		</div>
	</div>
	
</div>
<script src="${pageContext.request.contextPath}/js/zui.chart.js"></script>
<script src="${pageContext.request.contextPath}/js/adminMainCenter.js?version=1"></script>
