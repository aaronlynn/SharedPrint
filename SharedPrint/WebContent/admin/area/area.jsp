<%@ page language="java" pageEncoding="UTF-8"%>
<link href="${pageContext.request.contextPath}/css/zui.datagrid.css"
	rel="stylesheet">
<script src="${pageContext.request.contextPath}/js/zui.datagrid.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery.validate.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/localization/messages_zh.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/area.js?version=1"></script>

<div class="panel">
	<div class="panel-heading">查询地区</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon">地区编码</span> <input type="text"
						id="queryAreaCode" name="queryAreaCode" class="form-control"
						placeholder="">
				</div>
			</div>
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon">地区名称</span> <input type="text"
						id="queryAreaNameLike" name="queryAreaNameLike"
						class="form-control" placeholder="">
				</div>
			</div>
			<div class="col-md-4">
				<button class="btn btn-success" type="button" id="searchBtn">
					<i class="icon-search"></i>搜索
				</button>
			</div>
		</div>
	</div>
</div>
<div class="datagrid">
	<div class="search-box">
		<button class="btn btn-success " type="button" data-toggle="modal"
			data-target="#addModal" id="addBtn">
			<i class="icon-plus"></i>新增
		</button>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<button class="btn btn-info " type="button" id="updateBtn">
			<i class="icon icon-edit"></i>修改
		</button>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<button class="btn btn-danger " type="button" id="deleteBtn">
			<i class="icon icon-trash"></i>删除
		</button>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<button class="btn " type="button" id="detailBtn">
			<i class="icon icon-info"></i>详情
		</button>
	</div>

	<div id="listDataGrid" class="datagrid" data-ride="datagrid"></div>

	<div id="listPager" class="pager" data-ride="pager"></div>
</div>

<!-- 添加页面 -->
<div class="modal fade" id="addModal">
	<div class="modal-dialog">
		<form id="addForm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
					</button>
					<h4 class="modal-title">新增地区</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">地区编码</span> <input type="text"
									id="addAreaCode" name="addAreaCode" class="form-control"
									placeholder="">
							</div>
						</div>
						<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">地区名称</span> <input type="text"
										id="addAreaName" name="addAreaName" class="form-control"
										placeholder="">
								</div>
							</div>
							</div>
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">上级地区</span> <select
										id="addAreaParentCode" name="addAreaParentCode"
										class="form-control">
										<option value="">请选择</option>
									</select>
								</div>
							</div>

							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">排序码</span> <input type="text"
										id="addSortNo" name="addSortNo" class="form-control"
										placeholder="">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">描述</span>
									<textarea id="addDescription" name="addDescription" cols="60" rows="5"
										class="form-control"></textarea>
								</div>
							</div>


						</div>
					</div>
					<div class="modal-footer">
						<input type="hidden" name="action" value="addArea" />
						<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
						<button type="submit" id="addSaveBtn" class="btn btn-primary">保存</button>
					</div>
				</div>
		</form>
	</div>
</div>



<!-- 修改页面  -->
<div class="modal fade" id="updateModal">
	<div class="modal-dialog">
		<form id="updateForm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
					</button>
					<h4 class="modal-title">修改地区</h4>
				</div>

				<div class="modal-body">
					<div class="row">
					<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">地区编码</span> <input type="text"
									id="updateAreaCode" name="updateAreaCode" class="form-control"
									placeholder="">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">地区名称<font color="red">*</font></span>
								<input type="text" class="form-control" id="updateAreaName"
									name="updateAreaName">
							</div>
						</div>
						
					</div>
					<div style="margin-top: 10px;"></div>
					<div class="row">
					<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">所属上级地区名称</span> <select
									class="form-control" id="updateAreaParentCode"
									name="updateAreaParentCode">
									<option value="0">请选择</option>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">排序码<font color="red">*</font></span>
								<input type="text" class="form-control" id="updateSortCode"
									name="updateSortCode" maxlength="4">
							</div>
						</div>
					</div>
					<div class="row">
					<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">描述<font color="red">*</font></span>
								<textarea rows="5" cols="60" class="form-control" id="updateDescription"
									name="updateDescription"></textarea>
							</div>
						</div>
					</div>
				</div>

			</div>
			<div style="margin-top: 10px;"></div>
			<div class="row">
				<div class="modal-footer">
					<input type="hidden" id="updateAreaId" name="updateAreaId" /> <input
						type="hidden" id="action" name="action" value="updateArea" />
					<button type="button" class="btn btn-default " data-dismiss="modal">关闭</button>
					<button type="submit" class="btn btn-primary" id="updateSave"
						name="updateSave">保存</button>
				</div>
			</div>
		</form>
	</div>
</div>
<!-- 删除确认 -->
<div class="modal fade" id="deleteModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">删除地区</h4>
			</div>
			<div class="modal-body">
				<p>您确定要删除此地区信息吗?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary" id="deleteAreaBtn">确定</button>
			</div>
		</div>
	</div>
</div>
<!-- 详情页面  -->
<div class="modal fade" id="detailModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">地区详情</h4>
			</div>
			<form id="detailForm">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">地区编号</span> <span
									class="form-control" id="detailAreaCode"></span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">地区名称</span> <span
									class="form-control" id="detailAreaName"></span>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px;"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">所属上级地区</span> <span
									class="form-control" id="detailParentName"></span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">排序码</span> <span
									class="form-control" id="detailSortNo"></span>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px;"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">描述</span> <span
									class="form-control" id="detailDescription"></span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">操作时间</span> <span
									class="form-control" id="detailOprDate"></span>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px;"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default " data-dismiss="modal">关闭</button>
				</div>
			</form>
		</div>
	</div>
</div>