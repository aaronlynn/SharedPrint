<%@ page language="java" pageEncoding="UTF-8"%>
<head>
<link href="${pageContext.request.contextPath}/css/zui.uploader.css">
<link href="${pageContext.request.contextPath}/css/zui.datagrid.css"
	rel="stylesheet">
<script src="${pageContext.request.contextPath}/js/zui.datagrid.js"></script>
<script src="${pageContext.request.contextPath}/js/zui.uploader.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery.validate.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/localization/messages_zh.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/agent.js?version=1"></script>
</head>


<div class="panel">
	<div class="panel-heading">查询代理商</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-5">
				<div class="input-group">
					<span class="input-group-addon">代理商名称</span> <input type="text"
						id="queryAgentNameLike" name="queryAgentNameLike"
						class="form-control" placeholder="">
				</div>
			</div>
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon">代理商编码</span> <input type="text"
						id="queryAgentCode" name="queryAgentCode" class="form-control"
						placeholder="">
				</div>
			</div>

		</div>
		<div style="margin-top: 10px;"></div>
		<div class="row">
			<div class="col-md-5">
				<div class="input-group">
					<span class="input-group-addon">代理商状态</span>
						<select class="form-control" id="queryAgentStatus" name="queryAgentStatus">
						 <option value="">请选择</option>
						</select>
				</div>
			</div>
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon">所属机构</span> <input type="text"
						id="queryAgentOrgName" name="queryAgentOrgName"
						class="form-control" placeholder="">
				</div>
			</div>

			<div class="col-md-2">
				<button class="btn btn-success" type="button" id="searchBtn">
					<i class="icon-search"></i>搜索
				</button>
			</div>
		</div>
	</div>
</div>
<div class="datagrid">
	<div class="search-box">
		<button class="btn btn-success " type="button" data-toggle="modal"
			data-target="#addModal" id="addBtn">
			<i class="icon-plus"></i>新增
		</button>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<button class="btn btn-info " type="button" id="updateBtn">
			<i class="icon icon-edit"></i>修改
		</button>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<button class="btn btn-danger " type="button" id="deleteBtn">
			<i class="icon icon-trash"></i>删除
		</button>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<button class="btn " type="button" id="detailBtn">
			<i class="icon icon-info"></i>详情
		</button>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<button class="btn btn-primary" type="button" id="checkBtn">
			<i class="icon icon-linux"></i>审核
		</button>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<button class="btn btn-warning" type="button" id="unfreezeBtn">
			<i class="icon icon-yinyang"></i>解冻
		</button>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<button class="btn btn-warning" type="button" id="freezeBtn">
			<i class="icon icon-yinyang"></i>冻结
		</button>
	</div>
     <div style="margin-top: 10px"></div>
	<div id="listDataGrid" class="datagrid" data-ride="datagrid"></div>

	<div id="listPager" class="pager" data-ride="pager"></div>
</div>

<!-- 添加页面 -->
<div class="modal fade" id="addModal">
	<div class="modal-dialog">
		<form id="addForm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
					</button>
					<h4 class="modal-title">新增代理商</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">代理商名称</span> <input type="text"
									id="AgeInfName" name="AgeInfName" class="form-control"
									placeholder="">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">代理商类型</span> <select
									id="AgeInfType" name="AgeInfType" class="form-control">
									<option value="">请选择</option>
								</select>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">证件类型</span> <select id="IDType"
									name="IDType" class="form-control">
									<option value="">请选择</option>
								</select>
							</div>
						</div>

						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">证件号</span> <input type="text"
									id="IDCode" name="IDCode" class="form-control" placeholder="">
							</div>
						</div>
					</div>
					<div style="margin-top: 10px"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">邮箱</span> <input type="text"
									id="Email" name="Email" class="form-control" placeholder="">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">代理商等级</span> <select
									id="AgeInfLevel" name="AgeInfLevel" class="form-control">
									<option value="">请选择</option>
								</select>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">简称</span> <input type="text"
									id="Alias" name="Alias" class="form-control" placeholder="">
							</div>
						</div>

						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">所属机构</span> <select
									id="Org_Name" name="Org_Name" class="form-control">
									<option value="">请选择</option>
								</select>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">所属地区</span> <select
									id="Area_Name" name="Area_Name" class="form-control">
									<option value="">请选择</option>
								</select>
							</div>
						</div>

						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">客户经理</span> <input type="text"
									id="CliMgrCode" name="CliMgrCode" class="form-control"
									placeholder="">
							</div>
						</div>
					</div>
					<div style="margin-top: 10px"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">联系人</span> <input type="text"
									id="Contactor" name="Contactor" class="form-control"
									placeholder="">
							</div>
						</div>

						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">联系人电话</span> <input type="text"
									id="ContactTel" name="ContactTel" class="form-control"
									placeholder="">
							</div>
						</div>
					</div>
					<div style="margin-top: 10px"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">备注</span>
								<textarea rows="5" cols="60" class="form-control" id="Remark"
									name="Remark"></textarea>
							</div>
						</div>

						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">经营场所</span>
								<textarea rows="5" cols="60" class="form-control"
									id="BusiAddress" name="BusiAddress"></textarea>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px"></div>
					<div class="row">


						<div class="col-md-12">
							<div class="input-group">
								<span class="input-group-addon">相关材料附件</span>

								<div id="Material" class="uploader">
									<div class="file-list" data-drag-placeholder="请拖拽文件到此处"></div>
									&nbsp;&nbsp;&nbsp;
									<button type="button"
										class="btn btn-primary uploader-btn-browse">
										<i class="icon icon-cloud-upload"></i> 选择文件
									</button>
								</div>

							</div>
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<input type="hidden" id="addMaterial" name="addMaterial"> <input
						type="hidden" name="action" value="addArea" />
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="submit" id="addSaveBtn" class="btn btn-primary">保存</button>
				</div>
			</div>
		</form>
	</div>
</div>


<!-- 修改页面  -->
<div class="modal fade" id="updateModal">
	<div class="modal-dialog">
		<form id="updateForm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
					</button>
					<h4 class="modal-title">修改代理商</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-4">
							<div class="input-group">
								<span class="input-group-addon">代理商号</span> <input type="text"
									id="updateAgeInfCode" name="updateAgeInfCode"
									class="form-control" placeholder="" readonly="readonly">
							</div>
						</div>
						<div class="col-md-4">
							<div class="input-group">
								<span class="input-group-addon">代理商名称</span> <input type="text"
									id="updateAgeInfName" name="updateAgeInfName"
									class="form-control" placeholder="">
							</div>
						</div>
						<div class="col-md-4">
							<div class="input-group">
								<span class="input-group-addon">代理商类型</span> <select
									id="updateAgeInfType" name="updateAgeInfType"
									class="form-control">
									<option value="">请选择</option>
								</select>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">证件类型</span> <select
									id="updateIDType" name="updateIDType" class="form-control">
									<option value="">请选择</option>
								</select>
							</div>
						</div>

						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">证件号</span> <input type="text"
									id="updateIDCode" name="updateIDCode" class="form-control"
									placeholder="">
							</div>
						</div>
					</div>
					<div style="margin-top: 10px"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">邮箱</span> <input type="text"
									id="updateEmail" name="updateEmail" class="form-control"
									placeholder="">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">代理商等级</span> <select
									id="updateAgeInfLevel" name="updateAgeInfLevel"
									class="form-control">
									<option value="">请选择</option>
								</select>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">简称</span> <input type="text"
									id="updateAlias" name="updateAlias" class="form-control"
									placeholder="">
							</div>
						</div>

						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">所属机构</span> <select
									id="updateOrg_Name" name="updateOrg_Name" class="form-control">
									<option value="">请选择</option>
								</select>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">所属地区</span> <select
									id="updateArea_Name" name="updateArea_Name"
									class="form-control">
									<option value="">请选择</option>
								</select>
							</div>
						</div>

						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">客户经理</span> <input type="text"
									id="updateCliMgrCode" name="updateCliMgrCode"
									class="form-control" placeholder="">
							</div>
						</div>
					</div>
					<div style="margin-top: 10px"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">联系人</span> <input type="text"
									id="updateContactor" name="updateContactor"
									class="form-control" placeholder="">
							</div>
						</div>

						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">联系人电话</span> <input type="text"
									id="updateContactTel" name="updateContactTel"
									class="form-control" placeholder="">
							</div>
						</div>
					</div>
					<div style="margin-top: 10px"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">备注</span>
								<textarea rows="5" cols="60" class="form-control"
									id="updateRemark" name="updateRemark"></textarea>
							</div>
						</div>

						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">经营场所</span>
								<textarea rows="5" cols="60" class="form-control"
									id="updateBusiAddress" name="updateBusiAddress"></textarea>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px"></div>
					<div class="row">
						<div class="col-md-6">

							<div class="input-group ">
								<span class="input-group-addon"
									style="background-color: #D3D3D3;">附件： </span><input
									type="text" class="form-control" id="showMaterialName"
									name="showMaterialName" readonly="readonly">
								<div id="updateUploadHeadpicDiv2" class="hide">
									<div id="updateUploadHeadpicDiv" class="uploader "
										style="display: none;">
										<div class="file-list" data-drag-placeholder="请拖拽文件到此处"></div>
										<button type="button"
											class="btn btn-primary uploader-btn-browse">
											<i class="icon icon-cloud-upload"></i> 选择文件
										</button>
									</div>
								</div>
								<div style="margin-top: 2px">
									<button class="btn btn-primary hide" type="button"
										style="margin: 2px;" id="changeMaterialBtn"
										onclick="updateImgChange()">更换材料附件</button>

									<button class="btn btn-primary hide" type="button"
										style="margin: 2px;" id="updateShowMaterialBtn" style=""
										onclick="updateShow()">点击预览</button>
								</div>
							</div>


						</div>
						<div class="col-md-6">
							<textarea rows="5" cols="60" class="form-control"
								readonly="readonly" id="updateShowMaterial"
								name="updateShowMaterial"></textarea>
						</div>
					</div>

				</div>
				<div style="margin-top: 10px;"></div>
				<div class="row">
					<div class="modal-footer">
						<input type="hidden" id="updateAgentId" name="updateAgentId" /> <input
							type="hidden" id="excludeAgentId" name="excludeAgentId" /> <input
							type="hidden" id="updateMaterial" name="updateMaterial" />
						<button type="button" class="btn btn-default "
							data-dismiss="modal">关闭</button>
						<button type="submit" class="btn btn-primary" id="updateSave"
							name="updateSave">保存</button>
					</div>
				</div>
			</div>

		</form>
	</div>
</div>
<!-- 删除确认 -->
<div class="modal fade" id="deleteModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">删除代理商</h4>
			</div>
			<div class="modal-body">
				<p>您确定要删除此代理商信息吗?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary" id="deleteAreaBtn">确定</button>
			</div>
		</div>
	</div>
</div>
<!-- 详情页面  -->
<div class="modal fade" id="detailModal">
	<div class="modal-dialog">
		<div class="modal-content">

			<form id="detailForm">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">
							<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
						</button>
						<h4 class="modal-title">代理商详情</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-4">
								<div class="input-group">
									<span class="input-group-addon">代理商号</span> <input type="text"
										id="detailAgeInfCode" name="detailAgeInfCode"
										class="form-control" placeholder="" readonly="readonly">
								</div>
							</div>
							<div class="col-md-4">
								<div class="input-group">
									<span class="input-group-addon">代理商名称</span> <input type="text"
										id="detailAgeInfName" name="detailAgeInfName"
										class="form-control" placeholder="" readonly="readonly">
								</div>
							</div>
							<div class="col-md-4">
								<div class="input-group">
									<span class="input-group-addon">代理商类型</span> <input type="text"
										readonly="readonly" id="detailAgeInfType"
										name="detailAgeInfType" class="form-control">
								</div>
							</div>
						</div>
						<div style="margin-top: 10px"></div>
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">证件类型</span> <input type="text"
										readonly="readonly" id="detailIDType" name="detailIDType"
										class="form-control">
								</div>
							</div>

							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">证件号</span> <input type="text"
										id="detailIDCode" name="detailIDCode" class="form-control"
										readonly="readonly" placeholder="">
								</div>
							</div>
						</div>
						<div style="margin-top: 10px"></div>
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">邮箱</span> <input type="text"
										id="detailEmail" name="detailEmail" class="form-control"
										readonly="readonly" placeholder="">
								</div>
							</div>
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">代理商等级</span> <input type="text"
										readonly="readonly" id="detailAgeInfLevel"
										name="detailAgeInfLevel" class="form-control">
								</div>
							</div>
						</div>
						<div style="margin-top: 10px"></div>
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">简称</span> <input type="text"
										id="detailAlias" name="detailAlias" class="form-control"
										readonly="readonly" placeholder="">
								</div>
							</div>

							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">所属机构</span> <input type="text"
										readonly="readonly" id="detailOrg_Name" name="detailOrg_Name"
										class="form-control">
								</div>
							</div>
						</div>
						<div style="margin-top: 10px"></div>
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">所属地区</span> <input type="text"
										id="detailArea_Name" name="detailArea_Name"
										class="form-control" readonly="readonly">
								</div>
							</div>

							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">客户经理</span> <input type="text"
										id="detailCliMgrCode" name="detailCliMgrCode"
										readonly="readonly" class="form-control" placeholder="">
								</div>
							</div>
						</div>
						<div style="margin-top: 10px"></div>
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">联系人</span> <input type="text"
										id="detailContactor" name="detailContactor"
										readonly="readonly" class="form-control" placeholder="">
								</div>
							</div>

							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">联系人电话</span> <input type="text"
										id="detailContactTel" name="detailContactTel"
										readonly="readonly" class="form-control" placeholder="">
								</div>
							</div>
						</div>
						<div style="margin-top: 10px"></div>
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">备注</span>
									<textarea rows="5" cols="60" class="form-control"
										id="detailRemark" name="detailRemark" readonly="readonly">1111</textarea>
								</div>
							</div>

							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">经营场所</span>
									<textarea rows="5" cols="60" class="form-control"
										id="detailBusiAddress" name="updateBusiAddress"
										readonly="readonly"></textarea>
								</div>
							</div>
						</div>
						<div style="margin-top: 10px"></div>
						<div class="row">
							<div class="col-md-6">

								<div class="input-group ">
									<span class="input-group-addon"
										style="background-color: #D3D3D3;">附件： </span><input
										type="text" class="form-control" id="detailShowMaterialName"
										name="detailShowMaterialName" readonly="readonly">

								</div>


							</div>
							<div class="col-md-6">
								<div class="input-group " id="fujian">
									<span class="input-group-addon"
										style="background-color: #D3D3D3;">附件预览： </span>
									<textarea rows="5" cols="60" class="form-control"
										id="detailShowMaterial" name="detailShowMaterial"
										readonly="readonly"></textarea>

								</div>

							</div>
						</div>

					</div>
					<div style="margin-top: 10px;"></div>
					<div class="row">
						<div class="modal-footer">
							<button type="button" class="btn btn-default "
								data-dismiss="modal">关闭</button>
						</div>
					</div>
				</div>


			</form>
		</div>
	</div>
</div>
<!-- 审核页面  -->
<div class="modal fade" id="checkModal">
	<div class="modal-dialog">
		<div class="modal-content">

			<form id="checkForm">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">
							<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
						</button>
						<h4 class="modal-title">代理商审核</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-4">
								<div class="input-group">
									<span class="input-group-addon">代理商号</span> <input type="text"
										id="checkAgeInfCode" name="checkAgeInfCode"
										class="form-control" placeholder="" readonly="readonly">
								</div>
							</div>
							<div class="col-md-4">
								<div class="input-group">
									<span class="input-group-addon">代理商名称</span> <input type="text"
										id="checkAgeInfName" name="checkAgeInfName"
										class="form-control" placeholder="" readonly="readonly">
								</div>
							</div>
							<div class="col-md-4">
								<div class="input-group">
									<span class="input-group-addon">代理商类型</span> <input type="text"
										readonly="readonly" id="checkAgeInfType"
										name="checkAgeInfType" class="form-control">
								</div>
							</div>
						</div>
						<div style="margin-top: 10px"></div>
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">证件类型</span> <input type="text"
										readonly="readonly" id="checkIDType" name="checkIDType"
										class="form-control">
								</div>
							</div>

							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">证件号</span> <input type="text"
										id="checkIDCode" name="checkIDCode" class="form-control"
										readonly="readonly" placeholder="">
								</div>
							</div>
						</div>
						<div style="margin-top: 10px"></div>
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">邮箱</span> <input type="text"
										id="checkEmail" name="checkEmail" class="form-control"
										readonly="readonly" placeholder="">
								</div>
							</div>
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">代理商等级</span> <input type="text"
										readonly="readonly" id="checkAgeInfLevel"
										name="checkAgeInfLevel" class="form-control">
								</div>
							</div>
						</div>
						<div style="margin-top: 10px"></div>
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">简称</span> <input type="text"
										id="checkAlias" name="checkAlias" class="form-control"
										readonly="readonly" placeholder="">
								</div>
							</div>

							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">所属机构</span> <input type="text"
										readonly="readonly" id="checkOrg_Name" name="checkOrg_Name"
										class="form-control">
								</div>
							</div>
						</div>
						<div style="margin-top: 10px"></div>
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">所属地区</span> <input type="text"
										id="checkArea_Name" name="checkArea_Name" class="form-control"
										readonly="readonly">
								</div>
							</div>

							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">客户经理</span> <input type="text"
										id="checkCliMgrCode" name="checkCliMgrCode"
										readonly="readonly" class="form-control" placeholder="">
								</div>
							</div>
						</div>
						<div style="margin-top: 10px"></div>
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">联系人</span> <input type="text"
										id="checkContactor" name="checkContactor" readonly="readonly"
										class="form-control" placeholder="">
								</div>
							</div>

							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">联系人电话</span> <input type="text"
										id="checkContactTel" name="checkContactTel"
										readonly="readonly" class="form-control" placeholder="">
								</div>
							</div>
						</div>
						<div style="margin-top: 10px"></div>
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">备注</span>
									<textarea rows="5" cols="60" class="form-control"
										id="checkRemark" name="checkRemark" readonly="readonly">1111</textarea>
								</div>
							</div>

							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">经营场所</span>
									<textarea rows="5" cols="60" class="form-control"
										id="checkBusiAddress" name="updateBusiAddress"
										readonly="readonly"></textarea>
								</div>
							</div>
						</div>
						<div style="margin-top: 10px"></div>
						<div class="row">
							<div class="col-md-6">

								<div class="input-group ">
									<span class="input-group-addon"
										style="background-color: #D3D3D3;">附件： </span><input
										type="text" class="form-control" id="checkShowMaterialName"
										name="checkShowMaterialName" readonly="readonly">

								</div>


							</div>
							<div class="col-md-6">
								<div class="input-group " id="fujian2">
									<span class="input-group-addon"
										style="background-color: #D3D3D3;">附件预览： </span>
									<textarea rows="5" cols="60" class="form-control"
										id="checkShowMaterial" name="checkShowMaterial"
										readonly="readonly"></textarea>

								</div>

							</div>
						</div>
						<div style="margin-top: 10px"></div>
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">审核状态</span> 
									<select id="checkStatus" name="checkStatus" class="form-control">
									
									</select>
								</div>
							</div>
							
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">审核意见</span> 
									<textarea rows="5" cols="60" class="form-control"
										id="checkAdvice" name="checkAdvice"
										></textarea>
								</div>
							</div>
						</div>

					</div>
					<div style="margin-top: 10px;"></div>
					<div class="row">
					<input type="hidden" id="checkAgentId" name="checkAgentId">
						<div class="modal-footer">
							<button type="button" class="btn btn-default "
								data-dismiss="modal">关闭</button>
							<button type="button" class="btn btn-primary" id="checkSaveBtn">提交审核</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- 解冻确认 -->
<div class="modal fade" id="unFreezeModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">冻结代理商</h4>
			</div>
			<div class="modal-body">
				<p>您确定要解冻此代理商吗?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary" id="unFreezeSaveBtn">确定</button>
			</div>
		</div>
	</div>
</div>
<!-- 冻结确认 -->
<div class="modal fade" id="freezeModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">冻结代理商</h4>
			</div>
			<div class="modal-body">
				<p>您确定要冻结此代理商吗?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary" id="freezeSaveBtn">确定</button>
			</div>
		</div>
	</div>
</div>