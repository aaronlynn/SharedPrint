<%@ page language="java" pageEncoding="UTF-8"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/agentUser.js?version=1"></script>

<div class="panel">
	<div class="panel-heading">查询代理商用户</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon">用户登录名</span> <input type="text"
						id="queryAgentUserLoginName" name="queryAgentUserLoginName" class="form-control"
						placeholder="">
				</div>
			</div>
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon">用户名称</span> <input type="text"
						id="queryAgentUserNameLike" name="queryAgentUserNameLike"
						class="form-control" placeholder="">
				</div>
			</div>
			<div class="col-md-4">
				<button class="btn btn-success" type="button" id="searchBtn">
					<i class="icon-search"></i>搜索
				</button>
			</div>
		</div>
	</div>
</div>
<div class="datagrid">
	<div class="search-box">
		&nbsp;&nbsp;&nbsp;&nbsp;
		<button class="btn btn-info " type="button" id="resetPwdBtn">
			<i class="icon icon-star"></i>密码重置
		</button>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<button class="btn " type="button" id="detailBtn">
			<i class="icon icon-info"></i>详情
		</button>
	</div>
    <div style="margin-top: 10px"></div>
	<div id="listDataGrid" class="datagrid" data-ride="datagrid"></div>

	<div id="listPager" class="pager" data-ride="pager"></div>
</div>


<!-- 删除确认 -->
<div class="modal fade" id="resetPwdModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">密码重置</h4>
			</div>
			<div class="modal-body">
				<p>您确定要重置该用户密码吗?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary" id="ResetPwdBtn">确定</button>
			</div>
		</div>
	</div>
</div>
<!-- 详情页面  -->
<div class="modal fade" id="detailModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">代理商用户详情</h4>
			</div>
			<form id="detailForm">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">登录名</span> <span
									class="form-control" id="loginName"></span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">登录密码</span> <span
									class="form-control" id="loginPwd"></span>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px;"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">用户名称</span> <span
									class="form-control" id="AgeTrueName"></span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">用户状态</span> <span
									class="form-control" id="AgeUserStatus"></span>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px;"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">所属代理商</span> <span
									class="form-control" id="AgeInfName"></span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">登录密码状态</span> <span
									class="form-control" id="InitPwdFlag"></span>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px;"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">操作者</span> <span
									class="form-control" id="Opr_id"></span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">操作时间</span> <span
									class="form-control" id="oprDate"></span>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px;"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">找回登录密码的问题</span> <span
									class="form-control" id="PwdQuestion"></span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">找回登录密码的问题答案</span> <span
									class="form-control" id="PwdAnswer"></span>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px;"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">第一次登录标志</span> <span
									class="form-control" id="FirstLoginFlag"></span>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px;"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default " data-dismiss="modal">关闭</button>
				</div>
			</form>
		</div>
	</div>
</div>