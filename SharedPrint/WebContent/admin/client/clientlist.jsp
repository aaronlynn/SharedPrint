<%@ page language="java" pageEncoding="UTF-8"%>
<link href="${pageContext.request.contextPath}/css/zui.datagrid.css"
	rel="stylesheet">
<script src="${pageContext.request.contextPath}/js/zui.datagrid.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery.validate.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/localization/messages_zh.js"></script>
	<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/zui.uploader.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/client.js?version=1"></script>

<div class="panel">
	<div class="panel-heading">查询客户</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon">客户号</span> <input type="text"
						id="queryClientCode" name="queryClientCode" class="form-control"
						placeholder="">
				</div>
			</div>
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon">客户名称</span> <input type="text"
						id="queryClientNameLike" name="queryClientNameLike"
						class="form-control" placeholder="">
				</div>
			</div>
			<div class="col-md-4">
				<button class="btn btn-success" type="button" id="searchBtn">
					<i class="icon-search"></i>搜索
				</button>
			</div>
		</div>
	</div>
</div>
<div class="datagrid">
	<div class="search-box">
		<button class="btn btn-success " type="button" data-toggle="modal"
			data-target="#resetPwdModal" id="resetPwdBtn">
			<i class="icon-plus"></i>重置密码
		</button>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<button class="btn btn-info " type="button" data-toggle="modal"
			data-target="#freezeModal" id="freezeBtn">
			<i class="icon icon-edit"></i>冻结
		</button>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<button class="btn btn-danger " type="button" data-toggle="modal"
			data-target="#unfreezeModal"id="unfreezeBtn">
			<i class="icon icon-trash"></i>解冻
		</button>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<button class="btn " type="button" id="detailBtn">
			<i class="icon icon-info"></i>详情
		</button>
	</div>

	<div id="listDataGrid" class="datagrid" data-ride="datagrid"></div>

	<div id="listPager" class="pager" data-ride="pager"></div>
</div>

<!-- 客户密码重置 -->
<div class="modal fade" id="resetPwdModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">客户密码重置</h4>
			</div>
			<div class="modal-body">
				<p>您确定要重置该客户密码吗?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary" id="resetPwdClientBtn" name="resetPwdClientBtn">确定</button>
			</div>
		</div>
	</div>
</div>
<!-- 客户冻结确认 -->
<div class="modal fade" id="freezeModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">客户冻结</h4>
			</div>
			<div class="modal-body">
				<p>您确定要冻结该客户吗?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary" id="freezeClientBtn">确定</button>
			</div>
		</div>
	</div>
</div>
<!-- 客户解冻确认 -->
<div class="modal fade" id="unfreezeModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">解冻客户</h4>
			</div>
			<div class="modal-body">
				<p>您确定要解冻该客户吗?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary" id="unfreezeClientBtn">确定</button>
			</div>
		</div>
	</div>
</div>
<!-- 详情页面  -->
<div class="modal fade" id="detailModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">地区详情</h4>
			</div>
			<form id="detailForm">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">客户号</span> <input type="text"
									id="detailClientCode" name="detailClientCode" class="form-control"
									placeholder="">
							</div>
						</div>
						<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">客户名称</span> <input type="text"
										id="detailClientName" name="detailClientName" class="form-control"
										placeholder="">
								</div>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">登录名称</span> <input type="text"
										id="detailLoginName" name="detailLoginName" class="form-control"
										placeholder="">
								</div>
							</div>

							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">登录密码</span> <input type="text"
										id="detailLoginPwd" name="detailLoginPwd" class="form-control"
										placeholder="">
								</div>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">支付密码</span>
									<input id="detailPaymentPwd" name="detailPaymentPwd"
										class="form-control" placeholder="">
								</div>
							</div>
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">电话</span>
									<input id="detailMobile" name="detailMobile"
										class="form-control" placeholder="">
								</div>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">找回密码问题</span>
									<textarea id="detailPwdQuestion" name="detailPwdQuestion" cols="60" rows="5"
										class="form-control"></textarea>
								</div>
							</div>
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">问题答案</span>
									<textarea id="detailPwdAnswer" name="detailPwdAnswer" cols="60" rows="5"
										class="form-control"></textarea>
								</div>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">客户状态</span>
									<input id="detailclientStatus" name="detailclientStatus"
										class="form-control" placeholder="">
								</div>
							</div>
						</div>
						<hr>
						<div class="row">
        					<div class="col-md-12">
				 				<div class="input-group">
				 					<span class="input-group-addon">头像<font color="red">*</font></span>
				 					<div  id="HeadPicPath"><!-- 使用图片 --></div>
	 							</div>      			      	
        					</div>
        				<hr>
						</div>
					</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default " data-dismiss="modal">关闭</button>
				</div>
				<input type="hidden" id="contextPath" name="contextPath" value="${pageContext.request.contextPath}"/>
			</form>
		</div>
	</div>
</div>