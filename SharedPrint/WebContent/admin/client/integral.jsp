<%@ page language="java" pageEncoding="UTF-8"%>
<link href="${pageContext.request.contextPath}/css/zui.datagrid.css"
	rel="stylesheet">
<script src="${pageContext.request.contextPath}/js/zui.datagrid.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery.validate.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/localization/messages_zh.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/integral.js?version=1"></script>

<div class="panel">
	<div class="panel-heading">查询客户积分</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon">客户号</span> <input type="text"
						id="queryClientCode" name="queryClientCode" class="form-control"
						placeholder="">
				</div>
			</div>
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon">客户名称</span> <input type="text"
						id="queryClientNameLike" name="queryClientNameLike"
						class="form-control" placeholder="">
				</div>
			</div>
			<div class="col-md-4">
				<button class="btn btn-success" type="button" id="searchBtn">
					<i class="icon-search"></i>搜索
				</button>
			</div>
		</div>
	</div>
</div>
<div class="datagrid">
	<div id="listDataGrid" class="datagrid" data-ride="datagrid"></div>

	<div id="listPager" class="pager" data-ride="pager"></div>
</div>