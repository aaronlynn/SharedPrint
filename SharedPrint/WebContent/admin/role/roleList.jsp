<%@ page language="java" import="java.util.*" pageEncoding="utf-8" contentType="text/html; charset=utf-8"%>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/zTreeStyle.css"/>
<style type="text/css">
	ul.ztree {margin-top: 10px;
	border: 1px solid #617775;
	background: #f0f6e4;
	width:180px;
	overflow-y:scroll;
	overflow-x:auto;}
</style>
<div class="panel">
	<div class="panel-heading">
		查询角色
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon">角色名称</span>
					<input type="text" id="queryRoleNameLike" name="queryRoleNameLike" class="form-control" placeholder="">
				</div>
			</div>
			<div class="col-md-4">
				<button class="btn btn-success" type="button" id="searchBtn"><i class="icon-search"></i>搜索</button>
			</div>
		</div>
	</div>
</div>
	<div class="search-box">
		<button class="btn btn-success " type="button" data-toggle="modal" data-target="#addModal" id="addBtn"><i class="icon-plus"></i>新增</button>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<button class="btn btn-info " data-toggle="modal" data-target="#updateModal" type="button" id="updateBtn"><i class="icon icon-edit"></i>修改</button>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<button class="btn btn-danger " type="button" id="deleteBtn"><i class="icon icon-trash"></i>删除</button>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<button class="btn " type="button" id="detailBtn" data-toggle="modal" data-target="#detailModal"><i class="icon icon-info"></i>详情</button>
	</div>
	<hr>
	<div id="listDataGrid" class="datagrid"></div>
	<div id="listPager" class="pager" data-ride="pager"></div>
	<div class="modal fade" id="addModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
					<h4 class="modal-title">新增角色</h4>
				</div>
				<form id="addForm" name="addForm">
					<div class="modal-body">
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">角色名称</span>
									<input type="text" class="form-control" id="addRoleName" name="addRoleName" placeholder="角色名称">
								</div>
								<div class="input-group">
									<span class="input-group-addon">角色描述</span>
									<textarea id="addRemark" name="addRemark"  style="width: 280px;" cols="60" rows="5" class="form-control"></textarea>
								</div>
							</div>
							<div class="col-md-2">
									<span class="input-group-addon">角色权限</span>
							</div>
							<div class="col-md-4">
									<input type="hidden" name="menuId" id="menuId" value="" />
									<input id="menuSel" name="menuSel" type="text" readonly value="" style="width:180px;"  />
									
									<div id="menuContent"  style="display:none; ">
										<ul id="treeDemo" class="ztree" style="margin-top:0; position: absolute;z-index: 999; height: 130px;"></ul>
									</div>
								</div>
						<!-- </div>
						<div class="row"> -->
							<div class="col-md-12">
								
							</div>
						</div>
					</div>
					<input type="hidden" id="action" name="action" value="add" />
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
						<button type="submit" class="btn btn-primary" id="addSaveBtn" name="addSaveBtn">保存</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal fade" id="updateModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
					<h4 class="modal-title">修改角色信息</h4>
				</div>
				<form id="updateForm" name="updateForm">
					<div class="modal-body">
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">角色名称</span>
									<input type="text" class="form-control" id="updateRoleName" name="updateRoleName" placeholder="角色名称">
								</div>
							</div>
							<div class="col-md-2">
									<span class="input-group-addon">角色权限</span>
							</div>
							<div class="col-md-4">
									<input type="hidden" name="updatemenuId" id="updatemenuId" value="" />
									<input id="updatemenuSel" name="updatemenuSel" type="text" readonly value="" style="width:180px;"  />
									<div id="updatemenuContent" style="display:none; ">
										<ul id="updatetreeDemo" class="ztree" style="margin-top:0; position: absolute;z-index: 999; height: 130px;"></ul>
									</div>
								</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="input-group">
									<span class="input-group-addon">角色描述</span>
									<textarea id="updateRemark" name="updateRemark" cols="60" rows="5" class="form-control"></textarea>
								</div>
							</div>
						</div>
					</div>
					<input type="hidden" id="action" name="action" value="update">
					<input type="hidden" id="updateRole" name="updateRole">
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
						<button type="submit" class="btn btn-primary" id="updateSaveBtn" name="updateSaveBtn">保存</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal fade" id="detailModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
					<h4 class="modal-title">角色详情</h4>
				</div>
				<form id="detailForm" name="detailForm">
					<div class="modal-body">
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">角色名称</span>
									<input type="text" class="form-control" id="detailRoleName" readonly name="detailRoleName" placeholder="角色名称">
								</div>
							</div>
							<div class="col-md-2">
									<span class="input-group-addon">角色权限</span>
							</div>
							<div class="col-md-4">
									<div id="detailmenuContent"  >
										<ul id="detailtreeDemo" class="ztree" style="margin-top:0; position: absolute;z-index: 999; height: 100px;"></ul>
									</div>
								</div>
						</div>
						<div class="row">
							<div class="col-md-8">
								<div class="input-group">
									<span class="input-group-addon">角色描述</span>
									<textarea id="detailRemark" name="detailRemark" readonly cols="60" rows="5" class="form-control"></textarea>
								</div>
							</div>
						</div>
					</div>
					<input type="hidden" id="detailRole" name="detailRole" />
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- 删除确认 -->
<div class="modal fade" id="deleteModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">删除角色</h4>
			</div>
			<div class="modal-body">
				<p>您确定要删除此角色信息吗?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary" id="deleteRoleBtn">确定</button>
			</div>
		</div>
	</div>
</div>
	<script src="${pageContext.request.contextPath}/js/jquery.ztree.core.js" type="text/javascript" charset="utf-8"></script>
	<script src="${pageContext.request.contextPath}/js/jquery.ztree.excheck.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/role.js?ver=2"></script>
