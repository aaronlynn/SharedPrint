<%@ page language="java" pageEncoding="UTF-8"%>
<div class="panel ">
	<div class="panel-heading">查询区域</div>
	<div class="panel-body row">
		<div class="row">
			<div class="col-md-4">
				<div class="input-group ">
					<span class="input-group-addon">用户名</span> <input type="text"
						class="form-control" id="queryUserName" name="queryUserName">
				</div>
			</div>
			<div class="col-md-2">
				<div class="input-group ">
					<span class="input-group-addon">名称</span> <input type="text"
						class="form-control" id="queryUserNameLike"
						name="queryUserNameLike">
				</div>
			</div>
			<div class="col-md-2">
				<div class="input-group ">
					<span class="input-group-addon">用户状态</span> <select
						class="form-control" id="queryUserState" name="queryUserState">
						<option value="">请选择</option>
					</select>
				</div>
			</div>
			<div class="col-md-4">
				<button class="btn btn-success" type="button" id="searchBtn">
					<i class="icon-search"></i>搜索
				</button>
			</div>
		</div>
	</div>
</div>
<div class="datagrid">
	<div class="search-box ">
		<button class="btn btn-success " type="button" data-toggle="modal"
			data-target="#addModal" id="addBtn">
			<i class="icon-plus"></i>新增
		</button>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<button class="btn btn-info " type="button" id="updateBtn">
			<i class="icon icon-edit"></i>修改
		</button>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<button class="btn btn-danger " type="button" id="deleteBtn">
			<i class="icon icon-trash"></i>删除
		</button>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<button class="btn " type="button" id="detailBtn">
			<i class="icon icon-info"></i>详情
		</button>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<button class="btn " type="button" id="resetBtn">
			<i class="icon icon-info"></i>重置密码
		</button>
	</div>
	<div id="listDataGrid" class="datagrid-container datagrid-striped"
		data-ride="datagrid"></div>
	<div class="pager" id="listPager" align="right"></div>
</div>
<!-- 添加页面  -->
<div class="modal fade" id="addModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">新增用户</h4>
			</div>
			<form id="addForm">
				<div class="modal-body">
					<div class="row with-padding">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">姓名<font color="red">*</font></span>
								<input type="text" class="form-control" id="addTrueName"
									name="addTrueName">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">用户名<font color="red">*</font></span>
								<input type="text" class="form-control" id="addUserName"
									name="addUserName">
							</div>
						</div>
					</div>
					<div class="row with-padding">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">所属角色<font color="red">*</font></span>
								<select class="form-control" id="addUserRole" name="addUserRole">
									<option value="">请选择</option>

								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">所属机构<font color="red">*</font></span>
								<select class="form-control" id="addUserOrp" name="addUserOrp">
									<option value="">请选择</option>

								</select>
							</div>
						</div>

					</div>
					<div class="row with-padding">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">所属地区<font color="red">*</font></span>
								<select class="form-control" id="addUserArea" name="addUserArea">
									<option value="">请选择</option>

								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">微信<font color="red">*</font></span>
								<input type="text" class="form-control" id="addUserWeixin"
									name="addUserWeixin">
							</div>
						</div>
					</div>
				</div>
				<div class="row with-padding">
					<div class="col-md-6">
						<div class="input-group ">
							<span class="input-group-addon">性别<font color="red">*</font></span>
							<select class="form-control" id="addUserSex" name="addUserSex">
								<option value="">请选择</option>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="input-group ">
							<span class="input-group-addon">电话<font color="red">*</font></span>
							<input type="text" class="form-control" id="addUserMobile"
								name="addUserMobile">
						</div>
					</div>
				</div>
				<div class="row with-padding">
					<div class="col-md-6">
						<div class="input-group ">
							<span class="input-group-addon">Email<font color="red">*</font></span>
							<input type="text" class="form-control" id="addUserEmail"
								name="addUserEmail">
						</div>
					</div>
					<div class="col-md-6">
						<div class="input-group ">
							<span class="input-group-addon">QQ<font color="red">*</font></span>
							<input type="text" class="form-control" id="addUserQQ"
								name="addUserQQ">
						</div>
					</div>
				</div>

				<div class="row with-padding">
					<div class="col-md-12">
						<div class="input-group ">
							<span class="input-group-addon">地址<font color="red">*</font></span>
							<textarea rows="3" cols="10" class="form-control"
								id="addUserAddress" name="addUserAddress"></textarea>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input type="hidden" id="defaultPwd" name="defaultPwd" value="">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="submit" class="btn btn-primary" id="addSave">保存</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- 修改页面 -->
<div class="modal fade" id="updateModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">修改用户</h4>
			</div>

			<form id="updateForm">
				<div class="modal-body">
					<div class="row with-padding">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">用户名<font color="red">*</font></span>
								<input type="text" class="form-control" id="updateUserName"
									name="updateUserName">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">用户姓名<font color="red">*</font></span>
								<input type="text" class="form-control" id="updateTrueName"
									name="updateTrueName">
							</div>
						</div>
					</div>
					<div class="row with-padding">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">密码<font color="red">*</font></span>
								<input type="text" class="form-control" id="updateUserPasswd"
									name="updateUserPasswd">
							</div>
						</div>

					</div>


				</div>
				<div class="modal-footer">
					<input type="hidden" id="updateUserId" name="updateUserId" />
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="submit" class="btn btn-primary" id="updateSave">保存</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- 删除确认 -->
<div class="modal fade" id="deleteModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">删除用户</h4>
			</div>
			<div class="modal-body">
				<p>您确定要删除此用户信息吗?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary" id="deleteUserBtn">确定</button>
			</div>
		</div>
	</div>
</div>
<!-- 详情页面  -->
<div class="modal fade" id="detailModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">用户详情</h4>
			</div>
			<form id="detailForm">
				<div class="modal-body">
					<div class="row with-padding">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">姓名<font color="red">*</font></span>
								<span class="form-control" id="TrueName" name="TrueName"></span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">用户名<font color="red">*</font></span>
								<span class="form-control" id="UserName" name="UserName"></span>
							</div>
						</div>
					</div>
					<div class="row with-padding">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">状态<font color="red">*</font></span>
								<span class="form-control" id="UserState" name="UserState"></span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">所属机构编码<font color="red">*</font></span>
								<span class="form-control" id="UserOrp" name="UserOrp"> </span>
							</div>
						</div>

					</div>
					<div class="row with-padding">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">所属地区编码<font color="red">*</font></span>
								<span class="form-control" id="UserArea" name="UserArea"></span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">密码<font color="red">*</font></span>
								<span class="form-control" id="UserPwd" name="UserPwd"></span>
							</div>
						</div>
					</div>
					<div class="row with-padding">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">性别<font color="red">*</font></span>
								<span class="form-control" id="UserSex" name="UserSex"></span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">电话<font color="red">*</font></span>
								<span class="form-control" id="UserMobile" name="UserMobile"></span>
							</div>
						</div>
					</div>
					<div class="row with-padding">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">Email<font color="red">*</font></span>
								<span class="form-control" id="UserEmail" name="UserEmail"></span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">QQ<font color="red">*</font></span>
								<span class="form-control" id="UserQQ" name="UserQQ"></span>
							</div>
						</div>
					</div>
					<div class="row with-padding">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">微信<font color="red">*</font></span>
								<span class="form-control" id="UserWeixin" name="UserWeixin"></span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">地址<font color="red">*</font></span>
								<span class="form-control" id="UserAddress" name="UserAddress"></span>
							</div>
						</div>
					</div>
					<div class="row with-padding">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">操作者<font color="red">*</font></span>
								<span class="form-control" id="OprId" name="OprId"></span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">操作时间<font color="red">*</font></span>
								<span class="form-control" id="OprDate" name="OprDate"></span>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default " data-dismiss="modal">关闭</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- 重置密码 -->
<div class="modal fade" id="resetModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">重置密码</h4>
			</div>
			<div class="modal-body">
				<p>您确定要重置此用户密码吗?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary" id="resetUserBtn">确定</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/config.js?version=5"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery.validate.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/localization/messages_zh.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/user.js?version=1"></script>