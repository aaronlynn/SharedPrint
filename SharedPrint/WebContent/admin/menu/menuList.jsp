<%@ page language="java" pageEncoding="UTF-8"%>
<link href="${pageContext.request.contextPath}/css/zui.datagrid.css"
	rel="stylesheet">
<script src="${pageContext.request.contextPath}/js/zui.datagrid.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery.validate.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/localization/messages_zh.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/menuList.js?ver=114"></script>

<div class="panel">
	<div class="panel-heading">查询菜单</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon">菜单编码</span> <input type="text"
						id="queryMenuCode" name="queryMenuCode" class="form-control"
						placeholder="">
				</div>
			</div>
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon">菜单名称</span> <input type="text"
						id="queryMenuNameLike" name="queryMenuNameLike"
						class="form-control" placeholder="">
				</div>
			</div>
			<div class="col-md-4">
				<button class="btn btn-success" type="button" id="searchBtn">
					<i class="icon-search"></i>搜索
				</button>
			</div>
		</div>
	</div>
</div>
<div class="datagrid">
	<div class="search-box">
		<button class="btn btn-success " type="button" data-toggle="modal"
			data-target="#addModal" id="addBtn">
			<i class="icon-plus"></i>新增
		</button>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<button class="btn btn-info " type="button" id="updateBtn">
			<i class="icon icon-edit"></i>修改
		</button>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<button class="btn btn-danger " type="button" id="deleteBtn">
			<i class="icon icon-trash"></i>删除
		</button>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<button class="btn " type="button" id="detailBtn">
			<i class="icon icon-info"></i>详情
		</button>
	</div>

	<div id="listDataGrid" class="datagrid" data-ride="datagrid"></div>

	<div id="listPager" class="pager" data-ride="pager"></div>
</div>

<!-- 添加页面 -->
<div class="modal fade" id="addModal">
	<div class="modal-dialog">
	<form id="addForm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">新增菜单	</h4>
			</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">菜单名称</span> <input type="text"
									id="addMenuName" name="addMenuName" class="form-control"
									placeholder="">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">上级菜单</span> <select
									id="addMenuParentCode" name="addMenuParentCode"
									class="form-control">
									<option value="">请选择</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">链接地址</span>
								<textarea id="addUrl" name="addUrl" cols="60" rows="5"
									class="form-control"></textarea>
							</div>
						</div>

						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">排序码</span> <input type="text"
									id="addSortNo" name="addSortNo" class="form-control"
									placeholder="">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="action" value="addMenu" />
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="submit" id="addSaveBtn" class="btn btn-primary">保存</button>
				</div>
			</form>
		</div>
	</div>
</div>


<!-- 修改页面  -->
<div class="modal fade" id="updateModal">
	<div class="modal-dialog">
	<form id="updateForm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">修改菜单</h4>
			</div>
			
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
						<div class="input-group ">
							<span class="input-group-addon">菜单名称<font color="red">*</font></span>
							<input type="text" class="form-control" id="updateMenuName"
								name="updateMenuName">
						</div>
					</div>
					<div class="col-md-6">
						<div class="input-group ">
							<span class="input-group-addon">所属上级菜单名称</span> <select
								class="form-control" id="updateMenuParentCode"
								name="updateMenuParentCode">
								<option value="0">请选择</option>
							</select>
						</div>
					</div>
				</div>
				<div style="margin-top: 10px;"></div>
				<div class="row">
					<div class="col-md-6">
						<div class="input-group ">
							<span class="input-group-addon">排序码<font color="red">*</font></span>
							<input type="text" class="form-control" id="updateSortCode"
								name="updateSortCode" maxlength="4">
						</div>
					</div>
					<div class="col-md-6">
						<div class="input-group ">
							<span class="input-group-addon">链接地址<font color="red">*</font></span>
							<textarea rows="5" cols="60" class="form-control" id="updateUrl"
								name="updateUrl"></textarea>
						</div>
					</div>
				</div>	
			</div>
			
		</div>
		<div style="margin-top: 10px;"></div>
		<div class="row">
			<div class="modal-footer">
				<input type="hidden" id="updateMenuId" name="updateMenuId" />
				<input type="hidden" id="action" name="action" value="updateMenu" />
				<button type="button" class="btn btn-default " data-dismiss="modal">关闭</button>
				<button type="submit" class="btn btn-primary" id="updateSave" name="updateSave">保存</button>
			</div>
		</div>
		</form>
	</div>
</div>
<!-- 删除确认 -->
<div class="modal fade" id="deleteModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">删除菜单</h4>
			</div>
			<div class="modal-body">
				<p>您确定要删除此菜单信息吗?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary" id="deleteMenuBtn">确定</button>
			</div>
		</div>
	</div>
</div>
<!-- 详情页面  -->
<div class="modal fade" id="detailModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">菜单详情</h4>
			</div>
			<form id="detailForm">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">菜单编号</span> <span
									class="form-control" id="detailAreaCode"></span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">菜单名称</span> <span
									class="form-control" id="detailmenuName"></span>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px;"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">所属上级菜单</span> <span
									class="form-control" id="detailParentName"></span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">排序码</span> <span
									class="form-control" id="detailSortNo"></span>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px;"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">链接地址</span> <span
									class="form-control" id="detailURL"></span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">操作时间</span> <span
									class="form-control" id="detailOprDate"></span>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px;"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default " data-dismiss="modal">关闭</button>
				</div>
			</form>
		</div>
	</div>
</div>