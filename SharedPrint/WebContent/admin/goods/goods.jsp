<%@ page language="java" pageEncoding="UTF-8"%>
<link href="${pageContext.request.contextPath}/css/zui.datagrid.css"
	rel="stylesheet">
<script src="${pageContext.request.contextPath}/js/zui.datagrid.js"></script>
<script src="${pageContext.request.contextPath}/js/zui.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery.validate.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/localization/messages_zh.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/goods.js?version=1"></script>

<div class="panel">
	<div class="panel-heading">查询商品信息</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon">所属商品类别</span> <input type="text"
						id="queryGoodsCategory" name="queryGoodsCategory" class="form-control"
						placeholder="">
				</div>
			</div>
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon">所属商品品牌</span> <input type="text"
						id="queryGoodsBrand" name="queryGoodsBrand" class="form-control"
						placeholder="">
				</div>
			</div>
		</div>
		<div style="margin-top: 10px;"></div>
		<div class="row">
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon">最低价格</span> <input type="text"
						id="queryMinGoodsPrice" name="queryMinGoodsPrice" class="form-control"
						placeholder="">
				</div>
			</div>
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon">最高价格</span> <input type="text"
						id="queryMaxGoodsPrice" name="queryMaxGoodsPrice" class="form-control"
						placeholder="">
				</div>
			</div>
			</div>
			<div style="margin-top: 10px;"></div>
			<div class="row">
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon">商品名称</span> <input type="text"
						id="queryGoodsNameLike" name="queryGoodsNameLike" class="form-control"
						placeholder="">
				</div>
			</div>
			<div class="col-md-4">
				<button class="btn btn-success" type="button" id="searchBtn">
					<i class="icon-search"></i>搜索
				</button>
			</div>
		</div>
	</div>
</div>
<div class="datagrid">
	<div class="search-box">
		&nbsp;&nbsp;&nbsp;&nbsp;
		<button class="btn " type="button" id="detailBtn">
			<i class="icon icon-info"></i>详情
		</button>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<button class="btn " type="button" id="checkBtn">
			<i class="icon icon-info"></i>审核
		</button>
	</div>

	<div id="listDataGrid" class="datagrid" data-ride="datagrid"></div>

	<div id="listPager" class="pager" data-ride="pager"></div>
</div>
<!-- 详情页面  -->
<div class="modal fade" id="detailModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">商品信息详情</h4>
			</div>
			<form id="detailForm">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">商品编号</span> <span
									class="form-control" id="detailGoodsCode"></span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">商品名称</span> <span
									class="form-control" id="detailGoodsName"></span>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px;"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">所属类别名称</span> <span
									class="form-control" id="CategoryName"></span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">所属品牌名称</span> <span
									class="form-control" id="BrandName"></span>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px;"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">商品价格</span> <span
									class="form-control" id="Price"></span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">商品描述</span> <span
									class="form-control" id="Goodsdetail"></span>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px;"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">排序码</span> <span
									class="form-control" id="SortNo"></span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">操作者Id</span> <span
									class="form-control" id="detailOprId"></span>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px;"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">操作时间</span> <span
									class="form-control" id="detailOprDate"></span>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px;"></div>
					<div class="row">
						<div class="col-md-12">
							<div class="input-group ">
							<a href="${pageContext.request.contextPath}/images/c_0001.jpg" data-toggle="lightbox" data-group="image-group-1"><img src="${pageContext.request.contextPath}/images/c_0001.jpg" class="img-rounded" alt=""></a>
                            <a href="${pageContext.request.contextPath}/images/c_0002.jpg" data-toggle="lightbox" data-group="image-group-1"><img src="${pageContext.request.contextPath}/images/c_0002.jpg" class="img-rounded" alt=""></a>
                            <a href="${pageContext.request.contextPath}/images/c_0003.jpg" data-toggle="lightbox" data-group="image-group-1"><img src="${pageContext.request.contextPath}/images/c_0003.jpg" class="img-rounded" alt=""></a>
                            <a href="${pageContext.request.contextPath}/images/c_0004.jpg" data-toggle="lightbox" data-group="image-group-1"><img src="${pageContext.request.contextPath}/images/c_0004.jpg" class="img-rounded" alt=""></a>
                            <a href="${pageContext.request.contextPath}/images/c_0001.jpg" data-toggle="lightbox" data-group="image-group-1"><img src="${pageContext.request.contextPath}/images/c_0001.jpg" class="img-rounded" alt=""></a>
                            <a href="${pageContext.request.contextPath}/images/c_0002.jpg" data-toggle="lightbox" data-group="image-group-1"><img src="${pageContext.request.contextPath}/images/c_0002.jpg" class="img-rounded" alt=""></a>
							</div>								
						</div>
					</div>
					<div style="margin-top: 10px;"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default " data-dismiss="modal">关闭</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- 商品审核 -->
<div class="modal fade" id="checkModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">商品审核</h4>
			</div>
			<form id="checkForm">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">商品编号</span>
									<input type="text" class="form-control" id="GoodsCode" name="GoodsCode" readonly="readonly">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">商品名称</span> <span
									class="form-control" id="GoodsName"></span>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px;"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">所属类别名称</span> <span
									class="form-control" id="CategoryName1"></span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">所属品牌名称</span> <span
									class="form-control" id="BrandName1"></span>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px;"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">商品价格</span> <span
									class="form-control" id="Price1"></span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">商品描述</span> <span
									class="form-control" id="Goodsdetail1"></span>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px;"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">排序码</span> <span
									class="form-control" id="SortNo1"></span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">操作者Id</span> <span
									class="form-control" id="detailOprId1"></span>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px;"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">操作时间</span> <span
									class="form-control" id="detailOprDate1"></span>
							</div>
						</div>
					</div>
					
					<div style="margin-top: 10px;"></div>
					<div class="row">
						<div class="col-md-12">
							<div class="input-group ">
							<a href="${pageContext.request.contextPath}/images/c_0001.jpg" data-toggle="lightbox" data-group="image-group-1"><img id="picFirst" src="${pageContext.request.contextPath}/images/c_0001.jpg" class="img-rounded" alt=""></a>
                            <a href="${pageContext.request.contextPath}/images/c_0002.jpg" data-toggle="lightbox" data-group="image-group-1"><img id="picFirst" src="${pageContext.request.contextPath}/images/c_0002.jpg" class="img-rounded" alt=""></a>
                            <a href="${pageContext.request.contextPath}/images/c_0003.jpg" data-toggle="lightbox" data-group="image-group-1"><img id="picFirst" src="${pageContext.request.contextPath}/images/c_0003.jpg" class="img-rounded" alt=""></a>
                            <a href="${pageContext.request.contextPath}/images/c_0004.jpg" data-toggle="lightbox" data-group="image-group-1"><img id="picFirst" src="${pageContext.request.contextPath}/images/c_0004.jpg" class="img-rounded" alt=""></a>
                            <a href="${pageContext.request.contextPath}/images/c_0001.jpg" data-toggle="lightbox" data-group="image-group-1"><img src="${pageContext.request.contextPath}/images/c_0001.jpg" class="img-rounded" alt=""></a>
                            <a href="${pageContext.request.contextPath}/images/c_0002.jpg" data-toggle="lightbox" data-group="image-group-1"><img src="${pageContext.request.contextPath}/images/c_0002.jpg" class="img-rounded" alt=""></a>
							</div>								
						</div>
					</div>
				</div>
				<div style="margin-top: 10px;"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">审核状态<font color="red">*</font></span>
								<select class="form-control" id="checkStatus" name="checkStatus">
									<option value="">请选择</option>
								</select>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px;"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">审核建议<font color="red">*</font></span>
								<textarea rows="3" cols="60" name="checkAdvise" id="checkAdvise"></textarea>
							</div>
						</div>
					</div>
				<div style="margin-top: 10px;"></div>
				<div class="modal-footer">
					<button type="submit" id="upCheckBtn" name="upCheckBtn" class="btn btn-default " data-dismiss="modal">提交</button>
				</div>
			</form>
		</div>
	</div>
</div>