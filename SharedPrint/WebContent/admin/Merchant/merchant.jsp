<%@ page language="java" import="java.util.*" pageEncoding="utf-8" contentType="text/html; charset=utf-8"%>
<div class="panel">
  <div class="panel-heading">
    查询区域
  </div>
  <div class="panel-body">
    <div class="row">
    	<div class="col-md-4">
    		<div class="input-group">
			  <span class="input-group-addon">商户条件</span>
			  <input type="text" id="queryCondition" name="queryCondition" class="form-control" placeholder="请输入你要查询的商户名称或商户号">
			</div>
    	</div>
    	<!--  
    	<div class="col-md-4">
    		<div class="input-group">
			  <span class="input-group-addon">商户号</span>
			  <input type="text" id="queryMerInfCode" name="queryMerInfCode" class="form-control" placeholder="">
			</div>
    	</div>-->
    	<div class="col-md-4">
    		<div class="input-group">
			  <span class="input-group-addon">商户状态</span> 
			  <select id="queryMerInfStatus" name="queryMerInfStatus"class="form-control">
						<option value="">请选择</option>
						<option value="1">未激活</option>
						<option value="2">正常</option>
						<option value="3">注销</option>
						<option value="4">待审核</option>
						<option value="5">审核不通过</option>
				</select>
			</div>
    	</div><hr>
    	<div class="col-md-4">
    		<div class="input-group">
			  <span class="input-group-addon">所属机构</span>
			  <input type="text" id="queryOrgCode" name="queryOrgCode" class="form-control" placeholder="">
			</div>
    	</div>
    	<div class="col-md-4">
    		<button class="btn btn-success" type="button" id="searchBtn"><i class="icon-search"></i>搜索</button>
    	</div>
    </div>
  </div>
</div>
<div class="datagrid">
  <div class="search-box">
  	<button class="btn btn-success " type="button"
  	  	 data-toggle="modal" data-target="#addModal" id="addBtn"><i class="icon-plus"></i>新增</button>
  	 	 &nbsp;&nbsp;&nbsp;&nbsp;
  	<button class="btn btn-info "
  		 type="button" id="updateBtn"><i class="icon icon-edit"></i>修改</button>
  		 &nbsp;&nbsp;&nbsp;&nbsp;
    <button class="btn btn-danger " type="button" id="deleteBtn"><i class="icon icon-trash"></i>删除</button>
    	 &nbsp;&nbsp;&nbsp;&nbsp;
    <button class="btn " type="button" id="detailBtn"><i class="icon icon-info"></i>详情</button>
   		 &nbsp;&nbsp;&nbsp;&nbsp;
   	<button class="btn btn-warning " type="button" id="checkBtn"><i class="icon icon-check-circle"></i>审核</button>
    	 &nbsp;&nbsp;&nbsp;&nbsp;
    <button class="btn btn-danger " type="button" id="thawBtn"><i class="icon icon-star-empty"></i>解冻</button>
    	 &nbsp;&nbsp;&nbsp;&nbsp;
    <button class="btn btn-info " type="button" id="freezeBtn"><i class="icon icon-star"></i>冻结</button>
    
  </div><hr>
 </div>
<div id="listDataGrid" class="datagrid"></div>
 <div id="listPager" class="pager"  data-ride="pager" ></div>
 <div class="modal fade" id="addModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
        <h4 class="modal-title">新增商户</h4>
      </div>
      <form id="addForm" name="addForm">
      <div class="modal-body">
        	<div class="row">       			 
        			<div class="col-md-12">
        				<div class="input-group">
						  <span class="input-group-addon">商户名称</span>
						  <input type="text" class="form-control" id="addMerInfName" name="addMerInfName" placeholder="商户名称">
						</div>	
        			</div>       	
        	</div><hr>
        	<div class="row">       			
        			<div class="col-md-6">
        				<div class="input-group">
								<span class="input-group-addon">证件类型</span> 
								<select
									id="addIDType" name="addIDType"
									class="form-control">
									<option value="">请选择</option>
								</select>
						</div>	
        			</div>
        			<div class="col-md-6">
	    				<div class="input-group">
						 <span class="input-group-addon">证件号</span>
						 <input type="text" id="addIDCode" name="addIDCode" class="form-control">
						</div>
	    			</div>       			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
								<span class="input-group-addon">商户类型</span> 
								<select
									id="addMerInfType" name="addMerInfType"
									class="form-control">
									<option value="">请选择</option>
								</select>
						</div>	
        			</div>
        			<div class="col-md-6">
        				<div class="input-group">
								<span class="input-group-addon">所属代理商</span> 
								<select
									id="addAgeInfCode" name="addAgeInfCode"
									class="form-control">
									<option value="">请选择</option>
								</select>
						</div>	
        			</div>          			        			       			      	
        	</div><hr>
        	<div class="row">	        			
        			<div class="col-md-6">
        				<div class="input-group">
								<span class="input-group-addon">商户等级</span> 
								<select
									id="addMerInfLevel" name="addMerInfLevel"
									class="form-control">
									<option value="">请选择</option>
								</select>
						</div>	
        			</div> 
        			<div class="col-md-6">
	    				<div class="input-group">
						 <span class="input-group-addon">简称</span>
						 <input type="text" id="addAlias" name="addAlias" class="form-control">
						</div>
	    			</div>       			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
								<span class="input-group-addon">所属机构</span> 
								<select
									id="addOrgCode" name="addOrgCode"
									class="form-control">
									<option value="">请选择</option>
								</select>
						</div>	
        			</div>
	    			<div class="col-md-6">
        				<div class="input-group">
								<span class="input-group-addon">所属地区</span> 
								<select
									id="addAreaCode" name="addAreaCode"
									class="form-control">
									<option value="">请选择</option>
								</select>
						</div>	
        			</div>      			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">经营场所</span>
						  <input type="text" class="form-control" id="addBusiAddress" name="addBusiAddress">
						</div>	
        			</div> 
        			<div class="col-md-6">
	    				<div class="input-group">
						 <span class="input-group-addon">联系人</span>
						 <input type="text" id="addContactor" name="addContactor" class="form-control">
						</div>
	    			</div>       			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">联系人电话</span>
						  <input type="text" class="form-control" id="addContactTel" name="addContactTel">
						</div>	
        			</div> 
        			<div class="col-md-6">
	    				<div class="input-group">
						 <span class="input-group-addon">邮箱</span>
						 <input type="text" id="addEmail" name="addEmail" class="form-control">
						</div>
	    			</div>       			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">QQ号</span>
						  <input type="text" class="form-control" id="addQQ" name="addQQ">
						</div>	
        			</div> 
        			<div class="col-md-6">
	    				<div class="input-group">
						 <span class="input-group-addon">微信号</span>
						 <input type="text" id="addWeixin" name="addWeixin" class="form-control">
						</div>
	    			</div>       			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">客户经理</span>
						  <input type="text" class="form-control" id="addCliMgrCode" name="addCliMgrCode">
						</div>	
        			</div>        		   	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-12">
				 			<div class="input-group ">
				 				<span class="input-group-addon">证件照片<font color="red">*</font></span>
								<div  id="uploadIDPath" class="uploader with-padding">
								  <div class="file-list" data-drag-placeholder="请拖拽文件到此处"></div>
								  <button type="button" class="btn btn-primary uploader-btn-browse"><i class="icon icon-cloud-upload"></i> 选择文件</button>
								</div>
								 <input type="hidden" id="addIDPath" name="addIDPath">	
							</div>
	 				</div>      			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-12">
				 			<div class="input-group ">
				 				<span class="input-group-addon">经营场所照片<font color="red">*</font></span>
								<div  id="uploadBusiAddressPath" class="uploader with-padding">
								  <div class="file-list" data-drag-placeholder="请拖拽文件到此处"></div>
								  <button type="button" class="btn btn-primary uploader-btn-browse"><i class="icon icon-cloud-upload"></i> 选择文件</button>
								</div>
								 <input type="hidden" id="addBusiAddressPath" name="addBusiAddressPath">	
							</div>
	 				</div>      			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-12">
        				<div class="input-group">
						  <span class="input-group-addon">备注</span>
						    <textarea  id="addRemark" name="addRemark" 
				  				cols="60" rows="5"class="form-control"></textarea>
						</div>	
        			</div>        			      	
        	</div>
      </div>
      <input type="hidden" id="action" name="action" value="add"/>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
        <button type="submit" class="btn btn-primary" id="addSaveBtn" name="addSaveBtn">保存</button>
      </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="updateModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
        <h4 class="modal-title">修改商户信息</h4>
      </div>
      <form id="updateForm" name="updateForm">
      <div class="modal-body">
        	<div class="row">    
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">商户号</span>
						  <input type="text" readonly="readonly" class="form-control" id="updateMerInfCode" name="updateMerInfCode" placeholder="商户号">
						</div>	
        			</div> 			
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">商户名称</span>
						  <input type="text" class="form-control" id="updateMerInfName" name="updateMerInfName" placeholder="商户名称">
						</div>	
        			</div>       	
        	</div><hr>
        	<div class="row">       			
        			<div class="col-md-6">
        				<div class="input-group">
								<span class="input-group-addon">证件类型</span> 
								<select
									id="updateIDType" name="updateIDType"
									class="form-control">
									<option value="">请选择</option>
								</select>
						</div>	
        			</div>
        			<div class="col-md-6">
	    				<div class="input-group">
						 <span class="input-group-addon">证件号</span>
						 <input type="text" id="updateIDCode" name="updateIDCode" class="form-control">
						</div>
	    			</div>       			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
								<span class="input-group-addon">商户类型</span> 
								<select
									id="updateMerInfType" name="updateMerInfType"
									class="form-control">
									<option value="">请选择</option>
								</select>
						</div>	
        			</div>
        			<div class="col-md-6">
        				<div class="input-group">
								<span class="input-group-addon">所属代理商</span> 
								<select
									id="updateAgeInfCode" name="updateAgeInfCode"
									class="form-control">
									<option value="">请选择</option>
								</select>
						</div>	
        			</div>          			        			       			      	
        	</div><hr>
        	<div class="row">	        			
        			<div class="col-md-6">
        				<div class="input-group">
								<span class="input-group-addon">商户等级</span> 
								<select
									id="updateMerInfLevel" name="updateMerInfLevel"
									class="form-control">
									<option value="">请选择</option>
								</select>
						</div>	
        			</div> 
        			<div class="col-md-6">
	    				<div class="input-group">
						 <span class="input-group-addon">简称</span>
						 <input type="text" id="updateAlias" name="updateAlias" class="form-control">
						</div>
	    			</div>       			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
								<span class="input-group-addon">所属机构</span> 
								<select
									id="updateOrgCode" name="updateOrgCode"
									class="form-control">
									<option value="">请选择</option>
								</select>
						</div>	
        			</div>
	    			<div class="col-md-6">
        				<div class="input-group">
								<span class="input-group-addon">所属地区</span> 
								<select
									id="updateAreaCode" name="updateAreaCode"
									class="form-control">
									<option value="">请选择</option>
								</select>
						</div>	
        			</div>      			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">经营场所</span>
						  <input type="text" class="form-control" id="updateBusiAddress" name="updateBusiAddress">
						</div>	
        			</div> 
        			<div class="col-md-6">
	    				<div class="input-group">
						 <span class="input-group-addon">联系人</span>
						 <input type="text" id="updateContactor" name="updateContactor" class="form-control">
						</div>
	    			</div>       			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">联系人电话</span>
						  <input type="text" class="form-control" id="updateContactTel" name="updateContactTel">
						</div>	
        			</div> 
        			<div class="col-md-6">
	    				<div class="input-group">
						 <span class="input-group-addon">邮箱</span>
						 <input type="text" id="updateEmail" name="updateEmail" class="form-control">
						</div>
	    			</div>       			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">QQ号</span>
						  <input type="text" class="form-control" id="updateQQ" name="updateQQ">
						</div>	
        			</div> 
        			<div class="col-md-6">
	    				<div class="input-group">
						 <span class="input-group-addon">微信号</span>
						 <input type="text" id="updateWeixin" name="updateWeixin" class="form-control">
						</div>
	    			</div>       			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">客户经理</span>
						  <input type="text" class="form-control" id="updateCliMgrCode" name="updateCliMgrCode">
						</div>	
        			</div>        		   	
        	</div><hr>
			<div class="row">
						<div class="col-md-12">
							<div class="input-group ">
								<span class="input-group-addon">证件照片<font color="red">*</font></span>
								<div id="uploadIDPathDiv" class="hide"></div>
								<div id="uploadUpdateIDPathDiv2" class="hide">
									<div id="uploadUpdateIDPathDiv" class="uploader with-padding "
										style="display: none;">
										<div class="file-list" data-drag-placeholder="请拖拽文件到此处"></div>
										<button type="button"
											class="btn btn-primary uploader-btn-browse">
											<i class="icon icon-cloud-upload"></i> 选择文件
										</button>
									</div>
								</div>
								<button class="btn btn-primary hide" type="button"
									id="updateIDPathImgChangeBtn" onclick="updateIDPathImgChange();">更换图片</button>
							</div>
							<input type="hidden" id="updateIDPath" name="updateIDPath">						
						</div>
			</div><hr>
			<div class="row">
        			<div class="col-md-12">
				 			<div class="input-group ">
				 				<span class="input-group-addon">经营场所照片<font color="red">*</font></span>
								<div id="uploadBusiAddressPathDiv" class="hide"></div>
								<div id="uploadUpdateBusiAddressPathDiv2" class="hide">
									<div id="uploadUpdateBusiAddressPathDiv" class="uploader with-padding "
										style="display: none;">
										<div class="file-list" data-drag-placeholder="请拖拽文件到此处"></div>
										<button type="button"
											class="btn btn-primary uploader-btn-browse">
											<i class="icon icon-cloud-upload"></i> 选择文件
										</button>
									</div>
								</div>
								<button class="btn btn-primary hide" type="button"
									id="updateBusiAddressPathImgChangeBtn" onclick="updateBusiAddressPathImgChange();">更换图片</button>
							</div>
							<input type="hidden" id="updateBusiAddressPath" name="updateBusiAddressPath">
	 				</div>      			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-12">
        				<div class="input-group">
						  <span class="input-group-addon">备注</span>
						    <textarea  id="updateRemark" name="updateRemark" 
				  				cols="60" rows="5"class="form-control"></textarea>
						</div>	
        			</div>        			      	
        	</div>
      </div>
      <input type="hidden" id="updateMerInfId" name="updateMerInfId">
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
        <button type="submit" class="btn btn-primary" id="updateSaveBtn" name="updateSaveBtn">保存</button>
      </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="detailModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
        <h4 class="modal-title">商户详情</h4>
      </div>
      <form id="detailForm" name="detailForm">
       <div class="modal-body">
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">商户号</span>
						  <input type="text" readonly="readonly" class="form-control" id="detailMerInfCode" name="detailMerInfCode" placeholder="商户号">
						</div>	
        			</div> 
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">商户名称</span>
						  <input type="text" readonly="readonly" class="form-control" id="detailMerInfName" name="detailMerInfName" placeholder="商户名称">
						</div>	
        			</div>       	
        	</div><hr>
        	<div class="row">       			
        			<div class="col-md-6">
        				<div class="input-group">
								<span class="input-group-addon">证件类型</span> 
								<input type="text" readonly="readonly" id="detailIDType" name="detailIDType" class="form-control">
						</div>	
        			</div>
        			<div class="col-md-6">
	    				<div class="input-group">
						 <span class="input-group-addon">证件号</span>
						 <input type="text" id="detailIDCode" readonly="readonly" name="detailIDCode" class="form-control">
						</div>
	    			</div>       			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
								<span class="input-group-addon">商户类型</span> 
								<input type="text" readonly="readonly" id="detailMerInfType" name="detailMerInfType" class="form-control">
						</div>	
        			</div>
        			<div class="col-md-6">
        				<div class="input-group">
								<span class="input-group-addon">所属代理商</span> 
								<input type="text" readonly="readonly" id="detailAgeInfCode" name="detailAgeInfCode" class="form-control">
						</div>	
        			</div>          			        			       			      	
        	</div><hr>
        	<div class="row">	        			
        			<div class="col-md-6">
        				<div class="input-group">
								<span class="input-group-addon">商户等级</span> 
								<input type="text" readonly="readonly" id="detailMerInfLevel" name="detailMerInfLevel" class="form-control">
						</div>	
        			</div> 
        			<div class="col-md-6">
	    				<div class="input-group">
						 <span class="input-group-addon">简称</span>
						 <input type="text" readonly="readonly" id="detailAlias" name="detailAlias" class="form-control">
						</div>
	    			</div>       			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
								<span class="input-group-addon">所属机构</span> 								
								<input type="text" id="detailOrgCode" readonly="readonly" name="detailOrgCode" class="form-control">
						</div>	
        			</div>
	    			<div class="col-md-6">
        				<div class="input-group">
								<span class="input-group-addon">所属地区</span> 								
								<input type="text" readonly="readonly" id="detailAreaCode" name="detailAreaCode" class="form-control">
						</div>	
        			</div>      			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">经营场所</span>
						  <input type="text" class="form-control" readonly="readonly" id="detailBusiAddress" name="detailBusiAddress">
						</div>	
        			</div> 
        			<div class="col-md-6">
	    				<div class="input-group">
						 <span class="input-group-addon">联系人</span>
						 <input type="text" readonly="readonly" id="detailContactor" name="detailContactor" class="form-control">
						</div>
	    			</div>       			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">联系人电话</span>
						  <input type="text" readonly="readonly" class="form-control" id="detailContactTel" name="detailContactTel">
						</div>	
        			</div> 
        			<div class="col-md-6">
	    				<div class="input-group">
						 <span class="input-group-addon">邮箱</span>
						 <input type="text" readonly="readonly" id="detailEmail" name="detailEmail" class="form-control">
						</div>
	    			</div>       			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">QQ号</span>
						  <input type="text" readonly="readonly" class="form-control" id="detailQQ" name="detailQQ">
						</div>	
        			</div> 
        			<div class="col-md-6">
	    				<div class="input-group">
						 <span class="input-group-addon">微信号</span>
						 <input type="text" readonly="readonly" id="detailWeixin" name="detailWeixin" class="form-control">
						</div>
	    			</div>       			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">客户经理</span>
						  <input type="text" readonly="readonly" class="form-control" id="detailCliMgrCode" name="detailCliMgrCode">
						</div>	
        			</div>    
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">商户状态</span>
						  <input type="text" readonly="readonly" class="form-control" id="detailMerInfStatus" name="detailMerInfStatus">
						</div>	
        			</div>    		   	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-12">
				 			<div class="input-group ">
				 				<span class="input-group-addon">证件照片<font color="red">*</font></span>
								<div  id="uploadDetailIDPathDiv" class="hide"></div>								
							</div>
							<input type="hidden" id="detailIDPath" name="detailIDPath">
	 				</div>      			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-12">
				 			<div class="input-group ">
				 				<span class="input-group-addon">经营场所照片<font color="red">*</font></span>
								<div  id="uploadDetailBusiAddressPathDiv" class="hide"></div>
							</div>
							<input type="hidden" id="detailBusiAddressPath" name="detailBusiAddressPath">
	 				</div>      			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-12">
        				<div class="input-group">
						  <span class="input-group-addon">备注</span>
						    <textarea  id="detailRemark" name="detailRemark" readonly="readonly"
				  				cols="60" rows="5"class="form-control"></textarea>
						</div>	
        			</div>        			      	
        	</div>
      </div>
      <input type="hidden" id="detailOrgId" name="detailOrgId"/>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
      </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="checkModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
        <h4 class="modal-title">商户审核</h4>
      </div>
      <form id="checkForm" name="checkForm">
       <div class="modal-body">
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">商户号</span>
						  <input type="text" readonly="readonly" class="form-control" id="checkMerInfCode" name="checkMerInfCode" placeholder="商户号">
						</div>	
        			</div> 
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">商户名称</span>
						  <input type="text" readonly="readonly" class="form-control" id="checkMerInfName" name="checkMerInfName" placeholder="商户名称">
						</div>	
        			</div>       	
        	</div><hr>
        	<div class="row">       			
        			<div class="col-md-6">
        				<div class="input-group">
								<span class="input-group-addon">证件类型</span> 
								<input type="text" readonly="readonly" id="checkIDType" name="checkIDType" class="form-control">
						</div>	
        			</div>
        			<div class="col-md-6">
	    				<div class="input-group">
						 <span class="input-group-addon">证件号</span>
						 <input type="text" id="checkIDCode" readonly="readonly" name="checkIDCode" class="form-control">
						</div>
	    			</div>       			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
								<span class="input-group-addon">商户类型</span> 
								<input type="text" readonly="readonly" id="checkMerInfType" name="checkMerInfType" class="form-control">
						</div>	
        			</div>
        			<div class="col-md-6">
        				<div class="input-group">
								<span class="input-group-addon">所属代理商</span> 
								<input type="text" readonly="readonly" id="checkAgeInfCode" name="checkAgeInfCode" class="form-control">
						</div>	
        			</div>          			        			       			      	
        	</div><hr>
        	<div class="row">	        			
        			<div class="col-md-6">
        				<div class="input-group">
								<span class="input-group-addon">商户等级</span> 
								<input type="text" readonly="readonly" id="checkMerInfLevel" name="checkMerInfLevel" class="form-control">
						</div>	
        			</div> 
        			<div class="col-md-6">
	    				<div class="input-group">
						 <span class="input-group-addon">简称</span>
						 <input type="text" readonly="readonly" id="checkAlias" name="checkAlias" class="form-control">
						</div>
	    			</div>       			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
								<span class="input-group-addon">所属机构</span> 								
								<input type="text" id="checkOrgCode" readonly="readonly" name="checkOrgCode" class="form-control">
						</div>	
        			</div>
	    			<div class="col-md-6">
        				<div class="input-group">
								<span class="input-group-addon">所属地区</span> 								
								<input type="text" readonly="readonly" id="checkAreaCode" name="checkAreaCode" class="form-control">
						</div>	
        			</div>      			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">经营场所</span>
						  <input type="text" class="form-control" readonly="readonly" id="checkBusiAddress" name="checkBusiAddress">
						</div>	
        			</div> 
        			<div class="col-md-6">
	    				<div class="input-group">
						 <span class="input-group-addon">联系人</span>
						 <input type="text" readonly="readonly" id="checkContactor" name="checkContactor" class="form-control">
						</div>
	    			</div>       			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">联系人电话</span>
						  <input type="text" readonly="readonly" class="form-control" id="checkContactTel" name="checkContactTel">
						</div>	
        			</div> 
        			<div class="col-md-6">
	    				<div class="input-group">
						 <span class="input-group-addon">邮箱</span>
						 <input type="text" readonly="readonly" id="checkEmail" name="checkEmail" class="form-control">
						</div>
	    			</div>       			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">QQ号</span>
						  <input type="text" readonly="readonly" class="form-control" id="checkQQ" name="checkQQ">
						</div>	
        			</div> 
        			<div class="col-md-6">
	    				<div class="input-group">
						 <span class="input-group-addon">微信号</span>
						 <input type="text" readonly="readonly" id="checkWeixin" name="checkWeixin" class="form-control">
						</div>
	    			</div>       			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">客户经理</span>
						  <input type="text" readonly="readonly" class="form-control" id="checkCliMgrCode" name="checkCliMgrCode">
						</div>	
        			</div>    
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">商户状态</span>
						  <input type="text" readonly="readonly" class="form-control" id="checkMerInfStatus" name="checkMerInfStatus">
						</div>	
        			</div>    		   	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-12">
				 			<div class="input-group ">
				 				<span class="input-group-addon">证件照片<font color="red">*</font></span>
								<div  id="uploadcheckIDPathDiv" class="hide"></div>								
							</div>
							<input type="hidden" id="checkIDPath" name="checkIDPath">
	 				</div>      			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-12">
				 			<div class="input-group ">
				 				<span class="input-group-addon">经营场所照片<font color="red">*</font></span>
								<div  id="uploadcheckBusiAddressPathDiv" class="hide"></div>
							</div>
							<input type="hidden" id="checkBusiAddressPath" name="checkBusiAddressPath">
	 				</div>      			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-12">
        				<div class="input-group">
						  <span class="input-group-addon">审核意见备注</span>
						    <textarea  id="checkRemark" name="checkRemark"
				  				cols="60" rows="5"class="form-control"></textarea>
						</div>	
        			</div>        			      	
        	</div>
      </div>
      <div class="modal-footer">
      	<input type="hidden" id="checkResult" name="checkResult"/>
      	<input type="hidden" id="checkMerInfId" name="checkMerInfId"/>
      	<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
        <button type="submit" class="btn btn-success"  id="checkPassBtn" name="checkPassBtn">审核通过</button>
        <button type="submit" class="btn btn-danger"  id="checkNotPassBtn" name="checkNotPassBtn">审核不通过</button>      
      </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="thawAndFreezeModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
        <h4 class="modal-title"></h4>
      </div>
      <form id="thawAndFreezeForm" name="thawAndFreezeForm">  
      <div class="modal-body">
      		<div class="row">
        			<div class="col-md-12">
        				<div class="input-group">
						 	<input type="text" readonly="readonly" id="determineText" name="determineText" class="form-control">
						</div>
        			</div>
        	</div>
      </div>   
      <div class="modal-footer">
      	<input type="hidden" id="oprResult" name="oprResult"/>
      	<input type="hidden" id="MerInfId" name="MerInfId"/>
      	<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
        <button type="submit" class="btn btn-success"  id="determineBtn" name="determineBtn">确定</button>     
      </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/merchant.js"></script>
