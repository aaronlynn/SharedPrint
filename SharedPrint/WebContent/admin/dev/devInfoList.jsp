<%@ page language="java" import="java.util.*" pageEncoding="utf-8" contentType="text/html; charset=utf-8"%>
<div class="panel">
	<div class="panel-heading">
		查询设备
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon">设备编码</span>
					<input type="text" id="queryDevInfCode" name="queryDevInfCode" class="form-control" placeholder="">
				</div>
			</div>
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon">设备名称</span>
					<input type="text" id="queryDevInfNameLike" name="queryDevInfNameLike" class="form-control" placeholder="">
				</div>
			</div>
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon">设备状态</span>
					<select id="queryDevInfStatus" name="queryDevInfStatus" class="form-control">
						<option value="">请选择</option>
					</select>
				</div>
			</div>

		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon">所属厂商</span>
					<select id="queryDevFactory" name="queryDevFactory" class="form-control">
						<option value="">请选择</option>
					</select>
				</div>
			</div>
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon">所属型号</span>
					<select id="queryDevModel" name="queryDevModel" class="form-control">
						<option value="">请选择</option>
					</select>
				</div>
			</div>
			<div class="col-md-4">
				<button class="btn btn-success" type="button" id="searchBtn"><i class="icon-search"></i>搜索</button>
			</div>
		</div>
	</div>
</div>
<div class="search-box">
	<button class="btn btn-success " type="button" data-toggle="modal" data-target="#addModal" id="addBtn"><i class="icon-plus"></i>新增</button>
	&nbsp;&nbsp;&nbsp;&nbsp;
	<button class="btn btn-info " data-toggle="modal" data-target="#updateModal" type="button" id="updateBtn"><i class="icon icon-edit"></i>修改</button>
	&nbsp;&nbsp;&nbsp;&nbsp;
	<button class="btn btn-danger " type="button" id="deleteBtn"><i class="icon icon-trash"></i>删除</button>
	&nbsp;&nbsp;&nbsp;&nbsp;
	<button class="btn " type="button" id="detailBtn" data-toggle="modal" data-target="#detailModal"><i class="icon icon-info"></i>详情</button>
</div>
<hr>
<div id="listDataGrid" class="datagrid"></div>
<div id="listPager" class="pager" data-ride="pager"></div>
<div class="modal fade" id="addModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
				<h4 class="modal-title">新增设备</h4>
			</div>
			<form id="addForm" name="addForm">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备型号</span>
								<select id="addDevModel_Code" name="addDevModel_Code" class="form-control">
									<option value="">请选择</option>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">所属厂商</span>
								<select id="addDevFactory_Code" name="addDevFactory_Code" class="form-control">
									<option value="">请选择</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备名称</span>
								<input type="text" class="form-control" id="addDevInfName" name="addDevInfName" placeholder="设备名称">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">所属机构</span>
								<select id="addOrg_Code" name="addOrg_Code" class="form-control">
									<option value="">请选择</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备固件版本号</span>
								<input type="text" class="form-control" id="addDevHwVer" name="addDevHwVer">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备应用程序版本号</span>
								<input type="text" class="form-control" id="addDevAppVer" name="addDevAppVer">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备参数数据版本号</span>
								<input type="text" class="form-control" id="addDevParamVer" name="addDevParamVer">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备其他版本号</span>
								<input type="text" class="form-control" id="addDevOtherVer" name="addDevOtherVer">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<input type="hidden" name="devimage1" id="devimage1" value="" />
							<div class="input-group">
								<span class="input-group-addon">设备图片</span>
								<div id="DevImage1" class="uploader">
									<div class="file-list" data-drag-placeholder="请拖拽文件到此处"></div>
									&nbsp;&nbsp;&nbsp;
									<button type="button" class="btn btn-primary uploader-btn-browse">
										<i class="icon icon-cloud-upload"></i> 选择文件
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<input type="hidden" id="action" name="action" value="add" />
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="submit" class="btn btn-primary" id="addSaveBtn" name="addSaveBtn">保存</button>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="modal fade" id="updateModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
				<h4 class="modal-title">修改设备型号信息</h4>
			</div>
			<form id="updateForm" name="updateForm">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备编码</span>
								<input type="text" class="form-control" readonly id="updateDevInf_Code" name="updateDevInf_Code">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备型号</span>
								<select id="updateDevModel_Code" name="updateDevModel_Code" class="form-control">
									<option value="">请选择</option>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">所属厂商</span>
								<select id="updateDevFactory_Code" name="updateDevFactory_Code" class="form-control">
									<option value="">请选择</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备名称</span>
								<input type="text" class="form-control" id="updateDevInfName" name="updateDevInfName" placeholder="设备名称">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">所属机构</span>
								<select id="updateOrg_Code" name="updateOrg_Code" class="form-control">
									<option value="">请选择</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备固件版本号</span>
								<input type="text" class="form-control" id="updateDevHwVer" name="updateDevHwVer">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备应用程序版本号</span>
								<input type="text" class="form-control" id="updateDevAppVer" name="updateDevAppVer">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备参数数据版本号</span>
								<input type="text" class="form-control" id="updateDevParamVer" name="updateDevParamVer">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备其他版本号</span>
								<input type="text" class="form-control" id="updateDevOtherVer" name="updateDevOtherVer">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-9">
							<input type="hidden" name="updatedevimage1" id="updatedevimage1" value="" />
							<div class="input-group">
								<span class="input-group-addon">设备图片</span>
								<div id="updateDevImage1" class="uploader">
									<div class="file-list" data-drag-placeholder="请拖拽文件到此处"></div>
									&nbsp;&nbsp;&nbsp;
									<button type="button" class="btn btn-primary uploader-btn-browse">
										<i class="icon icon-cloud-upload"></i> 选择文件
									</button>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<img src="" id="updatedevimage1Show">
						</div>
					</div>
				</div>
				<input type="hidden" id="excludeDevInfoId" name="excludeDevInfoId">
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="submit" class="btn btn-primary" id="updateSaveBtn" name="updateSaveBtn">保存</button>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="modal fade" id="detailModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
				<h4 class="modal-title">设备型号详情</h4>
			</div>
			<form id="detailForm" name="detailForm">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备编码</span>
								<input type="text" class="form-control" readonly id="detailDevInf_Code" name="detailDevInf_Code">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备状态</span>
								<input type="text" class="form-control" readonly id="detailDevInfStatus" name="detailDevInfStatus">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备型号</span>
								<input type="text" id="detailDevModel_Code" readonly name="detailDevModel_Code" class="form-control">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">所属厂商</span>
								<input type="text" id="detailDevFactory_Code" readonly name="detailDevFactory_Code" class="form-control">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备名称</span>
								<input type="text" class="form-control" readonly id="detailDevInfName" name="detailDevInfName">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">所属机构</span>
								<input type="text" id="detailOrg_Code" readonly name="detailOrg_Code" class="form-control">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备固件版本号</span>
								<input type="text" class="form-control" readonly id="detailDevHwVer" name="detailDevHwVer">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备应用程序版本号</span>
								<input type="text" class="form-control" readonly id="detailDevAppVer" name="detailDevAppVer">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备参数数据版本号</span>
								<input type="text" class="form-control" readonly id="detailDevParamVer" name="detailDevParamVer">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备其他版本号</span>
								<input type="text" class="form-control" readonly id="detailDevOtherVer" name="detailDevOtherVer">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备图片</span>
								<img src="" id="detaildevimage1Show">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">操作者</span>
								<input type="text" class="form-control" readonly id="Opr_id" name="Opr_id">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">操作日期</span>
								<input type="text" class="form-control" readonly id="Opr_Date" name="Opr_Date">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- 删除确认 -->
<div class="modal fade" id="deleteModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">删除设备</h4>
			</div>
			<div class="modal-body">
				<p>您确定要删除此设备信息吗?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary" id="deleteDevModelBtn">确定</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/DevInfo.js?ver=2"></script>
