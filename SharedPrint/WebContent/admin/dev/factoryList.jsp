<%@ page language="java" import="java.util.*" pageEncoding="utf-8" contentType="text/html; charset=utf-8"%>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/zTreeStyle.css" />
<div class="panel">
	<div class="panel-heading">
		查询设备厂商
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon">设备厂商名称</span>
					<input type="text" id="queryFactoryNameLike" name="queryFactoryNameLike" class="form-control" placeholder="">
				</div>
			</div>
			<div class="col-md-4">
				<button class="btn btn-success" type="button" id="searchBtn"><i class="icon-search"></i>搜索</button>
			</div>
		</div>
	</div>
</div>
<div class="search-box">
	<button class="btn btn-success " type="button" data-toggle="modal" data-target="#addModal" id="addBtn"><i class="icon-plus"></i>新增</button>
	&nbsp;&nbsp;&nbsp;&nbsp;
	<button class="btn btn-info " data-toggle="modal" data-target="#updateModal" type="button" id="updateBtn"><i class="icon icon-edit"></i>修改</button>
	&nbsp;&nbsp;&nbsp;&nbsp;
	<button class="btn btn-danger " type="button" id="deleteBtn"><i class="icon icon-trash"></i>删除</button>
	&nbsp;&nbsp;&nbsp;&nbsp;
	<button class="btn " type="button" id="detailBtn" data-toggle="modal" data-target="#detailModal"><i class="icon icon-info"></i>详情</button>
	<!-- &nbsp;&nbsp;&nbsp;&nbsp;预留功能
	<button class="btn btn-warning" type="button" id="statusBtn"  ><i class="icon icon-lock"></i>冻结/<i class="icon icon-key"></i>解冻</button> -->
</div>
<hr>
<div id="listDataGrid" class="datagrid"></div>
<div id="listPager" class="pager" data-ride="pager"></div>
<div class="modal fade" id="addModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
				<h4 class="modal-title">新增设备厂商 </h4>
			</div>
			<form id="addForm" name="addForm">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备厂商名称</span>
								<input type="text" class="form-control" id="addDevFactoryName" name="addDevFactoryName" placeholder="设备厂商名称">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">厂商网站</span>
								<input type="text" class="form-control" id="addWebURL" name="addWebURL">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">联系人</span>
								<input type="text" class="form-control" id="addContactName" name="addContactName">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">联系人电话</span>
								<input type="text" class="form-control" id="addContactTel" name="addContactTel">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">地址</span>
								<textarea id="addAddress" name="addAddress"  cols="60" rows="5" class="form-control"></textarea>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">备注</span>
								<textarea id="addRemark" name="addRemark"  cols="60" rows="5" class="form-control"></textarea>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">排序值</span>
								<input type="text" class="form-control" id="addSortNo" name="addSortNo">
							</div>
						</div>
					</div>
				</div>
				<input type="hidden" id="action" name="action" value="add" />
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="submit" class="btn btn-primary" id="addSaveBtn" name="addSaveBtn">保存</button>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="modal fade" id="updateModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
				<h4 class="modal-title">修改设备厂商信息</h4>
			</div>
			<form id="updateForm" name="updateForm">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">厂商编码</span>
								<input type="text" class="form-control" readonly id="updateDevFactory_Code" name="DevFactory_Code" >
							</div>
						</div>
						<div class="col-md-3">
							<div class="input-group">
							  <span class="input-group-addon">厂商状态</span>
							</div>	
						</div> 
						<div class="col-md-3" id="updateStatus">
							
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备厂商名称</span>
								<input type="text" class="form-control" id="updateDevFactoryName" name="updateDevFactoryName" placeholder="设备厂商名称">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">厂商网站</span>
								<input type="text" class="form-control" id="updateWebURL" name="updateWebURL">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">联系人</span>
								<input type="text" class="form-control" id="updateContactName" name="updateContactName">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">联系人电话</span>
								<input type="text" class="form-control" id="updateContactTel" name="updateContactTel">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">地址</span>
								<textarea id="updateAddress" name="updateAddress"  cols="60" rows="5" class="form-control"></textarea>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">备注</span>
								<textarea id="updateRemark" name="updateRemark"  cols="60" rows="5" class="form-control"></textarea>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">排序值</span>
								<input type="text" class="form-control" id="updateSortNo" name="updateSortNo">
							</div>
						</div>
					</div>
				</div>
				<input type="hidden" id="action" name="action" value="update">
				<input type="hidden" id="updateDevFactory" name="updateDevFactory">
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="submit" class="btn btn-primary" id="updateSaveBtn" name="updateSaveBtn">保存</button>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="modal fade" id="detailModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
				<h4 class="modal-title">设备厂商详情</h4>
			</div>
			<form id="detailForm" name="detailForm">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">厂商编码</span>
								<input type="text" class="form-control" readonly id="detailDevFactory_Code" name="detaiDevFactory_Code" >
							</div>
						</div>

					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备厂商名称</span>
								<input type="text" class="form-control" readonly id="detailDevFactoryName" name="detailDevFactoryName" >
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">厂商网站</span>
								<input type="text" class="form-control" readonly id="detailWebURL" name="detailWebURL">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">联系人</span>
								<input type="text" class="form-control" readonly id="detailContactName" name="detailContactName">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">联系人电话</span>
								<input type="text" class="form-control" readonly id="detailContactTel" name="detailContactTel">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">地址</span>
								<textarea id="detailAddress" name="detailAddress" readonly  cols="60" rows="5" class="form-control"></textarea>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">备注</span>
								<textarea id="detailRemark" name="detailRemark" readonly  cols="60" rows="5" class="form-control"></textarea>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">排序值</span>
								<input type="text" class="form-control" readonly id="detailSortNo" name="detailSortNo">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">厂商状态</span>
								<input type="text" class="form-control" readonly id="detailStatus" name="detailStatus">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">操作者</span>
								<input type="text" class="form-control" readonly id="Opr_id" name="Opr_id">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">操作日期</span>
								<input type="text" class="form-control" readonly id="Opr_Date" name="Opr_Date">
							</div>
						</div>
					</div>
				</div>
				<input type="hidden" id="detailDevFactory" name="detailDevFactory" />
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- 删除确认 -->
<div class="modal fade" id="deleteModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">删除设备厂商</h4>
			</div>
			<div class="modal-body">
				<p>您确定要删除此设备厂商信息吗?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary" id="deleteDevFactoryBtn">确定</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/DevFactory.js?ver=2"></script>
