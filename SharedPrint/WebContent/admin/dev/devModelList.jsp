<%@ page language="java" import="java.util.*" pageEncoding="utf-8" contentType="text/html; charset=utf-8"%>
<div class="panel">
	<div class="panel-heading">
		查询设备型号
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon">设备型号名称</span>
					<input type="text" id="queryDevModelNameLike" name="queryDevModelNameLike" class="form-control" placeholder="">
				</div>
			</div>
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon">所属厂商</span>
					<select id="queryDevFactory" name="queryDevFactory" class="form-control">
						<option value="">请选择</option>
					</select>
				</div>
			</div>
			<div class="col-md-4">
				<button class="btn btn-success" type="button" id="searchBtn"><i class="icon-search"></i>搜索</button>
			</div>
		</div>
	</div>
</div>
<div class="search-box">
	<button class="btn btn-success " type="button" data-toggle="modal" data-target="#addModal" id="addBtn"><i class="icon-plus"></i>新增</button>
	&nbsp;&nbsp;&nbsp;&nbsp;
	<button class="btn btn-info " data-toggle="modal" data-target="#updateModal" type="button" id="updateBtn"><i class="icon icon-edit"></i>修改</button>
	&nbsp;&nbsp;&nbsp;&nbsp;
	<button class="btn btn-danger " type="button" id="deleteBtn"><i class="icon icon-trash"></i>删除</button>
	&nbsp;&nbsp;&nbsp;&nbsp;
	<button class="btn " type="button" id="detailBtn" data-toggle="modal" data-target="#detailModal"><i class="icon icon-info"></i>详情</button>
</div>
<hr>
<div id="listDataGrid" class="datagrid"></div>
<div id="listPager" class="pager" data-ride="pager"></div>
<div class="modal fade" id="addModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
				<h4 class="modal-title">新增设备型号</h4>
			</div>
			<form id="addForm" name="addForm">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备型号名称</span>
								<input type="text" class="form-control" id="addDevModelName" name="addDevModelName" placeholder="设备型号名称">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">所属厂商</span>
								<select id="addDevFactory_Code" name="addDevFactory_Code" class="form-control">
									<option value="">请选择</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">排序值</span>
								<input type="text" class="form-control" id="addSortNo" name="addSortNo">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="input-group">
								<span class="input-group-addon">备注</span>
								<textarea id="addRemark" name="addRemark" cols="60" rows="5" class="form-control"></textarea>
							</div>
						</div>
					</div>
				</div>
				<input type="hidden" id="action" name="action" value="add" />
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="submit" class="btn btn-primary" id="addSaveBtn" name="addSaveBtn">保存</button>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="modal fade" id="updateModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
				<h4 class="modal-title">修改设备型号信息</h4>
			</div>
			<form id="updateForm" name="updateForm">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">型号编码</span>
								<input type="text" class="form-control" readonly id="updateDevModel_Code" name="updateDevModel_Code">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备型号名称</span>
								<input type="text" class="form-control" id="updateDevModelName" name="updateDevFactoryName" placeholder="设备型号名称">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">所属厂商</span>
								<select id="updateDevFactory_Code" name="updateDevFactory_Code" class="form-control">
									<option value="">请选择</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">排序值</span>
								<input type="text" class="form-control" id="updateSortNo" name="updateSortNo">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="input-group">
								<span class="input-group-addon">备注</span>
								<textarea id="updateRemark" name="updateRemark" cols="60" rows="5" class="form-control"></textarea>
							</div>
						</div>
					</div>
				</div>
				<input type="hidden" id="action" name="action" value="update">
				<input type="hidden" id="updateDevFactory" name="updateDevFactory">
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="submit" class="btn btn-primary" id="updateSaveBtn" name="updateSaveBtn">保存</button>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="modal fade" id="detailModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
				<h4 class="modal-title">设备型号详情</h4>
			</div>
			<form id="detailForm" name="detailForm">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">型号编码</span>
								<input type="text" class="form-control" readonly id="detailDevModel_Code" name="detailDevModel_Code">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">排序值</span>
								<input type="text" class="form-control" id="detailSortNo" name="detailSortNo" readonly>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备型号名称</span>
								<input type="text" class="form-control" id="detailDevModelName" name="detailDevFactoryName"  readonly >
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">所属厂商</span>
								<input type="text" class="form-control" id="detailDevFactory_Code" name="detailDevFactory_Code" readonly>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="input-group">
								<span class="input-group-addon">备注</span>
								<textarea id="detailRemark" name="detailRemark" readonly cols="60" rows="5" class="form-control"></textarea>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">操作者</span>
								<input type="text" class="form-control" readonly id="Opr_id" name="Opr_id">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">操作日期</span>
								<input type="text" class="form-control" readonly id="Opr_Date" name="Opr_Date">
							</div>
						</div>
					</div>
				</div>
				<input type="hidden" id="detailDevFactory" name="detailDevModel" />
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- 删除确认 -->
<div class="modal fade" id="deleteModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">删除设备型号</h4>
			</div>
			<div class="modal-body">
				<p>您确定要删除此设备型号信息吗?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary" id="deleteDevModelBtn">确定</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/DevModel.js?ver=1"></script>
