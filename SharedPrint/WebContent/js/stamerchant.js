$(document).ready(function(){
	var currentPage = 1;
	var pageSize = 10;
	var token = $.zui.store.get("token");
	var userId = $.zui.store.get("userId");
	
	var queryListFun = function(){
		//获取列表数据
		var queryListUrl = adminQueryUrl + StamerService + queryPageListMethod;
		
		var param = "";
		//搜索参数
		if ($("#queryAgeInfCode").val()!=''){
			param += "&queryAgeInfCode="+$("#queryAgeInfCode").val();
		}
		if ($("#querycrtyear").val()!=''){
			param += "&querycrtyear="+$("#querycrtyear").val();
		}
		if (currentPage>0){
			param += "&currentPage="+(currentPage-1);
		}
		param += "&pageSize="+pageSize;
		param += "&currentPage="+(currentPage-1);
		
		console.log('queryListUrl='+queryListUrl);
		console.log('token='+token+",userId="+userId);
		console.log('param='+param);
		$.ajax({
			url:queryListUrl,
			type:'post',
			beforeSend:function(xhr){
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",userId);
			},
			header:{"token":token,"userId":userId},
			data:param,
			dataType:"JSON",
			success:function(result){
				console.log(result.success);
				console.log(result.pageList);
				if (result.success== '1'){
					var data = {
						    // labels 数据包含依次在X轴上显示的文本标签
						    labels: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
						    datasets: [{
						        label: "新增商户",
						        color: "red",
						        data: result.monthCount
						    }]
						};	
					var options = {}; // 图表配置项，可以留空来使用默认的配置
					var myLineChart = $("#stamerLineChart").lineChart(data, options);
					
					$('#listDataGrid').empty();
					$('#listDataGrid').data('zui.datagrid',null);
					$('#listPager').empty();
					$('#listPager').data('zui.pager',null);
					$('#mernum').val(result.pageTotal+"项记录");
					$("#querycrtyear").empty();
					$("#querycrtyear").append('<option value="">请选择</option>');
					for(var i=0; i <= 10;i++)
					{
						$("#querycrtyear").append("<option value='"+result.yearCount[i]+"'>"+result.yearCount[i]+"年</option>");
					}

					$('#listDataGrid').datagrid({
						checkable: true,
					    checkByClickRow: true,
					    selectable: true,
					    dataSource: {
					        cols:[
					        	{name: 'MerInf_ID', label: '编号', width: 132},
					            {name: 'MerInf_Code', label: '商户号', width: 132},
					            {name: 'MerInfName', label: '商户名称', width: 132},
					            {name: 'AgeInf_Code', label: '代理商号', width: 132},
					            {name: 'MerInfType', label: '商户类型', width: 132},
					            {name: 'MerInfStatus', label: '商户状态', width: 109},
					            {name: 'crtDate', label: '创建日期', width: 109},
					        ],
					        cache:false, 
					        array:result.pageList
					    }
					 
					});	
					
					// 手动进行初始化
					$('#listPager').pager({
					    page: currentPage,
					    recPerPage:pageSize,
					    elements:['first_icon', 'prev_icon', 'pages', 'next_icon', 'last_icon', 'total_text','size_menu','goto'],
					    pageSizeOptions:[5,10,20,30,50,100],
					    recTotal: result.pageTotal
					});
				}
				else {
					new $.zui.Messager(result.msg, {
					    type: 'warning', // 定义颜色主题
					    placement: 'center' // 定义显示位置
					}).show();
				}
			},
			error:function(result){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning', // 定义颜色主题
				    placement: 'center' // 定义显示位置
				}).show();
			}
		});
	};
	//调起查询数据
	queryListFun();
	//查询
	$("#searchBtn").click(function(){
		queryListFun();
	});
	//监听分页，修改当前页，条数
	$('#listPager').on('onPageChange', function(e, state, oldState) {
		currentPage = state.page;
		pageSize = state.recPerPage;
		queryListFun();
	});
});