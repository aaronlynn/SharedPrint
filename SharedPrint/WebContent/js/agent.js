$(document).ready(function(){
	var currentPage = 1;
	var pageSize = 10;
	var token = $.zui.store.get("token");
	var userId = $.zui.store.get("userId");
	
	var queryListFun = function(){
		//获取列表数据
		var queryListUrl = adminQueryUrl + agentService + queryPageListMethod;
		
		var param = "";
		//搜索参数
		if ($("#queryAgentNameLike").val()!=''){
			param += "&queryAgentNameLike="+$("#queryAgentNameLike").val();
		}
		if ($("#queryAgentCode").val()!=''){
			param += "&queryAgentCode="+$("#queryAgentCode").val();
		}
		if ($("#queryAgentStatus").val()!=''){
			param += "&queryAgentStatus="+$("#queryAgentStatus").val();
		}
		if ($("#queryAgentOrgName").val()!=''){
			param += "&queryAgentOrgName="+$("#queryAgentOrgName").val();
		}
		if (currentPage>0){
			param += "&currentPage="+(currentPage-1);
		}
		param += "&pageSize="+pageSize;
		param += "&currentPage="+(currentPage-1);
		
		console.log('queryListUrl='+queryListUrl);
		console.log('token='+token+",userId="+userId);
		console.log('param='+param);
		$.ajax({
			url:queryListUrl,
			type:'post',
			beforeSend:function(xhr){
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",userId);
			},
			header:{"token":token,"userId":userId},
			data:param,
			dataType:"JSON",
			success:function(result){
				console.log(result.success);
				console.log(result.pageList);
				if (result.success== '1'){
					
					$('#listDataGrid').empty();
					$('#listDataGrid').data('zui.datagrid',null);
					$('#listPager').empty();
					$('#listPager').data('zui.pager',null);
					
					$('#listDataGrid').datagrid({
						checkable: true,
					    checkByClickRow: true,
					    selectable: true,
					    dataSource: {
					        cols:[
					            {name: 'AgeInf_Code', label: '代理商号', width: 132},
					            {name: 'AgeInfName', label: '代理商名称', width: 134},
					            {name: 'Org_Name', label: '所属机构', width: 109},
					            {name: 'AgeInfStatus', label: '代理商状态', width: 109},
					            {name: 'Contactor', label: '联系人', width:109},
					            {name: 'ContactTel', label: '联系电话', width: 109},
					            {name: 'CrtDate', label: '操作时间', width: 109},
					            {name: 'AgeInf_ID', label: '代理商主键编号', hidden:true}
					        ],
					        cache:false,
					        array:result.pageList
					    }
					 
					});	
					
					// 手动进行初始化
					$('#listPager').pager({
					    page: currentPage,
					    recPerPage:pageSize,
					    elements:['first_icon', 'prev_icon', 'pages', 'next_icon', 'last_icon', 'total_text','size_menu','goto'],
					    pageSizeOptions:[5,10,20,30,50,100],
					    recTotal: result.pageTotal
					});
					//代理商状态下拉框
					$("#queryAgentStatus").empty();
					$("#queryAgentStatus").append('<option value="">请选择</option>');
					$.each(result.dicsAgeInfStatusMap, function(i, item){  
						$("#queryAgentStatus").append("<option value='"+item.key_value+"'>"+item.key_name+"</option>");
					}); 
				}
				else {
					new $.zui.Messager(result.msg, {
					    type: 'warning', // 定义颜色主题
					    placement: 'center' // 定义显示位置
					}).show();
				}
			},
			error:function(result){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning', // 定义颜色主题
				    placement: 'center' // 定义显示位置
				}).show();
			}
		});
	};
	//调起查询数据
	queryListFun();
	//查询
	$("#searchBtn").click(function(){
		queryListFun();
	});
	//监听分页，修改当前页，条数
	$('#listPager').on('onPageChange', function(e, state, oldState) {
		currentPage = state.page;
		pageSize = state.recPerPage;
		queryListFun();
	});
	
	
	//初始化添加
	$("#addBtn").click(function(){
		//获取地区列表
		var url = adminQueryUrl + areaService +queryArea ;
		var param = '';//请求到列表页面
		$("#addForm")[0].reset();
		console.log(url+param);
		$.ajax({
			url:url,
			type:'post',
			dataType:'JSON',
			beforeSend:function(xhr){//设置请求头信息
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",userId);
			},
			header:{'token':token,'userId':userId},
			data:param,
			success:function(data){
				console.log('data='+data.pageList[0]);
				if (data.success=='1'){
					$("#Area_Name").empty();
					$("#Area_Name").append('<option value="">请选择</option>');
					$.each(data.pageList, function(i, item){ 
						if(item.AreaParentId == null)
						{
							$("#Area_Name").append("<option value='"+item.areaCode+"'>"+item.areaName+"</option>");
						}
						
					});  
				}
			},
			error:function(e){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning',
				    placement:'center'
				}).show();
			}
		});
		//获取代理商类型列表
		var url = adminQueryUrl + dicsService +dicsMethod ;
		var param = 'dicsCode=AgeInfType';//请求到列表页面
		$("#addForm")[0].reset();
		console.log(url+param);
		$.ajax({
			url:url,
			type:'post',
			dataType:'JSON',
			beforeSend:function(xhr){//设置请求头信息
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",userId);
			},
			header:{'token':token,'userId':userId},
			data:param,
			success:function(data){
				console.log('data='+data.dicsMap[0]);
				if (data.success=='1'){
					$("#AgeInfType").empty();
					$("#AgeInfType").append('<option value="">请选择</option>');
					$.each(data.dicsMap, function(i, item){ 
						
						$("#AgeInfType").append("<option value='"+item.key_value+"'>"+item.key_name+"</option>");
						
						
					});  
				}
			},
			error:function(e){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning',
				    placement:'center'
				}).show();
			}
		});
		
		//获取证件类型列表
		var url = adminQueryUrl + dicsService +dicsMethod ;
		var param = 'dicsCode=IDType';//请求到列表页面
		$("#addForm")[0].reset();
		console.log(url+param);
		$.ajax({
			url:url,
			type:'post',
			dataType:'JSON',
			beforeSend:function(xhr){//设置请求头信息
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",userId);
			},
			header:{'token':token,'userId':userId},
			data:param,
			success:function(data){
				console.log('data='+data.dicsMap[0]);
				if (data.success=='1'){
					$("#IDType").empty();
					$("#IDType").append('<option value="">请选择</option>');
					$.each(data.dicsMap, function(i, item){ 
						
						$("#IDType").append("<option value='"+item.key_value+"'>"+item.key_name+"</option>");
						
						
					});  
				}
			},
			error:function(e){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning',
				    placement:'center'
				}).show();
			}
		});
		
		//获取代理商等级列表
		var url = adminQueryUrl + dicsService +dicsMethod ;
		var param = 'dicsCode=AgeInfLevel';//请求到列表页面
		$("#addForm")[0].reset();
		console.log(url+param);
		$.ajax({
			url:url,
			type:'post',
			dataType:'JSON',
			beforeSend:function(xhr){//设置请求头信息
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",userId);
			},
			header:{'token':token,'userId':userId},
			data:param,
			success:function(data){
				console.log('data='+data.dicsMap[0]);
				if (data.success=='1'){
					$("#AgeInfLevel").empty();
					$("#AgeInfLevel").append('<option value="0">请选择</option>');
					$.each(data.dicsMap, function(i, item){ 
						
						$("#AgeInfLevel").append("<option value='"+item.key_value+"'>"+item.key_name+"</option>");
						
						
					});  
				}
			},
			error:function(e){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning',
				    placement:'center'
				}).show();
			}
		});
		
		//初始化机构列表下拉框
		var url = adminQueryUrl + orgService +queryOrgList ;
		var param = '';//请求到列表页面
		$("#addForm")[0].reset();
		console.log(url+param);
		$.ajax({
			url:url,
			type:'post',
			dataType:'JSON',
			beforeSend:function(xhr){//设置请求头信息
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",userId);
			},
			header:{'token':token,'userId':userId},
			data:param,
			success:function(data){
				console.log('data='+data.orgList[0]);
				if (data.success=='1'){
					$("#Org_Name").empty();
					$("#Org_Name").append('<option value="">请选择</option>');
					$.each(data.orgList, function(i, item){ 
						
						$("#Org_Name").append("<option value='"+item.Org_Code	+"'>"+item.Org_Name+"</option>");
						
						
					});  
				}
			},
			error:function(e){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning',
				    placement:'center'
				}).show();
			}
		});
		
	});
	var tempFile ;//用于清除刚上传的文件
	  //附件上传
	$('#Material').uploader({// 当选择文件后立即自动进行上传操作
		filters:{
		    // 只允许上传图片或图标（.ico）
		    mime_types: [
		    	{title: '文件', extensions: 'txt,docx,doc'}
		    ],
		    // 最大上传文件为 1MB
		    max_file_size: '1mb',
		    // 不允许上传重复文件
		    prevent_duplicates: true
		},
		previewImageSize:{width: 200, height: 200},
	    deleteActionOnDone: function(file, doRemoveFile) {
	          doRemoveFile();
	          $("#addMaterial").val("");
	      	  $("#Material").removeAttr("src");
	     },
		limitFilesCount:1,
		autoUpload: true, 
		url: adminQueryUrl+fileService +"?action=addUserHeadPic", // 文件上传提交地址
	    responseHandler: function(responseObject, file) {
	    	addTempFile = file;
	    	if (responseObject!=null && responseObject.response!=null){
	    		var resultJson = JSON.parse(responseObject.response);
	    		if (resultJson.success!=null && resultJson.success!=''
		    			&& typeof(resultJson.success)!='undefined'
	    				&& resultJson.fileName!=''
		    			&& typeof(resultJson.fileName)!='undefined'){
	    			if (resultJson.success=='1' ){
	    				$("#addMaterial").val(resultJson.fileName);
	    			}
	    		}
	    	}
	    }
	});

	$("#addSaveBtn").click(function(){
			var validAgentNameURl = adminQueryUrl + agentService + validAgentName;
			console.log(validAgentNameURl);
			$.validator.addMethod("isMobile",function(value,element){
				var regBox = {
				        regEmail : /^([a-zA-Z0-9._-])+@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-])+/,   //....邮箱
				        regName : /^[a-z0-9_-]{3,16}$/,                       //....用户名
				        regMobile : /^0?1[3|4|5|8][0-9]\d{8}$/,                 //....手机
				        regTel : /^0[\d]{2,3}-[\d]{7,8}$/                     //....电话 
				        }
				if (value.length!=11){
					return false;
				}
				if(value != '' &&  !regBox.regMobile.test(value))
				{
					
					return false;
				}
				return true;
			},"请输入正确的手机号码");
			$("#addForm").validate({
				rules:{
					AgeInfName:{
						"required":true,
						"remote":{
							url:validAgentNameURl,
							type:'post',
							beforeSend:function(xhr){//设置请求头信息
								xhr.setRequestHeader("token",token);
								xhr.setRequestHeader("userId",userId);
							},
							header:{'token':token,'userId':userId},
							dataType:'json',
							data:{
								'AgeInfName':function(){
									return $("#AgeInfName").val();
								}
							}
						}
					},
					Email:{
						"required":false,
						"email":true
					},
					IDType:"required",
					AgeInfType:"required",
					IDCode:"required",
					Org_Name:"required",
					Area_Name:"required",
					BusiAddress:"required",
					Contactor:"required",
					ContactTel:{
						"required":true,
						"digits":true,
						"isMobile":true
					},
					
				},
				messages:{
					AgeInfName:{
						"required":"请输入代理商名称！",
						"remote":"该代理商名称已经存在！"
					},
				    AgeInfType:{"required":"请选择代理商类型！"}
					
				},
				errorPlacement:function(error,element){
					error.appendTo(element.parent());
				},
				submitHandler:function(form){
					console.log("xx:"+adminQueryUrl + agentService + addMethod);
					
					$.ajax({
						url:adminQueryUrl + agentService + addMethod ,
						type:'post',
						dataType:'JSON',
						beforeSend:function(xhr){//设置请求头信息
							xhr.setRequestHeader("token",token);
							xhr.setRequestHeader("userId",userId);
						},
						header:{'token':token,'userId':userId},
						data:$(form).serialize(),
						success:function(data){
							console.log('data='+data);
							if (data.success == '1') {
			                	new $.zui.Messager('添加成功!', {
			    				    type: 'success',
			    				    placement:'center'
			    				}).show();
			                	new $.zui.Messager(data.msg, {
			    				    type: 'success',
			    				    placement:'center'
			    				}).show();
			                	queryListFun();
			                	$('#addModal').modal('hide', 'fit');
			                }
			                else{
			                	new $.zui.Messager(data.msg, {
			    				    type: 'warning',
			    				    placement:'center'
			    				}).show();
			                }
						},
						error:function(e){
							new $.zui.Messager('系统繁忙,请稍候再试!', {
		    				    type: 'warning',
		    				    placement:'center'
		    				}).show();
						}
					});
				}
			});	
	    });
	
	
	$("#updateBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		if (selectedItems.length == 0) {
			new $.zui.Messager('请选择要修改的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else if (selectedItems.length > 1) {
			new $.zui.Messager('请只选择一条要修改的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else {
			
			$("#updateForm")[0].reset();
			var url = adminQueryUrl + agentService +initUpdateMethod;
			var param = 'AgeInf_ID='+selectedItems[0].AgeInf_ID;//请求到列表页面
			console.log('param='+param);
			$.ajax({
				url:url,
				type:'post',
				dataType:'JSON',
				beforeSend:function(xhr){//设置请求头信息
					xhr.setRequestHeader("token",token);
					xhr.setRequestHeader("userId",userId);
				},
				headers:{'token':token,'userId':userId},
				data:param,
				success:function(data){
					console.log('data='+data);
					if (data.success=='1'){
						
						$('#updateModal').modal('show', 'fit');
						
						$("#updateAgeInfType").empty();
						$("#updateAgeInfType").append('<option value="">请选择</option>');
						$.each(data.dicsAgeInfType, function(i, item){   
							if (item.key_value == data.pageMap.AgeInfType){
								$("#updateAgeInfType").append("<option value='"+item.key_value+"' selected='selected'>"+item.key_name+"</option>");
							}
							else {
								$("#updateAgeInfType").append("<option value='"+item.key_value+"'>"+item.key_name+"</option>");
							}
						});  
						$("#updateIDType").empty();
						$("#updateIDType").append('<option value="">请选择</option>');
						$.each(data.dicsIDType, function(i, item){   
							if (item.key_value == data.pageMap.IDType){
								$("#updateIDType").append("<option value='"+item.key_value+"' selected='selected'>"+item.key_name+"</option>");
							}
							else {
								$("#updateIDType").append("<option value='"+item.key_value+"'>"+item.key_name+"</option>");
							}
						}); 
						$("#updateAgeInfLevel").empty();
						$("#updateAgeInfLevel").append('<option value="0">请选择</option>');
						$.each(data.dicsAgeInfLevel, function(i, item){   
							if (item.key_value == data.pageMap.AgeInfLevel){
								$("#updateAgeInfLevel").append("<option value='"+item.key_value+"' selected='selected'>"+item.key_name+"</option>");
							}
							else {
								$("#updateAgeInfLevel").append("<option value='"+item.key_value+"'>"+item.key_name+"</option>");
							}
						}); 
						$("#updateOrg_Name").empty();
						$("#updateOrg_Name").append('<option value="">请选择</option>');
						$.each(data.orgList, function(i, item){   
							if (item.Org_Code == data.pageMap.Org_Code){
								$("#updateOrg_Name").append("<option value='"+item.Org_Code+"' selected='selected'>"+item.Org_Name+"</option>");
							}
							else {
								$("#updateOrg_Name").append("<option value='"+item.Org_Code+"'>"+item.Org_Name+"</option>");
							}
						}); 
						$("#updateArea_Name").empty();
						$("#updateArea_Name").append('<option value="">请选择</option>');
						$.each(data.areaList, function(i, item){   
							if (item.areaCode == data.pageMap.Area_Code){
								$("#updateArea_Name").append("<option value='"+item.areaCode+"' selected='selected'>"+item.areaName+"</option>");
							}
							else {
								$("#updateArea_Name").append("<option value='"+item.areaCode+"'>"+item.areaName+"</option>");
							}
						}); 
						$("#updateAgeInfCode").val(data.pageMap.AgeInf_Code);
						$("#updateAgeInfName").val(data.pageMap.AgeInfName);
						$("#updateIDCode").val(data.pageMap.IDCode);
						$("#updateEmail").val(data.pageMap.Email);
						$("#updateAlias").val(data.pageMap.Alias);
						$("#updateCliMgrCode").val(data.pageMap.CliMgrCode);
						$("#updateContactor").val(data.pageMap.Contactor);
						$("#updateContactTel").val(data.pageMap.ContactTel);
						$("#updateRemark").val(data.pageMap.Remark);
						$("#updateBusiAddress").val(data.pageMap.BusiAddress);
						$("#updateAgentId").val(data.pageMap.AgeInf_ID);
						
						var Material = data.pageMap.Material;
						$("#showMaterialName").empty();
						$("#updateShowMaterial").css("display","none");
						$("#updateShowMaterialBtn").css("display","none");
						$("#showMaterialName").css("display","none");
						if (Material!=null && Material!='' && typeof(Material)!='undefined'){
							$("#updateShowMaterial").load(adminQueryUrl + "/"+data.pageMap.Material);
							$("#showMaterialName").val("      文件名:  "+Material);
							$("#showMaterialName").css("display","block");
							$("#updateUploadHeadpicDiv2").css("display","none");
							$("#changeMaterialBtn").css("display","block");
							$("#showMaterial").css("display","block");
							$("#updateMaterial").val(data.pageMap.Material);
							$("#updateShowMaterialBtn").css("display","block");
						}
						else{
							
							$("#changeMaterialBtn").css("display","none");
							$("#uploadHeadpicDiv").css("display","none");
							$("#updateImgChangeBtn").css("display","none");
							$("#updateUploadHeadpicDiv2").css("display","block");
							$("#updateUploadHeadpicDiv").css("display","block");
						}
					}
				},
				error:function(e){
					new $.zui.Messager('系统繁忙,请稍候再试!', {
					    type: 'warning',
					    placement:'center'
					}).show();
				}
			});
		}
	});
	//监听修改页面的文件上传
	$('#updateUploadHeadpicDiv').uploader({
		filters:{
		    // 只允许上传附件
		    mime_types: [
		        {title: '文件', extensions: 'txt,doc,docx'},
		    ],
		    // 最大上传文件为 1MB
		    max_file_size: '1mb',
		    // 不允许上传重复文件
		    prevent_duplicates: true
		},
		previewImageSize:{width: 200, height: 200},
	    autoUpload: true,            // 当选择文件后立即自动进行上传操作
	    deleteActionOnDone: function(file, doRemoveFile) {
	          doRemoveFile();
	          $("#addMaterial").val("");
	      	  $("#Material").removeAttr("src");
	     },
	    url: adminQueryUrl + fileService + "?action=addUserHeadPic",  // 文件上传提交地址
	    limitFilesCount:1,
	    responseHandler: function(responseObject, file) {
	    	tempFile = file;
	    	if (responseObject!=null && responseObject.response!=null){
	    		var resultJson = JSON.parse(responseObject.response);
	    		if (resultJson.success!=null && resultJson.success!=''
		    			&& typeof(resultJson.success)!='undefined'
	    				&& resultJson.fileName!=''
		    			&& typeof(resultJson.fileName)!='undefined'){
	    			if (resultJson.success=='1' ){
	    				$("#updateMaterial").val(resultJson.fileName);
	    			}
	    		}
	    	}
	    }
	});
	//修改保存
	$("#updateSave").click(function(){
	
		var validAgentNameURl = adminQueryUrl + agentService + validAgentName;
		
		$("#updateForm").validate({
			rules:{
				updateAgeInfName:{
					"required":true,
					"remote":{
						url:validAgentNameURl,
						type:'post',
						beforeSend:function(xhr){//设置请求头信息
							xhr.setRequestHeader("token",token);
							xhr.setRequestHeader("userId",userId);
						},
						header:{'token':token,'userId':userId},
						dataType:'json',
						data:{
							'AgeInfName':function(){
								return $("#updateAgeInfName").val();
							},
							'excludeAgentId':function(){
								return $("#updateAgentId").val();
							}
						}
					}
				},
				updateIDType:"required",
				updateAgeInfType:"required",
				updateIDCode:"required",
				updateOrg_Name:"required",
				updateArea_Name:"required",
				updateBusiAddress:"required",
				updateContactor:"required",
				updateContactTel:{
					"required":true,
					"digits":true
				},
				
			},
			messages:{
				updateAgeInfName:{
					"required":"请输入代理商名称！",
					"remote":"该代理商名称已经存在！"
				},
				updateAgeInfType:{"required":"请选择代理商类型！"}
				
			},
			errorPlacement:function(error,element){
				error.appendTo(element.parent());
			},
			submitHandler:function(form){
				var saveUrl = adminQueryUrl + agentService + updateMethod;
				$.ajax({
					url:saveUrl,
					type:'post',
					dataType:'JSON',
					beforeSend:function(xhr){//设置请求头信息
						xhr.setRequestHeader("token",token);
						xhr.setRequestHeader("userId",userId);
					},
					header:{'token':token,'userId':userId},
					data:$(form).serialize(),
					success:function(data){
						console.log('data='+data);
						if (data.success == '1') {
		                	new $.zui.Messager('修改成功!', {
		    				    type: 'success',
		    				    placement:'center'
		    				}).show();
		                	queryListFun();
		                	$('#updateModal').modal('hide', 'fit');
		                	queryAreaListFun();
		                }
		                else{
		                	new $.zui.Messager(data.msg, {
		    				    type: 'warning',
		    				    placement:'center'
		    				}).show();
		                }
					},
					error:function(e){
						new $.zui.Messager('系统繁忙,请稍候再试!', {
	    				    type: 'warning',
	    				    placement:'center'
	    				}).show();
					}
				});
			}
		});
	});
	//显示删除确认框
	$("#deleteBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		if (selectedItems.length == 0) {
			new $.zui.Messager('请选择要删除的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else if (selectedItems.length > 1) {
			new $.zui.Messager('请只选择一条要删除的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else {
			$('#deleteModal').modal('show', 'fit');
		}
	});
	//删除菜单
	$("#deleteAreaBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		var url = adminQueryUrl + agentService + deleteMethod;
		var param = 'AgeInf_ID='+selectedItems[0].AgeInf_ID + '&ageInfCode=' + selectedItems[0].AgeInf_Code;//请求到列表页面
		console.log('url='+url);
		console.log('param2='+param);
		$.ajax({
			url:url,
			type:'post',
			dataType:'JSON',
			beforeSend:function(xhr){//设置请求头信息
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",userId);
			},
			header:{'token':token,'userId':userId},
			data:param,
			success:function(data){
				console.log('data='+data);
				$('#deleteModal').modal('hide', 'fit');
				if (data.success=='1'){
	                	new $.zui.Messager(data.msg, {
	    				    type: 'success',
	    				    placement:'center'
	    				}).show();
	                	queryListFun();
				}
                else{
                	new $.zui.Messager(data.msg, {
    				    type: 'warning',
    				    placement:'center'
    				}).show();
				}
			},
			error:function(e){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning',
				    placement:'center'
				}).show();
			}
		});
	});
	//初始化详情
	$("#detailBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		if (selectedItems.length == 0) {
			new $.zui.Messager('请选择要查看的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else if (selectedItems.length > 1) {
			new $.zui.Messager('请只选择一条要查看的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else {
			$("#detailForm")[0].reset();
			var url = adminQueryUrl + agentService + detailMethod;
			var param = 'AgeInf_ID='+selectedItems[0].AgeInf_ID;//请求到列表页面
			console.log('param='+param);
			$.ajax({
				url:url,
				type:'post',
				dataType:'JSON',
				beforeSend:function(xhr){//设置请求头信息
					xhr.setRequestHeader("token",token);
					xhr.setRequestHeader("userId",userId);
				},
				header:{'token':token,'userId':userId},
				data:param,
				success:function(data){
					console.log('data='+data);
					if (data.success=='1'){
					$('#detailModal').modal('show', 'fit');
					$("#detailAgeInfCode").val(data.pageMap.AgeInf_Code);
					$("#detailAgeInfName").val(data.pageMap.AgeInfName);
					$("#detailIDCode").val(data.pageMap.IDCode);
					$("#detailEmail").val(data.pageMap.Email);
					$("#detailAlias").val(data.pageMap.Alias);
					$("#detailCliMgrCode").val(data.pageMap.CliMgrCode);
					$("#detailContactor").val(data.pageMap.Contactor);
					$("#detailContactTel").val(data.pageMap.ContactTel);
					$("#detailRemark").val(data.pageMap.Remark);
					$("#detailBusiAddress").val(data.pageMap.BusiAddress);
					$("#detailAgentId").val(data.pageMap.AgeInf_ID);
					
					$("#detailAgeInfType").val(data.pageMap.AgeInfType);
					$("#detailIDType").val(data.pageMap.IDType);
					$("#detailAgeInfLevel").val(data.pageMap.AgeInfLevel);
					$("#detailOrg_Name").val(data.pageMap.Org_Name);
					$("#detailArea_Name").val(data.pageMap.Area_Name);
					
					var Material = data.pageMap.Material;
					$("#detailShowMaterial").empty();
					$("#detailShowMaterialBtn").css("display","none");
					$("#detailShowMaterialName").css("display","none");
					$("#detailShowMaterialName").empty();
					if (Material!=null && Material!='' && typeof(Material)!='undefined'){
						$("#detailShowMaterial").load(adminQueryUrl + "/"+data.pageMap.Material);
						$("#detailShowMaterialName").val("      文件名:  "+Material);
						$("#detailShowMaterialName").css("display","block");
						
						
						$("#showMaterial").css("display","block");
					}
					else{
						$("#detailShowMaterialName").val("无附件");
						$("#detailShowMaterial").css("display","none");
						$("#detailShowMaterialName").css("display","block");
						$("#fujian").css("display","none");
						
					}
					
					}
				},
				error:function(e){
					new $.zui.Messager('系统繁忙,请稍候再试!', {
					    type: 'warning',
					    placement:'center'
					}).show();
				}
			});
		}
	});
	
	//审核
	$("#checkBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		if (selectedItems.length == 0) {
			new $.zui.Messager('请选择要审核的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else if (selectedItems.length > 1) {
			new $.zui.Messager('请只选择一条要审核的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else {
			$("#checkForm")[0].reset();
			var url = adminQueryUrl + agentService + detailMethod;
			var param = 'AgeInf_ID='+selectedItems[0].AgeInf_ID;//请求到列表页面
			console.log('param='+param);
			$.ajax({
				url:url,
				type:'post',
				dataType:'JSON',
				beforeSend:function(xhr){//设置请求头信息
					xhr.setRequestHeader("token",token);
					xhr.setRequestHeader("userId",userId);
				},
				header:{'token':token,'userId':userId},
				data:param,
				success:function(data){
					console.log('data='+data);
					if (data.success=='1'){
					//判断是否审核过显示审核框
					if(data.pageMap.AgeInfStatus == '待审核')
					{
						$('#checkModal').modal('show', 'fit');
					}
					else
					{
						new $.zui.Messager('该代理商已经审核过！', {
						    type: 'danger',
						    placement:'center'
						}).show();
					}
					$("#checkAgeInfCode").val(data.pageMap.AgeInf_Code);
					$("#checkAgeInfName").val(data.pageMap.AgeInfName);
					$("#checkIDCode").val(data.pageMap.IDCode);
					$("#checkEmail").val(data.pageMap.Email);
					$("#checkAlias").val(data.pageMap.Alias);
					$("#checkCliMgrCode").val(data.pageMap.CliMgrCode);
					$("#checkContactor").val(data.pageMap.Contactor);
					$("#checkContactTel").val(data.pageMap.ContactTel);
					$("#checkRemark").val(data.pageMap.Remark);
					$("#checkBusiAddress").val(data.pageMap.BusiAddress);
					$("#checkAgentId").val(data.pageMap.AgeInf_ID);
					
					$("#checkAgeInfType").val(data.pageMap.AgeInfType);
					$("#checkIDType").val(data.pageMap.IDType);
					$("#checkAgeInfLevel").val(data.pageMap.AgeInfLevel);
					$("#checkOrg_Name").val(data.pageMap.Org_Name);
					$("#checkArea_Name").val(data.pageMap.Area_Name);
					$("#checkAgentId").val(data.pageMap.AgeInf_ID);
					var Material = data.pageMap.Material;
					$("#checkShowMaterial").empty();
					$("#checkShowMaterialBtn").css("display","none");
					$("#checkShowMaterialName").css("display","none");
					$("#checkShowMaterialName").empty();
					if (Material!=null && Material!='' && typeof(Material)!='undefined'){
						$("#checkShowMaterial").load(adminQueryUrl + "/"+data.pageMap.Material);
						$("#checkShowMaterialName").val("      文件名:  "+Material);
						$("#checkShowMaterialName").css("display","block");
						
						$("#showMaterial").css("display","block");
					}
					else{
						$("#checkShowMaterialName").val("无附件");
						$("#checkShowMaterial").css("display","none");
						$("#checkShowMaterialName").css("display","block");
						$("#fujian2").css("display","none");
						
					}
					$("#checkAdvice").val(data.pageMap.ChkRemark);
					$("#checkStatus").empty();
					$.each(data.dicsAgeInfStatusMap, function(i, item){ 
//						<option value="4">待审核</option>
//						<option value="1">审核通过</option>
//						<option value="5">审核不通过</option>
						if((data.pageMap.AgeInfStatus == "正常" || data.pageMap.AgeInfStatus == "冻结"
							|| data.pageMap.AgeInfStatus == "注销")  && item.key_name == "审核通过" )
						{
							$("#checkStatus").append("<option value='"+item.key_value+"' selected='selected'>"+item.key_name+"</option>");
						}
						else if(data.pageMap.AgeInfStatus == item.key_name) 
						{
							$("#checkStatus").append("<option value='"+item.key_value+"' selected='selected'>"+item.key_name+"</option>");
						}
						else 
						{
							$("#checkStatus").append("<option value='"+item.key_value+"'>"+item.key_name+"</option>");
						}
					});  
					
					}
				},
				error:function(e){
					new $.zui.Messager('系统繁忙,请稍候再试!', {
					    type: 'warning',
					    placement:'center'
					}).show();
				}
			});
		}
	});
	
	//审核保存
	$("#checkSaveBtn").click(function (){
	var saveUrl = adminQueryUrl + agentService + checkAgent;   
	var param  = "checkAgentId="+$("#checkAgentId").val()+"&checkStatus="+$("#checkStatus").val() + "&checkAdvice="+$("#checkAdvice").val();
	$.ajax({
			url:saveUrl,
			type:'post',
			dataType:'JSON',
			beforeSend:function(xhr){//设置请求头信息
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",userId);
			},
			header:{'token':token,'userId':userId},
			data:param,
			success:function(data){
				console.log('data='+data);
				if (data.success == '1') {
	            	new $.zui.Messager('操作成功!', {
					    type: 'success',
					    placement:'center'
					}).show();
	            	queryListFun();
	            	$('#checkModal').modal('hide', 'fit');
	            	queryAreaListFun();
	            }
	            else{
	            	new $.zui.Messager(data.msg, {
					    type: 'warning',
					    placement:'center'
					}).show();
	            }
			},
			error:function(e){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning',
				    placement:'center'
				}).show();
			}
		});
		
	});
	
	//显示解冻确认框
	$("#unfreezeBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		if (selectedItems.length == 0) {
			new $.zui.Messager('请选择要解冻的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else if (selectedItems.length > 1) {
			new $.zui.Messager('请只选择一条要解冻的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else {
			$('#unFreezeModal').modal('show', 'fit');
		}
	});
	//解冻
	$("#unFreezeSaveBtn").click(function (){
		
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		var unnFreezeUrl = adminQueryUrl + agentService + unFreezeMethod;
		var param = 'AgeInf_ID='+selectedItems[0].AgeInf_ID;//请求到列表页面
		console.log('url='+unnFreezeUrl);
		console.log('param2='+param);
		$.ajax({
				url:unnFreezeUrl,
				type:'post',
				dataType:'JSON',
				beforeSend:function(xhr){//设置请求头信息
					xhr.setRequestHeader("token",token);
					xhr.setRequestHeader("userId",userId);
				},
				header:{'token':token,'userId':userId},
				data:param,
				success:function(data){
					console.log('data='+data);
					if (data.success == '1') {
		            	new $.zui.Messager(data.msg, {
						    type: 'success',
						    placement:'center'
						}).show();
		            	queryListFun();
		            	$('#unFreezeModal').modal('hide', 'fit');
		            	queryAreaListFun();
		            }
		            else{
		            	$('#unFreezeModal').modal('hide', 'fit');
		            	new $.zui.Messager(data.msg, {
						    type: 'danger',
						    placement:'center'
						}).show();
		            }
				},
				error:function(e){
					new $.zui.Messager('系统繁忙,请稍候再试!', {
					    type: 'warning',
					    placement:'center'
					}).show();
				}
			});
		
	});
	//显示冻结确认框
	$("#freezeBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		if (selectedItems.length == 0) {
			new $.zui.Messager('请选择要冻结的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else if (selectedItems.length > 1) {
			new $.zui.Messager('请只选择一条要冻结的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else {
			$('#freezeModal').modal('show', 'fit');
		}
	});
	//冻结
	$("#freezeSaveBtn").click(function (){

		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		var freezeUrl = adminQueryUrl + agentService + freezeMethod;
		var param = 'AgeInf_ID='+selectedItems[0].AgeInf_ID;//请求到列表页面
		console.log('url='+freezeUrl);
		console.log('param2='+param);
		$.ajax({
				url:freezeUrl,
				type:'post',
				dataType:'JSON',
				beforeSend:function(xhr){//设置请求头信息
					xhr.setRequestHeader("token",token);
					xhr.setRequestHeader("userId",userId);
				},
				header:{'token':token,'userId':userId},
				data:param,
				success:function(data){
					console.log('data='+data);
					if (data.success == '1') {
		            	new $.zui.Messager(data.msg, {
						    type: 'success',
						    placement:'center'
						}).show();
		            	queryListFun();
		            	$('#freezeModal').modal('hide', 'fit');
		            	queryAreaListFun();
		            }
		            else{
		            	$('#freezeModal').modal('hide', 'fit');
		            	new $.zui.Messager(data.msg, {
						    type: 'danger',
						    placement:'center'
						}).show();
		            }
				},
			error:function(e){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning',
				    placement:'center'
				}).show();
			}
		});
		
	});
});
		//变更附件
		function updateImgChange(){
			$("#showMaterial").css("hide","none");
			$("#updateShowMaterial").css("display","none");
			$("#updateShowMaterialBtn").css("display","none");
			$("#changeMaterialBtn").css("display","none");
			$("#showMaterialName").css("display","none");
			$("#updateUploadHeadpicDiv").css("display","block");
			$("#updateUploadHeadpicDiv2").css("display","block");
			//$('#updateUploadHeadpicDiv').data('zui.uploader').destroy();
		}
		function updateShow()
		{
			$("#updateShowMaterial").toggle();
		}
		function detailShow()
		{
			$("#detailShowMaterial").toggle();
		}