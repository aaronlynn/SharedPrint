$(document).ready(function(){
	var token = $.zui.store.get("token");
	var userId = $.zui.store.get("userId");
	var currentPage = 1;
	var pageSize = 10;
	console.log('token='+token+",userId="+userId);
	var queryListFun = function(){
		var queryMerDevListUrl = adminQueryUrl + merDevService + queryPageListMethod;
		var param = "";	
		if($("#queryMerInfo").val()!=''){
			param += "&queryMerInfo="+$("#queryMerInfo").val();
		}
		if($("#queryAgeInfCode").val()!=''){
			param += "&queryAgeInfCode="+$("#queryAgeInfCode").val();
		}
		if($("#queryOrgCode").val()!=''){
			param += "&queryOrgCode="+$("#queryOrgCode").val();
		}
		param += "&pageSize="+pageSize;
		param += "&currentPage="+(currentPage-1);
		$.ajax({
			url:queryMerDevListUrl,
			type:'post',
			beforeSend:function(xhr){
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",userId);
			},
			header:{"token":token,"userId":userId},
			data:param,
			dataType:"JSON",
			success:function(result){
				console.log(result.success);
				if(result.success=='1'){
					$('#listPager').empty();
					$('#listPager').data('zui.datagrid',null);
					$('#listDataGrid').empty();
					$('#listDataGrid').data('zui.datagrid',null);
					
					console.log(result);
					$('#listDataGrid').datagrid({
						checkable:true,
						checkByClickRow:true,
					    dataSource: {
					        cols:[
					              {name: 'MerDev_Code', label: '设备号', width: 0.1},
					              {name: 'MerDevName', label: '投放名称', width: 0.1},
					              {name: 'merInfName', label: '商户名称', width: 0.1},
					              {name: 'SN', label: '硬件序列号', width: 0.1},
					              {name: 'devWorkStatus', label: '设备状态', width: 0.1},
					              {name: 'merDevStatus', label: '审核状态', width: 0.1},
					              {name: 'oprDate', label: '操作时间', width: 0.1},
					        ],
					        array:result.listDataGrid
					    }				
					});
					console.log(currentPage);
					console.log(pageSize);
					console.log(result.pageTotal);									
					$('#listPager').pager({
					    page: currentPage,
					    recPerPage:pageSize,
					    elements:['first_icon', 'prev_icon', 'pages', 'next_icon', 'last_icon', 'total_text','size_menu','goto'],
					    pageSizeOptions:[5,10,20,30,50,100],
					    recTotal: result.pageTotal
					});					
				}else {
					new $.zui.Messager(result.msg, {
					    type: 'warning', // 定义颜色主题
					    placement: 'center' // 定义显示位置
					}).show();
				}
			},
			error:function(result){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning', // 定义颜色主题
				    placement: 'center' // 定义显示位置
				}).show();
			}
		});		
	}
	queryListFun();	
	//查询
	$("#searchBtn").click(function(){
		queryListFun();
	});	
	
	//审核页面初始化
	$("#checkBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		if (selectedItems.length == 0) {
			new $.zui.Messager('请选择要查询的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else if (selectedItems.length > 1) {
			new $.zui.Messager('请只选择一条要查询的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}else{	
			var url = adminQueryUrl + merDevService + detailMethod;
			var param='merDevInfId='+selectedItems[0].MerDev_ID;
			$.ajax({
				url:url,
				type:'post',
				dataType:'JSON',
				beforeSend:function(xhr){//设置请求头信息
					xhr.setRequestHeader("token",token);
					xhr.setRequestHeader("userId",+userId);
				},
				data:param,
				success:function(data){
					console.log('data='+data);
					if (data.success=='1'){
							$('#checkModal').modal('show', 'fit');
							$("#checkMerDevId").val(selectedItems[0].MerDev_ID);
							$("#checkMerDevCode").val(data.merDevInfo.merdevCode);
							$("#checkMerDevName").val(data.merDevInfo.merdevname);	
							$("#checkMerInfCode").val(data.merDevInfo.merinfName);	
							$("#checkAgeInfCode").val(data.merDevInfo.ageinfName);	
							$("#checkOrgCode").val(data.merDevInfo.orgName);	
							$("#checkAreaCode").val(data.merDevInfo.areaName);	
							$("#checkMerDevStatus").val(data.merDevStatus);	
							$("#checkDevInfCode").val(data.merDevInfo.devinfCode);	
							$("#checkDevWorkStatus").val(data.devWorkStatus);	
							$("#checkSN").val(data.merDevInfo.sn);	
							$("#checkDevHwVer").val(data.merDevInfo.devhwver);	
							$("#checkDevAppVer").val(data.merDevInfo.devappver);	
							$("#checkDevParamVer").val(data.merDevInfo.devparamver);	
							$("#checkDeliveryAddress").val(data.merDevInfo.deliveryaddress);
							$("#checkLongitude").val(data.merDevInfo.longitude);	
							$("#checkLatitude").val(data.merDevInfo.latitude);	
							$("#checkCrtUserId").val(data.merDevInfo.crtuserid);	
							$("#checkCrtDate").val(data.merDevInfo.crtdate);	
							$("#checkUptUserId").val(data.merDevInfo.uptuserid);	
							$("#checkLastUptDate").val(data.merDevInfo.lastuptdate);	
							$("#checkChkUserId").val(data.merDevInfo.chkuserid);	
							$("#checkChkDate").val(data.merDevInfo.chkdate);
							$("#checkChkRemark").val(data.merDevInfo.chkremark);
					}else{
						new $.zui.Messager(data.msg, {
						    type: 'warning',
						    placement:'center'
						}).show();
						queryListFun();
					}
				},
				error:function(e){
					new $.zui.Messager('系统繁忙,请稍候再试!', {
					    type: 'warning',
					    placement:'center'
					}).show();
				}
			});
		}
	});
	
	//审核通过
	$("#checkPassBtn").click(function(){
		$("#checkResult").val("checkPass");
		$("#checkForm").validate({
		rules:{	
			checkSN:{
				"required":true
			},
			checkRemark:{
				"required":true,
			}
		},	
		submitHandler:function(form){	
			$.ajax({
				url:adminQueryUrl + merDevService + checkMethod,
				type:'post',
				dataType:'JSON',
				beforeSend:function(xhr){//设置请求头信息
					xhr.setRequestHeader("token",token);
					xhr.setRequestHeader("userId",userId);
				},
				headers:{'token':token,'userId':userId},
				data:$(form).serialize(),
				success:function(data){
					//('data='+data);
					if (data.success == '1') {
	                	new $.zui.Messager('审核成功!', {
	    				    type: 'success',
	    				    placement:'center'
	    				}).show();
	                	$('#checkModal').modal('hide', 'fit');
	                	queryListFun();
	                }
	                else{
	                	new $.zui.Messager(data.msg, {
	    				    type: 'warning',
	    				    placement:'center'
	    				}).show();
	                }
				},
				error:function(e){
					new $.zui.Messager('系统繁忙,请稍候再试!', {
    				    type: 'warning',
    				    placement:'center'
    				}).show();
				}
			});
		}
	})
  });
	
	//审核不通过
	$("#checkNotPassBtn").click(function(){
		$("#checkResult").val("checkNotPass");
		console.log($("#checkResult").val());
		$("#checkForm").validate({
		rules:{			
			checkRemark:{
				"required":true,
			}
		},	
		submitHandler:function(form){	
			$.ajax({
				url:adminQueryUrl + merDevService + checkMethod,
				type:'post',
				dataType:'JSON',
				beforeSend:function(xhr){//设置请求头信息
					xhr.setRequestHeader("token",token);
					xhr.setRequestHeader("userId",userId);
				},
				headers:{'token':token,'userId':userId},
				data:$(form).serialize(),
				success:function(data){
					//('data='+data);
					if (data.success == '1') {
	                	new $.zui.Messager('审核成功!', {
	    				    type: 'success',
	    				    placement:'center'
	    				}).show();
	                	$('#checkModal').modal('hide', 'fit');
	                	queryListFun();
	                }
	                else{
	                	new $.zui.Messager(data.msg, {
	    				    type: 'warning',
	    				    placement:'center'
	    				}).show();
	                }
				},
				error:function(e){
					new $.zui.Messager('系统繁忙,请稍候再试!', {
    				    type: 'warning',
    				    placement:'center'
    				}).show();
				}
			});
		  }
	   })
    });
});