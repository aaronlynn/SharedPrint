/*商品信息*/
$(document).ready(function(){
	var currentPage = 1;
	var pageSize = 10;
	var token = $.zui.store.get("token");
	var userId = $.zui.store.get("userId");
	
	
	var queryListFun = function(){
		// 获取列表数据
		var queryListUrl = adminQueryUrl + goodsService + queryInfo;
		
		var param = "";
		// 搜索参数
		if ($("#queryGoodsCategory").val()!=''){
			param += "&GoodsCategory="+$("#queryGoodsCategory").val();
			alert($("#queryGoodsCategory").val())
		}
		if ($("#queryGoodsBrand").val()!='' ){
			param += "&GoodsBrand="+$("#queryGoodsBrand").val();
		}
		if ($("#queryMinGoodsPrice").val()!=''){
			param += "&MinGoodsPrice="+$("#queryMinGoodsPrice").val();
		}
		if ($("#queryMaxGoodsPrice").val()!=''){
			param += "&MaxGoodsPrice="+$("#queryMaxGoodsPrice").val();
		
		}
		if ($("#queryGoodsNameLike").val()!=''){
			param += "&GoodsNameLike="+$("#queryGoodsNameLike").val();
			
		}
		if (currentPage>0){
			param += "&currentPage="+(currentPage-1);
		}
		param += "&pageSize="+pageSize;
		param += "&currentPage="+(currentPage-1);
		
		console.log('queryListUrl='+queryListUrl);
		console.log('token='+token+",userId="+userId);
		console.log('param='+param);
		$.ajax({
			url:queryListUrl,
			type:'post',
			beforeSend:function(xhr){
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",userId);
			},
			header:{"token":token,"userId":userId},
			data:param,
			dataType:"JSON",
			success:function(result){
				console.log(result.success);
				console.log(result.pageList);
				if (result.success== '1'){
					
					$('#listDataGrid').empty();
					$('#listDataGrid').data('zui.datagrid',null);
					$('#listPager').empty();
					$('#listPager').data('zui.pager',null);
					
					$('#listDataGrid').datagrid({
						checkable: true,
					    checkByClickRow: true,
					    selectable: true,
					    dataSource: {
					        cols:[
					            {name: 'goodsName', label: '商品名称', width: 132},
					            {name: 'goodsCode', label: '所属商户', width: 159},
					            {name: 'categoryName', label: '所属商品类别', width: 109},
					            {name: 'brandName', label: '所属商品品牌', width: 132},
					            {name: 'price', label: '商品价格', width: 132},
					            {name: 'oprDate', label: '操作时间',width: 132},
					            {name: 'goodsId', label: '商品信息编号', hidden:true}
					        ],
					        cache:false,
					        array:result.pageList
					    }
					 
					});	
					
					// 手动进行初始化
					$('#listPager').pager({
					    page: currentPage,
					    recPerPage:pageSize,
					    elements:['first_icon', 'prev_icon', 'pages', 'next_icon', 'last_icon', 'total_text','size_menu','goto'],
					    pageSizeOptions:[5,10,20,30,50,100],
					    recTotal: result.pageTotal
					});
				}
				else {
					new $.zui.Messager(result.msg, {
					    type: 'warning', // 定义颜色主题
					    placement: 'center' // 定义显示位置
					}).show();
				}
			},
			error:function(result){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning', // 定义颜色主题
				    placement: 'center' // 定义显示位置
				}).show();
			}
		});
	};
	// 调起查询数据
	queryListFun();
	// 查询
	$("#searchBtn").click(function(){
		queryListFun();
	});
	// 监听分页，修改当前页，条数
	$('#listPager').on('onPageChange', function(e, state, oldState) {
		currentPage = state.page;
		pageSize = state.recPerPage;
		queryListFun();
	});
	
	
	// 初始化详情
	$("#detailBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		if (selectedItems.length == 0) {
			new $.zui.Messager('请选择要查看的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else if (selectedItems.length > 1) {
			new $.zui.Messager('请只选择一条要查看的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else {
			$("#detailForm")[0].reset();
			var url = adminQueryUrl + goodsService + detailInfoMethod;
			var param = 'goodsId='+selectedItems[0].goodsId;// 请求到列表页面
			console.log('param='+param);
			$.ajax({
				url:url,
				type:'post',
				dataType:'JSON',
				beforeSend:function(xhr){// 设置请求头信息
					xhr.setRequestHeader("token",token);
					xhr.setRequestHeader("userId",userId);
				},
				header:{'token':token,'userId':userId},
				data:param,
				success:function(data){
					console.log('data='+data);
					if (data.success=='1'){
						$('#detailModal').modal('show', 'fit');
						
						$("#detailGoodsCode").html(data.goodsMap.goodsCode);
						$("#detailGoodsName").html(data.goodsMap.goodsName);
						$("#CategoryName").html(data.goodsMap.categoryName);
						$("#BrandName").html(data.goodsMap.brandName);
						$("#Price").html(data.goodsMap.price);
						$("#Goodsdetail").html(data.goodsMap.remark);
						$("#SortNo").html(data.goodsMap.sortNo);
						$("#detailOprId").html(data.goodsMap.oprId);
						$("#detailOprDate").html(data.goodsMap.oprDate);
					}
				},
				error:function(e){
					new $.zui.Messager('系统繁忙,请稍候再试!', {
					    type: 'warning',
					    placement:'center'
					}).show();
				}
			});
		}
	});
	// 显示审核信息框
	$("#checkBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		if (selectedItems.length == 0) {
			new $.zui.Messager('请选择要查看的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else if (selectedItems.length > 1) {
			new $.zui.Messager('请只选择一条要查看的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else {
			$("#checkForm")[0].reset();
			var url = adminQueryUrl + goodsService + checkInfoMethod;
			var param = 'goodsId='+selectedItems[0].goodsId;// 请求到列表页面
			console.log('param='+param);
			$.ajax({
				url:url,
				type:'post',
				dataType:'JSON',
				beforeSend:function(xhr){// 设置请求头信息
					xhr.setRequestHeader("token",token);
					xhr.setRequestHeader("userId",userId);
				},
				header:{'token':token,'userId':userId},
				data:param,
				success:function(data){
					console.log('data='+data);
					if (data.success=='1'){
						$('#checkModal').modal('show', 'fit');
						// 所属角色
						$("#checkStatus").empty();
						$.each(data.checkInfoList, function(i, item){ 
								$("#checkStatus").append("<option value='"+item.key_value+"'>"+item.key_name+"</option>");   
							});
						
						$("#GoodsCode").val(data.goodsMap.goodsCode);
						$("#GoodsName").html(data.goodsMap.goodsName);
						$("#CategoryName1").html(data.goodsMap.categoryName);
						$("#BrandName1").html(data.goodsMap.brandName);
						$("#Price1").html(data.goodsMap.price);
						$("#Goodsdetail1").html(data.goodsMap.remark);
						$("#SortNo1").html(data.goodsMap.sortNo);
						$("#detailOprId1").html(data.goodsMap.oprId);
						$("#detailOprDate1").html(data.goodsMap.oprDate);
					}
				},
				error:function(e){
					new $.zui.Messager('系统繁忙,请稍候再试!', {
					    type: 'warning',
					    placement:'center'
					}).show();
				}
			});
		}
	});
	// 提交审核信息
	$("#upCheckBtn").click(function(){
		$("#checkForm").validate({
			
			errorPlacement:function(error,element){
				error.appendTo(element.parent().parent());
			},
			submitHandler:function(form){
				$.ajax({
					url:adminQueryUrl + goodsService + upcheckMethod,
					type:'post',
					dataType:'JSON',
					beforeSend:function(xhr){// 设置请求头信息
						xhr.setRequestHeader("token:'"+token+"'");
						xhr.setRequestHeader("userId:'"+userId+"'");
					},
					headers:{'token':token,'userId':userId},
					data:$(form).serialize(),
					success:function(data){
						console.log('data='+data);
						if (data.success == '1') {
		                	new $.zui.Messager('提交成功!', {
		    				    type: 'success',
		    				    placement:'center'
		    				}).show();
		                	$('#checkModal').modal('hide', 'fit');
		                	queryListFun();
		                }
		                else{
		                	new $.zui.Messager(data.msg, {
		    				    type: 'warning',
		    				    placement:'center'
		    				}).show();
		                }
					},
					error:function(e){
						new $.zui.Messager('系统繁忙,请稍候再试!', {
	    				    type: 'warning',
	    				    placement:'center'
	    				}).show();
					}
				});
			}
	});

  });
});
