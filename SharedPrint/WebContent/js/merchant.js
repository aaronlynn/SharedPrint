$(document).ready(function(){
	var token = $.zui.store.get("token");
	var userId = $.zui.store.get("userId");
	var currentPage = 1;
	var pageSize = 10;
	console.log('token='+token+",userId="+userId);
	var queryListFun = function(){
		var queryMerListUrl = adminQueryUrl + merService + queryPageListMethod;
		var param = "";	
		if($("#queryCondition").val()!=''){
			param += "&queryCondition="+$("#queryCondition").val();
		}
		/*if($("#queryMerInfCode").val()!=''){
			param += "&queryMerInfCode="+$("#queryMerInfCode").val();
		}*/
		if($("#queryMerInfStatus").val()!=''){
			param += "&queryMerInfStatus="+$("#queryMerInfStatus").val();
		}
		if($("#queryOrgCode").val()!=''){
			param += "&queryOrgCode="+$("#queryOrgCode").val();
		}
		/*if($("#queryMenuNameLike").val()!=null){
			param += "&menuNameLike="+$("#queryMenuNameLike").val()+"";
		}*/
		param += "&pageSize="+pageSize;
		param += "&currentPage="+(currentPage-1);
		console.log($("#queryMerNameLike").val());
		$.ajax({
			url:queryMerListUrl,
			type:'post',
			beforeSend:function(xhr){
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",userId);
			},
			header:{"token":token,"userId":userId},
			data:param,
			dataType:"JSON",
			success:function(result){
				console.log(result.success);
				if(result.success=='1'){
					//$('#listPager').empty();
					//$('#listPager').data('zui.datagrid',null);
					$('#listDataGrid').empty();
					$('#listDataGrid').data('zui.datagrid',null);
					
					console.log(result);
					$('#listDataGrid').datagrid({
						checkable:true,
						checkByClickRow:true,
					    dataSource: {
					        cols:[
					              {name: 'MerInf_Code', label: '商户编号', width: 0.1},
					              {name: 'MerInfName', label: '商户名称', width: 0.1},
					              {name: 'orgName', label: '所属机构', width: 0.1},
					              {name: 'agentName', label: '所属代理商', width: 0.1},
					              {name: 'merStatus', label: '商户状态', width: 0.1},
					              {name: 'Contactor', label: '联系人', width: 0.1},
					              {name: 'ContactTel', label: '联系电话', width: 0.1},
					              {name: 'oprDate', label: '操作时间', width: 0.1},
					        ],
					        array:result.listDataGrid
					    }				
					});
					console.log(currentPage);
					console.log(pageSize);
					console.log(result.pageTotal);									
					$('#listPager').pager({
					    page: currentPage,
					    recPerPage:pageSize,
					    elements:['first_icon', 'prev_icon', 'pages', 'next_icon', 'last_icon', 'total_text','size_menu','goto'],
					    pageSizeOptions:[5,10,20,30,50,100],
					    recTotal: result.pageTotal
					});					
				}else {
					new $.zui.Messager(result.msg, {
					    type: 'warning', // 定义颜色主题
					    placement: 'center' // 定义显示位置
					}).show();
				}
			},
			error:function(result){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning', // 定义颜色主题
				    placement: 'center' // 定义显示位置
				}).show();
			}
		});		
	}
	queryListFun();
	$("#searchBtn").click(function(){
		queryListFun();
	});
	
	$('#listPager').on('onPageChange', function(e, state, oldState) {
		currentPage = state.page;
		pageSize = state.recPerPage;
		queryListFun();
	});	
	
	var IDPathDeleteFile;
	var BusiAddressPathDeleteFile;
	//添加证件照片上传
	$('#uploadIDPath').uploader({
	    autoUpload: true,            // 当选择文件后立即自动进行上传操作
	    url: adminQueryUrl + fileService + uploadMethod,  // 文件上传提交地址
	    limitFilesCount:1,
	    responseHandler: function(responseObject, file) {
	    	IDPathDeleteFile = file;
	    	if (responseObject!=null && responseObject.response!=null){
	    		var resultJson = JSON.parse(responseObject.response);
	    		if (resultJson.success!=null && resultJson.success!=''
		    			&& typeof(resultJson.success)!='undefined'
	    				&& resultJson.fileName!=''
		    			&& typeof(resultJson.fileName)!='undefined'){
	    			if (resultJson.success=='1' ){
	    				$("#addIDPath").val(resultJson.fileName);
	    			}
	    		}
	    	}
	    } ,//responseHandler
		deleteActionOnDone:function(file, doRemoveFile){
			file = IDPathDeleteFile;
			if(file !=null){
				return true;
			}
		}
	});
	
	//添加经营场所照片上传
	$('#uploadBusiAddressPath').uploader({
	    autoUpload: true,            // 当选择文件后立即自动进行上传操作
	    url: adminQueryUrl + fileService + uploadMethod,  // 文件上传提交地址
	    limitFilesCount:1,
	    responseHandler: function(responseObject, file) {
	    	BusiAddressPathDeleteFile = file;
	    	if (responseObject!=null && responseObject.response!=null){
	    		var resultJson = JSON.parse(responseObject.response);
	    		if (resultJson.success!=null && resultJson.success!=''
		    			&& typeof(resultJson.success)!='undefined'
	    				&& resultJson.fileName!=''
		    			&& typeof(resultJson.fileName)!='undefined'){
	    			if (resultJson.success=='1' ){
	    				$("#addBusiAddressPath").val(resultJson.fileName);
	    			}
	    		}
	    	}
	    }, //responseHandler
	    deleteActionOnDone:function(file, doRemoveFile){   // 删除已上传文件
			file = BusiAddressPathDeleteFile;
			if(file !=null){
				return true;
			}
		}
	});
	
	//修改证件照片上传
	/*$('#uploadUpdateIDPath').uploader({
	    autoUpload: true,            // 当选择文件后立即自动进行上传操作
	    url: adminQueryUrl + fileService + uploadMethod,  // 文件上传提交地址
	    limitFilesCount:1,
	    responseHandler: function(responseObject, file) {	    	
	    	if (responseObject!=null && responseObject.response!=null){
	    		var resultJson = JSON.parse(responseObject.response);
	    		if (resultJson.success!=null && resultJson.success!=''
		    			&& typeof(resultJson.success)!='undefined'
	    				&& resultJson.fileName!=''
		    			&& typeof(resultJson.fileName)!='undefined'){
	    			if (resultJson.success=='1' ){
	    				$("#updateIDPath").val(resultJson.fileName);
	    			}
	    		}
	    	}
	    } //responseHandler
	});
	
	//修改经营场所照片上传
	$('#uploadUpdateBusiAddressPath').uploader({
	    autoUpload: true,            // 当选择文件后立即自动进行上传操作
	    url: adminQueryUrl + fileService + uploadMethod,  // 文件上传提交地址
	    limitFilesCount:1,
	    responseHandler: function(responseObject, file) {
	    	if (responseObject!=null && responseObject.response!=null){
	    		var resultJson = JSON.parse(responseObject.response);
	    		if (resultJson.success!=null && resultJson.success!=''
		    			&& typeof(resultJson.success)!='undefined'
	    				&& resultJson.fileName!=''
		    			&& typeof(resultJson.fileName)!='undefined'){
	    			if (resultJson.success=='1' ){
	    				$("#updateBusiAddressPath").val(resultJson.fileName);
	    			}
	    		}
	    	}
	    } //responseHandler
	});*/
	//添加页面预加载
	$("#addBtn").click(function(){
		$("#addForm")[0].reset();
		var queryParentMerUrl = adminQueryUrl + merService + initAddMethod;
		var param = '';	
		console.log(queryParentMerUrl)
		$("#addAgeInfCode").empty();
		$("#addOrgCode").empty();
		$("#addAreaCode").empty();
		$("#addAgeInfCode").empty();
		$("#addMerInfType").empty();
		$("#addMerInfLevel").empty();
		$("#addIDType").empty();
		$.ajax({
				url:queryParentMerUrl,
				type:'post',
				dataType:'JSON',
				beforeSend:function(xhr){
					xhr.setRequestHeader("token",token);
					xhr.setRequestHeader("userId",userId);
				},
				headers:{'token':token,'userId':userId},
				data:param,
				success:function(data){
					console.log('data='+data);
					if (data.success=='1'){												
						$.each(data.agentInfoList, function(i, item){     
							$("#addAgeInfCode").append("<option value='"+item.AgeInf_Code+"'>"+item.AgeInfName+"</option>");
						});
						$.each(data.orgInfoList, function(i, item1){     
							$("#addOrgCode").append("<option value='"+item1.Org_Code+"'>"+item1.Org_Name+"</option>");
						}); 
						$.each(data.areaInfoList, function(i, item2){     
							$("#addAreaCode").append("<option value='"+item2.areaCode+"'>"+item2.areaName+"</option>");
						}); 
						$.each(data.dicsInfoList, function(i, item3){
							if(item3.dics_code=="MerInfType"){
								$("#addMerInfType").append("<option value='"+item3.key_value+"'>"+item3.key_name+"</option>");
							}else if(item3.dics_code=="MerInfLevel"){
								$("#addMerInfLevel").append("<option value='"+item3.key_value+"'>"+item3.key_name+"</option>");
							}else if(item3.dics_code=="MerInfIDType"){
								$("#addIDType").append("<option value='"+item3.key_value+"'>"+item3.key_name+"</option>");
							}
							
						}); 
					}
				},
				error:function(e){
					new $.zui.Messager('系统繁忙,请稍候再试!', {
					    type: 'warning',
					    placement:'center'
					}).show();
				}			
		});
	});
	//添加商户保存
	$("#addSaveBtn").click(function(){			
			var validMerCodeUrl = adminQueryUrl + merService + validMerCode;
			var validMerNameUrl = adminQueryUrl + merService + validMerName;
			console.log(validMerNameUrl);
			$("#addForm").validate({
				rules:{
					addMerInfCode:{
						"required":true,
						"remote":{
							url:validMerCodeUrl,
							type:'post',
							dataType:'json',
							beforeSend:function(xhr){//设置请求头信息
								xhr.setRequestHeader("token",token);
								xhr.setRequestHeader("userId",userId);
							},
							headers:{'token':token,'userId':userId},
							data:{
								'merInfCode':function(){
									return $("#addMerInfCode").val();
								}
							}
						}
					},
					addMerInfName:{
						"required":true,
					    "remote":{
							url:validMerNameUrl,
							type:'post',
							beforeSend:function(xhr){//设置请求头信息
								xhr.setRequestHeader("token",token);
								xhr.setRequestHeader("userId",userId);
							},
							headers:{'token':token,'userId':userId},
							dataType:'json',
							data:{
								'merInfName':function(){
									return $("#addMerInfName").val();
								},															
							}
						}
					},
					addMerInfType:{
						"required":true
					}, //商户类型
					addIDCode:{
						"required":true
					}, //商户证件号
					/*addOrgConnTelephone:{
						"required":true,
						"digits":true
					},*/
					/*addIDType:{
						"required":true
					},*/ //商户证件类型
					addAgeInfCode:{
						"required":true
					},//所属代理商
					addOrgCode:{
						"required":true
					},//所属机构				
					addAreaCode:{
						"required":true
					},//所属地区
					addBusiAddress:{
						"required":true
					},//经营场所
					addContactor:{
						"required":true
					},//联系人
					addContactTel:{
						"required":true,
						"digits":true
					},//联系人电话
				},
				messages:{
					addMerInfCode:{
						"remote":"商户号已经存在"
					},
					addMerInfName:{
						"remote":"商户名称已经存在"
					}
				},
				submitHandler:function(form){	
					$.ajax({
						url:adminQueryUrl + merService + addMethod,
						type:'post',
						dataType:'JSON',
						beforeSend:function(xhr){//设置请求头信息
							xhr.setRequestHeader("token",token);
							xhr.setRequestHeader("userId",userId);
						},
						headers:{'token':token,'userId':userId},
						data:$(form).serialize(),
						success:function(data){
							if (data.success == '1') {
			                	new $.zui.Messager('添加成功!', {
			    				    type: 'success',
			    				    placement:'center'
			    				}).show();
			                	$('#addModal').modal('hide', 'fit');
			                	queryListFun();
			                }
			                else{
			                	new $.zui.Messager(data.msg, {
			    				    type: 'warning',
			    				    placement:'center'
			    				}).show();
			                }
						},
						error:function(e){
							new $.zui.Messager('系统繁忙,请稍候再试!', {
		    				    type: 'warning',
		    				    placement:'center'
		    				}).show();
						}
					});
				}
			});
	});
	var IDPathTempFile;
	var busiAddressPathTempFile;
	
	//修改页面初始化
	$("#updateBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		if(selectedItems.length == 0) {
			new $.zui.Messager('请选择要修改的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}else if(selectedItems.length > 1){
			new $.zui.Messager('请只选择一条要修改的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}else {
			$("#updateForm")[0].reset();
			//证件照片区域清空
			if (IDPathTempFile!=null && $('#uploadUpdateIDPathDiv')!=null){
				$('#uploadUpdateIDPathDiv').data('zui.uploader').removeFile(IDPathTempFile);
				IDPathTempFile = null;
			}			
			$("#uploadIDPathDiv").css("display","none");
			$("#updateIDPathImgChangeBtn").css("display","none");
			$("#uploadUpdateIDPathDiv2").css("display","none");
			$("#uploadUpdateIDPathDiv").css("display","none");
			
			//经营场所照片区域清空BusiAddressPath
			if (busiAddressPathTempFile!=null && $('#uploadUpdateBusiAddressPathDiv')!=null){
				$('#uploadUpdateBusiAddressPathDiv').data('zui.uploader').removeFile(BusiAddressPathTempFile);
				IDPathTempFile = null;
			}			
			$("#uploadBusiAddressPathDiv").css("display","none");
			$("#updateBusiAddressPathImgChangeBtn").css("display","none");
			$("#uploadUpdateBusiAddressPathDiv2").css("display","none");
			$("#uploadUpdateBusiAddressPathDiv").css("display","none"); 
			
			var url = adminQueryUrl + merService + initUpdateMethod;
			var param = 'merInfId='+selectedItems[0].MerInf_ID;//请求到列表页面
			console.log("url="+adminQueryUrl + merService + initUpdateMethod);
			console.log('param'+param);
			$.ajax({
				url:url,
				type:'post',
				dataType:'JSON',
				beforeSend:function(xhr){//设置请求头信息
					xhr.setRequestHeader("token",token);
					xhr.setRequestHeader("userId",+userId);
				},
				headers:{'token':token,'userId':userId},
				data:param,
				success:function(data){
					console.log('data='+data);
					if (data.success=='1'){
						$('#updateModal').modal('show', 'fit');
						$("#updateOrgCode").empty();
						$("#updateAreaCode").empty();
						$("#updateMerInfType").empty();
						$("#updateMerInfLevel").empty();
						$.each(data.agentInfoList, function(i, item){   
							if(data.merInfo.ageinfCode == item.AgeInf_Code){
								$("#updateAgeInfCode").append("<option value='"+item.AgeInf_Code+"'>"+item.AgeInfName+"</option>");
							}else {
								$("#updateAgeInfCode").append("<option value='"+item.AgeInf_Code+"'>"+item.AgeInfName+"</option>");
							}
						});
						$.each(data.orgInfoList, function(i, item1){ 
							if (data.merInfo.orgCode == item1.Org_Code){						
								$("#updateOrgCode").append("<option value='"+item1.Org_Code+"' selected='selected'>"+item1.Org_Name+"</option>");
							}
							else {
								$("#updateOrgCode").append("<option value='"+item1.Org_Code+"'>"+item1.Org_Name+"</option>");
							}
						}); 
						$.each(data.areaInfoList, function(i, item2){ 
							if (data.merInfo.areaCode == item2.areaCode){
								$("#updateAreaCode").append("<option value='"+item2.areaCode+"' selected='selected'>"+item2.areaName+"</option>");
							}
							else {
								$("#updateAreaCode").append("<option value='"+item2.areaCode+"'>"+item2.areaName+"</option>");
							}
						}); 
						$.each(data.dicsInfoList, function(i, item3){
							if(item3.dics_code=="MerInfType"){
								if (data.merInfo.merinftype == item3.key_value){
									$("#updateMerInfType").append("<option value='"+item3.key_value+"' selected='selected'>"+item3.key_name+"</option>");
								}
								else {
									$("#updateMerInfType").append("<option value='"+item3.key_value+"'>"+item3.key_name+"</option>");
								}
							}else if(item3.dics_code=="MerInfLevel"){
								if (data.merInfo.merinflevel == item3.key_value){
									$("#updateMerInfLevel").append("<option value='"+item3.key_value+"' selected='selected'>"+item3.key_name+"</option>");
								}
								else {
									$("#updateMerInfLevel").append("<option value='"+item3.key_value+"'>"+item3.key_name+"</option>");
								}
							}else if(item3.dics_code=="MerInfIDType"){
								if (data.merInfo.merinflevel == item3.key_value){
									$("#updateIDType").append("<option value='"+item3.key_value+"' selected='selected'>"+item3.key_name+"</option>");
								}
								else {
									$("#updateIDType").append("<option value='"+item3.key_value+"'>"+item3.key_name+"</option>");
								}
							}							
						}); 
						var updateIDPath = data.merInfo.idpath;
						var updateBusiAddressPath = data.merInfo.busiaddresspath;
						
						//修改证件照片						
						if (updateIDPath!=null && updateIDPath!='' && typeof(updateIDPath)!='undefined'){
							var imgSrc = adminQueryUrl + "/" + updateIDPath;
							var imgHtml = '<a href="'+imgSrc+'" target="_blank"><img src="'+imgSrc+'" width="200px" height="200px" class="img-thumbnail" alt="缩略图"></a>';
							$("#uploadIDPathDiv").css("display","block");
							$("#uploadIDPathDiv").html(imgHtml);
							$("#updateIDPath").val(data.merInfo.idpath);
							$("#updateIDPathImgChangeBtn").css("display","block");
						}
						else{
							$("#uploadIDPathDiv").css("display","none");
							$("#updateIDPathImgChangeBtn").css("display","none");
							$("#uploadUpdateIDPathDiv2").css("display","block");
							$("#uploadUpdateIDPathDiv").css("display","block");
						}
						
						//修改经营场所照片						
						if (updateBusiAddressPath!=null && updateBusiAddressPath!='' && typeof(updateBusiAddressPath)!='undefined'){
							var imgSrc = adminQueryUrl + "/" + updateBusiAddressPath;
							var imgHtml = '<a href="'+imgSrc+'" target="_blank"><img src="'+imgSrc+'" width="200px" height="200px" class="img-thumbnail" alt="缩略图"></a>';
							$("#uploadBusiAddressPathDiv").css("display","block");
							$("#uploadBusiAddressPathDiv").html(imgHtml);
							$("#updateBusiAddressPath").val(data.merInfo.idpath);
							$("#updateBusiAddressPathImgChangeBtn").css("display","block");
						}
						else{
							$("#uploadBusiAddressPathDiv").css("display","none");
							$("#updateBusiAddressPathImgChangeBtn").css("display","none");
							$("#uploadUpdateBusiAddressPathDiv2").css("display","block");
							$("#uploadUpdateBusiAddressPathDiv").css("display","block");
						}
						//($("#uploadBusiAddressPathDiv").innerHTML);
						$("#updateMerInfId").val(data.merInfo.merinfId);
						$("#updateMerInfCode").val(data.merInfo.merinfCode);
						$("#updateMerInfName").val(data.merInfo.merinfname);
						$("#updateIDType").val(data.merInfo.idtype);
						$("#updateIDCode").val(data.merInfo.idcode);
						$("#updateMerInfType").val(data.merInfo.merinftype);
						$("#updateAgeInfCode").val(data.merInfo.ageinfCode);
						$("#updateMerInfLevel").val(data.merInfo.merinflevel);
						$("#updateAlias").val(data.merInfo.alias);
						$("#updateOrgCode").val(data.merInfo.orgCode);
						$("#updateAreaCode").val(data.merInfo.areaCode);
						$("#updateBusiAddress").val(data.merInfo.busiaddress);
						$("#updateContactor").val(data.merInfo.contactor);
						$("#updateContactTel").val(data.merInfo.contacttel);
						$("#updateEmail").val(data.merInfo.email);
						$("#updateQQ").val(data.merInfo.qq);
						$("#updateWeixin").val(data.merInfo.weixin);										
						$("#updateCliMgrCode").val(data.merInfo.climgrcode);
						$("#updateRemark").val(data.merInfo.remark);
					}
				},
				error:function(e){
					new $.zui.Messager('系统繁忙,请稍候再试!', {
					    type: 'warning',
					    placement:'center'
					}).show();
				}
			});
		}
	});
	
	//监听修改证件照片的文件上传
	$('#uploadUpdateIDPathDiv').uploader({
	    autoUpload: true,            // 当选择文件后立即自动进行上传操作
	    url: adminQueryUrl + fileService + uploadMethod,  // 文件上传提交地址
	    limitFilesCount:1,
	    responseHandler: function(responseObject, file) {
	    	IDPathTempFile = file;
	    	if (responseObject!=null && responseObject.response!=null){
	    		var resultJson = JSON.parse(responseObject.response);
	    		if (resultJson.success!=null && resultJson.success!=''
		    			&& typeof(resultJson.success)!='undefined'
	    				&& resultJson.fileName!=''
		    			&& typeof(resultJson.fileName)!='undefined'){
	    			if (resultJson.success=='1' ){
	    				$("#updateIDPath").val(resultJson.fileName);
	    			}
	    		}
	    	}
	    }, //responseHandler
	    deleteActionOnDone:function(file, doRemoveFile){   // 删除已上传文件
			file = IDPathTempFile;
			if(file !=null){
				return true;
			}
		}
	});

	//监听修改经营场所照片的文件上传
	$('#uploadUpdateBusiAddressPathDiv').uploader({
	    autoUpload: true,            // 当选择文件后立即自动进行上传操作
	    url: adminQueryUrl + fileService + uploadMethod,  // 文件上传提交地址
	    limitFilesCount:1,
	    responseHandler: function(responseObject, file) {
	    	busiAddressPathTempFile = file;
	    	if (responseObject!=null && responseObject.response!=null){
	    		var resultJson = JSON.parse(responseObject.response);
	    		if (resultJson.success!=null && resultJson.success!=''
		    			&& typeof(resultJson.success)!='undefined'
	    				&& resultJson.fileName!=''
		    			&& typeof(resultJson.fileName)!='undefined'){
	    			if (resultJson.success=='1' ){
	    				$("#updateBusiAddressPath").val(resultJson.fileName);
	    			}
	    		}
	    	}
	    }, //responseHandler
	    deleteActionOnDone:function(file, doRemoveFile){   // 删除已上传文件
			file = busiAddressPathTempFile;
			if(file !=null){
				return true;
			}
		}
	});
	//商户修改保存
	$("#updateSaveBtn").click(function(){			
		var validMerCodeUrl = adminQueryUrl + merService + validMerCode;
		var validMerNameUrl = adminQueryUrl + merService + validMerName;
		//(validMerNameUrl);
		$("#updateForm").validate({
			rules:{
				updateMerInfCode:{
					"required":true,
					"remote":{
						url:validMerCodeUrl,
						type:'post',
						dataType:'json',
						beforeSend:function(xhr){//设置请求头信息
							xhr.setRequestHeader("token",token);
							xhr.setRequestHeader("userId",userId);
						},
						headers:{'token':token,'userId':userId},
						data:{
							'merInfCode':function(){
								return $("#updateMerInfCode").val();
							},
							'merInfId':function(){
								return $("#updateMerInfId").val();
							}
						}
					}
				},
				updateMerInfName:{
					"required":true,
				    "remote":{
						url:validMerNameUrl,
						type:'post',
						beforeSend:function(xhr){//设置请求头信息
							xhr.setRequestHeader("token",token);
							xhr.setRequestHeader("userId",userId);
						},
						headers:{'token':token,'userId':userId},
						dataType:'json',
						data:{
							'merInfName':function(){
								return $("#updateMerInfName").val();
							},	
							'merInfId':function(){
								return $("#updateMerInfId").val();
							}
						}
					}
				},
				updateMerInfType:{
					"required":true
				}, //商户类型
				updateIDCode:{
					"required":true
				}, //商户证件号
				addOrgConnTelephone:{
					"required":true,
					"digits":true
				},
				addIDType:{
					"required":true
				}, //商户证件类型
				addAgeInfCode:{
					"required":true
				},//所属代理商
				updateOrgCode:{
					"required":true
				},//所属机构				
				updateAreaCode:{
					"required":true
				},//所属地区
				updateBusiAddress:{
					"required":true
				},//经营场所
				updateContactor:{
					"required":true
				},//联系人
				updateContactTel:{
					"required":true,
					"digits":true
				},//联系人电话
			},
			messages:{
				updateMerInfCode:{
					"remote":"商户号已经存在"
				},
				updateMerInfName:{
					"remote":"商户名称已经存在"
				}
			},
			submitHandler:function(form){	
				$.ajax({
					url:adminQueryUrl + merService + updateMethod,
					type:'post',
					dataType:'JSON',
					beforeSend:function(xhr){//设置请求头信息
						xhr.setRequestHeader("token",token);
						xhr.setRequestHeader("userId",userId);
					},
					headers:{'token':token,'userId':userId},
					data:$(form).serialize(),
					success:function(data){
						//('data='+data);
						if (data.success == '1') {
		                	new $.zui.Messager('修改成功!', {
		    				    type: 'success',
		    				    placement:'center'
		    				}).show();
		                	$('#updateModal').modal('hide', 'fit');
		                	queryListFun();
		                }
		                else{
		                	new $.zui.Messager(data.msg, {
		    				    type: 'warning',
		    				    placement:'center'
		    				}).show();
		                }
					},
					error:function(e){
						new $.zui.Messager('系统繁忙,请稍候再试!', {
	    				    type: 'warning',
	    				    placement:'center'
	    				}).show();
					}
				});
			}
		})
});
	
	//删除机构
	$("#deleteBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		if (selectedItems.length == 0) {
			new $.zui.Messager('请选择要删除的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}else if(selectedItems.length >1){
			new $.zui.Messager('只能选择一条记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}else {			
				var url = adminQueryUrl + merService + deleteMethod;
				var param = 'merInfId='+selectedItems[0].MerInf_ID;//请求到列表页面
				$.ajax({
					url:url,
					type:'post',
					dataType:'JSON',
					beforeSend:function(xhr){//设置请求头信息
						xhr.setRequestHeader("token",token);
						xhr.setRequestHeader("userId",+userId);
					},
					headers:{'token':token,'userId':userId},
					data:param,
					success:function(data){
						console.log('data='+data);
						if (data.success=='1'){
							new $.zui.Messager('删除成功!', {
							    type: 'success',
							    placement:'center'
							}).show();
							queryListFun();
						}else{
							new $.zui.Messager('系统繁忙,删除失败!', {
							    type: 'warning',
							    placement:'center'
							}).show();
						}
					},
					error:function(e){
						new $.zui.Messager('系统繁忙,请稍候再试!', {
						    type: 'warning',
						    placement:'center'
						}).show();
					}
				});			
		}			
	});
	//详情页面
	$("#detailBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		if(selectedItems.length == 0) {
			new $.zui.Messager('请选择要查询的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}else if(selectedItems.length > 1){
			new $.zui.Messager('请只选择一条要查询的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}else {
			$("#detailForm")[0].reset();
			var url = adminQueryUrl + merService + detailMethod;
			var param = 'merInfId='+selectedItems[0].MerInf_ID;//请求到列表页面
			console.log("url="+adminQueryUrl + merService + detailMethod);
			console.log('param'+param);
			$.ajax({
				url:url,
				type:'post',
				dataType:'JSON',
				beforeSend:function(xhr){//设置请求头信息
					xhr.setRequestHeader("token",token);
					xhr.setRequestHeader("userId",+userId);
				},
				headers:{'token':token,'userId':userId},
				data:param,
				success:function(data){
					console.log('data='+data);
					if (data.success=='1'){
						$('#detailModal').modal('show', 'fit');		
						$.each(data.agentInfoList, function(i, item){ 
							if (data.merInfo.ageinfCode == item.AgeInf_Code){						
								$("#detailAgeInfCode").val(item.AgeInfName);
							}							
						});
						$.each(data.orgInfoList, function(i, item1){ 
							if (data.merInfo.orgCode == item1.Org_Code){						
								$("#detailOrgCode").val(item1.Org_Name);
							}							
						}); 
						$.each(data.areaInfoList, function(i, item2){ 
							if (data.merInfo.areaCode == item2.areaCode){
								$("#detailAreaCode").val(item2.areaName);
							}
						}); 
						$.each(data.dicsInfoList, function(i, item3){
							if(item3.dics_code=="MerInfType"){
								if (data.merInfo.merinftype == item3.key_value){
									$("#detailMerInfType").val(item3.key_name);	
									//(item3.key_name);
								}								
							}else if(item3.dics_code=="MerInfLevel"){
								if (data.merInfo.merinflevel == item3.key_value){
									$("#detailMerInfLevel").val(item3.key_name);
									//(item3.key_name);
								}		
							}else if(item3.dics_code=="MerInfIDType"){
								if (data.merInfo.idtype == item3.key_value){
									$("#detailIDType").val(item3.key_name);									
								}		
							}
							else if(item3.dics_code=="MerInfStatus"){
								if (data.merInfo.merinfstatus == item3.key_value){
									$("#detailMerInfStatus").val(item3.key_name);									
								}		
							}
						}); 
						var updateIDPath = data.merInfo.idpath;
						var updateBusiAddressPath = data.merInfo.busiaddresspath;
						//显示证件照片
						if (updateIDPath!=null && updateIDPath!='' && typeof(updateIDPath)!=''){
							var imgSrc = adminQueryUrl + "/" + updateIDPath;
							var imgHtml = '<a href="'+imgSrc+'" target="_blank"><img src="'+imgSrc+'" width="200px" height="200px" class="img-thumbnail" alt="缩略图"></a>';	
							$("#uploadDetailIDPathDiv").removeClass();
							$("#uploadDetailIDPathDiv").html(imgHtml);
							$("#detailIDPath").val(data.merInfo.idpath);
						}		
						//显示经营场所照片
						if (updateBusiAddressPath!=null && updateBusiAddressPath!='' && typeof(updateBusiAddressPath)!=''){
							var imgSrc = adminQueryUrl + "/" + updateBusiAddressPath;
							var imgHtml = '<a href="'+imgSrc+'" target="_blank"><img src="'+imgSrc+'" width="200px" height="200px" class="img-thumbnail" alt="缩略图"></a>';
							$("#uploadDetailBusiAddressPathDiv").removeClass();
							$("#uploadDetailBusiAddressPathDiv").html(imgHtml);
							$("#detailBusiAddressPath").val(data.merInfo.busiaddresspath);
						}
						$("#detailMerInfId").val(data.merInfo.merinfId);
						$("#detailMerInfCode").val(data.merInfo.merinfCode);
						$("#detailMerInfName").val(data.merInfo.merinfname);					
						$("#detailIDCode").val(data.merInfo.idcode);
						$("#detailAlias").val(data.merInfo.alias);
						$("#detailBusiAddress").val(data.merInfo.busiaddress);
						$("#detailContactor").val(data.merInfo.contactor);
						$("#detailContactTel").val(data.merInfo.contacttel);
						$("#detailEmail").val(data.merInfo.email);
						$("#detailQQ").val(data.merInfo.qq);
						$("#detailWeixin").val(data.merInfo.weixin);										
						$("#detailCliMgrCode").val(data.merInfo.climgrcode);
						$("#detailRemark").val(data.merInfo.remark);
					}
				},
				error:function(e){
					new $.zui.Messager('系统繁忙,请稍候再试!', {
					    type: 'warning',
					    placement:'center'
					}).show();
				}
			});
		}
	});
	//审核页面
	$("#checkBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		if(selectedItems.length == 0) {
			new $.zui.Messager('请选择要审核的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}else if(selectedItems.length > 1){
			new $.zui.Messager('请只选择一条要审核的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}else {
			$("#checkForm")[0].reset();
			var url = adminQueryUrl + merService + detailMethod;
			var param = 'merInfId='+selectedItems[0].MerInf_ID;//请求到列表页面
			console.log("url="+adminQueryUrl + merService + detailMethod);
			console.log('param'+param);
			$.ajax({
				url:url,
				type:'post',
				dataType:'JSON',
				beforeSend:function(xhr){//设置请求头信息
					xhr.setRequestHeader("token",token);
					xhr.setRequestHeader("userId",+userId);
				},
				headers:{'token':token,'userId':userId},
				data:param,
				success:function(data){
					console.log('data='+data);
					if (data.success=='1'){
						$('#checkModal').modal('show', 'fit');		
						$.each(data.agentInfoList, function(i, item){ 
							if (data.merInfo.ageinfCode == item.AgeInf_Code){						
								$("#checkAgeInfCode").val(item.AgeInfName);
							}							
						});
						$.each(data.orgInfoList, function(i, item1){ 
							if (data.merInfo.orgCode == item1.Org_Code){						
								$("#checkOrgCode").val(item1.Org_Name);
							}							
						}); 
						$.each(data.areaInfoList, function(i, item2){ 
							if (data.merInfo.areaCode == item2.areaCode){
								$("#checkAreaCode").val(item2.areaName);
							}
						}); 
						$.each(data.dicsInfoList, function(i, item3){
							if(item3.dics_code=="MerInfType"){
								if (data.merInfo.merinftype == item3.key_value){
									$("#checkMerInfType").val(item3.key_name);	
									//(item3.key_name);
								}								
							}else if(item3.dics_code=="MerInfLevel"){
								if (data.merInfo.merinflevel == item3.key_value){
									$("#checkMerInfLevel").val(item3.key_name);
									//(item3.key_name);
								}		
							}else if(item3.dics_code=="MerInfIDType"){
								if (data.merInfo.idtype == item3.key_value){
									$("#checkIDType").val(item3.key_name);									
								}		
							}
							else if(item3.dics_code=="MerInfStatus"){
								if (data.merInfo.merinfstatus == item3.key_value){
									$("#checkMerInfStatus").val(item3.key_name);									
								}		
							}
						}); 
						var updateIDPath = data.merInfo.idpath;
						var updateBusiAddressPath = data.merInfo.busiaddresspath;
						//显示证件照片
						if (updateIDPath!=null && updateIDPath!='' && typeof(updateIDPath)!=''){
							var imgSrc = adminQueryUrl + "/" + updateIDPath;
							var imgHtml = '<a href="'+imgSrc+'" target="_blank"><img src="'+imgSrc+'" width="200px" height="200px" class="img-thumbnail" alt="缩略图"></a>';	
							$("#uploadcheckIDPathDiv").removeClass();
							$("#uploadcheckIDPathDiv").html(imgHtml);
							$("#checkIDPath").val(data.merInfo.idpath);
						}		
						//显示经营场所照片
						if (updateBusiAddressPath!=null && updateBusiAddressPath!='' && typeof(updateBusiAddressPath)!=''){
							var imgSrc = adminQueryUrl + "/" + updateBusiAddressPath;
							var imgHtml = '<a href="'+imgSrc+'" target="_blank"><img src="'+imgSrc+'" width="200px" height="200px" class="img-thumbnail" alt="缩略图"></a>';
							$("#uploadcheckBusiAddressPathDiv").removeClass();
							$("#uploadcheckBusiAddressPathDiv").html(imgHtml);
							$("#checkBusiAddressPath").val(data.merInfo.busiaddresspath);
						}
						$("#checkMerInfId").val(data.merInfo.merinfId);
						console.log($("#checkMerInfId").val());
						$("#checkMerInfCode").val(data.merInfo.merinfCode);
						$("#checkMerInfName").val(data.merInfo.merinfname);					
						$("#checkIDCode").val(data.merInfo.idcode);
						$("#checkAlias").val(data.merInfo.alias);
						$("#checkBusiAddress").val(data.merInfo.busiaddress);
						$("#checkContactor").val(data.merInfo.contactor);
						$("#checkContactTel").val(data.merInfo.contacttel);
						$("#checkEmail").val(data.merInfo.email);
						$("#checkQQ").val(data.merInfo.qq);
						$("#checkWeixin").val(data.merInfo.weixin);										
						$("#checkCliMgrCode").val(data.merInfo.climgrcode);
						$("#checkRemark").val(data.merInfo.remark);
					}
				},
				error:function(e){
					new $.zui.Messager('系统繁忙,请稍候再试!', {
					    type: 'warning',
					    placement:'center'
					}).show();
				}
			});
		}
	});
	//审核通过
	$("#checkPassBtn").click(function(){
		$("#checkResult").val("checkPass");
		console.log($("#checkResult").val());
		$("#checkForm").validate({
		rules:{			
			checkRemark:{
				"required":true,
			}
		},	
		submitHandler:function(form){	
			$.ajax({
				url:adminQueryUrl + merService + checkMethod,
				type:'post',
				dataType:'JSON',
				beforeSend:function(xhr){//设置请求头信息
					xhr.setRequestHeader("token",token);
					xhr.setRequestHeader("userId",userId);
				},
				headers:{'token':token,'userId':userId},
				data:$(form).serialize(),
				success:function(data){
					//('data='+data);
					if (data.success == '1') {
	                	new $.zui.Messager('审核成功!', {
	    				    type: 'success',
	    				    placement:'center'
	    				}).show();
	                	$('#checkModal').modal('hide', 'fit');
	                	queryListFun();
	                }
	                else{
	                	new $.zui.Messager(data.msg, {
	    				    type: 'warning',
	    				    placement:'center'
	    				}).show();
	                }
				},
				error:function(e){
					new $.zui.Messager('系统繁忙,请稍候再试!', {
    				    type: 'warning',
    				    placement:'center'
    				}).show();
				}
			});
		}
	})
  });
	//审核不通过
	$("#checkNotPassBtn").click(function(){
		$("#checkResult").val("checkNotPass");
		console.log($("#checkResult").val());
		$("#checkForm").validate({
		rules:{			
			checkRemark:{
				"required":true,
			}
		},	
		submitHandler:function(form){	
			$.ajax({
				url:adminQueryUrl + merService + checkMethod,
				type:'post',
				dataType:'JSON',
				beforeSend:function(xhr){//设置请求头信息
					xhr.setRequestHeader("token",token);
					xhr.setRequestHeader("userId",userId);
				},
				headers:{'token':token,'userId':userId},
				data:$(form).serialize(),
				success:function(data){
					//('data='+data);
					if (data.success == '1') {
	                	new $.zui.Messager('审核成功!', {
	    				    type: 'success',
	    				    placement:'center'
	    				}).show();
	                	$('#checkModal').modal('hide', 'fit');
	                	queryListFun();
	                }
	                else{
	                	new $.zui.Messager(data.msg, {
	    				    type: 'warning',
	    				    placement:'center'
	    				}).show();
	                }
				},
				error:function(e){
					new $.zui.Messager('系统繁忙,请稍候再试!', {
    				    type: 'warning',
    				    placement:'center'
    				}).show();
				}
			});
		}
	})
  });
	//解冻商户
	$("#thawBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		if(selectedItems.length == 0) {
			new $.zui.Messager('请选择要解冻的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}else if(selectedItems.length > 1){
			new $.zui.Messager('请只选择一条要解冻的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}else{
			$("#thawAndFreezeForm")[0].reset();
			$('#determineText').empty();
			var queryThawAndFreezeUrl = adminQueryUrl + merService + initFreezeMethod;
			var param = 'merInfId='+selectedItems[0].MerInf_ID;	
			$.ajax({
					url:queryThawAndFreezeUrl,
					type:'post',
					dataType:'JSON',
					beforeSend:function(xhr){
						xhr.setRequestHeader("token",token);
						xhr.setRequestHeader("userId",userId);
					},
					headers:{'token':token,'userId':userId},
					data:param,
					success:function(data){
						console.log('data='+data);
						if (data.success=='2'){
							$('#determineText').val("您确定要解冻此用户吗？");
							$('#thawAndFreezeModal').modal('show', 'fit');	
							$('#oprResult').val("解冻");
							$("#MerInfId").val(selectedItems[0].MerInf_ID);
						}else if(data.success=='1'){
							new $.zui.Messager('该商户状态已为正常，无需解冻', {
							    type: 'warning',
							    placement:'center'
							}).show();
						}else{
							new $.zui.Messager('系统繁忙,解冻失败!', {
							    type: 'warning',
							    placement:'center'
							}).show();
						}
					},
					error:function(e){
						new $.zui.Messager('系统繁忙,请稍候再试!', {
						    type: 'warning',
						    placement:'center'
						}).show();
					}			
			});
		}
   });
	//冻结商户
	$("#freezeBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		if(selectedItems.length == 0) {
			new $.zui.Messager('请选择要冻结的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}else if(selectedItems.length > 1){
			new $.zui.Messager('请只选择一条要冻结的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}else{
			$("#thawAndFreezeForm")[0].reset();
			$('#determineText').empty();
			var queryThawAndFreezeUrl = adminQueryUrl + merService + initFreezeMethod;
			var param = 'merInfId='+selectedItems[0].MerInf_ID;	
			$.ajax({
					url:queryThawAndFreezeUrl,
					type:'post',
					dataType:'JSON',
					beforeSend:function(xhr){
						xhr.setRequestHeader("token",token);
						xhr.setRequestHeader("userId",userId);
					},
					headers:{'token':token,'userId':userId},
					data:param,
					success:function(data){
						console.log('data='+data);
						if (data.success=='1'){
							$('#determineText').val("您确定要冻结此用户吗？");
							$('#thawAndFreezeModal').modal('show', 'fit');
							$('#oprResult').val("冻结");
							$("#MerInfId").val(selectedItems[0].MerInf_ID);
						}else if(data.success=='2'){
							new $.zui.Messager('该商户状态已为冻结，无需再次冻结！', {
							    type: 'warning',
							    placement:'center'
							}).show();
						}else{
							new $.zui.Messager('系统繁忙,解冻失败!', {
							    type: 'warning',
							    placement:'center'
							}).show();
						}
					},
					error:function(e){
						new $.zui.Messager('系统繁忙,请稍候再试!', {
						    type: 'warning',
						    placement:'center'
						}).show();
					}			
			});
		}
   });
	$("#determineBtn").click(function(){
		$("#thawAndFreezeForm").validate({
			submitHandler:function(form){				
				$.ajax({
					url:adminQueryUrl + merService + thawAndFreezeMethod,
					type:'post',
					dataType:'JSON',
					beforeSend:function(xhr){//设置请求头信息
						xhr.setRequestHeader("token",token);
						xhr.setRequestHeader("userId",userId);
					},
					headers:{'token':token,'userId':userId},
					data:$(form).serialize(),
					success:function(data){
						if (data.success == '1') {
		                	new $.zui.Messager('冻结成功!', {
		    				    type: 'success',
		    				    placement:'center'
		    				}).show();
		                	$('#thawAndFreezeModal').modal('hide', 'fit');
		                	queryListFun();
		                }else if(data.success == '2'){
		                	new $.zui.Messager('解冻成功!', {
		    				    type: 'success',
		    				    placement:'center'
		    				}).show();
		                	$('#thawAndFreezeModal').modal('hide', 'fit');
		                	queryListFun();
		                }
		                else{
		                	new $.zui.Messager(data.msg, {
		    				    type: 'warning',
		    				    placement:'center'
		    				}).show();
		                }
					},
					error:function(e){
						new $.zui.Messager('系统繁忙,请稍候再试!', {
	    				    type: 'warning',
	    				    placement:'center'
	    				}).show();
					}
				});
			}			
		});
	});
});

//变更证件图片
function updateIDPathImgChange(){
	$("#uploadIDPathDiv").css("display","none");
	$("#updateIDPathImgChangeBtn").css("display","none");
	$("#uploadUpdateIDPathDiv").css("display","block");
	$("#uploadUpdateIDPathDiv2").css("display","block");
	//$('#updateUploadHeadpicDiv').data('zui.uploader').destroy();
}


//变更经营场所图片
function updateBusiAddressPathImgChange(){
	$("#uploadBusiAddressPathDiv").css("display","none");
	$("#updateBusiAddressPathImgChangeBtn").css("display","none");
	$("#uploadUpdateBusiAddressPathDiv").css("display","block");
	$("#uploadUpdateBusiAddressPathDiv2").css("display","block");
	//$('#updateUploadHeadpicDiv').data('zui.uploader').destroy();
}