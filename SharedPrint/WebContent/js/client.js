$(document).ready(function(){
	var currentPage = 1;
	var pageSize = 10;
	var token = $.zui.store.get("token");
	var userId = $.zui.store.get("userId");
	
	var queryListFun = function(){
		//获取列表数据
		var queryListUrl = adminQueryUrl + clientService + queryClient;
		
		var param = "";
		//搜索参数
		if ($("#queryClientCode").val()!=''){
			param += "&ClientCode="+$("#queryClientCode").val();
		}
		if ($("#queryClientNameLike").val()!=''){
			param += "&ClientNameLike="+$("#queryClientNameLike").val();
		}
		if (currentPage>0){
			param += "&currentPage="+(currentPage-1);
		}
		param += "&pageSize="+pageSize;
		param += "&currentPage="+(currentPage-1);
		
		console.log('queryListUrl='+queryListUrl);
		console.log('token='+token+",userId="+userId);
		console.log('param='+param);
		$.ajax({
			url:queryListUrl,
			type:'post',
			beforeSend:function(xhr){
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",userId);
			},
			header:{"token":token,"userId":userId},
			data:param,
			dataType:"JSON",
			success:function(result){
				console.log(result.success);
				console.log(result.pageList);
				if (result.success== '1'){
					
					$('#listDataGrid').empty();
					$('#listDataGrid').data('zui.datagrid',null);
					$('#listPager').empty();
					$('#listPager').data('zui.pager',null);
					
					$('#listDataGrid').datagrid({
						checkable: true,
					    checkByClickRow: true,
					    selectable: true,
					    dataSource: {
					        cols:[
					        	{name: 'clientID', label: '客户编码', width: 132},
					            {name: 'clientCode', label: '客户号', width: 132},
					            {name: 'clientName', label: '客户名称', width: 134},
					            {name: 'loginName', label: '登录名', width: 109},
					            {name: 'clientStatus', label: '客户状态', width: 109},
					            {name: 'realStatus', label: '实名状态', width: 287},
					        ],
					        cache:false,
					        array:result.pageList
					    }
					 
					});	
					
					// 手动进行初始化
					$('#listPager').pager({
					    page: currentPage,
					    recPerPage:pageSize,
					    elements:['first_icon', 'prev_icon', 'pages', 'next_icon', 'last_icon', 'total_text','size_menu','goto'],
					    pageSizeOptions:[5,10,20,30,50,100],
					    recTotal: result.pageTotal
					});
				}
				else {
					new $.zui.Messager(result.msg, {
					    type: 'warning', // 定义颜色主题
					    placement: 'center' // 定义显示位置
					}).show();
				}
			},
			error:function(result){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning', // 定义颜色主题
				    placement: 'center' // 定义显示位置
				}).show();
			}
		});
	};
	//调起查询数据
	queryListFun();
	//查询
	$("#searchBtn").click(function(){
		queryListFun();
	});
	//监听分页，修改当前页，条数
	$('#listPager').on('onPageChange', function(e, state, oldState) {
		currentPage = state.page;
		pageSize = state.recPerPage;
		queryListFun();
	});
	
	//重置密码选择
	$("#resetPwdBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		if (selectedItems.length == 0) {
			new $.zui.Messager('请选择要修改的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
			$('#resetPwdModal').modal('show', 'fit');
		}
		else if (selectedItems.length > 1) {
			new $.zui.Messager('请只选择一条要修改的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
			$('#resetPwdModal').modal('show', 'fit');
		}
	});
	//密码重置保存
	$("#resetPwdClientBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		var url = adminQueryUrl + clientService + resetPwd;
		var param = 'clientID='+selectedItems[0].clientID;//请求到列表页面
		console.log('url='+url);
		console.log('param='+param);
		$.ajax({
			url:url,
			type:'post',
			dataType:'JSON',
			beforeSend:function(xhr){//设置请求头信息
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",userId);
			},
			header:{'token':token,'userId':userId},
			data:param,
			success:function(data){
				console.log('data='+data);
				if (data.success=='1'){
	                	new $.zui.Messager('密码重置成功!新密码为123456', {
	    				    type: 'success',
	    				    placement:'center'
	    				}).show();
	                	$('#resetPwdModal').modal('hide', 'fit');
	                	queryListFun();
				}
                else{
                	new $.zui.Messager(data.msg, {
    				    type: 'warning',
    				    placement:'center'
    				}).show();
				}
			},
			error:function(e){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning',
				    placement:'center'
				}).show();
			}
		});
	});
	
	//客户冻结选择
	$("#freezeBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		if (selectedItems.length == 0) {
			new $.zui.Messager('请选择要修改的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
			$('#freezeModal').modal('show', 'fit');
		}
		else if (selectedItems.length > 1) {
			new $.zui.Messager('请只选择一条要修改的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
			$('#freezeModal').modal('show', 'fit');
		}
	});
	//客户冻结保存
	$("#freezeClientBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		var url = adminQueryUrl + clientService + freeze;
		var param = 'clientID='+selectedItems[0].clientID;//请求到列表页面
		console.log('url='+url);
		console.log('param='+param);
		$.ajax({
			url:url,
			type:'post',
			dataType:'JSON',
			beforeSend:function(xhr){//设置请求头信息
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",userId);
			},
			header:{'token':token,'userId':userId},
			data:param,
			success:function(data){
				console.log('data='+data);
				if (data.success=='1'){
	                	new $.zui.Messager('已成功冻结该客户！', {
	    				    type: 'success',
	    				    placement:'center'
	    				}).show();
	                	$('#freezeModal').modal('hide', 'fit');
	                	queryListFun();
				}else if(data.success=='2'){
					new $.zui.Messager('该用户为冻结状态，不可重复冻结', {
    				    type: 'warning',
    				    placement:'center'
    				}).show();
                	$('#freezeModal').modal('hide', 'fit');
                	queryListFun();
				}
                else{
                	new $.zui.Messager(data.msg, {
    				    type: 'warning',
    				    placement:'center'
    				}).show();
				}
			},
			error:function(e){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning',
				    placement:'center'
				}).show();
			}
		});
	});
	
	//客户解冻选择
	$("#unfreezeBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		if (selectedItems.length == 0) {
			new $.zui.Messager('请选择要修改的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
			$('#unfreezeModal').modal('show', 'fit');
		}
		else if (selectedItems.length > 1) {
			new $.zui.Messager('请只选择一条要修改的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
			$('#unfreezeModal').modal('show', 'fit');
		}
	});
	//客户解冻保存
	$("#unfreezeClientBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		var url = adminQueryUrl + clientService + unfreeze;
		var param = 'clientID='+selectedItems[0].clientID;//请求到列表页面
		console.log('url='+url);
		console.log('param='+param);
		$.ajax({
			url:url,
			type:'post',
			dataType:'JSON',
			beforeSend:function(xhr){//设置请求头信息
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",userId);
			},
			header:{'token':token,'userId':userId},
			data:param,
			success:function(data){
				console.log('data='+data);
				if (data.success=='1'){
	                	new $.zui.Messager('已成功解冻该客户！', {
	    				    type: 'success',
	    				    placement:'center'
	    				}).show();
	                	$('#unfreezeModal').modal('hide', 'fit');
	                	queryListFun();
				}else if(data.success=='2'){
					new $.zui.Messager('该用户为正常状态，无需解冻', {
    				    type: 'warning',
    				    placement:'center'
    				}).show();
                	$('#unfreezeModal').modal('hide', 'fit');
                	queryListFun();
				}
                else{
                	new $.zui.Messager(data.msg, {
    				    type: 'warning',
    				    placement:'center'
    				}).show();
				}
			},
			error:function(e){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning',
				    placement:'center'
				}).show();
			}
		});
	});
	
	
	
	//初始化详情
	$("#detailBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		if (selectedItems.length == 0) {
			new $.zui.Messager('请选择要查看的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else if (selectedItems.length > 1) {
			new $.zui.Messager('请只选择一条要查看的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else {
			$("#detailForm")[0].reset();
			var url = adminQueryUrl + clientService + detailMethod;
			var param = 'clientID='+selectedItems[0].clientID;//请求到列表页面
			console.log('param='+param);
			$.ajax({
				url:url,
				type:'post',
				dataType:'JSON',
				beforeSend:function(xhr){//设置请求头信息
					xhr.setRequestHeader("token",token);
					xhr.setRequestHeader("userId",userId);
				},
				header:{'token':token,'userId':userId},
				data:param,
				success:function(data){
					console.log('data='+data);
					if (data.success=='1'){
						$('#detailModal').modal('show', 'fit');
						$("#detailClientCode").val(data.clientMap.clientCode);
						$("#detailClientName").val(data.clientMap.clientName);
						$("#detailLoginName").val(data.clientMap.loginName);
						$("#detailLoginPwd").val(data.clientMap.loginPwd);
						$("#detailPaymentPwd").val(data.clientMap.paymentPwd);
						$("#detailMobile").val(data.clientMap.mobile);
						$("#detailPwdQuestion").html(data.clientMap.pwdQuestion);
						$("#detailPwdAnswer").html(data.clientMap.pwdAnswer);
						$("#detailclientStatus").val(data.clientMap.clientStatus);
						$("#HeadPicPath").empty();
						$("#HeadPicPath").append("<img data-toggle='lightbox' src='"+$("#contextPath").val()+"/images/"+data.clientMap.headPicPath+"' data-image='large-image.jpg' data-caption='小图看大图' class='img-thumbnail' alt='' width='200'>");

					}
				},
				error:function(e){
					new $.zui.Messager('系统繁忙,请稍候再试!', {
					    type: 'warning',
					    placement:'center'
					}).show();
				}
			});
		}
	});
});