$(document).ready(function(){
	var currentPage = 1;
	var pageSize = 10;
	var token = $.zui.store.get("token");
	var userId = $.zui.store.get("userId");
	
	var queryListFun = function(){
		//获取列表数据
		var queryListUrl = adminQueryUrl + devFactoryService + queryPageListMethod;
		
		var param = "";
		//搜索参数
		if ($("#queryFactoryNameLike").val()!=''){
			param += "DevFactoryNameLike="+$("#queryFactoryNameLike").val();
		}
		if (currentPage>0){
			param += "&currentPage="+(currentPage-1);
		}
		param += "&pageSize="+pageSize;
		param += "&currentPage="+(currentPage-1);
		
		console.log('queryListUrl='+queryListUrl);
		console.log('token='+token+",userId="+userId);
		console.log('param='+param);
		$.ajax({
			url:queryListUrl,
			type:'post',
			beforeSend:function(xhr){
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",userId);
			},
			header:{"token":token,"userId":userId},
			data:param,
			dataType:"JSON",
			success:function(result){
				console.log(result.success);
				console.log(result.pageList);
				if (result.success== '1'){
					
					$('#listDataGrid').empty();
					$('#listDataGrid').data('zui.datagrid',null);
					$('#listPager').empty();
					$('#listPager').data('zui.pager',null);
					
					$('#listDataGrid').datagrid({
						checkable: true,
					    checkByClickRow: true,
					    selectable: true,
					    dataSource: {
					        cols:[
					        		{name: 'DevFactory_ID', label: '主键', width: 0},
					        	 	{name: 'DevFactory_Code', label: '厂商编码', width: 150},
						            {name: 'DevFactoryName', label: '厂商名称', width: 134},
						            {name: 'ContactName', label: '联系人', width: 109},
						            {name: 'ContactTel', label: '联系电话', width: 200},
						            {name: 'Opr_Date', label: '操作时间', width: -1}
					        ],
					        cache:false,
					        array:result.pageList
					    }
					 
					});	
					$('#listDataGrid').data('zui.datagrid').sortBy('SortNo', 'asc');
					
					// 手动进行初始化
					$('#listPager').pager({
					    page: currentPage,
					    recPerPage:pageSize,
					    elements:['first_icon', 'prev_icon', 'pages', 'next_icon', 'last_icon', 'total_text','size_menu','goto'],
					    pageSizeOptions:[5,10,20,30,50,100],
					    recTotal: result.pageTotal
					});
				}
				else {
					new $.zui.Messager(result.msg, {
					    type: 'warning', // 定义颜色主题
					    placement: 'center' // 定义显示位置
					}).show();
				}
			},
			error:function(result){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning', // 定义颜色主题
				    placement: 'center' // 定义显示位置
				}).show();
			}
		});
	};
	//调起查询数据
	queryListFun();
	//查询
	$("#searchBtn").click(function(){
		queryListFun();
	});
	//监听分页，修改当前页，条数
	$('#listPager').on('onPageChange', function(e, state, oldState) {
		currentPage = state.page;
		pageSize = state.recPerPage;
		queryListFun();
	});
	
	
	$("#addSaveBtn").click(function(){
			var validDevFactoryNameUrl = adminQueryUrl + devFactoryService + validDevFactory ;
			$("#addForm").validate({
				rules:{
					addDevFactoryName:{
						"required":true,
						"remote":{
							url:validDevFactoryNameUrl,
							type:'post',
							beforeSend:function(xhr){//设置请求头信息
								xhr.setRequestHeader("token",token);
								xhr.setRequestHeader("userId",userId);
							},
							header:{'token':token,'userId':userId},
							dataType:'json',
							data:{
								'addDevFactoryName':function(){
									return $("#addDevFactoryName").val();
								}
							}
						}
					},
					addContactName:{
						"required":true
					},
					addContactTel:{
						"required":true
					},
					addSortNo:{
						"required":true
					}
					
				},
				messages:{
					addDevFactoryName:{
						"required":"请输入设备厂商名称！",
						"remote":"该设备厂商名称已经存在！"
					},
					addContactName:{
						"required":"请输入联系人"
					},
					addContactTel:{
						"required":"请输入联系人电话"
					},
					addSortNo:{
						"required":"请输入排序值"
					}
				},
				submitHandler:function(form){
					console.log("xx:"+adminQueryUrl +  devFactoryService + addMethod);
					
					$.ajax({
						url:adminQueryUrl + devFactoryService + addMethod ,
						type:'post',
						dataType:'JSON',
						beforeSend:function(xhr){//设置请求头信息
							xhr.setRequestHeader("token",token);
							xhr.setRequestHeader("userId",userId);
						},
						header:{'token':token,'userId':userId},
						data:$(form).serialize(),
						success:function(data){
							console.log('添加data='+data);
							if (data.success == '1') {
			                	new $.zui.Messager('添加成功!', {
			    				    type: 'success',
			    				    placement:'center'
			    				}).show();
			                	queryListFun();
			                	$('#addModal').modal('hide', 'fit');
			                }
			                else{
			                	new $.zui.Messager(data.msg, {
			    				    type: 'warning',
			    				    placement:'center'
			    				}).show();
			                }
						},
						error:function(e){
							new $.zui.Messager('系统繁忙,请稍候再试!', {
		    				    type: 'warning',
		    				    placement:'center'
		    				}).show();
						}
					});
				}
			});	
	    });
	
	$("#updateBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		if (selectedItems.length == 0) {
			new $.zui.Messager('请选择要修改的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else if (selectedItems.length > 1) {
			new $.zui.Messager('请只选择一条要修改的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else {
			$("#updateForm")[0].reset();
			var url = adminQueryUrl + devFactoryService +initUpdateMethod;
			var param = 'DevFactoryId='+selectedItems[0].DevFactory_ID;//请求到列表页面
			console.log('param='+param);
			$.ajax({
				url:url,
				type:'post',
				dataType:'JSON',
				beforeSend:function(xhr){//设置请求头信息
					xhr.setRequestHeader("token",token);
					xhr.setRequestHeader("userId",userId);
				},
				headers:{'token':token,'userId':userId},
				data:param,
				success:function(data){
					console.log('初始化修改data='+data);
					if (data.success=='1'){
						$("#updateDevFactory_Code").val(data.devFactoryMap.DevFactory_Code);
						$.each(data.dicsList, function(i, item){ 
							if(item.key_value == data.devFactoryMap.Status)
							{
								$("#updateStatus").html(item.key_name);
							}
							
						}); 
						$("#updateDevFactoryName").val(data.devFactoryMap.DevFactoryName);
						$("#updateWebURL").val(data.devFactoryMap.WebURL);
						$("#updateContactName").val(data.devFactoryMap.ContactName);
						$("#updateContactTel").val(data.devFactoryMap.ContactTel);
						$("#updateAddress").val(data.devFactoryMap.Address);
						$("#updateRemark").val(data.devFactoryMap.Remark);
						$("#updateSortNo").val(data.devFactoryMap.SortNo);
					}
				},
				error:function(e){
					new $.zui.Messager('系统繁忙,请稍候再试!', {
					    type: 'warning',
					    placement:'center'
					}).show();
				}
			});
		}
	});
	//修改保存
	$("#updateSaveBtn").click(function(){
		var excludeDevFactoryId = $('#listDataGrid').data('zui.datagrid').getCheckItems()[0].DevFactory_ID;
		console.log("修改保存的ID"+excludeDevFactoryId);
		var validDevFactoryNameUrl = adminQueryUrl + devFactoryService + validDevFactory;
		$("#updateForm").validate({
			rules:{
				updateDevFactoryName:{
					"required":true,
					"remote":{
						url:validDevFactoryNameUrl,
						type:'post',
						beforeSend:function(xhr){//设置请求头信息
							xhr.setRequestHeader("token",token);
							xhr.setRequestHeader("userId",userId);
						},
						header:{'token':token,'userId':userId},
						dataType:'json',
						data:{
							'updateDevFactoryName':function(){
								return $("#updateDevFactoryName").val();
							},
							'excludeDevFactoryId':function(){
								return excludeDevFactoryId;
							}
						}
					}
				},
				updateContactName:{
					"required":true
				},
				updateContactTel:{
					"required":true
				},
				updateSortNo:{
					"required":true
				}
				
			},
			messages:{
				updateDevFactoryName:{
					"required":"请输入设备厂商名称！",
					"remote":"该设备厂商名称已经存在！"
				},
				updateContactName:{
					"required":"请输入联系人"
				},
				updateContactTel:{
					"required":"请输入联系人电话"
				},
				updateSortNo:{
					"required":"请输入排序值"
				}
			},
			submitHandler:function(form){
				var saveUrl = adminQueryUrl + devFactoryService + updateMethod;
				$.ajax({
					url:saveUrl,
					type:'post',
					dataType:'JSON',
					beforeSend:function(xhr){//设置请求头信息
						xhr.setRequestHeader("token",token);
						xhr.setRequestHeader("userId",userId);
					},
					header:{'token':token,'userId':userId},
					data:$(form).serialize()+"&excludeDevFactoryId="+excludeDevFactoryId,
					success:function(data){
						console.log('data='+data);
						if (data.success == '1') {
		                	new $.zui.Messager('修改成功!', {
		    				    type: 'success',
		    				    placement:'center'
		    				}).show();
		                	queryListFun();
		                	$('#updateModal').modal('hide', 'fit');
		                	queryListFun();
		                }
		                else{
		                	new $.zui.Messager(data.msg, {
		    				    type: 'warning',
		    				    placement:'center'
		    				}).show();
		                }
					},
					error:function(e){
						new $.zui.Messager('系统繁忙,请稍候再试!', {
	    				    type: 'warning',
	    				    placement:'center'
	    				}).show();
					}
				});
			}
		});
	});
	//显示删除确认框
	$("#deleteBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		if (selectedItems.length == 0) {
			new $.zui.Messager('请选择要删除的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else if (selectedItems.length > 1) {
			new $.zui.Messager('请只选择一条要删除的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else {
			$('#deleteModal').modal('show', 'fit');
		}
	});
	//删除菜单
	$("#deleteDevFactoryBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		var url = adminQueryUrl + devFactoryService + deleteMethod;
		var param = 'DevFactory_ID='+selectedItems[0].DevFactory_ID;//请求到列表页面
		console.log('url='+url);
		console.log('param2='+param);
		$.ajax({
			url:url,
			type:'post',
			dataType:'JSON',
			beforeSend:function(xhr){//设置请求头信息
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",userId);
			},
			header:{'token':token,'userId':userId},
			data:param,
			success:function(data){
				console.log('data='+data);
				$('#deleteModal').modal('hide', 'fit');
				if (data.success=='1'){
	                	new $.zui.Messager('删除成功!', {
	    				    type: 'success',
	    				    placement:'center'
	    				}).show();
	                	queryListFun();
				}
                else{
                	new $.zui.Messager(data.msg, {
    				    type: 'warning',
    				    placement:'center'
    				}).show();
				}
			},
			error:function(e){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning',
				    placement:'center'
				}).show();
			}
		});
	});
	//初始化详情
	$("#detailBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		if (selectedItems.length == 0) {
			new $.zui.Messager('请选择要查看的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else if (selectedItems.length > 1) {
			new $.zui.Messager('请只选择一条要查看的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else {
			$("#detailForm")[0].reset();
			var url = adminQueryUrl + devFactoryService + detailMethod;
			var param = 'DevFactoryId='+selectedItems[0].DevFactory_ID;//请求到列表页面
			console.log('详情param='+param);
			$.ajax({
				url:url,
				type:'post',
				dataType:'JSON',
				beforeSend:function(xhr){//设置请求头信息
					xhr.setRequestHeader("token",token);
					xhr.setRequestHeader("userId",userId);
				},
				header:{'token':token,'userId':userId},
				data:param,
				success:function(data){
					console.log('详情data='+data);
					if (data.success=='1'){
						$("#detailDevFactory_Code").val(data.devFactoryMap.DevFactory_Code);
						$.each(data.dicsList, function(i, item){ 
							if(item.key_value == data.devFactoryMap.Status)
							{
								$("#detailStatus").html(item.key_name);
							}
							
						}); 
						$("#detailDevFactoryName").val(data.devFactoryMap.DevFactoryName);
						$("#detailWebURL").val(data.devFactoryMap.WebURL);
						$("#detailContactName").val(data.devFactoryMap.ContactName);
						$("#detailContactTel").val(data.devFactoryMap.ContactTel);
						$("#detailAddress").val(data.devFactoryMap.Address);
						$("#detailRemark").val(data.devFactoryMap.Remark);
						$("#detailSortNo").val(data.devFactoryMap.SortNo);
						$("#Opr_id").val(data.devFactoryMap.Opr_id);
						$("#Opr_Date").val(data.devFactoryMap.Opr_Date);
						
					}
				},
				error:function(e){
					new $.zui.Messager('系统繁忙,请稍候再试!', {
					    type: 'warning',
					    placement:'center'
					}).show();
				}
			});
		}
	});
});