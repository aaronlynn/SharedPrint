$(document).ready(function(){
	var currentPage = 1;
	var pageSize = 10;
	var token = $.zui.store.get("token");
	var userId = $.zui.store.get("userId");
	var queryListFun = function(){
		//获取列表数据
		var queryListUrl = adminQueryUrl + StamerService + queryPageListMethod;
		var param = "";
		if (currentPage>0){
			param += "&currentPage="+(currentPage-1);
		}
		param += "&pageSize="+pageSize;
		param += "&currentPage="+(currentPage-1);
		console.log('queryListUrl='+queryListUrl);
		console.log('token='+token+",userId="+userId);
		console.log('param='+param);
		$.ajax({
			url:queryListUrl,
			type:'post',
			beforeSend:function(xhr){
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",userId);
			},
			header:{"token":token,"userId":userId},
			data:param,
			dataType:"JSON",
			success:function(result){
				console.log(result.success);
				console.log(result.pageList);
				if (result.success== '1'){
					var data = {
						    // labels 数据包含依次在X轴上显示的文本标签
						    labels: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
						    datasets: [{
						        label: "新增商户",
						        color: "red",
						        data: result.monthCount
						    }]
						};	
					var options = {}; // 图表配置项，可以留空来使用默认的配置
					var myLineChart = $("#myLineChart").lineChart(data, options);
				}
			},
		});
		//获取列表数据
		var queryListUrl = adminQueryUrl + StaperService + queryPageListMethod;
		var param = "";
		param += "&pageSize="+pageSize;
		param += "&currentPage="+(currentPage-1);
		
		console.log('queryListUrl='+queryListUrl);
		console.log('token='+token+",userId="+userId);
		console.log('param='+param);
		$.ajax({
			url:queryListUrl,
			type:'post',
			beforeSend:function(xhr){
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",userId);
			},
			header:{"token":token,"userId":userId},
			data:param,
			dataType:"JSON",
			success:function(result){
				console.log(result.success);
				console.log(result.pageList);
				if (result.success== '1'){
					var data = {
						    // labels 数据包含依次在X轴上显示的文本标签
						    labels: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
						    datasets: [{
						        label: "新增商户",
						        color: "red",
						        data: result.monthCount
						    }]
						};	
					var options = {}; // 图表配置项，可以留空来使用默认的配置
					var myLineChart = $("#myBarChart").lineChart(data, options);
				}
			}
		});
	};
	//调起查询数据
	queryListFun();
});