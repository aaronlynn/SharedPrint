$(document).ready(function(){
	var token = $.zui.store.get("token");
	var userId = $.zui.store.get("userId");
	var currentPage = 1;
	var pageSize = 10;
	console.log('token='+token+",userId="+userId);
	var queryListFun = function(){
		var queryMerUserListUrl = adminQueryUrl + merUserService + queryPageListMethod;
		var param = "";	
		if($("#queryName").val()!=''){
			param += "&queryName="+$("#queryName").val();
		}
		if($("#queryMerUserStatus").val()!=''){
			param += "&queryMerUserStatus="+$("#queryMerUserStatus").val();
		}
		if($("#queryMerInfNameLike").val()!=''){
			param += "&queryMerInfNameLike="+$("#queryMerInfNameLike").val();
		}
		param += "&pageSize="+pageSize;
		param += "&currentPage="+(currentPage-1);
		console.log($("#queryOrgNameLike").val());
		$.ajax({
			url:queryMerUserListUrl,
			type:'post',
			beforeSend:function(xhr){
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",userId);
			},
			header:{"token":token,"userId":userId},
			data:param,
			dataType:"JSON",
			success:function(result){
				console.log(result.success);
				if(result.success=='1'){
					//$('#listPager').empty();
					//$('#listPager').data('zui.datagrid',null);
					$('#listDataGrid').empty();
					$('#listDataGrid').data('zui.datagrid',null);
					
					console.log(result);
					$('#listDataGrid').datagrid({
						checkable:true,
						checkByClickRow:true,
					    dataSource: {
					        cols:[
					              {name: 'LoginName', label: '登录名', width: 132},
					              {name: 'MerUserName', label: '用户名称', width: 134},
					              {name: 'merInfName', label: '所属商户', width: 109},
					              {name: 'merUserStatus', label: '用户状态', width: 109},
					              {name: 'oprDate', label: '操作时间', width: 109},
					        ],
					        array:result.listDataGrid
					    }				
					});
					console.log(currentPage);
					console.log(pageSize);
					console.log(result.pageTotal);									
					$('#listPager').pager({
					    page: currentPage,
					    recPerPage:pageSize,
					    elements:['first_icon', 'prev_icon', 'pages', 'next_icon', 'last_icon', 'total_text','size_menu','goto'],
					    pageSizeOptions:[5,10,20,30,50,100],
					    recTotal: result.pageTotal
					});					
				}else {
					new $.zui.Messager(result.msg, {
					    type: 'warning', // 定义颜色主题
					    placement: 'center' // 定义显示位置
					}).show();
				}
			},
			error:function(result){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning', // 定义颜色主题
				    placement: 'center' // 定义显示位置
				}).show();
			}
		});		
	}
	queryListFun();
	$("#searchBtn").click(function(){
		queryListFun();
	});
	
	//显示重置密码确认弹窗
	$("#resetPwdBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		if (selectedItems.length == 0) {
			new $.zui.Messager('请选择要修改的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else if (selectedItems.length > 1) {
			new $.zui.Messager('请只选择一条要修改的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}else{
			$('#resetPwdModal').modal('show', 'fit');
			$('#merUserInfId').val(selectedItems[0].MerUser_ID);
		}
	});
	
	//重置密码
	$("#resetDetermineBtn").click(function(){
		var Url = adminQueryUrl + merUserService + merUserResetPwdMethod;
		var param ='merUserInfId='+$('#merUserInfId').val();
		$.ajax({
			url:Url,
			type:'post',
			dataType:'JSON',
			beforeSend:function(xhr){//设置请求头信息
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",+userId);
			},
			headers:{'token':token,'userId':userId},
			data:param,
			success:function(data){
				console.log('data='+data);
				if (data.success=='1'){
					new $.zui.Messager('重置密码成功!', {
					    type: 'success',
					    placement:'center'
					}).show();		
					queryListFun();
				}else{
					new $.zui.Messager(data.msg, {
					    type: 'warning',
					    placement:'center'
					}).show();
					queryListFun();
				}
			},
			error:function(e){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning',
				    placement:'center'
				}).show();
			}
		});
	});
	
	$("#detailBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		if (selectedItems.length == 0) {
			new $.zui.Messager('请选择要修改的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else if (selectedItems.length > 1) {
			new $.zui.Messager('请只选择一条要修改的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}else{		
			var url = adminQueryUrl + merUserService + detailMethod;
			var param='merUserInfId='+selectedItems[0].MerUser_ID;
			$.ajax({
				url:url,
				type:'post',
				dataType:'JSON',
				beforeSend:function(xhr){//设置请求头信息
					xhr.setRequestHeader("token",token);
					xhr.setRequestHeader("userId",+userId);
				},
				headers:{'token':token,'userId':userId},
				data:param,
				success:function(data){
					console.log('data='+data);
					if (data.success=='1'){
							$('#detailModal').modal('show', 'fit');
							$("#detailMerUserCode").val(data.merUserInfo.meruserCode);
							console.log($("#detailMerUserCode").val());
							$("#detailMerUserName").val(data.merUserInfo.merusername);
							$("#detailMerInfCode").val(data.merInfName);
							$("#detailLoginName").val(data.merUserInfo.loginname);
							$("#detailLoginPwd").val(data.merUserInfo.loginpwd);
							$("#detailStatus").val(data.merUserStatus);
							$("#detailPwdQuestion").val(data.merUserInfo.pwdquestion);
							$("#detailPwdAnswer").val(data.merUserInfo.pwdanswer);
							$("#detailInitPwdFlag").val(data.initPwdFlag);
							$("#detailFirstLoginFlag").val(data.merUserInfo.firstloginflag);
					}else{
						new $.zui.Messager(data.msg, {
						    type: 'warning',
						    placement:'center'
						}).show();
						queryListFun();
					}
				},
				error:function(e){
					new $.zui.Messager('系统繁忙,请稍候再试!', {
					    type: 'warning',
					    placement:'center'
					}).show();
				}
			});
		}			
	});
});