//后台请求地址
var adminQueryUrl = 'http://localhost:8080/SharedPrintService';

// 通用的增删改查
var queryPageListMethod = '/queryPageList.action';
var initAddMethod = '/initAdd.action';
var addMethod = '/add.action';
var initUpdateMethod = '/initUpdate.action';
var updateMethod = '/update.action';
var deleteMethod = '/delete.action';
var detailMethod = '/detail.action';
var resetPwdMethod = '/resetPwd.action';
var checkMethod = '/check.action';

var resetPwdMethod = '/resetPwd.action';

//通用的冻结、解冻
var unFreezeMethod = '/unFreeze.action';
var freezeMethod = '/freeze.action';
var initFreezeMethod = '/initFreeze.action';
var thawAndFreezeMethod = '/thawAndFreezeMethod.action'

//获取角色服务
var roleService = '/api/roleInfo';
var RoleMenu = '/queryRoleMenu.action';
var validRole = '/validateRoleName.action';
//获取设备厂商服务
var devFactoryService = '/api/devFactoryInfo';
var validDevFactory = '/validateDevFactoryName.action';
//获取设备型号服务
var devModelService =  '/api/devModelInfo';
var validDevModel = '/validateDevModelName.action';
//获取设备信息服务
var devInfoService =  '/api/devInfo';
var validDevInfo = '/validateDevInfoName.action';
// 获取用户方法
var adminUserService = '/api/userInfo';
var loginMethod = '/login.action';
var queryUserList = '/queryUserList';


// 获取用户管理
var initUserAddMethod = '/initAdd.action'
var validLoginName = '/validLoginName.action'
var resetMethod = '/resetPwd.action'

// 获取商品类别
var categoryService = "/api/categoryInfo"
var queryCategory = "/queryCategory.action"
var deleteCategoryMethod = "/deleteCategory.action"
var detailCategoryMethod = "/detailCategory.action"

// 获取商品品牌
var brandService = "/api/brandInfo"
var queryBrand = "/queryBrand.action"
var deleteBrandMethod = "/deleteBrand.action"
var detailBrandMethod = "/detailBrand.action"

// 获取商品信息
var goodsService = "/api/info"
var queryInfo = "/queryInfo.action"
var deleteInfoMethod = "/checkInfo.action"
var detailInfoMethod = "/detailInfo.action"
var checkInfoMethod = "/checkInfo.action"
var upcheckMethod="/upcheckInfo.action"

// 获取商品类别
var categoryService = "/api/categoryInfo"
var queryCategory = "/queryCategory.action"
var deleteCategoryMethod = "/deleteCategory.action"
var detailCategoryMethod = "/detailCategory.action"

// 获取地区服务
var areaService = '/api/areaInfo';
var validAreaCode = '/validateAreaCode.action';
var validAreaName = '/validateAreaName.action';
var queryArea = '/queryAreaList.action';
var addArea = '/saveAddArea.action';
var saveAreaUpdate = '/saveAreaUpdate.action';
var detailArea = '/detailArea.action';

// 获取机构管理
var orgService = '/api/orgInfo';
var validOrgCode = '/validateOrgCode.action';
var validOrgName = '/validateOrgName.action';
var queryOrgList = '/getAllOrgList.action';
// 获取菜单服务
var menuService = '/api/menuInfo';
var queryLeftMenuMethod = '/queryLeftMenuList.action';
var queryMenu = '/queryMenuList.action';
var validateMenu = '/validateMenu.action';
var addmenu = '/saveAddMenu.action';
var initUpdate = '/initUpdate.action';
var saveUpdate = '/saveUpdate.action';
var delMenu = '/delMenu.action';
var detailMenu = '/detailMenu.action';

// 获取客户服务
var clientService = '/api/clientInfo';
var queryClient = '/queryClientList.action';
var resetPwd = '/resetPwd.action';
var freeze = '/freeze.action';
var unfreeze = '/unfreeze.action';
var detailClient = '/detailClient.action';

// 获取客户积分服务
var integralService = '/api/integral';
var queryIntegral = '/queryIntegralList.action';

// 获取客户积分明细服务
var intdetailService = '/api/intdetail';
var queryintdetail = '/queryintdetailList.action';

// 获取统计服务
var StamerService = '/api/Stamer';//商户
var queryStamer = '/queryStamerList.action';
var StaperService = '/api/Staper';//客户
var queryStaper = '/queryStaperList.action';
var StaordService = '/api/Staord';//订单
var queryStaord = '/queryStaordList.action';

//获取订单服务
var orderService = '/api/orderInfo';
var queryOrder = '/queryOrderList.action';

// 获取代理商用户服务
var agentService = '/api/agentInfo';
var validAgentName = '/validAgentName.action';
var checkAgent = '/checkAgent.action';

// 获取代理商用户服务
var agentUserService = '/api/agentUserInfo';
// 获取数据字典服务
var dicsService = '/api/dics';
var dicsMethod = '/getDicsList.action';
// 获取文件服务
var fileService = '/FileServlet';
var uploadMethod = '?action=addUserHeadPic'

// 获取商户服务
var merService = '/api/merInfo';
var validMerCode = '/validateMerCode.action';
var validMerName = '/validateMerName.action';

//获取商户用户服务
var merUserService = '/api/merUserInfo';
var merUserResetPwdMethod = '/resetPwd.action';

//获取商户设备服务
var merDevService = '/api/merDevInfo';