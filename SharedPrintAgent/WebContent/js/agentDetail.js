$(document).ready(function(){

	var token = $.zui.store.get("token");
	var userId = $.zui.store.get("userId");
	var loginName = $.zui.store.get("LoginName");
	$("#detailForm")[0].reset();
	var url = adminQueryUrl + agentService + agentDetailMethod;
	var param = 'loginName=' + loginName;//请求到列表页面
	console.log('param='+param);
	$.ajax({
		url:url,
		type:'post',
		dataType:'JSON',
		beforeSend:function(xhr){//设置请求头信息
			xhr.setRequestHeader("token",token);
			xhr.setRequestHeader("userId",userId);
		},
		header:{'token':token,'userId':userId},
		data:param,
		success:function(data){
			console.log('data='+data);
			if (data.success=='1'){
			$('#detailModal').modal('show', 'fit');
			$("#detailAgeInfCode").val(data.pageMap.AgeInf_Code);
			$("#detailAgeInfName").val(data.pageMap.AgeInfName);
			$("#detailIDCode").val(data.pageMap.IDCode);
			$("#detailEmail").val(data.pageMap.Email);
			$("#detailAlias").val(data.pageMap.Alias);
			$("#detailCliMgrCode").val(data.pageMap.CliMgrCode);
			$("#detailContactor").val(data.pageMap.Contactor);
			$("#detailContactTel").val(data.pageMap.ContactTel);
			$("#detailRemark").val(data.pageMap.Remark);
			$("#detailBusiAddress").val(data.pageMap.BusiAddress);
			$("#detailAgentId").val(data.pageMap.AgeInf_ID);
			
			$("#detailAgeInfType").val(data.pageMap.AgeInfType);
			$("#detailIDType").val(data.pageMap.IDType);
			$("#detailAgeInfLevel").val(data.pageMap.AgeInfLevel);
			$("#detailOrg_Name").val(data.pageMap.Org_Name);
			$("#detailArea_Name").val(data.pageMap.Area_Name);
			
			var Material = data.pageMap.Material;
			$("#detailShowMaterial").empty();
			$("#detailShowMaterialBtn").css("display","none");
			$("#detailShowMaterialName").css("display","none");
			$("#detailShowMaterialName").empty();
			if (Material!=null && Material!='' && typeof(Material)!='undefined'){
				$("#detailShowMaterial").load(adminQueryUrl + "/"+data.pageMap.Material);
				$("#detailShowMaterialName").val("      文件名:  "+Material);
				$("#detailShowMaterialName").css("display","block");
				
				
				$("#showMaterial").css("display","block");
			}
			else{
				$("#detailShowMaterialName").val("无附件");
				$("#detailShowMaterial").css("display","none");
				$("#detailShowMaterialName").css("display","block");
				$("#fujian").css("display","none");
				
			}
			
			}
		},
		error:function(e){
			new $.zui.Messager('系统繁忙,请稍候再试!', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
	});
	
});