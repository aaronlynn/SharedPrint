$(document).ready(function (){
	$("#resetPwdModal").modal('show','fit');

	var token = $.zui.store.get("token");
	var userId = $.zui.store.get("userId");
	$("#resetPwdSaveBtn").click(function (){
		var validPwdUrl = adminQueryUrl + agentUserService + validModifyPwdMethod;
		var resetPwdUrl = adminQueryUrl + agentUserService + modifyPwdMethod;
		
		
		$.validator.addMethod("minLength",function(value,element){
			var password = $("#newPwd").val();
			var l = password.length;   
			var blen = 0;   
			for(i=0; i<l; i++) {   
			if ((password.charCodeAt(i) & 0xff00) != 0) {   
			blen ++;   
			}   
			blen ++;   
			} 
			if(blen < 6)
			{
				return false;
			}
			return true;
		},"密码长度至少为6位！");
		
		$.validator.addMethod("isTheSame",function(value,element){
			var newPwd = $("#newPwd").val();
			var validNewPwd = $("#validNewPwd").val();
			
			if(newPwd != validNewPwd)
			{
				return false;
			}
			
			return true;
		},"两次密码输入不一样！");
		
		$.validator.addMethod("isOldAndNewTheSame",function(value,element){
			var newPwd = $("#newPwd").val();
			var oldPwd = $("#oldPwd").val();
			
			if(newPwd == oldPwd)
			{
				return false;
			}
			
			return true;
		},"新密码和旧密码不能一致！");
		
		$("#resetPwdForm").validate({
			rules:{
				oldPwd:{
					"required":true,
					"remote":{
						url:validPwdUrl,
						type:'post',
						beforeSend:function(xhr){//设置请求头信息
							xhr.setRequestHeader("token",token);
							xhr.setRequestHeader("userId",userId);
						},
						header:{'token':token,'userId':userId},
						dataType:'json',
						data:{
							'oldPwd':function(){
								return $("#oldPwd").val();
							}
						}
					},
					
				},
				newPwd:{
					"required":true,
					"minLength":true,
					"isOldAndNewTheSame":true
					 },
				validNewPwd:{
					"required":true,
					"isTheSame":true,
					
				 }
			},
			messages:{
				oldPwd:{
					"required":"请输入旧密码！",
					"remote":"原密码输入错误	！"
				},
				newPwd:{
					"required":"新密码不能为空!"
				},
				validNewPwd:{
					"required":"新密码不能为空!"
				}
				
			},
			errorPlacement:function(error,element){
				error.appendTo(element.parent());
			},
			submitHandler:function(form){
				console.log("resetPwdUrl:"+resetPwdUrl);
				$.ajax({
					url:resetPwdUrl ,
					type:'post',
					dataType:'JSON',
					beforeSend:function(xhr){//设置请求头信息
						xhr.setRequestHeader("token",token);
						xhr.setRequestHeader("userId",userId);
					},
					header:{'token':token,'userId':userId},
					data:$(form).serialize(),
					success:function(data){
						console.log('data='+data);
						if (data.success == '1') {
		                	new $.zui.Messager('密码修改成功!请重新登录！', {
		    				    type: 'success',
		    				    placement:'center'
		    				}).show();
		                	$.zui.store.clear();  
		            		location.href='../login.jsp';
		                	$('#resetPwdModal').modal('hide', 'fit');
		                }
		                else{
		                	new $.zui.Messager(data.msg, {
		    				    type: 'warning',
		    				    placement:'center'
		    				}).show();
		                }
					},
					error:function(e){
						new $.zui.Messager('系统繁忙,请稍候再试!', {
	    				    type: 'warning',
	    				    placement:'center'
	    				}).show();
					}
				});
			}
		});	
	});
});