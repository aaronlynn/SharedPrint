//后台请求地址
var adminQueryUrl = 'http://localhost:8080/SharedPrintService';

// 通用的增删改查
var queryPageListMethod = '/queryPageList.action';
var initAddMethod = '/initAdd.action';
var addMethod = '/add.action';
var initUpdateMethod = '/initUpdate.action';
var updateMethod = '/update.action';
var deleteMethod = '/delete.action';
var detailMethod = '/detail.action';
var resetPwdMethod = '/resetPwd.action';

//获取菜单服务
var menuService = '/api/menuInfo';
//获取代理商系统左侧菜单
var queryAgentLeftMenuList = '/queryAgentLeftMenuList.action';
// 获取用户方法
var agentUserService = '/api/agentUserInfo';
var loginMethod = '/agentLogin.action';

//通用的冻结、解冻
var unFreezeMethod = '/unFreeze.action';
var freezeMethod = '/freeze.action';
var initFreezeMethod = '/initFreeze.action';
var thawAndFreezeMethod = '/thawAndFreezeMethod.action'

//获取代理商用户服务
var agentService = '/api/agentInfo';
var agentDetailMethod = '/agentDetailMethod.action';
var modifyPwdMethod = '/modifyPwd.action';
var validModifyPwdMethod = '/validModifyPwdMethod.action';

//订单查询
var agentOrderService='/api/agentOrderInfo';
var queryOrder='/agentQueryOrderInfo.action';
var querychart='/querychart.action'

// 获取数据字典服务
var dicsService = '/api/dics';
var dicsMethod = '/getDicsList.action';

// 获取文件服务
var fileService = '/FileServlet';
var uploadMethod = '?action=addUserHeadPic';

// 获取商户服务
var merService = '/api/merInfo';
var validMerCode = '/validateMerCode.action';
var validMerName = '/validateMerName.action';

//获取商户用户服务
var merUserService = '/api/merUserInfo';
var merUserResetPwdMethod = '/resetPwd.action';
var checkMethod="/check.action"

//获取商户设备服务
var merDevService = '/api/merDevInfo';

// 获取统计服务
var StamerService = '/api/Stamer';//商户
var StaordService = '/api/Staord';//订单

