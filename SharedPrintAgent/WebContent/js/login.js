$(document).ready(function(){
	//监听登录事件
	$("#loginBtn").click(function(){
		if($("#userNameId").val()==''){
			$('#userNameId').tooltip('show','请输入用户名');
			return;
		}
		if($("#passwdId").val()==''){
			$('#passwdId').tooltip('show','请输入密码');
			return;
		}
		$(this).button('loading');
		var queryUrl = adminQueryUrl+agentUserService + loginMethod;
		var param = "username="+$("#userNameId").val()+"&upwd="+$("#passwdId").val();
		console.log('queryUrl='+queryUrl);
		//发送登录验证请求
		$.get(queryUrl,param,function(data,status){
			console.log('data='+data);
			//将字符串转成JSON对象
			var jsonObj = jQuery.parseJSON(data);
			if (jsonObj.success=='1'){
				//保存服务器返回值到本地中
				$.zui.store.set("LoginName", jsonObj.LoginName);
				$.zui.store.set("userId", jsonObj.userId);
				$.zui.store.set("agentId", jsonObj.agentId);
				$.zui.store.set("token", jsonObj.token);
				$.zui.store.set("trueName", jsonObj.trueName);
				location.href='agent/admin.jsp';
			}
			else {
				new $.zui.Messager('用户名或者密码错', {
				    type: 'warning',
				    placement:'center'
				}).show();
				$("#loginBtn").button('reset');
			}
		},"text");
	});
});