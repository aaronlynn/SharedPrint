$(document).ready(function(){
	var token = $.zui.store.get("token");
	var userId = $.zui.store.get("userId");
	var currentPage = 1;
	var pageSize = 10;
	var loginName=$.zui.store.get("LoginName");
	console.log('token='+token+",userId="+userId);
	var queryListFun = function(){
		var queryMerDevListUrl = adminQueryUrl + merDevService + queryPageListMethod;
		var param = "loginName="+loginName;	
		if($("#queryMerInfo").val()!=''){
			param += "&queryMerInfo="+$("#queryMerInfo").val();
		}
		if($("#queryOrgCode").val()!=''){
			param += "&queryOrgCode="+$("#queryOrgCode").val();
		}
		param += "&pageSize="+pageSize;
		param += "&currentPage="+(currentPage-1);
		console.log($("#queryMerNameLike").val());
		$.ajax({
			url:queryMerDevListUrl,
			type:'post',
			beforeSend:function(xhr){
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",userId);
			},
			header:{"token":token,"userId":userId},
			data:param,
			dataType:"JSON",
			success:function(result){
				console.log(result.success);
				if(result.success=='1'){
					$('#listPager').empty();
					$('#listPager').data('zui.datagrid',null);
					$('#listDataGrid').empty();
					$('#listDataGrid').data('zui.datagrid',null);
					
					console.log(result);
					$('#listDataGrid').datagrid({
						checkable:true,
						checkByClickRow:true,
					    dataSource: {
					        cols:[
					              {name: 'MerDev_Code', label: '设备号', width: 0.1},
					              {name: 'MerDevName', label: '投放名称', width: 0.1},
					              {name: 'merInfName', label: '商户名称', width: 0.1},
					              {name: 'SN', label: '硬件序列号', width: 0.1},
					              {name: 'devWorkStatus', label: '设备状态', width: 0.1},
					              {name: 'merDevStatus', label: '审核状态', width: 0.1},
					              {name: 'oprDate', label: '操作时间', width: 0.1},
					        ],
					        array:result.listDataGrid
					    }				
					});
					console.log(currentPage);
					console.log(pageSize);
					console.log(result.pageTotal);									
					$('#listPager').pager({
					    page: currentPage,
					    recPerPage:pageSize,
					    elements:['first_icon', 'prev_icon', 'pages', 'next_icon', 'last_icon', 'total_text','size_menu','goto'],
					    pageSizeOptions:[5,10,20,30,50,100],
					    recTotal: result.pageTotal
					});					
				}else {
					new $.zui.Messager(result.msg, {
					    type: 'warning', // 定义颜色主题
					    placement: 'center' // 定义显示位置
					}).show();
				}
			},
			error:function(result){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning', // 定义颜色主题
				    placement: 'center' // 定义显示位置
				}).show();
			}
		});		
	}
	queryListFun();
	$("#searchBtn").click(function(){
		queryListFun();
	});	
	//显示删除确认弹窗
	$("#deleteBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		if (selectedItems.length == 0) {
			new $.zui.Messager('请选择要删除的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else if (selectedItems.length > 1) {
			new $.zui.Messager('请只选择一条要删除的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}else{
			$('#deleteModal').modal('show', 'fit');
			$('#merDevInfId').val(selectedItems[0].MerDev_ID);
		}
	});
	
	//删除确认
	$("#deleteDetermineBtn").click(function(){
		var Url = adminQueryUrl + merDevService + deleteMethod;
		var param ='merDevInfId='+$('#merDevInfId').val();
		$.ajax({
			url:Url,
			type:'post',
			dataType:'JSON',
			beforeSend:function(xhr){//设置请求头信息
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",+userId);
			},
			headers:{'token':token,'userId':userId},
			data:param,
			success:function(data){
				console.log('data='+data);
				if (data.success=='1'){
					new $.zui.Messager('删除成功!', {
					    type: 'success',
					    placement:'center'
					}).show();
					$('#deleteModal').modal('hide', 'fit');
					queryListFun();				
				}else{
					new $.zui.Messager(data.msg, {
					    type: 'warning',
					    placement:'center'
					}).show();
					queryListFun();
				}
			},
			error:function(e){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning',
				    placement:'center'
				}).show();
			}
		});
	});
	
	//详情
	$("#detailBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		if (selectedItems.length == 0) {
			new $.zui.Messager('请选择要查询的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else if (selectedItems.length > 1) {
			new $.zui.Messager('请只选择一条要查询的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}else{	
			var url = adminQueryUrl + merDevService + detailMethod;
			var param='merDevInfId='+selectedItems[0].MerDev_ID;
			$.ajax({
				url:url,
				type:'post',
				dataType:'JSON',
				beforeSend:function(xhr){//设置请求头信息
					xhr.setRequestHeader("token",token);
					xhr.setRequestHeader("userId",+userId);
				},
				headers:{'token':token,'userId':userId},
				data:param,
				success:function(data){
					console.log('data='+data);
					if (data.success=='1'){
							$('#detailModal').modal('show', 'fit');
							$("#detailMerDevCode").val(data.merDevInfo.merdevCode);
							$("#detailMerDevName").val(data.merDevInfo.merdevname);	
							$("#detailMerInfCode").val(data.merDevInfo.merinfName);	
							$("#detailAgeInfCode").val(data.ageInfName);	
							$("#detailOrgCode").val(data.merDevInfo.orgName);	
							$("#detailAreaCode").val(data.merDevInfo.areaName);	
							$("#detailMerDevStatus").val(data.merDevStatus);	
							$("#detailDevInfCode").val(data.merDevInfo.devinfCode);	
							$("#detailDevWorkStatus").val(data.devWorkStatus);	
							$("#detailSN").val(data.merDevInfo.sn);	
							$("#detailDevHwVer").val(data.merDevInfo.devhwver);	
							$("#detailDevAppVer").val(data.merDevInfo.devappver);	
							$("#detailDevParamVer").val(data.merDevInfo.devparamver);	
							$("#detailDeliveryAddress").val(data.merDevInfo.deliveryaddress);
							$("#detailLongitude").val(data.merDevInfo.longitude);	
							$("#detailLatitude").val(data.merDevInfo.latitude);	
							$("#detailCrtUserId").val(data.merDevInfo.crtuserid);	
							$("#detailCrtDate").val(data.merDevInfo.crtdate);	
							$("#detailUptUserId").val(data.merDevInfo.uptuserid);	
							$("#detailLastUptDate").val(data.merDevInfo.lastuptdate);	
							$("#detailChkUserId").val(data.merDevInfo.chkuserid);	
							$("#detailChkDate").val(data.merDevInfo.chkdate);
							$("#detailChkRemark").val(data.merDevInfo.chkremark);
					}else{
						new $.zui.Messager(data.msg, {
						    type: 'warning',
						    placement:'center'
						}).show();
						queryListFun();
					}
				},
				error:function(e){
					new $.zui.Messager('系统繁忙,请稍候再试!', {
					    type: 'warning',
					    placement:'center'
					}).show();
				}
			});
		}
	});
});