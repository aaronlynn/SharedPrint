$(document).ready(function(){
	Highcharts.setOptions({
		global: {
			useUTC: false
		}
	});
	function activeLastPointToolip(chart) {
		var points = chart.series[0].points;
		chart.tooltip.refresh(points[points.length -1]);
	}
	var chart = Highcharts.chart('container2', {
		chart: {
			type: 'spline',
			marginRight: 10,
			events: {
				load: function () {
					var series = this.series[0],
						chart = this;
					activeLastPointToolip(chart);
					setInterval(function () {
						var x = (new Date()).getTime(), // 当前时间
							y = Math.random();          // 随机值
						series.addPoint([x, y], true, true);
						activeLastPointToolip(chart);
					}, 1000);
				}
			}
		},
		title: {
			text: '总公司股票行情实时数据'
		},
		xAxis: {
			type: 'datetime',
			tickPixelInterval: 150
		},
		yAxis: {
			title: {
				text: null
			}
		},
		tooltip: {
			formatter: function () {
				return '<b>' + this.series.name + '</b><br/>' +
					Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
					Highcharts.numberFormat(this.y, 2);
			}
		},
		legend: {
			enabled: false
		},
		series: [{
			name: '同比上涨',
			data: (function () {
				// 生成随机值
				var data = [],
					time = (new Date()).getTime(),
					i;
				for (i = -19; i <= 0; i += 1) {
					data.push({
						x: time + i * 1000,
						y: Math.random()
					});
				}
				return data;
			}())
		}]
	});
	
	Highcharts.chart('container1', {
		chart: {
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false,
			type: 'pie'
		},
		title: {
			text: '2019年打印机市场份额'
		},
		tooltip: {
			pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		},
		plotOptions: {
			pie: {
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: false
				},
				showInLegend: true
			}
		},
		series: [{
			name: 'Brands',
			colorByPoint: true,
			data: [{
				name: 'Chrome',
				y: 61.41,
				sliced: true,
				selected: true
			}, {
				name: 'Internet Explorer',
				y: 11.84
			}, {
				name: 'Firefox',
				y: 10.85
			}, {
				name: 'Edge',
				y: 4.67
			}, {
				name: 'Safari',
				y: 4.18
			}, {
				name: 'Other',
				y: 7.05
			}]
		}]
	});
	
	var currentPage = 1;
	var pageSize = 10;
	var token = $.zui.store.get("token");
	var userId = $.zui.store.get("userId");
		// 获取列表数据
		var queryListUrl = adminQueryUrl + agentOrderService + querychart;
		var param = "";
		$.ajax({
			url:queryListUrl,
			type:'post',
			beforeSend:function(xhr){
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",userId);
			},
			header:{"token":token,"userId":userId},
			data:param,
			dataType:"JSON",
			success:function(result){
				console.log(result.success);
				var chart = Highcharts.chart('container', {
					chart: {
						type: 'spline'
					},
					title: {
						text: '今年月订单量与盈利统计'
					},
					subtitle: {
						text: '数据来源: lan'
					},
					xAxis: {
						categories: ['一月', '二月', '三月', '四月', '五月', '六月',
									 '七月', '八月', '九月', '十月', '十一月', '十二月']
					},
					yAxis: {
						title: {
							text: '单、元'
						},
						labels: {
							formatter: function () {
								return this.value + '';
							}
						}
					},
					tooltip: {
						crosshairs: true,
						shared: true
					},
					plotOptions: {
						spline: {
							marker: {
								radius: 4,
								lineColor: '#666666',
								lineWidth: 1
							}
						}
					},
					series: [{
						name: '订单量(单)',
						marker: {
							symbol: 'square'
						},
						data: result.monthCount
					}, {
						name: '营业额（元）',
						marker: {
							symbol: 'diamond'
						},
						data: result.priceCount	
					}]
				});
            },
		});
});
