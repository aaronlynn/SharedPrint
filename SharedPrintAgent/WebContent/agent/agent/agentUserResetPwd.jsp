<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta charset="UTF-8">
<link href="${pageContext.request.contextPath}/css/zui.uploader.css">
<link href="${pageContext.request.contextPath}/css/zui.datagrid.css"
	rel="stylesheet">
<script src="${pageContext.request.contextPath}/js/zui.datagrid.js"></script>
<script src="${pageContext.request.contextPath}/js/zui.uploader.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery.validate.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/localization/messages_zh.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/resetPwd.js?version=1"></script>
</head>

<!-- 修改代理商用户重置密码 -->
<div class="modal fade" id="resetPwdModal">
	<div class="modal-dialog">
		<div class="modal-content">

			<form id="resetPwdForm">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">
							<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
						</button>
						<h4 class="modal-title">登录密码修改</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="input-group">
									<span class="input-group-addon">原密码：</span> <input type="password"
										id="oldPwd" name="oldPwd"
										class="form-control" placeholder="" >
								</div>
							</div>
							
						</div>
						<div style="margin-top: 10px"></div>
						<div class="row">
						  <div class="col-md-12">
								<div class="input-group">
									<span class="input-group-addon">新密码：</span> <input type="password"
										id="newPwd" name="newPwd"
										class="form-control" placeholder="">
								</div>
							</div>
						
						</div>
						<div style="margin-top: 10px"></div>
						<div class="row">
						<div class="col-md-12	">
								<div class="input-group">
									<span class="input-group-addon">确认密码：</span> <input type="password"
										id="validNewPwd"
										name="validNewPwd" class="form-control">
								</div>
							</div>
						</div>
						

					</div>
					<div style="margin-top: 10px;"></div>
					<div class="row">
					<input type="hidden" id="resetPwdAgentId" name="resetPwdAgentId">
						<div class="modal-footer">
							<button type="button" class="btn btn-default "
								data-dismiss="modal">关闭</button>
							<button type="submit" class="btn btn-primary" id="resetPwdSaveBtn">确定</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>