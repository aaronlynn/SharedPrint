<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="${pageContext.request.contextPath}/css/zui.uploader.css">
<link href="${pageContext.request.contextPath}/css/zui.datagrid.css"
	rel="stylesheet">
<script src="${pageContext.request.contextPath}/js/zui.datagrid.js"></script>
<script src="${pageContext.request.contextPath}/js/zui.uploader.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery.validate.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/localization/messages_zh.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/agentDetail.js?version=1"></script>
</head>
<body>
<!-- 详情页面  -->
		<div class="modal-content">

			<form id="detailForm">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">代理商详情</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-4">
								<div class="input-group">
									<span class="input-group-addon">代理商号</span> <input type="text"
										id="detailAgeInfCode" name="detailAgeInfCode"
										class="form-control" placeholder="" readonly="readonly">
								</div>
							</div>
							<div class="col-md-4">
								<div class="input-group">
									<span class="input-group-addon">代理商名称</span> <input type="text"
										id="detailAgeInfName" name="detailAgeInfName"
										class="form-control" placeholder="" readonly="readonly">
								</div>
							</div>
							<div class="col-md-4">
								<div class="input-group">
									<span class="input-group-addon">代理商类型</span> <input type="text"
										readonly="readonly" id="detailAgeInfType"
										name="detailAgeInfType" class="form-control">
								</div>
							</div>
						</div>
						<div style="margin-top: 10px"></div>
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">证件类型</span> <input type="text"
										readonly="readonly" id="detailIDType" name="detailIDType"
										class="form-control">
								</div>
							</div>

							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">证件号</span> <input type="text"
										id="detailIDCode" name="detailIDCode" class="form-control"
										readonly="readonly" placeholder="">
								</div>
							</div>
						</div>
						<div style="margin-top: 10px"></div>
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">邮箱</span> <input type="text"
										id="detailEmail" name="detailEmail" class="form-control"
										readonly="readonly" placeholder="">
								</div>
							</div>
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">代理商等级</span> <input type="text"
										readonly="readonly" id="detailAgeInfLevel"
										name="detailAgeInfLevel" class="form-control">
								</div>
							</div>
						</div>
						<div style="margin-top: 10px"></div>
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">简称</span> <input type="text"
										id="detailAlias" name="detailAlias" class="form-control"
										readonly="readonly" placeholder="">
								</div>
							</div>

							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">所属机构</span> <input type="text"
										readonly="readonly" id="detailOrg_Name" name="detailOrg_Name"
										class="form-control">
								</div>
							</div>
						</div>
						<div style="margin-top: 10px"></div>
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">所属地区</span> <input type="text"
										id="detailArea_Name" name="detailArea_Name"
										class="form-control" readonly="readonly">
								</div>
							</div>

							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">客户经理</span> <input type="text"
										id="detailCliMgrCode" name="detailCliMgrCode"
										readonly="readonly" class="form-control" placeholder="">
								</div>
							</div>
						</div>
						<div style="margin-top: 10px"></div>
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">联系人</span> <input type="text"
										id="detailContactor" name="detailContactor"
										readonly="readonly" class="form-control" placeholder="">
								</div>
							</div>

							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">联系人电话</span> <input type="text"
										id="detailContactTel" name="detailContactTel"
										readonly="readonly" class="form-control" placeholder="">
								</div>
							</div>
						</div>
						<div style="margin-top: 10px"></div>
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">备注</span>
									<textarea rows="5" cols="60" class="form-control"
										id="detailRemark" name="detailRemark" readonly="readonly">1111</textarea>
								</div>
							</div>

							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">经营场所</span>
									<textarea rows="5" cols="60" class="form-control"
										id="detailBusiAddress" name="updateBusiAddress"
										readonly="readonly"></textarea>
								</div>
							</div>
						</div>
						<div style="margin-top: 10px"></div>
						<div class="row">
							<div class="col-md-6">

								<div class="input-group ">
									<span class="input-group-addon"
										style="background-color: #D3D3D3;">附件： </span><input
										type="text" class="form-control" id="detailShowMaterialName"
										name="detailShowMaterialName" readonly="readonly">
								</div>
							</div>
							
						</div>
						<div style="margin-top: 10px;"></div>
						<div class="row">
						    <div class="col-md-12">
								<div class="input-group " id="fujian">
									<span class="input-group-addon"
										style="background-color: #D3D3D3;">附件预览： </span>
									<textarea rows="15" cols="60" class="form-control"
										id="detailShowMaterial" name="detailShowMaterial"
										readonly="readonly"></textarea>

								</div>
							</div>
						</div>

					</div>
					<div style="margin-top: 10px;"></div>
				</div>


			</form>
		</div>
	
</body>
</html>