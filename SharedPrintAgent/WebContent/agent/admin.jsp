<%@ page language="java"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!-- 兼容浏览器 -->
<meta http-equiv="X-UA-Compatiable" content="IE=edge">
<!-- 屏幕自适应 -->
<meta name="viewport" content="width=device-width,initial-scale=1" >
<title>代理商系统</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/zui.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/zui.datagrid.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/zui.uploader.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/admin.css?version=32">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery1.9.1.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.validate.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/zui.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/zui.datagrid.js?ver=12"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/zui.uploader.js"></script>
</head>
<body >
	<div class="container-fluid">
	  <div class="row adminTop">
	   	<div class="col-md-2 adminTopLeft ">
	   		<a href="${pageContext.request.contextPath}/agent/admin.jsp" class="adminTopLeftA" title="回到首页">代理商系统</a>
	   	</div>
	   	<div class="col-md-10 text-center adminTopRight text-white">
   			<div class="container-fluid">
                <ul class="nav navbar-nav">
                    <li><a href="javascript:;" data-toggle="push-menu"><i class="icon icon-bars"></i></a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="javascript:;" data-toggle="dropdown"><i class="icon icon-user"></i> <span id="trueName"></span> <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">修改资料</a></li>
                            <li><a href="#">修改密码</a></li>
                            <li><a href="javascript:;" id="loginOut">退出登录</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
	   	</div>
	  </div>
	  <div id="adminMain2" class="row adminMain">
	   	<div class="col-md-2 " style="background-color:gray;" id="adminMainMenu">
	   		<nav class="menu " data-ride="menu" >
	   		<ul id="leftMenuTree" class="tree tree-menu adminMainTreeMenu" data-ride="tree">
		  	    
		  	</ul>
		</nav>
	   	</div>
	   	<div class="col-md-10" id="mainRight">
           <%@ include file="adminMainCenter.jsp"%>
	   	</div>
	 </div>
	</div>
<input type="hidden" id="contextPath" value="${pageContext.request.contextPath}"/>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/authority.js?version=1"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/config.js?version=1"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/admin.js?version=3"></script>
</body>
</html>