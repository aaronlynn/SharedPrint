<%@ page language="java" import="java.util.*" pageEncoding="utf-8" contentType="text/html; charset=utf-8"%>
<div class="panel">
  <div class="panel-heading">
    查询区域
  </div>
  <div class="panel-body">
    <div class="row">
    	<div class="col-md-4">
    		<div class="input-group">
			  <span class="input-group-addon">商户查询</span>
			  <input type="text" id="queryMerInfo" name="queryMerInfo" class="form-control" placeholder="商户编号或商户名称">
			</div>
    	</div> 
    	<div class="col-md-4">
    		<div class="input-group">
			  <span class="input-group-addon">所属机构</span>
			  <input type="text" id="queryOrgCode" name="queryOrgCode" class="form-control" placeholder="">
			</div>
    	</div><hr>
    	<div class="col-md-4">
    		<button class="btn btn-success" type="button" id="searchBtn"><i class="icon-search"></i>搜索</button>
    	</div>
    </div>
  </div>
</div>
<div class="datagrid">
  <div class="search-box">
    <button class="btn btn-danger " type="button" id="deleteBtn"><i class="icon icon-trash"></i>删除</button>
    	 &nbsp;&nbsp;&nbsp;&nbsp;
    <button class="btn " type="button" id="detailBtn"><i class="icon icon-info"></i>详情</button>
   		 &nbsp;&nbsp;&nbsp;&nbsp;
  </div><hr>
 </div>
<div id="listDataGrid" class="datagrid"></div>
 <div id="listPager" class="pager"  data-ride="pager" ></div>
 <!-- 删除确认 -->
<div class="modal fade" id="deleteModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">删除商户设备</h4>
			</div>
			<div class="modal-body">
				<p>您确定要删除此商户设备信息吗?</p>
			</div>
			<div class="modal-footer">
				<input type="hidden" id="merDevInfId" name="merDevInfId"/>
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary" id="deleteDetermineBtn">确定</button>
			</div>
		</div>
	</div>
</div>
<!-- 商户设备详情 -->
<div class="modal fade" id="detailModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
        <h4 class="modal-title">商户设备详情</h4>
      </div>
      <form id="detailForm" name="detailForm">
       <div class="modal-body">
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">商户设备编号</span>
						  <input type="text" readonly="readonly" class="form-control" id="detailMerDevCode" name="detailMerDevCode" placeholder="商户号">
						</div>	
        			</div> 
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">商户设备名称</span>
						  <input type="text" readonly="readonly" class="form-control" id="detailMerDevName" name="detailMerDevName" placeholder="商户名称">
						</div>	
        			</div>       	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
								<span class="input-group-addon">商户号</span>
								<input type="text" readonly="readonly" id="detailMerInfCode" name="detailMerInfCode" class="form-control">
						</div>	
        			</div>
        			<div class="col-md-6">
        				<div class="input-group">
								<span class="input-group-addon">所属代理商</span> 
								<input type="text" readonly="readonly" id="detailAgeInfCode" name="detailAgeInfCode" class="form-control">
						</div>	
        			</div>          			        			       			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
								<span class="input-group-addon">所属机构</span> 								
								<input type="text" id="detailOrgCode" readonly="readonly" name="detailOrgCode" class="form-control">
						</div>	
        			</div>
	    			<div class="col-md-6">
        				<div class="input-group">
								<span class="input-group-addon">所属地区</span> 								
								<input type="text" readonly="readonly" id="detailAreaCode" name="detailAreaCode" class="form-control">
						</div>	
        			</div>      			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">商户设备状态</span>
						  <input type="text" class="form-control" readonly="readonly" id="detailMerDevStatus" name="detailMerDevStatus">
						</div>	
        			</div> 
        			<div class="col-md-6">
	    				<div class="input-group">
						 <span class="input-group-addon">设备编码</span>
						 <input type="text" readonly="readonly" id="detailDevInfCode" name="detailDevInfCode" class="form-control">
						</div>
	    			</div>       			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">设备工作状态</span>
						  <input type="text" readonly="readonly" class="form-control" id="detailDevWorkStatus" name="detailDevWorkStatus">
						</div>	
        			</div> 
        			<div class="col-md-6">
	    				<div class="input-group">
						 <span class="input-group-addon">硬件序列号</span>
						 <input type="text" readonly="readonly" id="detailSN" name="detailSN" class="form-control">
						</div>
	    			</div>       			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">设备固件版本号</span>
						  <input type="text" readonly="readonly" class="form-control" id="detailDevHwVer" name="detailDevHwVer">
						</div>	
        			</div> 
        			<div class="col-md-6">
	    				<div class="input-group">
						 <span class="input-group-addon">设备应用程序版本号</span>
						 <input type="text" readonly="readonly" id="detailDevAppVer" name="detailDevAppVer" class="form-control">
						</div>
	    			</div>       			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">设备参数数据版本号</span>
						  <input type="text" readonly="readonly" class="form-control" id="detailDevParamVer" name="detailDevParamVer">
						</div>	
        			</div>    
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">设备其他版本号</span>
						  <input type="text" readonly="readonly" class="form-control" id="detailDevOtherVer" name="detailDevOtherVer">
						</div>	
        			</div>    		   	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">投放地址</span>
						  <input type="text" readonly="readonly" class="form-control" id="detailDeliveryAddress" name="detailDeliveryAddress">
						</div>	
        			</div>    
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">经度</span>
						  <input type="text" readonly="readonly" class="form-control" id="detailLongitude" name="detailLongitude">
						</div>	
        			</div>    		   	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">纬度</span>
						  <input type="text" readonly="readonly" class="form-control" id="detailLatitude" name="detailLatitude">
						</div>	
        			</div>    
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">创建人</span>
						  <input type="text" readonly="readonly" class="form-control" id="detailCrtUserId" name="detailCrtUserId">
						</div>	
        			</div>    		   	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">创建时间</span>
						  <input type="text" readonly="readonly" class="form-control" id="detailCrtDate" name="detailCrtDate">
						</div>	
        			</div>    
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">修改人</span>
						  <input type="text" readonly="readonly" class="form-control" id="detailUptUserId" name="detailUptUserId">
						</div>	
        			</div>    		   	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">最后修改日期</span>
						  <input type="text" readonly="readonly" class="form-control" id="detailLastUptDate" name="detailLastUptDate">
						</div>	
        			</div>    
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">审核人</span>
						  <input type="text" readonly="readonly" class="form-control" id="detailChkUserId" name="detailChkUserId">
						</div>	
        			</div>    		   	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">审核日期</span>
						  <input type="text" readonly="readonly" class="form-control" id="detailChkDate" name="detailChkDate">
						</div>	
        			</div>       		   	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-12">
        				<div class="input-group">
						  <span class="input-group-addon">审核备注</span>
						    <textarea  id="detailChkRemark" name="detailChkRemark" readonly="readonly"
				  				cols="60" rows="5"class="form-control"></textarea>
						</div>	
        			</div>        			      	
        	</div>
      </div>
      <input type="hidden" id="detailMerDevId" name="detailMerDevId"/>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
      </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/merdev.js"></script>
