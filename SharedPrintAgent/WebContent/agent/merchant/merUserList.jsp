<%@ page language="java" pageEncoding="UTF-8"%>
<div class="panel">
	<div class="panel-heading">查询用户</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon">登录名或用户名称</span> 
					<input type="text" id="queryName" name="queryName" class="form-control"
						placeholder="">
				</div>
			</div>
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon">用户状态</span> 
					<select id="queryMerUserStatus" name="queryMerUserStatus"class="form-control">
						<option value="">请选择</option>
						<option value="0">未审核</option>
						<option value="1">正常</option>
						<option value="2">冻结</option>
						<option value="3">注销</option>
						<option value="4">临时冻结</option>
					</select>
				</div>
			</div>
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon">商户名称</span> 
					<input type="text" id="queryMerInfNameLike" name="queryMerInfNameLike"
						class="form-control" placeholder="">
				</div>
			</div><hr>
			<div class="col-md-4">
				<button class="btn btn-success" type="button" id="searchBtn">
					<i class="icon-search"></i>搜索
				</button>
			</div>
		</div>
	</div>
</div>
<div class="datagrid">
	<div class="search-box">
		<button class="btn btn-info " type="button" id="resetPwdBtn">
			<i class="icon icon-edit"></i>重置密码</button>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<button class="btn " type="button" id="detailBtn">
			<i class="icon icon-info"></i>详情
		</button>
	</div>
	<div id="listDataGrid" class="datagrid" data-ride="datagrid"></div>
	<div id="listPager" class="pager" data-ride="pager"></div>
</div>

<!-- 重置密码确认 -->
<div class="modal fade" id="resetPwdModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">重置商户用户密码</h4>
			</div>
			<div class="modal-body">
				<p>您确定要重置此用户密码信息吗?</p>
			</div>
			<div class="modal-footer">
				<input type="hidden" id="merUserInfId" name="merUserInfId"/>
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary" id="resetDetermineBtn">确定</button>
			</div>
		</div>
	</div>
</div>
<!-- 详情页面  -->
<div class="modal fade" id="detailModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">商户用户详情</h4>
			</div>
			<form id="detailForm">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">用户号</span> 
								<input type="text" readonly="readonly" class="form-control" id="detailMerUserCode" name="detailMerUserCode">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">用户名称</span>
								<input type="text"class="form-control" readonly="readonly" id="detailMerUserName" name="detailMerUserName">
							</div>
						</div>
					</div><hr>
					<div style="margin-top: 10px;"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">商户号</span>
								<input type="text" class="form-control" readonly="readonly" id="detailMerInfCode" name="detailMerInfCode">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">登录名</span> 
								<input type="text"class="form-control" readonly="readonly" id="detailLoginName" name="detailLoginName">
							</div>
						</div>
					</div><hr>
					<div style="margin-top: 10px;"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">用户登录密码</span> 
								<input type="text" class="form-control" readonly="readonly" id="detailLoginPwd" name="detailLoginPwd">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">用户状态</span> 
								<input type="text" class="form-control" readonly="readonly" id="detailStatus" name="detailStatus">
							</div>
						</div>
					</div><hr>
					<div style="margin-top: 10px;"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">找回登录密码问题</span> 
								<input type="text" class="form-control" readonly="readonly" id="detailPwdQuestion" name="detailPwdQuestion"></span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">问题答案</span> 
								<input type="text" class="form-control" readonly="readonly" id="detailPwdAnswer" name="detailPwdAnswer">
							</div>
						</div><hr>
					</div><hr>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">登录密码状态</span>
								<input type="text" class="form-control" readonly="readonly" id="detailInitPwdFlag" name="detailInitPwdFlag">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">第一次登录标志</span> 
								<input type="text" class="form-control" readonly="readonly" id="detailFirstLoginFlag" name="detailFirstLoginFlag">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default " data-dismiss="modal">关闭</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/meruser.js"></script>