<%@ page language="java" import="java.util.*" pageEncoding="utf-8" contentType="text/html; charset=utf-8"%>
<div class="panel">
  <div class="panel-heading">
    查询区域
  </div>
  <div class="panel-body">
   <div class="row">
    	<div class="col-md-4">
    		<div class="input-group">
			  <span class="input-group-addon">商户查询</span>
			  <input type="text" id="queryMerInfo" name="queryMerInfo" class="form-control" placeholder="商户编号或商户名称">
			</div>
    	</div> 
    	<div class="col-md-4">
    		<div class="input-group">
			  <span class="input-group-addon">所属代理商</span>
			  <input type="text" id="queryAgeInfCode" name="queryAgeInfCode" class="form-control" placeholder="">
			</div>
    	</div>
    	<div class="col-md-4">
    		<div class="input-group">
			  <span class="input-group-addon">所属机构</span>
			  <input type="text" id="queryOrgCode" name="queryOrgCode" class="form-control" placeholder="">
			</div>
    	</div><hr>
    	<div class="col-md-4">
    		<button class="btn btn-success" type="button" id="searchBtn"><i class="icon-search"></i>搜索</button>
    	</div>
    </div>
  </div>
</div>
<div class="datagrid">
  <div class="search-box">
   	<button class="btn btn-warning " type="button" id="checkBtn"><i class="icon icon-check-circle"></i>审核</button>    
  </div><hr>
 </div>
<div id="listDataGrid" class="datagrid"></div>
 <div id="listPager" class="pager"  data-ride="pager" ></div>
<!-- 商户设备审核 -->
<div class="modal fade" id="checkModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
        <h4 class="modal-title">商户设备详情</h4>
      </div>
      <form id="checkForm" name="checkForm">
       <div class="modal-body">
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">商户设备编号</span>
						  <input type="text" readonly="readonly" class="form-control" id="checkMerDevCode" name="checkMerDevCode" placeholder="商户号">
						</div>	
        			</div> 
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">商户设备名称</span>
						  <input type="text" readonly="readonly" class="form-control" id="checkMerDevName" name="checkMerDevName" placeholder="商户名称">
						</div>	
        			</div>       	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
								<span class="input-group-addon">商户号</span>
								<input type="text" readonly="readonly" id="checkMerInfCode" name="checkMerInfCode" class="form-control">
						</div>	
        			</div>
        			<div class="col-md-6">
        				<div class="input-group">
								<span class="input-group-addon">所属代理商</span> 
								<input type="text" readonly="readonly" id="checkAgeInfCode" name="checkAgeInfCode" class="form-control">
						</div>	
        			</div>          			        			       			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
								<span class="input-group-addon">所属机构</span> 								
								<input type="text" id="checkOrgCode" readonly="readonly" name="checkOrgCode" class="form-control">
						</div>	
        			</div>
	    			<div class="col-md-6">
        				<div class="input-group">
								<span class="input-group-addon">所属地区</span> 								
								<input type="text" readonly="readonly" id="checkAreaCode" name="checkAreaCode" class="form-control">
						</div>	
        			</div>      			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">商户设备状态</span>
						  <input type="text" class="form-control" readonly="readonly" id="checkMerDevStatus" name="checkMerDevStatus">
						</div>	
        			</div> 
        			<div class="col-md-6">
	    				<div class="input-group">
						 <span class="input-group-addon">设备编码</span>
						 <input type="text" readonly="readonly" id="checkDevInfCode" name="checkDevInfCode" class="form-control">
						</div>
	    			</div>       			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">设备工作状态</span>
						  <input type="text" readonly="readonly" class="form-control" id="checkDevWorkStatus" name="checkDevWorkStatus">
						</div>	
        			</div> 
        			<div class="col-md-6">
	    				<div class="input-group">
						 <span class="input-group-addon">硬件序列号</span>
						 <input type="text" id="checkSN" name="checkSN" class="form-control">
						</div>
	    			</div>       			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">设备固件版本号</span>
						  <input type="text" readonly="readonly" class="form-control" id="checkDevHwVer" name="checkDevHwVer">
						</div>	
        			</div> 
        			<div class="col-md-6">
	    				<div class="input-group">
						 <span class="input-group-addon">设备应用程序版本号</span>
						 <input type="text" readonly="readonly" id="checkDevAppVer" name="checkDevAppVer" class="form-control">
						</div>
	    			</div>       			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">设备参数数据版本号</span>
						  <input type="text" readonly="readonly" class="form-control" id="checkDevParamVer" name="checkDevParamVer">
						</div>	
        			</div>    
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">设备其他版本号</span>
						  <input type="text" readonly="readonly" class="form-control" id="checkDevOtherVer" name="checkDevOtherVer">
						</div>	
        			</div>    		   	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">投放地址</span>
						  <input type="text" readonly="readonly" class="form-control" id="checkDeliveryAddress" name="checkDeliveryAddress">
						</div>	
        			</div>    
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">经度</span>
						  <input type="text" readonly="readonly" class="form-control" id="checkLongitude" name="checkLongitude">
						</div>	
        			</div>    		   	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">纬度</span>
						  <input type="text" readonly="readonly" class="form-control" id="checkLatitude" name="checkLatitude">
						</div>	
        			</div>    
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">创建人</span>
						  <input type="text" readonly="readonly" class="form-control" id="checkCrtUserId" name="checkCrtUserId">
						</div>	
        			</div>    		   	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">创建时间</span>
						  <input type="text" readonly="readonly" class="form-control" id="checkCrtDate" name="checkCrtDate">
						</div>	
        			</div>    
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">修改人</span>
						  <input type="text" readonly="readonly" class="form-control" id="checkUptUserId" name="checkUptUserId">
						</div>	
        			</div>    		   	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">最后修改日期</span>
						  <input type="text" readonly="readonly" class="form-control" id="checkLastUptDate" name="checkLastUptDate">
						</div>	
        			</div>    
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">审核人</span>
						  <input type="text" readonly="readonly" class="form-control" id="checkChkUserId" name="checkChkUserId">
						</div>	
        			</div>    		   	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">审核日期</span>
						  <input type="text" readonly="readonly" class="form-control" id="checkChkDate" name="checkChkDate">
						</div>	
        			</div>       		   	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-12">
        				<div class="input-group">
						  <span class="input-group-addon">审核备注</span>
						    <textarea  id="checkChkRemark" name="checkChkRemark"
				  				cols="60" rows="5"class="form-control"></textarea>
						</div>	
        			</div>        			      	
        	</div>
      </div>
      <input type="hidden" id="checkMerDevId" name="checkMerDevId"/>
      <div class="modal-footer">
      	<input type="hidden" id="checkResult" name="checkResult"/>
        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
        <button type="submit" class="btn btn-success"  id="checkPassBtn" name="checkPassBtn">审核通过</button>
        <button type="submit" class="btn btn-danger"  id="checkNotPassBtn" name="checkNotPassBtn">审核不通过</button>  
      </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/merdevcheck.js"></script>
