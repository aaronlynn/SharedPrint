$(document).ready(function() {
	function change() {
		//alert("userId="+userId);
//		var token = 2;
//		var userId = 1;
		//		console.log("userId="+userId);
		//		$.zui.store.set("userNamem", "1");
		//		$.zui.store.set("userIdm", "1");
		//		var token = $.zui.store.get('token');
		var userId =$.zui.store.get('userId');
		var userName = localStorage.getItem("userName");
		console.log("zui取到的userId=" + userId);
		console.log("取到的userName=" + userName);
		if (userName != null) {
			$("#isLogin").css("display", "inline-block");
			$("#notLogin").css("display", "none");
			$("#loginUserId").html('欢迎来到云打印商城 ,'+userName);
		}
	}
	change();
	
	$("#logoutSave").click(function() {
		localStorage.clear();
		$.zui.store.clear();
		$("#notLogin").css("display", "inline-block");
		$("#isLogin").css("display", "none");
		$('#logoutModal').modal('hide', 'fit');
		location.reload();
	});
	$("#searchBtn").click(function() {
		var peruserId =$.zui.store.get('userId');
		var userName =$.zui.store.get('userName');

		var currentPage = 1;
		var pageSize = 10;

		var queryListFun = function() {
			//获取列表数据
			var queryListUrl = adminQueryUrl + integralService + queryintegral;

			var param = "";
			//搜索参数
			if (currentPage > 0) {
				param += "&currentPage=" + (currentPage - 1);
			}
			param += "&pageSize=" + pageSize;
			param += "&currentPage=" + (currentPage - 1);

			console.log('queryListUrl=' + queryListUrl);
			console.log('token=' + token + ",peruserId=" + peruserId);
			console.log('param=' + param);
			$.ajax({
				url: queryListUrl,
				type: 'post',
				beforeSend: function(xhr) {
					xhr.setRequestHeader("token", token);
					xhr.setRequestHeader("peruserId", peruserId);
					xhr.setRequestHeader("userId", userId);
				},
				header: {
					"token": token,
					"peruserId": peruserId,
					"userId": userId,
				},
				data: param,
				dataType: "JSON",
				success: function(result) {
					console.log(result.success);
					console.log(result.pageList);
					if (result.success == '1') {
						$('#listDataGrid').empty();
						$('#listDataGrid').data('zui.datagrid', null);
						$('#listPager').empty();
						$('#listPager').data('zui.pager', null);
						$('#intnum').empty();
						$('#intnum').val(result.pageList[0].clientName + "积分为:" + result.pageList[0].allIntegral + "分");
						$("#integralDetail").empty();
						$("#integralDetail").append("<div class='row'>");
						$("#integralDetail").append(
							"<div class='col-md-3'><div class='input-group'><input type='text' class='form-control' readonly='readonly' value='积分明细编码'></div></div>"
						);
						$("#integralDetail").append(
							"<div class='col-md-3'><div class='input-group'><input type='text' class='form-control' readonly='readonly' value='积分'></div></div>"
						);
						$("#integralDetail").append(
							"<div class='col-md-3'><div class='input-group'><input type='text' class='form-control' readonly='readonly' value='获取日期'></div></div>"
						);
						$("#integralDetail").append(
							"<div class='col-md-3'><div class='input-group'><input type='text' class='form-control' readonly='readonly' value='积分类型'></div></div>"
						);
						$("#integralDetail").append("</div>");
						for (var i = 0; i < result.pageList.length; i++) {
							$("#integralDetail").append("<div class='row'>");
							$("#integralDetail").append(
								"<div class='col-md-3'><div class='input-group'><input type='text' class='form-control' readonly='readonly' value='" +
								result.pageList[i].intDetailID + "'></div></div>");
							$("#integralDetail").append(
								"<div class='col-md-3'><div class='input-group'><input type='text' class='form-control' readonly='readonly' value='" +
								result.pageList[i].integral + "'></div></div>");
							$("#integralDetail").append(
								"<div class='col-md-3'><div class='input-group'><input type='text' class='form-control' readonly='readonly' value='" +
								result.pageList[i].integralDate + "'></div></div>");
							$("#integralDetail").append(
								"<div class='col-md-3'><div class='input-group'><input type='text' class='form-control' readonly='readonly' value='" +
								result.pageList[i].integralType + "'></div></div>");
							$("#integralDetail").append("</div>");
						}
					} else {
						new $.zui.Messager(result.msg('系统繁忙,请稍候再试!2'), {
							type: 'warning', // 定义颜色主题
							placement: 'center' // 定义显示位置
						}).show();
					}
				},
				error: function(result) {
					new $.zui.Messager('系统繁忙,请稍候再试!', {
						type: 'warning', // 定义颜色主题
						placement: 'center' // 定义显示位置
					}).show();
				}
			});
		};
		//调起查询数据
		queryListFun();
	});
	$("#searchBtn2").click(function() {
		alert("登陆后查看");
	});
});
