$(document).ready(function(){
	var currentPage = 1;
	var pageSize = 10;
	var token = $.zui.store.get("token");
	var userId = $.zui.store.get("userId");
	if(userId==null){
		new $.zui.Messager("未登录，请登录后查看文件信息！", {
		    type: 'warning', // 定义颜色主题
		    placement: 'center' // 定义显示位置
		}).show();
		return;
	}
	var fileCenterService =  "/api/fileCenter";
	var queryListFun = function(){
		//获取列表数据
		var queryListUrl = adminQueryUrl + fileCenterService + queryPageListMethod;
		
		var param = "";
		//搜索参数
		if ($("#queryFileNameLike").val()!=''){
			param += "queryFileNameLike="+$("#queryFileNameLike").val();
		}
		if (currentPage>0){
			param += "&currentPage="+(currentPage-1);
		}
		param += "&pageSize="+pageSize;
		param += "&currentPage="+(currentPage-1);
		
		console.log('queryListUrl='+queryListUrl);
		console.log('token='+token+",userId="+userId);
		console.log('param='+param);
		$.ajax({
			url:queryListUrl,
			type:'post',
			beforeSend:function(xhr){
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",userId);
			},
			header:{"token":token,"userId":userId},
			data:param,
			dataType:"JSON",
			success:function(result){
				console.log(result.success);
				console.log(result.pageList);
				if (result.success== '1'){
					$('#listDataGrid').empty();
					$('#listDataGrid').data('zui.datagrid',null);
					$('#listPager').empty();
					$('#listPager').data('zui.pager',null);
					
					$('#listDataGrid').datagrid({
						checkable: true,
					    checkByClickRow: true,
					    selectable: true,
					    dataSource: {
					        cols:[
					        		{name: 'PerFile_ID', label: '主键', width: 0},
					        	 	{name: 'FileName', label: '文件名称', width: 150},
						            {name: 'FilePath', label: '文件路径', width: 400},
						            {name: 'FileType', label: '文件扩展名', width: 109},
						            {name: 'Remark', label: '备注', width: 250},
						            {name: 'Opr_Date1', label: '操作时间', width: -1}
					        ],
					        cache:false,
					        array:result.pageList
					    }
					 
					});	
					$('#listDataGrid').data('zui.datagrid').sortBy('PerFile_ID', 'asc');
					
					// 手动进行初始化
					$('#listPager').pager({
					    page: currentPage,
					    recPerPage:pageSize,
					    elements:['first_icon', 'prev_icon', 'pages', 'next_icon', 'last_icon', 'total_text','size_menu','goto'],
					    pageSizeOptions:[5,10,20,30,50,100],
					    recTotal: result.pageTotal
					});
				}
				else {
					new $.zui.Messager(result.msg, {
					    type: 'warning', // 定义颜色主题
					    placement: 'center' // 定义显示位置
					}).show();
				}
			},
			error:function(result){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning', // 定义颜色主题
				    placement: 'center' // 定义显示位置
				}).show();
			}
		});
	};
	//调起查询数据
	queryListFun();
	
	//查询
	$("#searchBtnFile").click(function(){
		queryListFun();
	});
	//监听分页，修改当前页，条数
	$('#listPager').on('onPageChange', function(e, state, oldState) {
		currentPage = state.page;
		pageSize = state.recPerPage;
		queryListFun();
	});
	var printCount=$("#printCount").val();
	$("#add").click(function(){
		$("#printCount").empty();
		printCount =  Number(printCount)+1;
		$("#printCount").val(printCount);
	});
	$("#cut").click(function(){
		$("#printCount").empty();
		if(printCount>1){
			printCount =  Number(printCount)-1;
			$("#printCount").val(printCount);
		}else{
			new $.zui.Messager('至少打印1份！', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
	});
	//初始化打印
	$("#printBtn").click(function(){
		printCount = $("#printCount").val();
		
		$("#price").empty();
		var price = printCount*1;
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		if (selectedItems.length == 0) {
			new $.zui.Messager('请选择要打印的文件', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else if (selectedItems.length > 1) {
			new $.zui.Messager('请只选择一份要打印的文件', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else {
			if(Number(printCount)<1){
				new $.zui.Messager('至少打印1份！', {
				    type: 'warning',
				    placement:'center'
				}).show();
				return;
			}else{
				$('#printModal').modal('show', 'fit');
			}
			$("#printFileName").val(selectedItems[0].FileName);
			$("#price").val(price);
		}
	});
	
	$("#printSaveBtn").click(function(){
		$.ajax({
			url:adminQueryUrl + "/api/orderInfo/payFileOrder.action",
			type:'post',
			dataType:'JSON',
			beforeSend:function(xhr){//设置请求头信息
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",userId);
			},
			header:{'token':token,'userId':userId},
			data:$("#printForm").serialize(),
			success:function(data){
				if (data.success == '1') {
					$('#printModal').modal('hide', 'fit');
					var payParam = "WIDout_trade_no="+data.orderCode+"&WIDtotal_amount="+data.totalPrice
		 			 			 + "&WIDsubject="+data.goodsName+"&flag=file";
					if(data.number =='2'){
						payParam +=" 等多件";
					}
					payParam +="&WIDbody="+data.orderRemark;
					location.href="http://localhost:8080/SharedPrintService/api/orderInfo/alipay.action?"+payParam;
                }
                else{
                	new $.zui.Messager(data.msg, {
    				    type: 'warning',
    				    placement:'center'
    				}).show();
                }
			},
			error:function(e){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning',
				    placement:'center'
				}).show();
			}
		});
		/*$("#printForm").validate({
			submitHandler:function(form){
				alert(000)
				alert(888+adminQueryUrl + orderService + "/addOrder.action")
				console.log("xx:"+adminQueryUrl + orderService + "/addOrder.action");
				$.ajax({
					url:adminQueryUrl + orderService + "/addOrder.action" ,
					type:'post',
					dataType:'JSON',
					beforeSend:function(xhr){//设置请求头信息
						xhr.setRequestHeader("token",token);
						xhr.setRequestHeader("userId",userId);
					},
					header:{'token':token,'userId':userId},
					data:$(form).serialize(),
					success:function(data){
						alert(2333)
						console.log('提交订单data='+data);
						if (data.success == '1') {
		                	new $.zui.Messager('提交订单成功!', {
		    				    type: 'success',
		    				    placement:'center'
		    				}).show();
		                	queryListFun();
		                	$('#printModal').modal('hide', 'fit');
		                	 location.reload();
		                }
		                else{
		                	new $.zui.Messager(data.msg, {
		    				    type: 'warning',
		    				    placement:'center'
		    				}).show();
		                }
					},
					error:function(e){
						new $.zui.Messager('系统繁忙,请稍候再试!', {
	    				    type: 'warning',
	    				    placement:'center'
	    				}).show();
					}
				});
			}
		});*/
				
    });
	$("#FilePath").uploader({
	    autoUpload: true,            // 当选择文件后立即自动进行上传操作
	    url: adminQueryUrl + fileService + "?action=uploadDevImage1",  // 文件上传提交地址
	    limitFilesCount:1,
	    deleteActionOnDone:true,
	    deleteActionOnDone: function(file, doRemoveFile) {
	          doRemoveFile();
	      },
	    responseHandler: function(responseObject, file) {
	    	if (responseObject!=null && responseObject.response!=null){
	    		var resultJson = JSON.parse(responseObject.response);
	    		if (resultJson.success!=null && resultJson.success!=''
		    			&& typeof(resultJson.success)!='undefined'
	    				&& resultJson.fileName!=''
		    			&& typeof(resultJson.fileName)!='undefined'){
	    			if (resultJson.success=='1' ){
	    				$("#addFilePath").val(adminQueryUrl+"/"+resultJson.fileName);
	    				$("#addFileType").val(resultJson.fileType);
	    				console.log("addFileType="+resultJson.fileType);
	    				console.log("addFilePath="+adminQueryUrl+"/"+resultJson.fileName);
	    			}
	    		}
	    	}
	    } //responseHandler
	});
	$("#addSaveBtn").click(function(){
			$("#addForm").validate({
				rules:{
					addFileName:{
						"required":true,
					},
					addRemark:{
						"required":true
					}
					
				},
				messages:{
					addFileName:{
						"required":"请输文件名称！",
					},
					addRemark:{
						"required":"请选输入备注！"
					}
				},
				submitHandler:function(form){
					console.log("xx:"+adminQueryUrl +  fileCenterService + addMethod);
					
					$.ajax({
						url:adminQueryUrl + fileCenterService + addMethod ,
						type:'post',
						dataType:'JSON',
						beforeSend:function(xhr){//设置请求头信息
							xhr.setRequestHeader("token",token);
							xhr.setRequestHeader("userId",userId);
						},
						header:{'token':token,'userId':userId},
						data:$(form).serialize(),
						success:function(data){
							console.log('添加data='+data);
							//alert('添加token'+token+'添加userId'+userId);
							if (data.success == '1') {
			                	new $.zui.Messager('添加成功!', {
			    				    type: 'success',
			    				    placement:'center'
			    				}).show();
			                	queryListFun();
			                	$('#addModal').modal('hide', 'fit');
			                	 location.reload();
			                }
			                else{
			                	new $.zui.Messager(data.msg, {
			    				    type: 'warning',
			    				    placement:'center'
			    				}).show();
			                }
						},
						error:function(e){
							new $.zui.Messager('系统繁忙,请稍候再试!', {
		    				    type: 'warning',
		    				    placement:'center'
		    				}).show();
						}
					});
				}
			});	
	    });
	//初始化详情
	$("#detailBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		if (selectedItems.length == 0) {
			new $.zui.Messager('请选择要查看的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else if (selectedItems.length > 1) {
			new $.zui.Messager('请只选择一条要查看的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else {
			$("#detailForm")[0].reset();
			var url = adminQueryUrl + fileCenterService + detailMethod;
			var param = 'PerFile_ID='+selectedItems[0].PerFile_ID;//请求到列表页面
			console.log('详情param='+param);
			$.ajax({
				url:url,
				type:'post',
				dataType:'JSON',
				beforeSend:function(xhr){//设置请求头信息
					xhr.setRequestHeader("token",token);
					xhr.setRequestHeader("userId",userId);
				},
				header:{'token':token,'userId':userId},
				data:param,
				success:function(data){
					console.log('详情data='+data);
					if (data.success=='1'){
						$("#detailPerUser_Code").val(data.fileMap.PerUser_Code);
						$("#detailFileName").val(data.fileMap.FileName);
						$("#detailFileType").val(data.fileMap.FileType);
						$("#detailRemark").val(data.fileMap.Remark);
						$("#detailFilePath").html(data.fileMap.FilePath);
						$("#Opr_id").val(data.fileMap.Opr_id);
						$("#Opr_Date").val(data.fileMap.Opr_Date1);
					}
				},
				error:function(e){
					new $.zui.Messager('系统繁忙,请稍候再试!', {
					    type: 'warning',
					    placement:'center'
					}).show();
				}
			});
		}
	});
	//显示删除确认框
	$("#deleteBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		if (selectedItems.length == 0) {
			new $.zui.Messager('请选择要删除的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else if (selectedItems.length > 1) {
			new $.zui.Messager('请只选择一条要删除的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else {
			$('#deleteModal').modal('show', 'fit');
		}
	});
	//删除菜单
	$("#deleteDevModelBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		var url = adminQueryUrl + fileCenterService + deleteMethod;
		var param = 'DevInfo_ID='+selectedItems[0].PerFile_ID;//请求到列表页面
		console.log('url='+url);
		console.log('param2='+param);
		$.ajax({
			url:url,
			type:'post',
			dataType:'JSON',
			beforeSend:function(xhr){//设置请求头信息
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",userId);
			},
			header:{'token':token,'userId':userId},
			data:param,
			success:function(data){
				console.log('data='+data);
				$('#deleteModal').modal('hide', 'fit');
				if (data.success=='1'){
	                	new $.zui.Messager('删除成功!', {
	    				    type: 'success',
	    				    placement:'center'
	    				}).show();
	                	queryListFun();
				}
                else{
                	new $.zui.Messager(data.msg, {
    				    type: 'warning',
    				    placement:'center'
    				}).show();
				}
			},
			error:function(e){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning',
				    placement:'center'
				}).show();
			}
		});
	});
	
});