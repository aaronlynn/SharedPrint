//后台请求地址
var adminQueryUrl = 'http://localhost:8080/SharedPrintService';

//通用的增删改查
var queryPageListMethod = '/queryPageList.action';
var initAddMethod = '/initAdd.action';
var addMethod = '/add.action';
var initUpdateMethod = '/initUpdate.action';
var updateMethod = '/update.action';
var deleteMethod = '/delete.action';
var detailMethod = '/detail.action';

// 获取商品类别
var categoryService = "/api/categoryInfo"
var queryCategory = "/queryCategory.action"
var deleteCategoryMethod = "/deleteCategory.action"
var detailCategoryMethod = "/detailCategory.action"

// 获取商品品牌
var brandService = "/api/brandInfo"
var queryBrand = "/queryBrand.action"
var deleteBrandMethod = "/deleteBrand.action"
var detailBrandMethod = "/detailBrand.action"

// 获取积分信息
var integralService = "/api/intdetail"
var queryintegral = "/queryintdetailList.action"

// 订单查询
var orderService = '/api/orderInfo';
var agentOrderService = '/api/agentOrderInfo';
var queryOrder = '/agentQueryOrderInfo.action';
var addOrderMethod = '/addOrder.action';//添加订单
var payMethod = '/alipay.action';
var queryOrderRec = '/queryOrderRec.action'; //查询收货信息表
var queryBoughtGoods = '/queryBoughtGoods.action';//查询已购买的宝贝
var queryOrderGoodsCount = '/queryOrderGoodsCount.action';//查询购物商品数量
var payOrderMethod = '/payOrder.action';

// 获取商品信息
var goodsService = "/api/info"
var queryInfo = "/queryInfo.action"
var deleteInfoMethod = "/checkInfo.action"
var detailInfoMethod = "/detailInfo.action"
var checkInfoMethod = "/checkInfo.action"
var upcheckMethod="/upcheckInfo.action"
	
//登录功能
var UserService="/api/PerUser"
var loginMethod="/PerUserLogin.action"
// 获取文件服务
var fileService = '/FileServlet';

//获取购物车
var cartService = "/api/orderGoods";
var updateOrderGoodsNumber = "/updateNumber.action";
var addCartMethod = "/addCart.action";
var imdBuyGoods = "/imdBuyGoods.action";
