$(document).ready(function(){
	var token = "231231";
	var userId = "1";
	var pageSize = 100;
	var queryCategoryMenu = function(){
		//获取列表数据
		var queryListUrl = adminQueryUrl + categoryService + queryCategory;
		console.log('queryListUrl='+queryListUrl);
		var param = "pageSize="+pageSize;
		$.ajax({
			url:queryListUrl,
			type:'post',
			beforeSend:function(xhr){
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",userId);
			},
			header:{"token":token,"userId":userId},
			dataType:"JSON",
			data:param,
			success:function(result){
				console.log(result.success);
				console.log(result.pageList);
				if (result.success== '1'){
					var categoryList = result.pageList;
					var resultHtml = '';
					for(var i=0;i<categoryList.length;i++){
						resultHtml += '<div cid="'+categoryList[i].Category_ID+'" class="eachCategory">';
						resultHtml += '<span class="glyphicon glyphicon-link"></span>'+
			               ' <a href="searchResult.jsp?Category_ID='+categoryList[i].Category_ID+'">'+categoryList[i].categoryName +'</a></div>';
			                  
					}
					$("#categoryMenu").html(resultHtml);
				}
				else {
					console.log("商品类别加载失败！");
				}
			},
			error:function(result){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning', // 定义颜色主题
				    placement: 'center' // 定义显示位置
				}).show();
			}
		});
	};
	
	queryCategoryMenu();
});