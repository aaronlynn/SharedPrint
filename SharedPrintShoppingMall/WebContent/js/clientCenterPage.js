$(document).ready(function() {
	$("#userId").val($.zui.store.get('userId'));
	$("#userID").val($.zui.store.get('userId'));
	var token = $.zui.store.get("token");
	var userId = $.zui.store.get("userId");
	$("#SaveBtnName").click(function() {
		$("#userForm").validate({
			rules: {
				userName: {
					"required": true
				}
			},
			messages: {
				userName: {
					"required": "请输入昵称！"
				}
			},
			submitHandler: function(form) {
				$.ajax({
					url: adminQueryUrl + UserService + "/modifyUser.action",
					type: 'post',
					dataType: 'JSON',
					beforeSend:function(xhr){//设置请求头信息
						xhr.setRequestHeader("token",token);
						xhr.setRequestHeader("userId",userId);
					},
					header:{'token':token,'userId':userId},
					data: $(form).serialize(),
					success: function(data) {
						console.log('修改昵称=data=' + data);
						if (data.success == '1') {
							new $.zui.Messager('修改成功!', {
								type: 'success',
								placement: 'center'
							}).show();
							localStorage.setItem("userName", $("#userName").val());//js原生的本地存储
							location.href = "index.jsp";
						} else {
							new $.zui.Messager(data.msg, {
								type: 'warning',
								placement: 'center'
							}).show();
						}
					},
					error: function(e) {
						new $.zui.Messager('系统繁忙,请稍候再试!', {
							type: 'warning',
							placement: 'center'
						}).show();
					}
				});
			}
		});
	});
	$("#SaveBtnPwd").click(function() {
		var validPwdUrl = adminQueryUrl + UserService + "/validatePwd.action";
		$("#clientCenterForm").validate({
			rules: {
				oldPassword: {
					"required": true,
					"remote": {
						url: validPwdUrl,
						type: 'post',
						dataType: 'json',
						data: {
							'oldPassword': function() {
								return $("#oldPassword").val();
							},
							'userId': function() {
								return $("#userId").val();
							}
						}
					}
				},
				newPassword: {
					"required": true
				},
				repeatpassword: {
					"required": true,
					"equalTo": "#newPassword"
				}

			},
			messages: {
				oldPassword: {
					"required": "请输入旧密码！",
					"remote": "旧密码输入错误！"
				},
				newPassword: {
					"required": "请输入新密码！",
				},
				repeatpassword: {
					"required": "请再次输入新密码！",
					"equalTo": "两次输入的密码不一致！"
				},

			},
			submitHandler: function(form) {
				console.log("xx:" + adminQueryUrl + UserService + "/modifyUser.action");
				$.ajax({
					url: adminQueryUrl + UserService + "/modifyUser.action",
					type: 'post',
					dataType: 'JSON',
					beforeSend:function(xhr){//设置请求头信息
						xhr.setRequestHeader("token",token);
						xhr.setRequestHeader("userId",userId);
					},
					header:{'token':token,'userId':userId},
					data: $(form).serialize(),
					success: function(data) {
						console.log('修改密码data=' + data);
						if (data.success == '1') {
							new $.zui.Messager('修改成功!', {
								type: 'success',
								placement: 'center'
							}).show();
							location.href = "index.jsp";
						} else {
							new $.zui.Messager(data.msg, {
								type: 'warning',
								placement: 'center'
							}).show();
						}
					},
					error: function(e) {
						new $.zui.Messager('系统繁忙,请稍候再试!', {
							type: 'warning',
							placement: 'center'
						}).show();
					}
				});
			}
		});
	});
});
