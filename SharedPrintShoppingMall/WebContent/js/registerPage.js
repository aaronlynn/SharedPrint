$(document).ready(function(){
	$("#addSaveBtn").click(function(){
		var validNameUrl = adminQueryUrl + UserService +"/validate.action" ;
		console.log(validNameUrl)
		$("#registerForm").validate({
			rules:{
				name:{
					"required":true,
					"remote":{
						url:validNameUrl,
						type:'post',
						dataType:'json',
						data:{
							'name':function(){
								return $("#name").val();
							}
						}
					}
				},
				password:{
					"required":true
				},
				repeatpassword:{
					"required":true,
					"equalTo":"#password"
				}
				
			},
			messages:{
				name:{
					"required":"请输入用户名！",
					"remote":"该用户名已经存在！"
				},
				password:{
					"required":"请输入密码！"
				},
				repeatpassword:{
					"required":"请再次输入密码！",
					"equalTo":"两次输入的密码不一致！"
				},
				
			},
			submitHandler:function(form){
				console.log("xx:"+adminQueryUrl +  UserService + "/PerUserRegister.action");
				$.ajax({
					url:adminQueryUrl + UserService + "/PerUserRegister.action" ,
					type:'post',
					dataType:'JSON',
					data:$(form).serialize(),
					success:function(data){
						console.log('注册data='+data);
						if (data.success == '1') {
							new $.zui.Messager('注册成功！', {
							    type: 'success',
							    close: true,
							    time: 0,
							    placement:'center',
							    actions: [{
							        icon: 'ok-sign',
							        text: '点击登录',
							        action: function() {  // 点击该操作按钮的回调函数
							        	location.href='login.jsp';
							        }
							    }]
							}).show();
//		                	new $.zui.Messager('注册成功!', {
//		    				    type: 'success',
//		    				    placement:'center'
//		    				}).show();
//		                	location.href="login.jsp";
		                }
		                else{
		                	new $.zui.Messager(data.msg, {
		    				    type: 'warning',
		    				    placement:'center'
		    				}).show();
		                }
					},
					error:function(e){
						new $.zui.Messager('系统繁忙,请稍候再试!', {
	    				    type: 'warning',
	    				    placement:'center'
	    				}).show();
					}
				});
			}
		});	
    });
});