$(document).ready(function(){
	var token =123;
	var userId =123;
	//监听登录事件
	$("#loginBtn").click(function(){
		if($("#name").val()==''){
			$('#name').tooltip('show','请输入用户名');
			return;
		}
		if($("#password").val()==''){
			$('#password').tooltip('show','请输入密码');
			return;
		}
		$(this).button('loading');
		var queryUrl = adminQueryUrl+UserService + loginMethod;
		var param = "username="+$("#name").val()+"&upwd="+$("#password").val();
		console.log('queryUrl='+queryUrl);
		console.log('param='+param);
		//发送登录验证请求
		$.ajax({
			url:queryUrl,
			type:'post',
			dataType:'JSON',
			beforeSend:function(xhr){//设置请求头信息
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",userId);
			},
			header:{'token':token,'userId':userId},
			data:param,
			success:function(data){
				console.log('登录data='+data);
				if (data.success == '1') {
					if(1==data.PerUserStatus){
						new $.zui.Messager('登录成功!', {
	    				    type: 'success',
	    				    placement:'center'
	    				}).show();
	                	//保存服务器返回值到本地中
	    				$.zui.store.set("userName", data.LoginName);
	    				$.zui.store.set("userId", data.userId);
	    				$.zui.store.set("token", data.token);
	    				$.zui.store.set("trueName", data.trueName);
	    				$.zui.store.set("PerUserCode", data.PerUserCode);
	    				localStorage.setItem("userName",  data.trueName);//js原生的本地存储
	    				//alert("555userId="+userId);
						location.href='index.jsp?username='+ data.trueName+'&userId='+data.userId;
					}else{
						new $.zui.Messager('账号状态异常，请联系管理员', {
						    type: 'warning',
						    placement:'center'
						}).show();
						$("#loginBtn").button('reset');
					}
                	
                }else{
                	new $.zui.Messager('用户名或者密码错!', {
    				    type: 'warning',
    				    placement:'center'
    				}).show();
    				$("#loginBtn").button('reset');
                }
				 
			},
			error:function(e){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning',
				    placement:'center'
				}).show();
			}
		});
	});
});