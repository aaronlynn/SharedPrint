<%@ page language="java" pageEncoding="UTF-8"%> 
 <%@include file="include/header.jsp"%>
 <%@include file="include/top.jsp"%>
 <%@include file="include/search.jsp"%>
<head>
<meta http-equiv="X-UA-Compatiable" content="IE=edge">
<!-- 屏幕自适应 -->
<meta name="viewport" content="width=device-width,initial-scale=1">
<link href="${pageContext.request.contextPath}/css/zui.datagrid.css"
	rel="stylesheet">
<link href="css/zui.css" rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/zui.datagrid.css">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery1.9.1.js"></script>
<script src="js/zui.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery.validate.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/localization/messages_zh.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/config.js?version=6"></script>
<script src="${pageContext.request.contextPath}/js/zui.datagrid.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/loginPage.js?version=6"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/order.js?version=1"></script>

</head>
	<div class="panel">
		<div class="panel-heading">查询订单</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-2">
					<div class="input-group">
						<span class="input-group-addon">订单号</span> <input type="text"
							id="OrderCode" name="OrderCode" class="form-control"
							placeholder="">
					</div>
				</div>
				<div class="col-md-2">
					<div class="input-group">
						<span class="input-group-addon">订单状态</span> <select
							class="form-control" id="OrderStatus" name="OrderStatus">
							<option value="">请选择</option>
						</select>
					</div>
				</div>
				<div class="col-md-3">
					<div class="input-group">
						<span class="input-group-addon">订单创建年份</span> <select
							class="form-control" id="querycrtyear" name="querycrtyear">
							<option value="">请选择</option>
						</select>
					</div>
				</div>
				<div class="col-md-3">
					<div class="input-group">
						<span class="input-group-addon">订单创建时间</span> <select
							class="form-control" id="querycrtmonth" name="querycrtmonth">
							<option value="">请选择</option>
						</select>
					</div>
				</div>			
			</div><br>
			<div class="row">
				<div class="col-md-1">
					<button class="btn btn-success" type="button" id="searchBtn1">
						<i class="icon-search"></i>搜索
					</button>
				</div>
				<div class="col-md-1">
					<button class="btn btn-success" type="button" id="payOrderBtn">
						<i class="icon-search"></i>付款
					</button>
				</div>
				<div class="col-md-1">
					<button class="btn btn-success" type="button" id="detailBtn">
						<i class="icon-search"></i>详情
					</button>
				</div>
			</div>
		</div>
	</div>
	<div class="datagrid">
		<div id="listDataGrid" class="datagrid" data-ride="datagrid"></div>
		<div id="listPager" class="pager" data-ride="pager"></div>
	</div>
	<%@include file="include/footer.jsp"%>	
	<!-- 详情页面  -->
	<div class="modal fade" id="detailModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
					</button>
					<h4 class="modal-title">订单详情</h4>
				</div>
				<form id="detailForm">
					<div class="modal-body">
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">订单号</span> <input type="text"
										id="showOrderCode" name="showOrderCode" class="form-control"
										placeholder="" readonly="readonly">
								</div>
							</div>
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">下单人</span> <input type="text"
										id="PerUserName" name="PerUserName" class="form-control"
										placeholder="" readonly="readonly">
								</div>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">退款金额</span> <input type="text"
										id="Refund" name="Refund" class="form-control" placeholder=""
										readonly="readonly">
								</div>
							</div>

							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">退款时间</span> <input type="text"
										id="RefundDate" name="RefundDate" class="form-control"
										placeholder="" readonly="readonly">
								</div>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">收货人</span> <input
										id="ReceivedBy" name="ReceivedBy" class="form-control"
										placeholder="" readonly="readonly">
								</div>
							</div>
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">收货人电话</span> <input id="Phone"
										name="Phone" class="form-control" placeholder=""
										readonly="readonly">
								</div>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">收货地址</span> <input id="Address"
										name="Address" class="form-control" placeholder=""
										readonly="readonly">
								</div>
							</div>
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">商品名称</span> <input
										id="GoodsName" name="GoodsName" class="form-control"
										placeholder="" readonly="readonly">
								</div>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">商品数量</span> <input
										id="GoodsCount" name="GoodsCount" class="form-control"
										placeholder="" readonly="readonly">
								</div>
							</div>
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">商品合计</span> <input
										id="GoodsTotalPrice" name="GoodsTotalPrice"
										class="form-control" placeholder="" readonly="readonly">
								</div>
							</div>
						</div>
						<hr>						
						<div class="row">
							<div class="col-md-12">
								<div class="input-group">
									<span class="input-group-addon">订单备注</span>
									<textarea id="OrderRemark" name="OrderRemark"
										readonly="readonly" cols="60" rows="5" class="form-control"></textarea>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default "
							data-dismiss="modal">关闭</button>
					</div>
				</form>
			</div>
		</div>
	</div>