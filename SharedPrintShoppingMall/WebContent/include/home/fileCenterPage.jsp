<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/zui.uploader.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/zui.datagrid.css" />
<div class="container">
	<div class="panel ">
		<div class="panel-heading">查询文件</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-4">
					<div class="input-group">
						<span class="input-group-addon">文件名称</span> <input type="text" id="queryFileNameLike" name="queryFileNameLike"
						 class="form-control" placeholder="">
					</div>
				</div>
				<div class="col-md-2">
					<button class="btn btn-primary" type="button" id="searchBtnFile">
						<i class="icon-search"></i>搜索
					</button>
				</div>
				<div class="search-box col-md-6">
					<button class="btn btn-success " type="button" data-toggle="modal" data-target="#addModal" id="addBtn">
						<i class="icon-plus"></i>新增
					</button>
					&nbsp;&nbsp;&nbsp;&nbsp;
					<button class="btn btn-danger " type="button" id="deleteBtn">
						<i class="icon icon-trash"></i>删除
					</button>
					&nbsp;&nbsp;&nbsp;&nbsp;
					<button class="btn " type="button" id="detailBtn" data-toggle="modal" data-target="#detailModal">
						<i class="icon icon-info"></i>详情
					</button>
					&nbsp;&nbsp;&nbsp;&nbsp;
					<button class="btn btn-primary " type="button" id="printBtn" >
						<i class="icon icon-info"></i>打印
					</button>
					<button type="button" class="btn" id="add"><i class="icon icon-plus"></i></button>
					<input type="text" style="width: 50px;text-align: center;" id="printCount" name="printCount" value="1">
					<button type="button" class="btn" id="cut"><i class="icon icon-minus"></i></button>
					<b>份</b>
				</div>
			</div>
		</div>
	</div>

	<hr>
	<div id="listDataGrid" class="datagrid"></div>
	<div id="listPager" class="pager" data-ride="pager"></div>
	<div class="modal fade" id="printModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
					</button>
					<h4 class="modal-title">打印文件</h4>
				</div>
				<form id="printForm" name="printForm">
					<div class="modal-body">
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">文件名称</span>
									<input type="text" class="form-control" id="printFileName" readonly name="printFileName">
								</div>
							</div>
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">订单金额</span>
									<input type="text" style="width: 100px;" id="price" readonly name="price" ><b>元</b>
								</div>
							</div>
							<div class="col-md-12">
							<div class="input-group">
								<span class="input-group-addon">订单备注</span>
								<textarea id="OrderRemark" name="OrderRemark"  cols="60" rows="5" class="form-control"></textarea>
							</div>
						</div>
						</div>
					</div>
					<div class="modal-footer">
						<input  type="hidden" id="orderFlag" name="orderFlag" value="file">
						<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
						<button type="button" class="btn btn-primary" id="printSaveBtn" name="printSaveBtn">提交订单</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal fade" id="addModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
					</button>
					<h4 class="modal-title">新增文件</h4>
				</div>
				<form id="addForm" name="addForm">
					<div class="modal-body">
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">文件名称</span> <input type="text" class="form-control" id="addFileName" name="addFileName"
									 placeholder="文件名称">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">备注</span>
									<textarea id="addRemark" name="addRemark" cols="60" rows="5" class="form-control"></textarea>
								</div>
							</div>
							<div class="col-md-6">
								<input type="hidden" name="addFilePath" id="addFilePath" value="" />
								<input type="hidden" name="addFileType" id="addFileType" value="" />
								<div class="input-group">
									<span class="input-group-addon">文件路径</span>
									<div id="FilePath" class="uploader">
										<div class="file-list" data-drag-placeholder="请拖拽文件到此处"></div>
										&nbsp;&nbsp;&nbsp;
										<button type="button" class="btn btn-primary uploader-btn-browse">
											<i class="icon icon-cloud-upload"></i> 选择文件
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<input type="hidden" id="action" name="action" value="add" />
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
						<button type="submit" class="btn btn-primary" id="addSaveBtn" name="addSaveBtn">保存</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal fade" id="detailModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
					</button>
					<h4 class="modal-title">文件详情</h4>
				</div>
				<form id="detailForm" name="detailForm">
					<div class="modal-body">
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">用户号</span>
									<input type="text" readonly class="form-control" id="detailPerUser_Code" name="detailPerUser_Code">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">文件名称</span>
									<input type="text" readonly class="form-control" id="detailFileName" name="detailFileName">
								</div>
							</div>
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">文件扩展名</span>
									<input type="text" readonly class="form-control" id="detailFileType" name="detailFileType">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">备注</span>
									<textarea id="detailRemark" name="detailRemark" readonly cols="60" rows="5" class="form-control"></textarea>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="input-group">
									<span class="input-group-addon">文件路径</span>
									<span id="detailFilePath"></span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">操作者</span>
									<input type="text" class="form-control" readonly id="Opr_id" name="Opr_id">
								</div>
							</div>
							<div class="col-md-6">
								<div class="input-group">
									<span class="input-group-addon">操作日期</span>
									<input type="text" class="form-control" readonly id="Opr_Date" name="Opr_Date">
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal fade" id="deleteModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
					</button>
					<h4 class="modal-title">删除文件</h4>
				</div>
				<div class="modal-body">
					<p>您确定要删除此文件吗?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" id="deleteDevModelBtn">确定</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="${pageContext.request.contextPath}/js/jquery.validate.js" type="text/javascript" charset="utf-8"></script>
<script src="${pageContext.request.contextPath}/js/localization/messages_zh.js" type="text/javascript" charset="utf-8"></script>
<script src="${pageContext.request.contextPath}/js/zui.datagrid.js" type="text/javascript" charset="utf-8"></script>
<script src="${pageContext.request.contextPath}/js/config.js?ver=1" type="text/javascript" charset="utf-8"></script>
<script src="${pageContext.request.contextPath}/js/zui.uploader.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/fileCenterPage.js?ver=1"></script>
