<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%> 

 <script type="text/javascript">
    var token = "231231";
	var userId = "1";
	var pageSize = 100;
	var param = "pageSize=" + pageSize;
	var context = $("#contextPath1").val();
	var queryListFun = function(){
		// 获取列表数据
		var queryListUrl = adminQueryUrl + goodsService + queryInfo;
		console.log('queryGoodsListUrl='+queryListUrl);
		console.log('token='+token+",userId="+userId);
		console.log('param='+param);
		$.ajax({
			url:queryListUrl,
			type:'post',
			beforeSend:function(xhr){
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",userId);
			},
			header:{"token":token,"userId":userId},
			data:param,
			dataType:"JSON",
			success:function(result){
				console.log(result.success);
				console.log(result.pageList);
				if (result.success== '1'){
					
					var goodsList = result.pageList;
					var resultHtml = '<div class="left-mark"></div> <span class="categoryTitle">商品列表</span> <br>';
					for (var i = 0; i < goodsList.length; i++) {

						resultHtml  += '<div class="productItem" > '+
                           ' <a href="product.jsp?goodsId='+goodsList[i].goodsId+'"><img width="100px" src="'+context+ '/img/goods/'+goodsList[i].Picture1+'"></a> '+
                           ' <a class="productItemDescLink" href="product.jsp?goodsId='+goodsList[i].goodsId+'"> '+
                           '     <span class="productItemDesc">[热销] '+ goodsList[i].goodsName + 
                           '     </span> '+
                          '  </a> '+
                          '  <span class="productPrice"> '+
                         '&nbsp;'+goodsList[i].price +' 元 </span>'+
                       ' </div>';
					}
				    resultHtml += '<div style="clear:both"></div>';
					$("#eachHomepageCategoryProducts").html(resultHtml);
					
					    /* recTotal: result.pageTotal */
				}
				else {
					new $.zui.Messager(result.msg, {
					    type: 'warning', // 定义颜色主题
					    placement: 'center' // 定义显示位置
					}).show();
				}
			},
			error:function(result){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning', // 定义颜色主题
				    placement: 'center' // 定义显示位置
				}).show();
			}
		});
	};
	queryListFun();
</script>


<div class="homepageCategoryProducts">
        <input type="hidden" id="contextPath1" 
		value="${pageContext.request.contextPath}" />
            <div class="eachHomepageCategoryProducts" id="eachHomepageCategoryProducts">
                <div class="left-mark"></div>
                <span class="categoryTitle">${c.name}</span>
                <br>
                <c:forEach items="${c.products}" var="p" varStatus="st">
                    <c:if test="${st.count<=5}">
                        <div class="productItem" >
                            <a href="foreproduct?pid=${p.id}"><img width="100px" src="img/productSingle_middle/${p.firstProductImage.id}.jpg"></a>
                            <a class="productItemDescLink" href="foreproduct?pid=${p.id}">
                                <span class="productItemDesc">[热销]
                                ${fn:substring(p.name, 0, 20)}
                                </span>
                            </a>
                            <span class="productPrice">
                                <fmt:formatNumber type="number" value="${p.promotePrice}" minFractionDigits="2"/>
                            </span>
                        </div>
                    </c:if>              
                </c:forEach>
                <div style="clear:both"></div>
            </div>
  
    <img id="endpng" class="endpng" src="img/site/end.png">
 
</div>