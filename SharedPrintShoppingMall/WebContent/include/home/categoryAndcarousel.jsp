<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>

<script>
	//显示分类列表
	function showProductsAsideCategorys(cid) {
		$("div.eachCategory[cid=" + cid + "]").css("background-color", "white");
		$("div.eachCategory[cid=" + cid + "] a").css("color", "#87CEFA");
		$("div.productsAsideCategorys[cid=" + cid + "]").show();
	}

	function hideProductsAsideCategorys(cid) {
		$("div.eachCategory[cid=" + cid + "]").css("background-color",
				"#e2e2e3");
		$("div.eachCategory[cid=" + cid + "] a").css("color", "#000");
		$("div.productsAsideCategorys[cid=" + cid + "]").hide();
	}
	//显示品牌列表
	function showProductsAsideBrand(cid) {
		$("div.eachBrand[cid=" + cid + "]").css("background-color", "white");
		$("div.eachBrand[cid=" + cid + "] a").css("color", "#87CEFA");
		$("div.productsAsideCategorys[cid=" + cid + "]").show();
	}

	function hideProductsAsideBrand(cid) {
		$("div.eachCategory[cid=" + cid + "]").css("background-color",
				"#e2e2e3");
		$("div.eachCategory[cid=" + cid + "] a").css("color", "#000");
		$("div.productsAsideCategorys[cid=" + cid + "]").hide();
	}

	$(function() {
		$("div.eachCategory").mouseenter(function() {
			var cid = $(this).attr("cid");
			showProductsAsideCategorys(cid);
		});
		$("div.eachCategory").mouseleave(function() {
			var cid = $(this).attr("cid");
			hideProductsAsideCategorys(cid);
		});
		$("div.productsAsideCategorys").mouseenter(function() {
			var cid = $(this).attr("cid");
			showProductsAsideCategorys(cid);
		});
		$("div.productsAsideCategorys").mouseleave(function() {
			var cid = $(this).attr("cid");
			hideProductsAsideCategorys(cid);
		});

		$("div.rightMenu span").mouseenter(function() {
			var left = $(this).position().left;
			var top = $(this).position().top;
			var width = $(this).css("width");
			var destLeft = parseInt(left) + parseInt(width) / 2;
			$("img#catear").css("left", destLeft);
			$("img#catear").css("top", top - 20);
			$("img#catear").fadeIn(500);

		});
		$("div.rightMenu span").mouseleave(function() {
			$("img#catear").hide();
		});

		var left = $("div#carousel-of-product").offset().left;
		$("div.categoryMenu").css("left", left - 40);
		$("div.categoryWithCarousel div.head").css("margin-left", left);
		$("div.productsAsideCategorys").css("left", left - 40);

		//鼠标覆盖和离开品牌方法
		$("#brandMenu").mouseover(function(){
			
			$("#brandMenu").css("background-color","#00BFFF");
			$("#eachBrand").css("color","white");
			$("#productsAsideBrand").show();
		});
		$("#brandMenu").mouseout(function(){
			$("#brandMenu").css("background-color","#87CEEB");
			$("#brandMenu").css("color", "white");
			$("#productsAsideBrand").hide();
		});
 		$("#productsAsideBrand").mouseover(function(){
			$("#productsAsideBrand").show();
		});
		$("#productsAsideBrand").mouseout(function(){
			$("#productsAsideBrand").hide();
		});
	});
	
	function printOnline(){
		alert("2222");
	}
</script>

<img src="img/site/cloud.png" id="catear" class="catear"
	 style="width: 80px; height: 20px" />

<div class="categoryWithCarousel">

	<div class="headbar show1" style="background-color: #87CEEB">
		<div class="head " style="background-color: #87CEEB">

			<span style="margin-left: 10px" class="glyphicon glyphicon-th-list"></span>
			<span style="margin-left: 10px">商品分类</span>

		</div>
		<div class="rightMenu" id="brandMenu" >
			<span class="glyphicon glyphicon-list-alt">&nbsp;品牌分类</span>
		</div>
		<div class="rightMenu">
			<span class="glyphicon glyphicon-print" >&nbsp;<a href="${pageContext.request.contextPath}/fileCenter.jsp" style="text-decoration: none;">文件中心</a></span>
		</div>
		<div class=""
			style="width: 200px; height: 36px; line-height: 36px; font-size: 16px; font-weight: bold; color: white; margin-left: 20px; display: inline-block;">
			<span style="margin-left: 0px" class="">精美笔记</span>
		</div>
		<div class=""
			style="width: 200px; height: 36px; line-height: 36px; font-size: 16px; font-weight: bold; color: white; margin-left: 20px; display: inline-block;">
			<span style="margin-left: 0px" class="">多功能文具</span>
		</div>

	</div>

	<div style="position: relative">
		<%@include file="categoryMenu.jsp"%>
	</div>

	<div style="position: relative; left: 0; top: 0;">
		<%@include file="productsAsideCategorys.jsp"%>
	</div>

	<div style="position: relative; left: 0; top: 0;">
		<%@include file="productsAsideBrand.jsp"%>
	</div>

	<%@include file="carousel.jsp"%>

	<div class="carouselBackgroundDiv"></div>

</div>