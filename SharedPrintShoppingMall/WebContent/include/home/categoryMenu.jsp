<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery1.9.1.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery.validate.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/localization/messages_zh.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/config.js?version=1"></script> 
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/category.js?version=1"></script> 
<div class="categoryMenu" id="categoryMenu" name="categoryMenu">

 
           <div  class="eachCategory" >
                <span class="glyphicon glyphicon-link"></span>
                <a href="forecategory?cid=${c.id}">
                    ${c.name}
                </a>
           </div>
    </div> 