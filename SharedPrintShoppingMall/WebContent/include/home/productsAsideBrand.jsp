<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<script>
	$(function() {
		$("div.productsAsideBrand div.row a").each(function() {
			var v = Math.round(Math.random() * 6);
			if (v == 1)
				$(this).css("color", "#87CEFA");
		});

		var token = "231231";
		var userId = "1";
		var pageSize = 100;
		var queryBrandMenu = function() {
			//获取列表数据
			var queryListUrl = adminQueryUrl + brandService + queryBrand;
			console.log('queryListUrl=' + queryListUrl);
			var param = "pageSize=" + pageSize;
			$.ajax({
						url : queryListUrl,
						type : 'post',
						beforeSend : function(xhr) {
							xhr.setRequestHeader("token", token);
							xhr.setRequestHeader("userId", userId);
						},
						header : {
							"token" : token,
							"userId" : userId
						},
						dataType : "JSON",
						data : param,
						success : function(result) {
							console.log(result.success);
							console.log(result.pageList);
							if (result.success == '1') {
								var categoryList = result.pageList;
								var resultHtml = '<div cid="" class="eachCategory" id="eachBrand" >';
								for (var i = 0; i < categoryList.length; i++) {

									resultHtml += '<span >'
											+ ' <a href="searchResult.jsp?Brand_ID='+categoryList[i].Category_ID+'">'
											+ categoryList[i].categoryName
											+ '</a></span>';

								}
							    resultHtml += '</div>';
								$("#showBrandMenu").html(resultHtml);
							} else {
								console.log("商品类别加载失败！");
							}
						},
						error : function(result) {
							new $.zui.Messager('系统繁忙,请稍候再试!', {
								type : 'warning', // 定义颜色主题
								placement : 'center' // 定义显示位置
							}).show();
						}
					});
		};
		queryBrandMenu();
	});
</script>
<!-- 显示获取品牌列表 -->
<div class="productsAsideCategorys"  id="productsAsideBrand" style="background-color:white;height: 210px;margin-right: 20px;">
	<div class="row show1" id="showBrandMenu" style="margin-right: 5px;">

		<div class="seperator"></div>
	</div>

</div>
