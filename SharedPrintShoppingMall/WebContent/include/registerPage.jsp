<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
 

 
<form id="registerForm" name="registerForm"  class="registerForm">
 
    <table class="registerTable" align="center">
        <tr>
            <td  class="registerTip registerTableLeftTD">设置会员名</td>
            <td></td>
        </tr>
        <tr>
            <td class="registerTableLeftTD">登陆名</td>
            <td  class="registerTableRightTD"><input id="name" name="name" placeholder="会员名一旦设置成功，无法修改" > </td>
        </tr>
        <tr>
            <td  class="registerTip registerTableLeftTD">设置登陆密码</td>
            <td  class="registerTableRightTD">登陆时验证，保护账号信息</td>
        </tr>    
        <tr>
            <td class="registerTableLeftTD">登陆密码</td>
            <td class="registerTableRightTD"><input id="password" name="password" type="password"  placeholder="设置你的登陆密码" > </td>
        </tr>
        <tr>
            <td class="registerTableLeftTD">密码确认</td>
            <td class="registerTableRightTD"><input id="repeatpassword" name="repeatpassword" type="password"   placeholder="请再次输入你的密码" > </td>
        </tr>
                 
        <tr>
            <td colspan="2" class="registerButtonTD">
                <button id="addSaveBtn" name="addSaveBtn" type="submit" style="background-color: #87CEEB">提   交</button>
            </td>
        </tr>            
    </table>
</form>
<script src="${pageContext.request.contextPath}/js/jquery/2.0.0/jquery.min.js" type="text/javascript" charset="utf-8"></script>
<script src="${pageContext.request.contextPath}/js/zui.js" type="text/javascript" charset="utf-8"></script>
<script src="${pageContext.request.contextPath}/js/config.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.validate.js"></script>
<script src="${pageContext.request.contextPath}/js/localization/messages_zh.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/registerPage.js?ver=1"></script>