<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/config.js?version=3"></script>
<script>
//商品分类品牌搜索
    $(document).ready(function(){
    	//判断分类,品牌,商品名称是否为空
    	var context = $("#contextPath1").val();
    	var param="";
    	var categoryId = <%=request.getParameter("Category_ID")%> ;
    	var brandId = <%=request.getParameter("Brand_ID")%>;
    	<%String name = null;
			if (request.getParameter("goodsName") != null) {
				name = new String(request.getParameter("goodsName").getBytes("iso-8859-1"), "utf-8");
			}%>
    	var goodsName = "<%=name%>";
    	var goodBelowName = '<%=request.getParameter("goodBelowName")%>';
		if (categoryId != null) {
			param += "&GoodsCategory=" + categoryId;
		}
		if (brandId != null) {
			param += "&GoodsBrand=" + brandId;
		}
		if (goodBelowName != "null") {
			goodsName = goodBelowName;
		}
		if (goodsName != null) {
			param += "&GoodsNameLike=" + goodsName;

		}
		var token = "231231";
		var userId = "1";

		var goodsBySearchURL = adminQueryUrl + goodsService
				+ "/queryInfos.action";
		console.log("goodsBySearchURL=" + goodsBySearchURL);
		console.log("param=" + param);
		$.ajax({
					url : goodsBySearchURL,
					type : 'post',
					beforeSend : function(xhr) {
						xhr.setRequestHeader("token", token);
						xhr.setRequestHeader("userId", userId);
					},
					header : {
						"token" : token,
						"userId" : userId
					},
					data : param,
					dataType : "JSON",
					success : function(result) {
						console.log(result.success);
						console.log(result.pageList);
						if (result.success == '1') {
							var goodsList = result.pageList;
							var resultHtml = '<div class="categorySortBar" >'
									+

									' <table class="categorySortBarTable categorySortTable">'
									+ '<tr>'
									+ '       <td <c:if test="${'all'==param.sort||empty param.sort}">class="grayColumn"</c:if> ><a href="#">综合<span class="glyphicon glyphicon-arrow-down"></span></a></td>'
									+ '       <td <c:if test="${'review'==param.sort}">class="grayColumn"</c:if> ><a href="#">人气<span class="glyphicon glyphicon-arrow-down"></span></a></td>'
									+ '       <td <c:if test="${'date'==param.sort}">class="grayColumn"</c:if>><a href="#">新品<span class="glyphicon glyphicon-arrow-down"></span></a></td>'
									+ '        <td <c:if test="${'saleCount'==param.sort}">class="grayColumn"</c:if>><a href="#">销量<span class="glyphicon glyphicon-arrow-down"></span></a></td>'
									+ '        <td <c:if test="${'price'==param.sort}">class="grayColumn"</c:if>><a href="#">价格<span class="glyphicon glyphicon-resize-vertical"></span></a></td>'
									+ '     </tr>'
									+ '  </table>'
									+

									'   <table class="categorySortBarTable">'
									+ '        <tr>'
									+ '            <td><input class="sortBarPrice beginPrice" type="text" placeholder="请输入"></td>'
									+ '            <td class="grayColumn priceMiddleColumn">-</td>'
									+ '            <td><input class="sortBarPrice endPrice" type="text" placeholder="请输入"></td>'
									+ '        </tr>'
									+ '    </table>' +

									'</div>';
							resultHtml += '<div id="resultAllDiv" style="margin-left:35px;">';
							for (var i = 0; i < goodsList.length; i++) {

								resultHtml += '<div class="productUnit" price=""> '
										+ ' <a href="product.jsp?goodsId='+goodsList[i].goodsId+'"> '
										+ '  <img class="productImage" src="'+context+ '/img/goods/'+goodsList[i].Picture1+'">'
										+ ' </a>'
										+ '<span class="productPrice">¥'
										+ goodsList[i].price
										+ '</span>'
										+ '<a class="productLink" href="product.jsp?goodsId='+goodsList[i].goodsId+'"> '
										+ goodsList[i].goodsName
										+ ' </a>';

								resultHtml += '   <div class="productInfo"> '
										+ '      <span class="monthDeal ">月成交 <span class="productDealNumber">300笔</span></span>'
										+ '      <span class="productReview">评价<span class="productReviewNumber">88</span></span>'
										+ '      <span class="wangwang"><img src="img/site/wangwang.png"></span>'
										+ '   </div></div>';
							}
							resultHtml += '</div><div style="clear:both"></div> ';
							$("#searchProducts").html(
									resultHtml);

							/* recTotal: result.pageTotal */
						} else if (result.success == '0') {
							var resultHtml = '<div class="noMatch">没有满足条件的产品<div> <div style="clear:both"></div>';
							$("#searchProducts").html(
									resultHtml);
						} else {
							new $.zui.Messager(result.msg, {
								type : 'warning', // 定义颜色主题
								placement : 'center' // 定义显示位置
							}).show();
						}
					},
					error : function(result) {
						new $.zui.Messager('系统繁忙,请稍候再试!', {
							type : 'warning', // 定义颜色主题
							placement : 'center' // 定义显示位置
						}).show();
					}
				});
	});
</script>
<div class="searchProducts" id="searchProducts">


	<div class="noMatch">没有满足条件的产品</div>



</div>