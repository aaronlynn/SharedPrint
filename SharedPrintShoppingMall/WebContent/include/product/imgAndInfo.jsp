<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<!DOCTYPE html>
<html lang="zh">
<head>
	<link href="css/jquery.exzoom.css" rel="stylesheet" type="text/css"/>
	<style>
	    #exzoom {
	        width: 400px;
			height: 300px;
			margin: 10px auto;
	    }
	</style>
</head>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/config.js?version=3"></script>
<script>
var goodId =<%=request.getParameter("goodsId")%>;
var token = "231231";
var userId =$.zui.store.get('userId');

$(document).ready(function() {				
		var param = "goodsId=" + goodId;
		var context = $("#contextPath1").val();
		var goodsBySearchURL = adminQueryUrl + goodsService
				+ "/queryInfo.action";
		console.log("goodsBySearchURL=" + goodsBySearchURL);
		console.log("param=" + param);
		$.ajax({
					url : goodsBySearchURL,
					type : 'post',
					beforeSend : function(xhr) {
						xhr.setRequestHeader("token", token);
						xhr.setRequestHeader("userId", userId);
					},
					header : {
						"token" : token,
						"userId" : userId
					},
					data : param,
					dataType : "JSON",
					success : function(result) {
						console.log(result.success);
						console.log(result.pageList);
						if (result.success == '1') {
							var goodsList = result.pageList;
							var price = goodsList[0].price + 200;

							var resultHtml1 =' <title>云打印商城  '+goodsList[0].goodsName+'</title>';
							
							 var img1=document.getElementById("image1");
							 img1.src = context+ '/img/goods/'+goodsList[0].Picture1;
							 
							 var img2=document.getElementById("image2");
							 img2.src = context+ '/img/goods/'+goodsList[0].Picture2;
							 
							 var img3=document.getElementById("image3");
							 img3.src = context+ '/img/goods/'+goodsList[0].Picture3;
							 
							 var img4=document.getElementById("image4");
							 img4.src = context+ '/img/goods/'+goodsList[0].Picture4;
							 
							 var img5=document.getElementById("image5");
							 img5.src = context+ '/img/goods/'+goodsList[0].Picture5;
							 
							 var img6=document.getElementById("image6");
							 img6.src = context+ '/img/goods/'+goodsList[0].Picture6;
							 
							
							
						var	resultHtml =  ' <div style="margin-top:20px;"></div>  <div class="productTitle">'
									+ goodsList[0].goodsName
									+ '</div>'
									+ '<div class="productSubTitle">【立即抢购！1号支付尾款。新品更享6期免息！】 '
									+ goodsList[0].goodsName
									+ '</div>'
									+

									' <div class="productPrice">'
									+ ' <div class="juhuasuan">'
									+ '    <span class="juhuasuanBig" >秒杀专场</span>'
									+ '    <span>此商品即将参加<span style="color:red;font-size:20px;">1元</span>秒杀，<span class="juhuasuanTime">1天19小时</span>后开始，</span>'
									+ ' </div>'
									+ ' <div class="productPriceDiv">'
									+ '    <div class="gouwujuanDiv">'
									+ '    <span class="originalPriceDesc">限时优惠</span> <span>   购买1-3件时享受单件价优惠10%，超出数量以结算价为准</span>'
									+

									'    </div>'
									+ '    <div class="originalDiv">'
									+ '        <span class="originalPriceDesc">价格</span>'
									+ '        <span class="originalPriceYuan">¥</span>'
									+ '        <span class="originalPrice">'
									+ price
									+ '        </span>'
									+ '    </div>'
									+ '    <div class="promotionDiv">'
									+ '        <span class="promotionPriceDesc">促销价 </span>'
									+ '        <span class="promotionPriceYuan">¥</span>'
									+ '        <span class="promotionPrice">'
									+ goodsList[0].price
									+ '        </span>              '
									+ '    </div>'
									+ '</div>'
									+ ' </div>'
									+ '<div class="productSaleAndReviewNumber">'
									+ '  <div>销量 <span class="redColor boldWord"> 20192</span></div>  '
									+ ' <div>累计评价 <span class="redColor boldWord"> 25</span></div>   '
									+ '</div>'
									+ '<div class="productNumber" style="margin-top:20px;">'
									+ ' <span>数量&nbsp;&nbsp;&nbsp;</span>'
									+ ' <span>'
									+ '    <span class="productNumberSettingSpan">'
									+ '    <input class="productNumberSetting" id="amount" type="text" value="1" style="text-align:center;background-color:white;">'
									+ '    </span>'
									+ '    <span class="arrow">'
									+ '        <a href="javascript:addAmount()" class="increaseNumber" id="amountPlus">'
									+ '        <span class="updown">'
									+ '                <img src="img/site/increase.png">'
									+ '        </span>'
									+ '        </a>'
									+ '       <span class="updownMiddle"> </span>'
									+ '       <a href="javascript:subtractAmount()"  class="decreaseNumber" id="amountSubtract">'
									+ '     <span class="updown">'
									+ '             <img src="img/site/decrease.png">'
									+ '     </span>'
									+ '     </a>'
									+

									' </span>'
									+

									' 件</span>'
									+ ' <span>库存498件</span>'
									+ '</div>'
									+ '<div class="serviceCommitment">'
									+ '<span class="serviceCommitmentDesc">服务承诺</span>'
									+ '<span class="serviceCommitmentLink">'
									+ '  <a href="#nowhere">正品保证</a>'
									+ ' <a href="#nowhere">极速退款</a>'
									+ '  <a href="#nowhere">赠运费险</a>'
									+ '  <a href="#nowhere">七天无理由退换</a>'
									+ ' </span>'
									+ ' </div>   '
									+'<div class="buyDiv" style="margin-top:30px;">'
									+ '<a class="buyLink" href="buy.jsp"><button class="buyButton" onclick="imdBuy()" >立即购买</button></a>'
									+ '<a href="#nowhere" class="addCartLink"><button class="addCartButton" onclick="addCart()"><span class="glyphicon glyphicon-shopping-cart"></span>加入购物车</button></a> '
									+ '</div> '
									+ '</div> ';

							$("#imgInimgAndInfo").append(resultHtml1);
							$("#infoInimgAndInfo").html(resultHtml);
						} else if (result.success == '0') {
							var resultHtml = '<div class="noMatch">没有满足条件的产品<div> <div style="clear:both"></div>';
							$("#searchProducts").html(resultHtml);
						} else {
							new $.zui.Messager(result.msg, {
								type : 'warning', // 定义颜色主题
								placement : 'center' // 定义显示位置
							}).show();
						}
					},
					error : function(result) {
						new $.zui.Messager('系统繁忙,请稍候再试!', {
							type : 'warning', // 定义颜色主题
							placement : 'center' // 定义显示位置
						}).show();
					}
				});				
	});
	//立即购买
	function imdBuy(){
		var addCartUrl = adminQueryUrl + cartService + imdBuyGoods;
		var goodsNumber =document.getElementById("amount").value;//商品数量
		var param = "goodsId=" + goodId;
		param += "&userId="+userId;
		$.ajax({
			url : addCartUrl,
			type : 'post',
			beforeSend : function(xhr) {
				xhr.setRequestHeader("token", token);
				xhr.setRequestHeader("userId", userId);
			},
			header : {
				"token" : token,
				"userId" : userId
			},
			data : param,
			dataType : "JSON",
			success:function(result){
				if(result.success == '1'){
					location.href="${pageContext.request.contextPath}/imdBuy.jsp?goodsName="+result.goodsMap.goodsName
							+"&goodsId="+result.goodsMap.goodsId+"&goodsPrice="+result.goodsMap.price
							+"&totalPrice="+result.goodsMap.price*goodsNumber
							+"&goodsNumber="+goodsNumber+"&goodsPicture="+result.goodsMap.Picture1;
				}else if(result.success == '0'){
					new $.zui.Messager('系统繁忙,请稍候再试!', {
						type : 'warning', // 定义颜色主题
						placement : 'center' // 定义显示位置
					}).show();
				}
			},//success
			error : function(result) {
					new $.zui.Messager("错误", {
						type : 'warning', // 定义颜色主题
						placement : 'center' // 定义显示位置
					}).show();
			  } 
		 })//ajax	
	}
	//商品详情加数量
	function addAmount()
	{
		var amount =document.getElementById("amount");
		if(amount.value < 100)
		{
			amount.value = parseInt(amount.value) + 1;
		}
		else
		{
			alert("最多购买100件！");
		}
		if(amount.value <1)
		{
			amount.value = 1;
		}
		
	}
	//商品详情减数量
	function subtractAmount()
	{
		var amount =document.getElementById("amount");
		if(amount.value > 1)
		{
			amount.value = parseInt(amount.value) - 1;
		}
		if(amount.value <1)
		{
			amount.value = 1;
		}
	}
	//添加购物车
	function addCart(){
		var addCartUrl = adminQueryUrl + cartService + addCartMethod;
		var param = "goodsId=" + goodId;
		
		var goodsNumber =document.getElementById("amount").value;
		param += "&userId="+userId+"&number="+goodsNumber;
		$.ajax({
			url : addCartUrl,
			type : 'post',
			beforeSend : function(xhr) {
				xhr.setRequestHeader("token", token);
				xhr.setRequestHeader("userId", userId);
			},
			header : {
				"token" : token,
				"userId" : userId
			},
			data : param,
			dataType : "JSON",
			success:function(result){
				if(result.success == '1'){
					new $.zui.Messager('加入购物车成功', {
						type : 'success', // 定义颜色主题
						placement : 'center' // 定义显示位置
					}).show();
				}else if(result.success == '0'){
					new $.zui.Messager('系统繁忙,请稍候再试!', {
						type : 'warning', // 定义颜色主题
						placement : 'center' // 定义显示位置
					}).show();
				}
			},//success
			error : function(result) {
					new $.zui.Messager("错误", {
						type : 'warning', // 定义颜色主题
						placement : 'center' // 定义显示位置
					}).show();
			  } 
		 })//ajax	
	}	
</script>
<body>
<div id="imgAndInfo" >
    <div class="imgInimgAndInfo" id="imgInimgAndInfo" style="margin-bottom: 40px;">
        <div class="htmleaf-container">
		<div class="exzoom" id="exzoom">
		    <!--大图区域-->
		    <div class="exzoom_img_box" style="background-color:white;" >
		        <ul class='exzoom_img_ul' id="imageTables">
		            <li><img  id="image1" src="" /></li>
					<li><img  id="image2" src="" /></li>
					<li><img  id="image3" src="" /></li>
		            <li><img  id="image4" src="" /></li>
					<li><img  id="image5" src="" /></li>
					<li><img  id="image6" src="" /></li>
		        </ul>
		    </div>
		    <!--缩略图导航-->
		    <div class="exzoom_nav" ></div>
		    <!--控制按钮-->
		    <p class="exzoom_btn">
		        <a href="javascript:void(0);" class="exzoom_prev_btn"> &lt; </a>
		        <a href="javascript:void(0);" class="exzoom_next_btn"> &gt; </a>
		    </p>
		</div>
	</div>
    </div>
    
    <div class="infoInimgAndInfo" id="infoInimgAndInfo">
    
    </div>
    
    <div style="clear:both"></div>

</div>
	<script src="js/jquery.exzoom.js"></script>
    	<script type="text/javascript">
	    $("#exzoom").exzoom({
	        autoPlay: false,
			navItemMargin: 2,
	    });//方法调用，务必在加载完后执行
	</script>
</body>
</html>