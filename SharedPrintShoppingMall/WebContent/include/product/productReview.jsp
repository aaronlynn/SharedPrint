<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>

<div class="productReviewDiv" id="productReviewDiv" style="margin-top: 60px;">
	<div class="productReviewTopPart">
		<a href="#nowhere" class="productReviewTopPartSelectedLink">商品详情</a> <a
			href="#nowhere" class="selected">累计评价 <span
			class="productReviewTopReviewLinkNumber">25</span>
		</a>
	</div>

	<div class="productReviewContentPart">
		<div class="productReviewItem">

              <div><div class="productReviewItemDesc">
				<div class="productReviewItemContent">物美价廉</div>
				<div class="productReviewItemDate">2019-10-23</div>
			</div>
			<div class="productReviewItemUserInfo">
				sah****A<span class="userInfoGrayPart">（匿名）</span>
			</div><div style="clear: both"></div></div>
			
			<div><div class="productReviewItemDesc">
				<div class="productReviewItemContent">还不错</div>
				<div class="productReviewItemDate">2019-10-25</div>
			</div>
			<div class="productReviewItemUserInfo">
				2ah****A<span class="userInfoGrayPart">（匿名）</span>
			</div><div style="clear: both"></div></div>
			
			<div><div class="productReviewItemDesc">
				<div class="productReviewItemContent">可以的</div>
				<div class="productReviewItemDate">2019-10-25</div>
			</div>
			<div class="productReviewItemUserInfo">
				22***A<span class="userInfoGrayPart">（匿名）</span>
			</div><div style="clear: both"></div></div>
			
			
			<div><div class="productReviewItemDesc">
				<div class="productReviewItemContent">服务很ok</div>
				<div class="productReviewItemDate">2019-10-22</div>
			</div>
			<div class="productReviewItemUserInfo">
				你h****A<span class="userInfoGrayPart">（匿名）</span>
			</div><div style="clear: both"></div></div>
			
			
			<div><div class="productReviewItemDesc">
				<div class="productReviewItemContent">下次再来</div>
				<div class="productReviewItemDate">2019-10-10</div>
			</div>
			<div class="productReviewItemUserInfo">
				体****A<span class="userInfoGrayPart">（匿名）</span>
			</div><div style="clear: both"></div></div>
			
			
			<div><div class="productReviewItemDesc">
				<div class="productReviewItemContent">推荐大家买，良心啊</div>
				<div class="productReviewItemDate">2019-10-25</div>
			</div>
			<div class="productReviewItemUserInfo">
				哈桑****<span class="userInfoGrayPart">（匿名）</span>
			</div><div style="clear: both"></div></div>
			
			
			<div><div class="productReviewItemDesc">
				<div class="productReviewItemContent">一次性买了10件，回头客</div>
				<div class="productReviewItemDate">2019-10-25</div>
			</div>
			<div class="productReviewItemUserInfo">
				天使**阿萨<span class="userInfoGrayPart">（匿名）</span>
			</div><div style="clear: both"></div></div>
			
			<div><div class="productReviewItemDesc">
				<div class="productReviewItemContent">一次性买了10件，回头客</div>
				<div class="productReviewItemDate">2019-10-25</div>
			</div>
			<div class="productReviewItemUserInfo">
				天使**阿萨<span class="userInfoGrayPart">（匿名）</span>
			</div><div style="clear: both"></div></div>
			
			<div><div class="productReviewItemDesc">
				<div class="productReviewItemContent">一次性买了1000件，回头客</div>
				<div class="productReviewItemDate">2019-10-24</div>
			</div>
			<div class="productReviewItemUserInfo">
				天使**阿萨<span class="userInfoGrayPart">（匿名）</span>
			</div><div style="clear: both"></div></div>
			<div><div class="productReviewItemDesc">
				<div class="productReviewItemContent">回头客</div>
				<div class="productReviewItemDate">2019-10-25</div>
			</div>
			<div class="productReviewItemUserInfo">
				天使**阿萨<span class="userInfoGrayPart">（匿名）</span>
			</div><div style="clear: both"></div></div>
			<div><div class="productReviewItemDesc">
				<div class="productReviewItemContent">21312客</div>
				<div class="productReviewItemDate">2019-10-25</div>
			</div>
			<div class="productReviewItemUserInfo">
				天使**阿萨<span class="userInfoGrayPart">（匿名）</span>
			</div><div style="clear: both"></div></div>
			<div><div class="productReviewItemDesc">
				<div class="productReviewItemContent">2222</div>
				<div class="productReviewItemDate">2019-10-25</div>
			</div>
			<div class="productReviewItemUserInfo">
				天使**阿萨<span class="userInfoGrayPart">（匿名）</span>
			</div><div style="clear: both"></div></div>
			


			

		</div>
	</div>

</div>