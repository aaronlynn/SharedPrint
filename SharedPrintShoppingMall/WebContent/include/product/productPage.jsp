<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
 

<div class="productPageDiv">
 <input type="hidden" id="contextPath1" 
		value="${pageContext.request.contextPath}" />
    <%@include file="imgAndInfo.jsp" %>
     
    <%@include file="productReview.jsp" %>
     
    <%@include file="productDetail.jsp" %>
</div>