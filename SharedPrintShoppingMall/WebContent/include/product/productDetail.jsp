<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/config.js?version=3"></script>
<script>
	$(function() {
		var goodId =<%=request.getParameter("goodsId")%>;
		var token = "231231";
		var userId = "1";
		var context = $("#contextPath1").val();
		var param = "goodsId=" + goodId;
		var goodsBySearchURL = adminQueryUrl + goodsService
				+ "/queryInfo.action";
		console.log("goodsBySearchURL=" + goodsBySearchURL);
		console.log("param=" + param);
		$.ajax({
					url : goodsBySearchURL,
					type : 'post',
					beforeSend : function(xhr) {
						xhr.setRequestHeader("token", token);
						xhr.setRequestHeader("userId", userId);
					},
					header : {
						"token" : token,
						"userId" : userId
					},
					data : param,
					dataType : "JSON",
					success : function(result) {
						console.log(result.success);
						console.log(result.pageList);
						if (result.success == '1') {
							var goodsList = result.pageList;

						
						var	resultHtml = '<div class="productDetailTopPart">'
									+ '     <a href="javascript:productDetailBtn()" class="productDetailTopPartSelectedLink selected"  >商品详情</a> '
									+ '      <a href="javascript:productReviewBtn()" id="productReview" class="productDetailTopReviewLink">累计评价 <span class="productDetailTopReviewLinkNumber">25</span> </a>'
									+ '   </div>'
									+

									'<div class="productParamterPart">'
									+ ' <div class="productParamter"></div>'
									+

									' <div class="productParamterList" style="font-size:20px;">'
									+ '         <span>'
									+ goodsList[0].remark
									+ '</span>'
									+ '   </div>'
									+ '    <div style="clear:both"></div>'
									+ ' </div>'
									+

									' <div class="productDetailImagesPart">'
									+ '             <img src="'+context+ '/img/goods/'+goodsList[0].Picture3+'"><img src="'+context+ '/img/goods/'+goodsList[0].Picture4+'">'
									+ '  </div>';

							$("#productDetailDiv").html(resultHtml);
						} else if (result.success == '0') {
							var resultHtml = '<div class="noMatch">没有满足条件的产品<div> <div style="clear:both"></div>';
							$("#productDetailDiv").html(resultHtml);
						} else {
							new $.zui.Messager(result.msg, {
								type : 'warning', // 定义颜色主题
								placement : 'center' // 定义显示位置
							}).show();
						}
					},
					error : function(result) {
						new $.zui.Messager('系统繁忙,请稍候再试!', {
							type : 'warning', // 定义颜色主题
							placement : 'center' // 定义显示位置
						}).show();
					}
				});
	});
	function productDetailBtn(){
		$("#productReviewDiv").hide();
		$("#productDetailDiv").show();
	}
	function productReviewBtn(){
		$("#productDetailDiv").hide();
		$("#productReviewDiv").show();
	}
</script>

<div class="productDetailDiv" id="productDetailDiv" style="margin-top: 60px;">

</div>