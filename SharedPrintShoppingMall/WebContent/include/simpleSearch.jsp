<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
 
<div >
    <a href="${contextPath}">
        <img id="simpleLogo" class="simpleLogo" style="margin-left: 80px" src="img/site/icon.jpg">   
    </a>
     
    <form action="searchResult.jsp" method="post" >
    <div class="simpleSearchDiv pull-right" style="margin-top: 20px;">
        <input type="text" placeholder=" 国考考题 英语四级" name="goodsName">
        <button class="searchButton" type="submit" >搜索</button>
        <div class="searchBelow">
			<span> 
			    <a href='searchResult.jsp?goodBelowName=四级' style="color: gray;">四级 </a><span>|</span>
			</span>
			<span> 
			    <a href="searchResult.jsp?goodBelowName=六级"style="color: gray;">六级</a><span>|</span>
			</span>
			<span> 
			    <a href="searchResult.jsp?goodBelowName=钢笔" style="color: gray;">钢笔</a><span>|</span>
			</span>
			<span> 
			    <a href="searchResult.jsp?goodBelowName=CPA" style="color: gray;">CPA</a> 
			</span>
		</div>
    </div>
    </form>
    <div style="clear:both"></div>
</div>