<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>


<input type="hidden" id="contextPath1" 
		value="${pageContext.request.contextPath}" />
<form action="searchResult.jsp" method="post">
	<a href="${contextPath}"> <img id="logo" src="img/site/icon.jpg"
		class="logo" style="width: 230px; height: 140px; margin-left: 200px">
	</a>

	<div class="searchDiv" style="background-color: #87CEEB;">
		<input name="goodsName" type="text" placeholder=" 2020考研资料  英语六级 ">
		<button type="submit" class="searchButton"
			style="background-color: #87CEEB;">搜索</button>
		<div class="searchBelow">
			<span> 
			    <a href='searchResult.jsp?goodBelowName=公务员考试' style="color: gray;">公务员考试 </a> <span>|</span>
			</span>
			<span> 
			    <a href="searchResult.jsp?goodBelowName=司法考试" style="color: gray;">司法考试</a> <span>|</span>
			</span>
			<span> 
			    <a href="searchResult.jsp?goodBelowName=计算机二级" style="color: gray;">计算机二级</a> <span>|</span>
			</span>
			<span> 
			    <a href="searchResult.jsp?goodBelowName=CPA" style="color: gray;">CPA</a> 
			</span>
		</div>
	</div>
</form>
