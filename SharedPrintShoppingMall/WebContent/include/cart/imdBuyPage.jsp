<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/config.js?version=3"></script>
<script>
//商户号 订货单生成
var userId =$.zui.store.get('userId');
var token = "231231"; 
$(function(){
	var vNow = new Date();
	var sNow = "";
	sNow += String(vNow.getFullYear());
	sNow += String(vNow.getMonth() + 1);
	sNow += String(vNow.getDate());
	sNow += String(vNow.getHours());
	sNow += String(vNow.getMinutes());
	sNow += String(vNow.getSeconds());
	sNow += String(vNow.getMilliseconds());
	document.getElementById("WIDout_trade_no").value =  sNow;
	document.getElementById("WIDsubject").value = "商品测试";
	document.getElementById("WIDtotal_amount").value = "1999.99";
	
	
	var price= <%=request.getParameter("goodsPrice") %>;
	var goodsId = <%=request.getParameter("goodsId")%>;
	var goodsNumber = <%=request.getParameter("goodsNumber")%>;
	var totalPrice= <%=request.getParameter("totalPrice") %>;
	$("#price").val(totalPrice);
	$("#goodsId").val(goodsId);
	$("#goodsNumber").val(goodsNumber);
	//提交订单
	$("#submitOrderBtn").click(function(){
		$("#recForm").validate({
			rules:{	
				address:{
					"required":true
				},
				receiver:{
					"required":true
				},
				mobile:{
					"required":true
				}
			},	
			submitHandler:function(form){	
				$.ajax({
					url:adminQueryUrl + orderService + "/imdAddOrder.action",
					type:'post',
					dataType:'JSON',
					beforeSend:function(xhr){//设置请求头信息
						xhr.setRequestHeader("token",token);
						xhr.setRequestHeader("userId",userId);
					},
					headers:{'token':token,'userId':userId},
					data:$(form).serialize(),
					success:function(data){
						//('data='+data);
						if (data.success == '1') {
							var payParam = "WIDout_trade_no="+data.orderCode+"&WIDtotal_amount="+totalPrice
										 + "&WIDsubject="+data.goodsName;
							if(data.number =='2'){
								payParam +=" 等多件";
							}
							payParam +="&WIDbody=fadsfas";
							location.href="http://localhost:8080/SharedPrintService/api/orderInfo/alipay.action?"+payParam;
		                }
		                else{
		                	new $.zui.Messager("提交失败", {
		    				    type: 'warning',
		    				    placement:'center'
		    				}).show();
		                }
					},
					error:function(e){
						new $.zui.Messager('系统繁忙,请稍候再试!', {
	    				    type: 'warning',
	    				    placement:'center'
	    				}).show();
					}
				});//ajax
			 }//submitHandler
		 });//validate
  	});
		
});
</script>
<div class="buyPageDiv">

  <form name="recForm" id="recForm"  method="post" 
			target="rfFrame">
   
    <div class="buyFlow">
        <img class="pull-left" style="width: 200px;height:130px;" src="img/site/icon.jpg">
        <img class="pull-right" src="img/site/buyflow.png">
        <div style="clear:both"></div>
    </div>
    <div class="address">
        <div class="addressTip">输入收货地址</div>
        <div>
         
            <table class="addressTable">
                <tr>
                    <td class="firstColumn">详细地址<span class="redStar">*</span></td>
                     
                    <td><textarea name="address" placeholder="建议您如实填写详细收货地址，例如街道名称，门牌号码，楼层和房间号等信息"></textarea></td>
                </tr>
                <tr>
                    <td>邮政编码</td>
                    <td><input  name="post" placeholder="如果您不清楚邮递区号，请填写000000" type="text"></td>
                </tr>
                <tr>
                    <td>收货人姓名<span class="redStar">*</span></td>
                    <td><input  name="receiver"  placeholder="长度不超过25个字符" type="text"></td>
                </tr>
                <tr>
                    <td>手机号码 <span class="redStar">*</span></td>
                    <td><input name="mobile"  placeholder="请输入11位手机号码" type="text"></td>
                </tr>
            </table>
             
        </div>
 
    </div>
    <div class="productList">
        <div class="productListTip">确认订单信息</div>
         
        <table class="productListTable">
            <thead>
                <tr>
                    <th colspan="2" class="productListTableFirstColumn">
                        <img class="tmallbuy" src="img/site/tmallbuy.png">
                        <a class="marketLink" href="#nowhere">店铺：天猫店铺</a>
                        <a class="wangwanglink" href="#nowhere"> <span class="wangwangGif"></span> </a>
                    </th>
                    <th>单价</th>
                    <th>数量</th>
                    <th>小计</th>
                    <th>配送方式</th>
                </tr>
                <tr class="rowborder">
                    <td  colspan="2" ></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </thead>
            <tbody class="productListTableTbody">
                    <tr class="orderItemTR">
                        <td class="orderItemFirstTD"><img class="orderItemImg" src="${pageContext.request.contextPath}/img/goods/<%=request.getParameter("goodsPicture")%>"></td>
                        <td class="orderItemProductInfo">
                        <a  href="product.jsp?goodsId=<%=request.getParameter("goodsId") %>" class="orderItemProductLink">
                            <%=request.getParameter("goodsName") %>
                        </a>
                         
                            <img src="img/site/creditcard.png" title="支持信用卡支付">
                            <img src="img/site/7day.png" title="消费者保障服务,承诺7天退货">
                            <img src="img/site/promise.png" title="消费者保障服务,承诺如实描述">
                         
                        </td>
                        <td>
                         
                        <span class="orderItemProductPrice">￥<%=request.getParameter("goodsPrice")%></span>
                        </td>
                        <td>
                        <span class="orderItemProductNumber"><%=request.getParameter("goodsNumber") %></span>
                        </td>
                        <td><span class="orderItemUnitSum">
                        ￥<%=request.getParameter("totalPrice") %>
                        </span></td>
                        <td rowspan="5"  class="orderItemLastTD">
                        <label class="orderItemDeliveryLabel">
                            <input type="radio" value="" checked="checked">
                            普通配送
                        </label>
                         
                        <select class="orderItemDeliverySelect" class="form-control">
                            <option>快递 免邮费</option>
                        </select>
 
                        </td>
                         
                    </tr>                             
            </tbody>
             
        </table>
        <div class="orderItemSumDiv">
            <div class="pull-left">
                <span class="leaveMessageText">给卖家留言:</span>
                <span>
                    <img class="leaveMessageImg" src="img/site/leaveMessage.png">
                </span>
                <span class="leaveMessageTextareaSpan">
                    <textarea name="userMessage" class="leaveMessageTextarea"></textarea>
                    <div>
                        <span>还可以输入200个字符</span>
                    </div>
                </span>
            </div>
             
            <span class="pull-right">店铺合计(含运费): ￥<%=request.getParameter("totalPrice") %></span>
        </div>
         
    </div>
 
    <div class="orderItemTotalSumDiv">
        <div class="pull-right">
            <span>实付款：</span>
            <span class="orderItemTotalSumSpan">￥<%=request.getParameter("totalPrice") %></span>
        </div>
    </div>   
						<input id="WIDout_trade_no" name="WIDout_trade_no" type="hidden" />
						<input id="WIDsubject" name="WIDsubject" type="hidden"/>
						<input id="WIDtotal_amount" name="WIDtotal_amount" type="hidden"/>
						<input id="WIDbody" name="WIDbody" type="hidden"	/>
					
     
    <div class="submitOrderDiv">
    		<input type="hidden" id="price" name="price">
    		<input type="hidden" id="goodsId" name="goodsId">
    		<input type="hidden" id="goodsNumber" name="goodsNumber">
            <button type="submit" id="submitOrderBtn" class="submitOrderButton">提交订单</button>
    </div>
  </form> 
  <iframe id="rfFrame" name="rfFrame" src="about:blank" style="display:none;"></iframe>   	 
</div>