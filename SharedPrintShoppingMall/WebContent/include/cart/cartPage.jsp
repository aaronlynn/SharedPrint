<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/config.js?version=3"></script>     
<script>
var deleteOrderItem = false;
var deleteOrderItemid = 0;
var userId =$.zui.store.get('userId');
var token = "231231";
$(function(){
	var queryGoodsListUrl = adminQueryUrl + cartService + queryPageListMethod;
 	var param = "userId="+userId;
 	//var cartGoodsList = [];
 	$.ajax({
		url : queryGoodsListUrl,
		type : 'post',
		beforeSend : function(xhr) {
			xhr.setRequestHeader("token", token);
			xhr.setRequestHeader("userId", userId);
		},
		header : {
			"token" : token,
			"userId" : userId
		},
		data : param,
		dataType : "JSON",
		success:function(result){
			var resultHtml = '<table class="cartProductTable">'
            	+'<thead>'
                +'<tr>'
                +'<th class="selectAndImage">'
                +'<img selectit="false" class="selectAllItem" onclick="selectAllItem()" src="img/site/cartNotSelected.png"> '             
                +'全选'
    			+'</th>'
    			+'<th>商品信息</th>'
    			+'<th>单价</th>'
    			+'<th>数量</th>'
    			+'<th width="120px">金额</th>'
    			+'<th class="operation">操作</th>'
    			+'</tr>'
    			+'</thead>'
    			+'<tbody>';
			if(result.success ='1'){					
				$.each(result.cartGoodsList, function(i, item){				
                	resultHtml +='<tr oiid='+item.Ordergoods_ID+' class="cartProductItemTR"> <!-- 购物车商品信息行 -->'
                        		+'<td>'
                    			+'<img id="cartProductItemIfSelected'+item.Goods_ID+'" pid="'+item.Goods_ID+'" selectit="false" oiid="'+item.Ordergoods_ID+'" class="cartProductItemIfSelected" onclick="cartProductItemIfSelected('+item.Goods_ID+')" src="img/site/cartNotSelected.png">'
                    			+'<a style="display:none" href="#nowhere"><img src="img/site/cartSelected.png"></a>'
                   				+'<img class="cartProductImg"  src="${pageContext.request.contextPath}/img/goods/'+item.Picture1+'"> <!-- 商品图片信息 -->'
                				+'</td>'
                				+'<td>'
                    			+'<div class="cartProductLinkOutDiv">'
                       			+'<a href="product.jsp?goodsId='+item.Goods_ID+'" class="cartProductLink">'+item.Goods_Name+'</a> <!-- 购物车商品链接 -->'
                       			+'<div class="cartProductLinkInnerDiv">'
                       			+'<img src="img/site/creditcard.png" title="支持信用卡支付">&nbsp&nbsp'
                       			+'<img src="img/site/7day.png" title="消费者保障服务,承诺7天退货">&nbsp&nbsp'
                       			+'<img src="img/site/promise.png" title="消费者保障服务,承诺如实描述">&nbsp&nbsp'
                       			+'</div>'
                       			+'</div>'
                       			+'</td>'
                       			+'<td>   <!-- 购物车商品单价 -->'
                       			+'<span class="cartProductItemOringalPrice">￥'+(item.Goods_Unit_Price*1.6)+'</span>  <!-- 原始价格 -->'
                       			+'<span  class="cartProductItemPromotionPrice">￥'+item.Goods_Unit_Price+'</span> <!-- 现价 -->'
                       			+'</td>'
                       			+'<td>     <!-- 商品数量修改 -->'
                       			+'<div class="cartProductChangeNumberDiv">'
                       			+'<span class="hidden orderItemStock " pid="'+item.Goods_ID+'">'+item.Goods_Count+'</span>'
                       			+'<span class="hidden orderItemPromotePrice " pid="'+item.Goods_ID+'">'+item.Goods_Unit_Price+'</span>'
                       			+'<a pid="'+item.Goods_ID+'" class="numberMinus" onclick="numberMinus('+item.Goods_ID+')" href="#nowhere">-</a>  <!-- 数量减少-->'
                       			+'<input id="goodsNumber'+item.Goods_ID+'" pid="'+item.Goods_ID+'" oiid="'+item.Ordergoods_ID+'" class="orderItemNumberSetting" autocomplete="off" value="'+item.Goods_Count+'">'
                       			+'<a stock="1000" pid="'+item.Goods_ID+'" onclick="numberPlus('+item.Goods_ID+')" class="numberPlus" href="#nowhere">+</a> <!-- 数量增加 -->'
                       			+'</div>'                  
                       			+'</td>'
                       			+' <td>'
                       			+'<span class="cartProductItemSmallSumPrice" oiid="'+item.Ordergoods_ID+'" pid="'+item.Goods_ID+'" >'                   			
                       			+'￥'+(item.Goods_Unit_Price*item.Goods_Count)+'</span>'
                       			+'</td>'
                       			+'<td>'
                       			+'<a class="deleteOrderItem" oiid="'+item.Ordergoods_ID+'" onclick="deleteOrderGoods('+item.Ordergoods_ID+')"  href="#nowhere">删除</a>'
                       			+'</td>'
                       			+'</tr>';
                })
			}else if(result.success ='0'){
				new $.zui.Messager("错误", {
					type : 'warning', // 定义颜色主题
					placement : 'center' // 定义显示位置
				}).show();
			}
			resultHtml += '</tbody>'
		        	  	 +'</table>';
		    $("#cartProductList").html(resultHtml);
		},
		error:function(result){
			new $.zui.Messager("系统繁忙，请稍后再试", {
				type : 'warning', // 定义颜色主题
				placement : 'center' // 定义显示位置
			}).show();
		}
 	});
    /*$("a.deleteOrderItem").click(function(){
        deleteOrderItem = false;
        var oiid = $(this).attr("oiid")
        deleteOrderItemid = oiid;
        $("#deleteConfirmModal").modal('show');   
    });
    $("button.deleteConfirmButton").click(function(){
        deleteOrderItem = true;
        $("#deleteConfirmModal").modal('hide');
    });
     
    $('#deleteConfirmModal').on('hidden.bs.modal', function (e) {
        if(deleteOrderItem){
            var page="foredeleteOrderItem";
            $.post(
                    page,
                    {"oiid":deleteOrderItemid},
                    function(result){
                        if("success"==result){
                            $("tr.cartProductItemTR[oiid="+deleteOrderItemid+"]").hide();
                        }
                        else{
                            location.href="login.jsp";
                        }
                    }
                );          
        }
    })*/ 
     
    /*$("img.cartProductItemIfSelected").click(function(){
        var selectit = $(this).attr("selectit")
        if("selectit"==selectit){
            $(this).attr("src","img/site/cartNotSelected.png");
            $(this).attr("selectit","false")
            $(this).parents("tr.cartProductItemTR").css("background-color","#fff");
        }
        else{
            $(this).attr("src","img/site/cartSelected.png");
            $(this).attr("selectit","selectit")
            $(this).parents("tr.cartProductItemTR").css("background-color","#FFF8E1");
        }
        syncSelect();
        syncCreateOrderButton();
        calcCartSumPriceAndNumber();
    });*/
   $("img.selectAllItem").click(function(){
        var selectit = $(this).attr("selectit")
        if("selectit"==selectit){
            $("img.selectAllItem").attr("src","img/site/cartNotSelected.png");
            $("img.selectAllItem").attr("selectit","false")
            $(".cartProductItemIfSelected").each(function(){
                $(this).attr("src","img/site/cartNotSelected.png");
                $(this).attr("selectit","false");
                $(this).parents("tr.cartProductItemTR").css("background-color","#fff");
            });        
        }
        else{
            $("img.selectAllItem").attr("src","img/site/cartSelected.png");
            $("img.selectAllItem").attr("selectit","selectit")
            $(".cartProductItemIfSelected").each(function(){
                $(this).attr("src","img/site/cartSelected.png");
                $(this).attr("selectit","selectit");
                $(this).parents("tr.cartProductItemTR").css("background-color","#FFF8E1");
            });            
        }
        syncCreateOrderButton();
        calcCartSumPriceAndNumber();        
    });
     
    $(".orderItemNumberSetting").keyup(function(){
        var pid=$(this).attr("pid");
        var stock= $("span.orderItemStock[pid="+pid+"]").text();
        var price= $("span.orderItemPromotePrice[pid="+pid+"]").text();
         
        var num= $(".orderItemNumberSetting[pid="+pid+"]").val();
        num = parseInt(num);
        if(isNaN(num))
            num= 1;
        if(num<=0)
            num = 1;
        if(num>stock)
            num = stock;        
        syncPrice(pid,num,price);
    });
 
    /*$(".numberPlus").click(function(){
         
        var pid=$(this).attr("pid");
        var stock= $("span.orderItemStock[pid="+pid+"]").text();
        var price= $("span.orderItemPromotePrice[pid="+pid+"]").text();
        var num= $(".orderItemNumberSetting[pid="+pid+"]").val();
 
        num++;
        if(num>stock)
            num = stock;
        syncPrice(pid,num,price);
    });*/
    /*$(".numberMinus").click(function(){
        var pid=$(this).attr("pid");
        var stock= $("span.orderItemStock[pid="+pid+"]").text();
        var price= $("span.orderItemPromotePrice[pid="+pid+"]").text();
         
        var num= $(".orderItemNumberSetting[pid="+pid+"]").val();
        --num;
        if(num<=0)
            num=1;
        syncPrice(pid,num,price);
    });*/
     
    $("button.createOrderButton").click(function(){  //结算按钮
        var params = "";   	
    	var updateOrderGoodsNumberURL = adminQueryUrl + cartService + updateOrderGoodsNumber; 
        $(".cartProductItemIfSelected").each(function(){        	
            if("selectit"==$(this).attr("selectit")){
            	var param = "";
                var oiid = $(this).attr("oiid");
                var pid = $(this).attr("pid");
                params += ","+oiid;
                param +="orderGoodsId="+oiid
                	   +"&goodsNumber="+$("#goodsNumber"+pid).val();
                $.ajax({
            		url : updateOrderGoodsNumberURL,
            		type : 'post',
            		beforeSend : function(xhr) {
            			xhr.setRequestHeader("token", token);
            			xhr.setRequestHeader("userId", userId);
            		},
            		header : {
            			"token" : token,
            			"userId" : userId
            		},
            		data : param,
            		dataType : "JSON",
            		success:function(result){
            			if(result.success =='0'){
            				new $.zui.Messager("错误1", {
            					type : 'warning', // 定义颜色主题
            					placement : 'center' // 定义显示位置
            				}).show();
            			}
            		},
            		error:function(result){
            			new $.zui.Messager("错误2", {
        					type : 'warning', // 定义颜色主题
        					placement : 'center' // 定义显示位置
        				}).show();
            		}
                });
            }
        });
        params = params.substring(1);
        var cartSumPrice = $("span.cartSumPrice").html();
        cartSumPrice = cartSumPrice.replace(/￥/g, "");
        cartSumPrice = cartSumPrice.replace(/,/g, "");
        location.href="${pageContext.request.contextPath}/buy.jsp?params="+params
        		     +"&cartSumPrice="+cartSumPrice;
    });
     
})
function numberPlus(id){
	var pid=id;
    /*var stock= $("span.orderItemStock[pid="+pid+"]").text();*/
    var price= $("span.orderItemPromotePrice[pid="+pid+"]").text();
    var num= $("#goodsNumber"+id).val();
    num++; 
    $("#goodsNumber"+id).val(num);  
    syncPrice(pid,num,price);
}

function numberMinus(id){
	var pid=id;
   /* var stock= $("span.orderItemStock[pid="+pid+"]").text();*/
    var price= $("span.orderItemPromotePrice[pid="+pid+"]").text();     
    var num= $("#goodsNumber"+id).val();
    --num;
    if(num<=0)
        num=1;
    $("#goodsNumber"+id).val(num);
    syncPrice(pid,num,price);
}

function deleteOrderGoods(id){
	var url = adminQueryUrl + cartService + "/deleteOrderGoods.action";
	var param = "orderGoodsId="+id;
	$.ajax({
			url : url,
			type : 'post',
			beforeSend : function(xhr) {
				xhr.setRequestHeader("token", token);
				xhr.setRequestHeader("userId", userId);
			},
			header : {
				"token" : token,
				"userId" : userId
			},
			data : param,
			dataType : "JSON",
			success : function(result) {
				if(result.success == '1'){
					location.reload();
				}else if (result.success == '0') {
					new $.zui.Messager("错误1", {
						type : 'warning', // 定义颜色主题
						placement : 'center' // 定义显示位置
					}).show();
				}
			},
			error : function(result) {
				new $.zui.Messager("错误2", {
					type : 'warning', // 定义颜色主题
					placement : 'center' // 定义显示位置
				}).show();
			}
		})
	}
	function selectAllItem() {
		var selectit = $(this).attr("selectit")
		if ("selectit" == selectit) {
			$("img.selectAllItem").attr("src", "img/site/cartNotSelected.png");
			$("img.selectAllItem").attr("selectit", "false")
			$(".cartProductItemIfSelected").each(
					function() {
						$(this).attr("src", "img/site/cartNotSelected.png");
						$(this).attr("selectit", "false");
						$(this).parents("tr.cartProductItemTR").css(
								"background-color", "#fff");
					});
		} else {
			$("img.selectAllItem").attr("src", "img/site/cartSelected.png");
			$("img.selectAllItem").attr("selectit", "selectit")
			$(".cartProductItemIfSelected").each(
					function() {
						$(this).attr("src", "img/site/cartSelected.png");
						$(this).attr("selectit", "selectit");
						$(this).parents("tr.cartProductItemTR").css(
								"background-color", "#FFF8E1");
					});
		}
		syncCreateOrderButton();
		calcCartSumPriceAndNumber();
	}

	function cartProductItemIfSelected(id) {
		var selectit = $("#cartProductItemIfSelected" + id).attr("selectit");
		if ("selectit" == selectit) {
			$("#cartProductItemIfSelected" + id).attr("src",
					"img/site/cartNotSelected.png");
			$("#cartProductItemIfSelected" + id).attr("selectit", "false");
			$("#cartProductItemIfSelected" + id)
					.parents("tr.cartProductItemTR").css("background-color",
							"#fff");
		} else {
			$("#cartProductItemIfSelected" + id).attr("src",
					"img/site/cartSelected.png");
			$("#cartProductItemIfSelected" + id).attr("selectit", "selectit");
			$("#cartProductItemIfSelected" + id)
					.parents("tr.cartProductItemTR").css("background-color",
							"#FFF8E1");
		}
		syncSelect();
		syncCreateOrderButton();
		calcCartSumPriceAndNumber();
	}

	function syncCreateOrderButton() {
		var selectAny = false;
		$(".cartProductItemIfSelected").each(function() { //总结算界面显示
			if ("selectit" == $(this).attr("selectit")) {
				selectAny = true;
			}
		});

		if (selectAny) {
			$("button.createOrderButton").css("background-color", "#C40000");
			$("button.createOrderButton").removeAttr("disabled");
		} else {
			$("button.createOrderButton").css("background-color", "#AAAAAA");
			$("button.createOrderButton").attr("disabled", "disabled");
		}

	}
	function syncSelect() {
		var selectAll = true;
		$(".cartProductItemIfSelected").each(function() {
			if ("false" == $(this).attr("selectit")) {
				selectAll = false;
			}
		});

		if (selectAll)
			$("img.selectAllItem").attr("src", "img/site/cartSelected.png");
		else
			$("img.selectAllItem").attr("src", "img/site/cartNotSelected.png");

	}
	function calcCartSumPriceAndNumber() {
		var sum = 0;
		var totalNumber = 0;
		$("img.cartProductItemIfSelected[selectit='selectit']").each(
				function() {
					var oiid = $(this).attr("oiid");
					var price = $(
							".cartProductItemSmallSumPrice[oiid=" + oiid + "]")
							.text();
					price = price.replace(/,/g, "");
					price = price.replace(/￥/g, "");
					sum += new Number(price);

					var num = $(".orderItemNumberSetting[oiid=" + oiid + "]")
							.val();
					totalNumber += new Number(num);

				});

		$("span.cartSumPrice").html("￥" + formatMoney(sum));
		$("span.cartTitlePrice").html("￥" + formatMoney(sum));
		$("span.cartSumNumber").html(totalNumber);
	}
	function syncPrice(pid, num, price) {
		$(".orderItemNumberSetting[pid=" + pid + "]").val(num);
		var cartProductItemSmallSumPrice = formatMoney(num * price);
		$(".cartProductItemSmallSumPrice[pid=" + pid + "]").html(
				"￥" + cartProductItemSmallSumPrice);
		calcCartSumPriceAndNumber();

		/*var page = "forechangeOrderItem";
		$.post(
		        page,
		        {"pid":pid,"number":num},
		        function(result){
		            if("success"!=result){
		                location.href="login.jsp";
		            }
		        }
		    );*/

	}
</script>
 
<title>购物车</title>
<div class="cartDiv">
    <div class="cartTitle pull-right">
       
    </div>
     
    <div class="cartProductList" id="cartProductList">       
    </div>
     
    <div class="cartFoot">
        <img selectit="false" class="selectAllItem" src="img/site/cartNotSelected.png">
        <span>全选</span>
<!--         <a href="#">删除</a> -->
         
        <div class="pull-right">
            <span>已选商品 <span class="cartSumNumber" >0</span> 件</span>            
            <span>合计 (不含运费): </span>
            <span class="cartSumPrice" >￥0.00</span>
            <button class="createOrderButton" disabled="disabled" >结  算</button>
        </div>
    </div>
</div>