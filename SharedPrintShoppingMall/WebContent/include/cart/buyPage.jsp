<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/config.js?version=3"></script>

<script>
//商户号 订货单生成
var userId =$.zui.store.get('userId');
var token = "231231"; 
$(function(){
	var vNow = new Date();
	var sNow = "";
	sNow += String(vNow.getFullYear());
	sNow += String(vNow.getMonth() + 1);
	sNow += String(vNow.getDate());
	sNow += String(vNow.getHours());
	sNow += String(vNow.getMinutes());
	sNow += String(vNow.getSeconds());
	sNow += String(vNow.getMilliseconds());
	document.getElementById("WIDout_trade_no").value =  sNow;
	document.getElementById("WIDsubject").value = "商品测试";
	document.getElementById("WIDtotal_amount").value = "1999.99";
	
	
	var queryGoodsListUrl = adminQueryUrl + cartService + queryPageListMethod;
	console.log("queryGoodsListUrl="+queryGoodsListUrl);
 	var param = "userId="+userId;
 	var params = "<%=request.getParameter("params")%>";
 	var totalPrice = "<%=request.getParameter("cartSumPrice")%>";
 	var price = new Number(totalPrice);  //转换陈数字格式
 	$("#price").val(price); //商品总额
 	$("#userId").val(userId);
 	$("#orderGoodsIds").val(params);
 	
 	param +="&orderGoodsIds="+params; //获取选中的购物车商品ID
 	//var cartGoodsList = [];
 	$.ajax({
		url : queryGoodsListUrl,
		type : 'post',
		beforeSend : function(xhr) {
			xhr.setRequestHeader("token", token);
			xhr.setRequestHeader("userId", userId);
		},
		header : {
			"token" : token,
			"userId" : userId
		},
		data : param,
		dataType : "JSON",
		success:function(result){
			var resultHtml ='<div class="productListTip">确认订单信息</div>'
						  + '<table class="productListTable">'
            			  + '<thead>'
            			  + '<tr>'
           				  + '<th colspan="2" class="productListTableFirstColumn">'
          				  + '<img class="tmallbuy" src="img/site/tmallbuy.png">'
           				  + '<a class="marketLink" href="#nowhere">店铺：天猫店铺</a>'
           				  + '<a class="wangwanglink" href="#nowhere"> <span class="wangwangGif"></span> </a>'
           				  + '</th>'
           				  + '<th>单价</th>'
           				  + '<th>数量</th>'
           				  + '<th>小计</th>'
           				  + '<th>配送方式</th>'
           				  + '</tr>'
           				  + '<tr class="rowborder">'
           				  + '<td  colspan="2" ></td>'
           				  + '<td></td>'
           				  + '<td></td>'
           				  + '<td></td>'
           				  + ' <td></td>'
           				  + '</tr>'
           				  + '</thead>'
           				  + '<tbody class="productListTableTbody">';
			if(result.success ='1'){					
				$.each(result.cartGoodsList, function(i, item){				
                	 resultHtml += '<tr class="orderItemTR">'
		                	   	+ '<td class="orderItemFirstTD"><img class="orderItemImg" src="${pageContext.request.contextPath}/img/goods/'+item.Picture1+'"></td>'
		                		+ '<td class="orderItemProductInfo">'
		                		+ '<a  href="product.jsp?goodsId='+item.Goods_ID+'" class="orderItemProductLink">'
		                		+ item.Goods_Name
		                		+ '</a>'
		                		+ '<img src="img/site/creditcard.png" title="支持信用卡支付">'
		                		+ '<img src="img/site/7day.png" title="消费者保障服务,承诺7天退货">'
		                		+ ' <img src="img/site/promise.png" title="消费者保障服务,承诺如实描述">'		                		
		                		+ '</td>'
		                		+ '<td>'
		                		+ '<span class="orderItemProductPrice">￥'+item.Goods_Unit_Price+'</span>'
		                		+ '</td>'
		                		+ '<td>'
		                		+ '<span class="orderItemProductNumber">'+item.Goods_Count+'</span>'
		                		+ '</td>'
		                		+ '<td><span class="orderItemUnitSum">'
		                		+ '￥'+(item.Goods_Count*item.Goods_Unit_Price)+''
		                		+ '</span></td>'
		                		+ '<td class="orderItemLastTD">'
		                		+ '<label class="orderItemDeliveryLabel">'
		                		+ '<input type="radio" value="" checked="checked">'
		                		+ '普通配送'
		                		+ '</label>&nbsp&nbsp'                        
		                		+ '<select class="orderItemDeliverySelect" class="form-control">'
		                		+ '<option>快递 免邮费</option>'
		                		+ '</select>'
		                		+ '</td>'
                         		+ '</tr>';
                })
			}else if(result.success ='0'){
				new $.zui.Messager("错误", {
					type : 'warning', // 定义颜色主题
					placement : 'center' // 定义显示位置
				}).show();
			}
			resultHtml += '</tbody>'
		        	    + '</table>';
		    $("#productList").html(resultHtml);
		},
		error:function(result){
			new $.zui.Messager("系统繁忙，请稍后再试", {
				type : 'warning', // 定义颜色主题
				placement : 'center' // 定义显示位置
			}).show();
		}
 	});
 	//提交订单
	$("#submitOrderBtn").click(function(){
		$("#recForm").validate({
			rules:{	
				address:{
					"required":true
				},
				receiver:{
					"required":true
				},
				mobile:{
					"required":true
				}
			},	
			submitHandler:function(form){	
				$.ajax({
					url:adminQueryUrl + orderService + addOrderMethod,
					type:'post',
					dataType:'JSON',
					beforeSend:function(xhr){//设置请求头信息
						xhr.setRequestHeader("token",token);
						xhr.setRequestHeader("userId",userId);
					},
					headers:{'token':token,'userId':userId},
					data:$(form).serialize(),
					success:function(data){
						//('data='+data);
						if (data.success == '1') {
							var payParam = "WIDout_trade_no="+data.orderCode+"&WIDtotal_amount="+price
										 + "&WIDsubject="+data.goodsName;
							if(data.number =='2'){
								payParam +=" 等多件";
							}
							payParam +="&WIDbody=fadsfas";
							location.href="http://localhost:8080/SharedPrintService/api/orderInfo/alipay.action?"+payParam;
		                }
		                else{
		                	new $.zui.Messager(date.msg, {
		    				    type: 'warning',
		    				    placement:'center'
		    				}).show();
		                }
					},
					error:function(e){
						new $.zui.Messager('系统繁忙,请稍候再试!', {
	    				    type: 'warning',
	    				    placement:'center'
	    				}).show();
					}
				});//ajax
			 }//submitHandler
		 });//validate
  	});
})
</script>
<div class="buyPageDiv">
	<form id="recForm" name="alipayment" method="post" target="_self">
		<div class="buyFlow">
			<img class="pull-left" style="width: 200px; height: 130px;"
				src="img/site/icon.jpg"> <img class="pull-right"
				src="img/site/buyflow.png">
			<div style="clear: both"></div>
		</div>
		<div class="address">
			<div class="addressTip">输入收货地址</div>
			<div>

				<table class="addressTable">
					<tr>
						<td class="firstColumn">详细地址<span class="redStar">*</span></td>
						<td><textarea id="address" name="address"
								placeholder="建议您如实填写详细收货地址，例如街道名称，门牌号码，楼层和房间号等信息"></textarea></td>
					</tr>
					<tr>
						<td>邮政编码</td>
						<td><input name="post" placeholder="如果您不清楚邮递区号，请填写000000"
							type="text"></td>
					</tr>
					<tr>
						<td>收货人姓名<span class="redStar">*</span></td>
						<td><input id="receiver" name="receiver"
							placeholder="长度不超过25个字符" type="text"></td>
					</tr>
					<tr>
						<td>手机号码 <span class="redStar">*</span></td>
						<td><input id="mobile" name="mobile" placeholder="请输入11位手机号码"
							type="text"></td>
						<input type="hidden" id="userId" name="userId">
					</tr>
				</table>
			</div>
		</div>
		<div id="productList" class="productList">
			<div class="orderItemSumDiv">
				<div class="pull-left">
					<span class="leaveMessageText">给卖家留言:</span> <span> <img
						class="leaveMessageImg" src="img/site/leaveMessage.png">
					</span> <span class="leaveMessageTextareaSpan"> <textarea
							name="userMessage" class="leaveMessageTextarea"></textarea>
						<div>
							<span>还可以输入200个字符</span>
						</div>
					</span>
				</div>
				<span class="pull-right">店铺合计(含运费): ￥<fmt:formatNumber
						type="number" value="${total}" minFractionDigits="2" /></span>
			</div>
		</div>
		<div class="orderItemTotalSumDiv">
			<div class="pull-right">
				<span>实付款：</span> <input type="hidden" id="price" name="price">
				<span class="orderItemTotalSumSpan">￥<%=request.getParameter("cartSumPrice")%></span>
			</div>
		</div>
		<input id="WIDout_trade_no" name="WIDout_trade_no" type="hidden" /> <input
			id="WIDsubject" name="WIDsubject" type="hidden" /> <input
			id="WIDtotal_amount" name="WIDtotal_amount" type="hidden" /> <input
			id="WIDbody" name="WIDbody" type="hidden" />
		<div class="submitOrderDiv">
			<input type="hidden" id="orderGoodsIds" name="orderGoodsIds">
			<button id="submitOrderBtn" type="submit" class="submitOrderButton">提交订单</button>
		</div>
	</form>
</div>