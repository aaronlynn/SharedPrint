<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/config.js?version=3"></script>
<script>
var userId =$.zui.store.get('userId');
var token = "231231";
$(function(){
	var param = "orderCode="+"<%=request.getParameter("out_trade_no")%>";
	$.ajax({
		url:adminQueryUrl + orderService + '/payedFileSuccess.action',
		type:'post',
		dataType:'JSON',
		beforeSend:function(xhr){//设置请求头信息
			xhr.setRequestHeader("token",token);
			xhr.setRequestHeader("userId",userId);
		},
		headers:{'token':token,'userId':userId},
		data:param,
		success:function(data){
			if(data.success =='1'){
				$("#payedInfoPrice").val(data.orderInfoMap.price);
			}else{
				new $.zui.Messager("错误", {
					type : 'warning', // 定义颜色主题
					placement : 'center' // 定义显示位置
				}).show();
			}
		},
		error:function(data){
			new $.zui.Messager("系统繁忙，请稍后再试", {
				type : 'warning', // 定义颜色主题
				placement : 'center' // 定义显示位置
			}).show();
		}
	});
})
</script>     
<div class="payedDiv">
    <div class="payedTextDiv">
        <img src="img/site/paySuccess.png">
        <span>您已成功付款</span>
         
    </div>
    <div class="payedAddressInfo">  
    	 <ul>
            <li>实付款：￥<input id="payedInfoPrice" style="border: 0" name="payedInfoPrice">
            </li>
            <li>预计11月18日送达    </li>
        </ul>             
        <div class="paedCheckLinkDiv">
            您可以
            <a class="payedCheckLink" href="${pageContext.request.contextPath}/bought.jsp">查看已买到的宝贝</a>
            <a class="payedCheckLink" href="forebought">查看交易详情 </a>
        </div>
             
    </div>
     
    <div class="payedSeperateLine">
    </div>
     
    <div class="warningDiv">
        <img src="img/site/warning.png">
        <b>安全提醒：</b>下单后，<span class="redColor boldWord">用QQ给您发送链接办理退款的都是骗子！</span>云打印商城不存在系统升级，订单异常等问题，谨防假冒客服电话诈骗！
    </div>
 
</div>