<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/config.js?version=3"></script>

<script>
//商户号 订货单生成
var userId =$.zui.store.get('userId');
var token = "231231"; 
$(function(){		
	
	var queryGoodsListUrl = adminQueryUrl + orderService + queryBoughtGoods;
	console.log("queryGoodsListUrl="+queryGoodsListUrl);
 	var param = "userId="+userId;
 	
 	$.ajax({
		url : queryGoodsListUrl,
		type : 'post',
		beforeSend : function(xhr) {
			xhr.setRequestHeader("token", token);
			xhr.setRequestHeader("userId", userId);
		},
		header : {
			"token" : token,
			"userId" : userId
		},
		data : param,
		dataType : "JSON",
		success:function(result){
			var resultHtml ='<div class="productListTip">确认订单信息</div>'
						  + '<table class="productListTable">'
            			  + '<thead>'
            			  + '<tr>'
           				  + '<th colspan="2" class="productListTableFirstColumn">'
          				  + '<img class="tmallbuy" src="img/site/tmallbuy.png">'
           				  + '<a class="marketLink" href="#nowhere">店铺：天猫店铺</a>'
           				  + '<a class="wangwanglink" href="#nowhere"> <span class="wangwangGif"></span> </a>'
           				  + '</th>'
           				  + '<th>单价</th>'
           				  + '<th>数量</th>'
           				  + '<th>小计</th>'
           				  + '<th>配送方式</th>'
           				  + '</tr>'
           				  + '<tr class="rowborder">'
           				  + '<td  colspan="2" ></td>'
           				  + '<td></td>'
           				  + '<td></td>'
           				  + '<td></td>'
           				  + ' <td></td>'
           				  + '</tr>'
           				  + '</thead>'
           				  + '<tbody class="productListTableTbody">';
			if(result.success ='1'){					
				$.each(result.orderGoodsList, function(i, item){				
                	 resultHtml += '<tr class="orderItemTR">'
		                	   	+ '<td class="orderItemFirstTD"><img class="orderItemImg" src="${pageContext.request.contextPath}/img/goods/'+item.Picture1+'"></td>'
		                		+ '<td class="orderItemProductInfo">'
		                		+ '<a  href="product.jsp?goodsId='+item.Goods_ID+'" class="orderItemProductLink">'
		                		+ item.Goods_Name
		                		+ '</a>'
		                		+ '<img src="img/site/creditcard.png" title="支持信用卡支付">'
		                		+ '<img src="img/site/7day.png" title="消费者保障服务,承诺7天退货">'
		                		+ ' <img src="img/site/promise.png" title="消费者保障服务,承诺如实描述">'		                		
		                		+ '</td>'
		                		+ '<td>'
		                		+ '<span class="orderItemProductPrice">￥'+item.Goods_Unit_Price+'</span>'
		                		+ '</td>'
		                		+ '<td>'
		                		+ '<span class="orderItemProductNumber">'+item.Goods_Count+'</span>'
		                		+ '</td>'
		                		+ '<td><span class="orderItemUnitSum">'
		                		+ '￥'+(item.Goods_Count*item.Goods_Unit_Price)+''
		                		+ '</span></td>'
		                		+ '<td class="orderItemLastTD">'
		                		+ '<label class="orderItemDeliveryLabel">'
		                		+ '<input type="radio" value="" checked="checked">'
		                		+ '普通配送'
		                		+ '</label>&nbsp&nbsp'                        
		                		+ '<select class="orderItemDeliverySelect" class="form-control">'
		                		+ '<option>快递 免邮费</option>'
		                		+ '</select>'
		                		+ '</td>'
                         		+ '</tr>';
                })
			}else if(result.success ='0'){
				new $.zui.Messager("错误", {
					type : 'warning', // 定义颜色主题
					placement : 'center' // 定义显示位置
				}).show();
			}
			resultHtml += '</tbody>'
		        	    + '</table>';
		    $("#productList").html(resultHtml);
		},
		error:function(result){
			new $.zui.Messager("系统繁忙，请稍后再试", {
				type : 'warning', // 定义颜色主题
				placement : 'center' // 定义显示位置
			}).show();
		}
 	});
})
</script>
<div class="buyPageDiv">
	<form id="recForm" name="alipayment" method="post" target="_self">
		<div class="buyFlow"> <img class="pull-right"
				src="img/site/buyflow.png">
			<div style="clear: both"></div>
		</div>
		<div id="productList" class="productList">
			<div class="orderItemSumDiv">
				<div class="pull-left">
					<span class="leaveMessageText">给卖家留言:</span> <span> <img
						class="leaveMessageImg" src="img/site/leaveMessage.png">
					</span> <span class="leaveMessageTextareaSpan"> <textarea
							name="userMessage" class="leaveMessageTextarea"></textarea>
						<div>
							<span>还可以输入200个字符</span>
						</div>
					</span>
				</div>
				<span class="pull-right">店铺合计(含运费): ￥<fmt:formatNumber
						type="number" value="${total}" minFractionDigits="2" /></span>
			</div>
		</div>
		<!--  <div class="orderItemTotalSumDiv">
			<div class="pull-right">
				<span>实付款：</span> <input type="hidden" id="price" name="price">
				<span class="orderItemTotalSumSpan">￥</span>
			</div>
		</div>
		<input id="WIDout_trade_no" name="WIDout_trade_no" type="hidden" /> <input
			id="WIDsubject" name="WIDsubject" type="hidden" /> <input
			id="WIDtotal_amount" name="WIDtotal_amount" type="hidden" /> <input
			id="WIDbody" name="WIDbody" type="hidden" />
		<div class="submitOrderDiv">
			<input type="hidden" id="orderGoodsIds" name="orderGoodsIds">
			<button id="submitOrderBtn" type="submit" class="submitOrderButton">提交订单</button>
		</div>-->
	</form>
</div>