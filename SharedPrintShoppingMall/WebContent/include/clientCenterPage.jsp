<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
 

 <form id="userForm" name="userForm"  class="registerForm">
 
    <table class="registerTable" align="center">
        <tr>
            <td  class="registerTip registerTableLeftTD">修改资料<input id="userID" name="userID" type="hidden"   ></td>
        </tr>
         <tr>
            <td class="registerTableLeftTD">昵称</td>
            <td class="registerTableRightTD"><input id="userName" name="userName" type="text"  placeholder="请输入新昵称" > </td>
        </tr>    
                 
        <tr>
            <td colspan="2" class="registerButtonTD">
                <button id="SaveBtnName" name="SaveBtnName" type="submit" style="background-color: #87CEEB">提   交</button>
            </td>
        </tr>            
    </table>
</form>
<form id="clientCenterForm" name="clientCenterForm"  class="registerForm">
 
    <table class="registerTable" align="center">
        <tr>
            <td  class="registerTip registerTableLeftTD">修改登陆密码<input id="userId" name="userId" type="hidden"   ></td>
        </tr>
         <tr>
            <td class="registerTableLeftTD">旧密码</td>
            <td class="registerTableRightTD"><input id="oldPassword" name="oldPassword" type="password"  placeholder="请输入旧密码" > </td>
        </tr>    
        <tr>
            <td class="registerTableLeftTD">新密码</td>
            <td class="registerTableRightTD"><input id="newPassword" name="newPassword" type="password"  placeholder="请输入新密码" > </td>
        </tr>
        <tr>
            <td class="registerTableLeftTD">密码确认</td>
            <td class="registerTableRightTD"><input id="repeatpassword" name="repeatpassword" type="password"   placeholder="请再次输入你的新密码" > </td>
        </tr>
                 
        <tr>
            <td colspan="2" class="registerButtonTD">
                <button id="SaveBtnPwd" name="SaveBtnPwd" type="submit" style="background-color: #87CEEB">提   交</button>
            </td>
        </tr>            
    </table>
</form>
<script src="${pageContext.request.contextPath}/js/jquery/2.0.0/jquery.min.js" type="text/javascript" charset="utf-8"></script>
<script src="${pageContext.request.contextPath}/js/zui.js" type="text/javascript" charset="utf-8"></script>
<script src="${pageContext.request.contextPath}/js/config.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.validate.js"></script>
<script src="${pageContext.request.contextPath}/js/localization/messages_zh.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/clientCenterPage.js?ver=2"></script>