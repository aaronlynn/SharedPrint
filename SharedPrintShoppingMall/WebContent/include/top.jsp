<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>

<html>
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="css/zui.css" rel="stylesheet">
		<title>云打印商城</title>
		<script>
var userId =$.zui.store.get('userId');
var token = "231231"; 
$(function(){
	var cartSpan =document.getElementById("cartSpan");
	var param ="userId="+userId;
	
	if(userId !=null){
		cartSpan.style.display="";
		$.ajax({
			url:adminQueryUrl + cartService + queryOrderGoodsCount,
			type:'post',
			dataType:'JSON',
			beforeSend:function(xhr){//设置请求头信息
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",userId);
			},
			headers:{'token':token,'userId':userId},
			data:param,
			success:function(data){
				if(data.success=='1'){
					$("#cartGoodsCount").val(data.count);
				}else{
					$("#cartGoodsCount").val(0);
				}
			},
			error:function(data){
				alert(11);
			}
		});
	}else{
		cartSpan.style.display="none";
	}		
})

</script>
	</head>
	<body>
		<nav class="top" style="text-align: left;">
		<span >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
			<a href="${pageContext.request.contextPath}/index.jsp">
				<span style="color:#C40000;margin:0px" class=" glyphicon glyphicon-home blue" ></span>
				云打印商城首页
			</a>

			<div id="isLogin" style="display: none;">
				<a id="loginUserId" href="login.jsp"></a>
				<a href="${pageContext.request.contextPath}/clientCenter.jsp">客户中心</a>
				<a  id="logout" data-toggle="modal" data-target="#logoutModal">退出</a>
			</div>
			<div id="notLogin" style="display: inline-block">
				<span>欢迎来到云打印商城</span>
				<a href="login.jsp">请登录</a>
				<a href="register.jsp">免费注册</a>
			</div>
			
			<span class="pull-right" id="cartSpan">
				<a href="order.jsp">我的订单</a>
				<a href="cart.jsp">
					<span style="color:#C40000;margin:0px" class=" glyphicon glyphicon-shopping-cart " style="background-color:#87CEEB "></span>
					购物车<strong><input id="cartGoodsCount" name="cartGoodsCount" style="width: 10px;border: 0px;background-color: #f2f2f2;"></strong>件</a>
				<a href="" id="searchBtn" data-toggle="modal" data-target="#integralcenter">积分中心</a>
			<span >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
			</span>
		</nav>
		<!-- tui确认 -->
<div class="modal fade" id="logoutModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">退出登录</h4>
			</div>
			<div class="modal-body">
				<p>您确定要退出吗?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary" id="logoutSave">确定</button>
			</div>
		</div>
	</div>
</div>
		<%@include file="integral/integral.jsp"%>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/config.js?version=3"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery1.9.1.js"></script>
		<script type="text/javascript" src="js/zui.js"></script>
		<script type="text/javascript" src="js/integral.js?ver=2"></script>
	</body>
</html>
