<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>

<!-- 用户积分中心 -->
<div class="modal fade" id="integralcenter">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">用户积分中心</h4>
			</div>
			<div class="row">
				<div class="col-md-5">
					<div class="input-group">
						<input type="text" class="form-control" id="intnum"
									name="intnum" readonly="readonly">
					</div>
				</div>
			</div>
			<div id="integralDetail" ></div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">确定</button>
			</div>
		</div>
	</div>
</div>
