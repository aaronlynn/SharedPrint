<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
 <script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery1.9.1.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/zui.js"></script>
<%@include file="include/header.jsp"%>
<%@include file="include/top.jsp"%>
<%@include file="include/simpleSearch.jsp"%>
<%@include file="include/cart/payedFilePage.jsp"%>
<%@include file="include/footer.jsp"%>