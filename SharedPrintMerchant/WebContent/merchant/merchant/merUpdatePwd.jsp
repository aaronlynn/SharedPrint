<%@ page language="java" import="java.util.*" pageEncoding="utf-8" contentType="text/html; charset=utf-8"%>
<div class="modal fade" id="updatePwdModal">
	<div class="modal-dialog">
	<form id="updatePwdForm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">修改密码</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="input-group ">
							<span class="input-group-addon">原密码<font color="red">*</font></span>
							<input type="password" class="form-control" id="originalPwd"
								name="originalPwd">
						</div>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-md-12">
						<div class="input-group ">
							<span class="input-group-addon">新密码<font color="red">*</font></span>
							<input type="password" class="form-control" id="updateNewPwd"
								name="updateNewPwd" maxlength="4">
						</div>
					</div>
				</div><hr>
				<div class="row">
					<div class="col-md-12">
						<div class="input-group ">
							<span class="input-group-addon">确认密码<font color="red">*</font></span>
							<input type="password" class="form-control" id="determinePwd"
								name="determinePwd" maxlength="4">
						</div>
					</div>
				</div>	
			</div>
		</div>
		<div style="margin-top: 10px;"></div>
		<div class="row">
			<div class="modal-footer">
				<input type="hidden" id="merUserId" name="merUserId">
				<button type="button" class="btn btn-default " data-dismiss="modal">关闭</button>
				<button type="submit" class="btn btn-primary" id="updateSave" name="updateSave">保存</button>
			</div>
		</div>
		</form>
	</div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/merUpdatePwd.js"></script>