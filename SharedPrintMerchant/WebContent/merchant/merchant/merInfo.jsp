<%@ page language="java" import="java.util.*" pageEncoding="utf-8" contentType="text/html; charset=utf-8"%>
<div class="modal fade" id="detailModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
        <h4 class="modal-title">商户详情</h4>
      </div>
      <form id="detailForm" name="detailForm">
       <div class="modal-body">
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">商户号</span>
						  <input type="text" readonly="readonly" class="form-control" id="detailMerInfCode" name="detailMerInfCode" placeholder="商户号">
						</div>	
        			</div> 
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">商户名称</span>
						  <input type="text" readonly="readonly" class="form-control" id="detailMerInfName" name="detailMerInfName" placeholder="商户名称">
						</div>	
        			</div>       	
        	</div><hr>
        	<div class="row">       			
        			<div class="col-md-6">
        				<div class="input-group">
								<span class="input-group-addon">证件类型</span> 
								<input type="text" readonly="readonly" id="detailIDType" name="detailIDType" class="form-control">
						</div>	
        			</div>
        			<div class="col-md-6">
	    				<div class="input-group">
						 <span class="input-group-addon">证件号</span>
						 <input type="text" id="detailIDCode" readonly="readonly" name="detailIDCode" class="form-control">
						</div>
	    			</div>       			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
								<span class="input-group-addon">商户类型</span> 
								<input type="text" readonly="readonly" id="detailMerInfType" name="detailMerInfType" class="form-control">
						</div>	
        			</div>
        			<div class="col-md-6">
        				<div class="input-group">
								<span class="input-group-addon">所属代理商</span> 
								<input type="text" readonly="readonly" id="detailAgeInfCode" name="detailAgeInfCode" class="form-control">
						</div>	
        			</div>          			        			       			      	
        	</div><hr>
        	<div class="row">	        			
        			<div class="col-md-6">
        				<div class="input-group">
								<span class="input-group-addon">商户等级</span> 
								<input type="text" readonly="readonly" id="detailMerInfLevel" name="detailMerInfLevel" class="form-control">
						</div>	
        			</div> 
        			<div class="col-md-6">
	    				<div class="input-group">
						 <span class="input-group-addon">简称</span>
						 <input type="text" readonly="readonly" id="detailAlias" name="detailAlias" class="form-control">
						</div>
	    			</div>       			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
								<span class="input-group-addon">所属机构</span> 								
								<input type="text" id="detailOrgCode" readonly="readonly" name="detailOrgCode" class="form-control">
						</div>	
        			</div>
	    			<div class="col-md-6">
        				<div class="input-group">
								<span class="input-group-addon">所属地区</span> 								
								<input type="text" readonly="readonly" id="detailAreaCode" name="detailAreaCode" class="form-control">
						</div>	
        			</div>      			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">经营场所</span>
						  <input type="text" class="form-control" readonly="readonly" id="detailBusiAddress" name="detailBusiAddress">
						</div>	
        			</div> 
        			<div class="col-md-6">
	    				<div class="input-group">
						 <span class="input-group-addon">联系人</span>
						 <input type="text" readonly="readonly" id="detailContactor" name="detailContactor" class="form-control">
						</div>
	    			</div>       			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">联系人电话</span>
						  <input type="text" readonly="readonly" class="form-control" id="detailContactTel" name="detailContactTel">
						</div>	
        			</div> 
        			<div class="col-md-6">
	    				<div class="input-group">
						 <span class="input-group-addon">邮箱</span>
						 <input type="text" readonly="readonly" id="detailEmail" name="detailEmail" class="form-control">
						</div>
	    			</div>       			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">QQ号</span>
						  <input type="text" readonly="readonly" class="form-control" id="detailQQ" name="detailQQ">
						</div>	
        			</div> 
        			<div class="col-md-6">
	    				<div class="input-group">
						 <span class="input-group-addon">微信号</span>
						 <input type="text" readonly="readonly" id="detailWeixin" name="detailWeixin" class="form-control">
						</div>
	    			</div>       			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">客户经理</span>
						  <input type="text" readonly="readonly" class="form-control" id="detailCliMgrCode" name="detailCliMgrCode">
						</div>	
        			</div>    
        			<div class="col-md-6">
        				<div class="input-group">
						  <span class="input-group-addon">商户状态</span>
						  <input type="text" readonly="readonly" class="form-control" id="detailMerInfStatus" name="detailMerInfStatus">
						</div>	
        			</div>    		   	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-12">
				 			<div class="input-group ">
				 				<span class="input-group-addon">证件照片<font color="red">*</font></span>
								<div  id="uploadDetailIDPathDiv" class="hide"></div>								
							</div>
							<input type="hidden" id="detailIDPath" name="detailIDPath">
	 				</div>      			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-12">
				 			<div class="input-group ">
				 				<span class="input-group-addon">经营场所照片<font color="red">*</font></span>
								<div  id="uploadDetailBusiAddressPathDiv" class="hide"></div>
							</div>
							<input type="hidden" id="detailBusiAddressPath" name="detailBusiAddressPath">
	 				</div>      			      	
        	</div><hr>
        	<div class="row">
        			<div class="col-md-12">
        				<div class="input-group">
						  <span class="input-group-addon">备注</span>
						    <textarea  id="detailRemark" name="detailRemark" readonly="readonly"
				  				cols="60" rows="5"class="form-control"></textarea>
						</div>	
        			</div>        			      	
        	</div>
      </div>
      <input type="hidden" id="detailOrgId" name="detailOrgId"/>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
      </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/merInfo.js"></script>
