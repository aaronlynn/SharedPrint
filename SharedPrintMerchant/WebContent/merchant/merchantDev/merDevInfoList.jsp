<%@ page language="java" import="java.util.*" pageEncoding="utf-8" contentType="text/html; charset=utf-8"%>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/zTreeStyle.css"/>
<style type="text/css">
	ul.ztree {margin-top: 10px;
	border: 1px solid #617775;
	background: #f0f6e4;
	width:180px;
	overflow-y:scroll;
	overflow-x:auto;}
</style>
<div class="panel">
	<div class="panel-heading">
		查询商户设备
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon">设备号</span>
					<input type="text" id="queryMerDevCode" name="queryMerDevCode" class="form-control" placeholder="">
				</div>
			</div>
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon">投放名称</span>
					<input type="text" id="queryMerDevNameLike" name="queryMerDevNameLike" class="form-control" placeholder="">
				</div>
			</div>
			<div class="col-md-4">
				<button class="btn btn-success" type="button" id="searchBtn"><i class="icon-search"></i>搜索</button>
			</div>
		</div>
	</div>
</div>
<div class="search-box">
	<button class="btn btn-success " type="button" data-toggle="modal" data-target="#addModal" id="addBtn"><i class="icon-plus"></i>新增</button>
	&nbsp;&nbsp;&nbsp;&nbsp;
	<button class="btn btn-info " data-toggle="modal" data-target="#updateModal" type="button" id="updateBtn"><i class="icon icon-edit"></i>修改</button>
	&nbsp;&nbsp;&nbsp;&nbsp;
	<button class="btn btn-danger " type="button" id="deleteBtn"><i class="icon icon-trash"></i>删除</button>
	&nbsp;&nbsp;&nbsp;&nbsp;
	<button class="btn " type="button" id="detailBtn" data-toggle="modal" data-target="#detailModal"><i class="icon icon-info"></i>详情</button>
</div>
<hr>
<div id="listDataGrid" class="datagrid"></div>
<div id="listPager" class="pager" data-ride="pager"></div>
<div class="modal fade" id="addModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
				<h4 class="modal-title">新增设备</h4>
			</div>
			<form id="addForm" name="addForm">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">投放名称</span>
								<input type="text" class="form-control" id="addMerDevName" name="addMerDevName" placeholder="投放名称">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">所属地区</span>
								<input type="hidden" name="addArea_Code" id="addArea_Code" value="" />
								<input id="citySel" name="citySel" type="text" readonly value="" style="width:180px;"  />
								<div id="menuContent"  style="display:none; ">
									<ul id="addtreeDemo" class="ztree" style="margin-top:0; position: absolute;z-index: 999; height: 130px;"></ul>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备编码</span>
								<select id="addDevInf_Code" name="addDevInf_Code" class="form-control">
									<option value="">请选择</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备固件版本号</span>
								<input type="text" class="form-control" id="addDevHwVer" name="addDevHwVer">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备应用程序版本号</span>
								<input type="text" class="form-control" id="addDevAppVer" name="addDevAppVer">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备参数数据版本号</span>
								<input type="text" class="form-control" id="addDevParamVer" name="addDevParamVer">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备其他版本号</span>
								<input type="text" class="form-control" id="addDevOtherVer" name="addDevOtherVer">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备经度</span>
								<input type="text" class="form-control" id="addLongitude" name="addLongitude">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备纬度</span>
								<input type="text" class="form-control" id="addLatitude" name="addLatitude">
							</div>
						</div>
						<div class="col-md-12">
							<div class="input-group">
								<span class="input-group-addon">投放地址</span>
								<textarea id="addDeliveryAddress" name="addDeliveryAddress"  cols="60" rows="5" class="form-control"></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="submit" class="btn btn-primary" id="addSaveBtn" name="addSaveBtn">保存</button>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="modal fade" id="updateModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
				<h4 class="modal-title">修改设备信息</h4>
			</div>
			<form id="updateForm" name="updateForm">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备号</span>
								<input type="text" readonly="readonly" id="updateMerDev_Code" name="updateMerDev_Code" class="form-control">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">硬件序列号</span>
								<input type="text" readonly="readonly" id="updateSN" name="updateSN" class="form-control">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备编码</span>
								<select id="updateDevInf_Code" name="updateDevInf_Code" class="form-control">
									<option value="">请选择</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">投放名称</span>
								<input type="text" class="form-control" id="updateMerDevName" name="updateMerDevName" placeholder="投放名称">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">所属地区</span>
								<input type="hidden" name="updateArea_Code" id="updateArea_Code" value="" />
								<input id="updatecitySel" name="updatecitySel" type="text" readonly value="" style="width:180px;"  />
								<div id="updatemenuContent"  style="display:none; ">
									<ul id="updatetreeDemo" class="ztree" style="margin-top:0; position: absolute;z-index: 999; height: 130px;"></ul>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">商户设备状态</span>
								<input type="text" class="form-control" readonly id="updateMerDevStatus" name="updateMerDevStatus">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备工作状态</span>
								<input type="text" class="form-control" readonly id="updateDevWorkStatus" name="updateDevWorkStatus">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备固件版本号</span>
								<input type="text" class="form-control" id="updateDevHwVer" name="updateDevHwVer">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备应用程序版本号</span>
								<input type="text" class="form-control" id="updateDevAppVer" name="updateDevAppVer">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备参数数据版本号</span>
								<input type="text" class="form-control" id="updateDevParamVer" name="updateDevParamVer">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备其他版本号</span>
								<input type="text" class="form-control" id="updateDevOtherVer" name="updateDevOtherVer">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备经度</span>
								<input type="text" class="form-control" id="updateLongitude" name="updateLongitude">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备纬度</span>
								<input type="text" class="form-control" id="updateLatitude" name="updateLatitude">
							</div>
						</div>
						<div class="col-md-12">
							<div class="input-group">
								<span class="input-group-addon">投放地址</span>
								<textarea id="updateDeliveryAddress" name="updateDeliveryAddress"  cols="60" rows="5" class="form-control"></textarea>
							</div>
						</div>
					</div>
				</div>
				<input type="hidden" id="excludeId" name="excludeId">
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="submit" class="btn btn-primary" id="updateSaveBtn" name="updateSaveBtn">保存</button>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="modal fade" id="detailModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
				<h4 class="modal-title">设备型号详情</h4>
			</div>
			<form id="detailForm" name="detailForm">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备号</span>
								<input type="text" readonly="readonly" id="detailMerDev_Code" name="detailMerDev_Code" class="form-control">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">硬件序列号</span>
								<input type="text" readonly="readonly" id="detailSN" name="detailSN" class="form-control">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备编码</span>
								<input type="text" id="detailDevInf_Code" readonly name="detailDevInf_Code" class="form-control">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">所属机构</span>
								<input type="text" id="detailOrg_Code" readonly name="detailOrg_Code" class="form-control">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">所属代理商</span>
								<input type="text" class="form-control" readonly id="detailAgeInf_Code" name="detailAgeInf_Code" >
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">商户号</span>
								<input readonly id="detailMerInf_Code" name="detailMerInf_Code" type="text" readonly class="form-control"  >
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">投放名称</span>
								<input type="text" class="form-control" readonly id="detailMerDevName" name="detailMerDevName" >
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">所属地区</span>
								<input readonly id="detailcitySel" name="detailcitySel" type="text" readonly class="form-control"  >
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">商户设备状态</span>
								<input type="text" class="form-control" readonly id="detailMerDevStatus" name="detailMerDevStatus">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备工作状态</span>
								<input type="text" class="form-control" readonly id="detailDevWorkStatus" name="detailDevWorkStatus">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备固件版本号</span>
								<input type="text" class="form-control" readonly id="detailDevHwVer" name="detailDevHwVer">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备应用程序版本号</span>
								<input type="text" class="form-control" readonly id="detailDevAppVer" name="detailDevAppVer">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备参数数据版本号</span>
								<input type="text" class="form-control" readonly id="detailDevParamVer" name="detailDevParamVer">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备其他版本号</span>
								<input type="text" class="form-control" readonly id="detailDevOtherVer" name="detailDevOtherVer">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备经度</span>
								<input type="text" class="form-control" readonly id="detailLongitude" name="detailLongitude">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备纬度</span>
								<input type="text" class="form-control" readonly  id="detailLatitude" name="detailLatitude">
							</div>
						</div>
						<div class="col-md-12">
							<div class="input-group">
								<span class="input-group-addon">投放地址</span>
								<textarea readonly id="detailDeliveryAddress" name="detailDeliveryAddress"  cols="60" rows="5" class="form-control"></textarea>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">创建人</span>
								<input type="text" class="form-control" readonly id="detailCrtUserId" name="detailCrtUserId">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">创建日期</span>
								<input type="text" class="form-control" readonly id="detailCrtDate" name="detailCrtDate">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">修改人</span>
								<input type="text" class="form-control" readonly id="detailUptUserId" name="detailUptUserId">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">最后修改日期</span>
								<input type="text" class="form-control" readonly id="detailLastUptDate" name="detailLastUptDate">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">审核人</span>
								<input type="text" class="form-control" readonly id="detailChkUserId" name="detailChkUserId">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">审核日期</span>
								<input type="text" class="form-control" readonly id="detailChkDate" name="detailChkDate">
							</div>
						</div>
						<div class="col-md-12">
							<div class="input-group">
								<span class="input-group-addon">审核意见</span>
								<textarea readonly id="detailChkRemark" name="detailChkRemark"  cols="60" rows="5" class="form-control"></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- 删除确认 -->
<div class="modal fade" id="deleteModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">删除设备</h4>
			</div>
			<div class="modal-body">
				<p>您确定要删除此设备吗?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary" id="deleteDevModelBtn">确定</button>
			</div>
		</div>
	</div>
</div>
<script src="${pageContext.request.contextPath}/js/jquery.ztree.core.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/merDevInfo.js?ver=2"></script>
