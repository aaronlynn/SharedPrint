<%@ page language="java" import="java.util.*" pageEncoding="utf-8" contentType="text/html; charset=utf-8"%>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/zTreeStyle.css"/>
<style type="text/css">
	ul.ztree {margin-top: 10px;
	border: 1px solid #617775;
	background: #f0f6e4;
	width:180px;
	overflow-y:scroll;
	overflow-x:auto;}
</style>
<div class="panel">
	<div class="panel-heading">
		查询设备故障
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="input-group">
					<span class="input-group-addon">设备编码</span>
					<input type="text" id="queryDevInfCode" name="queryDevInfCode" class="form-control" placeholder="">
				</div>
			</div>
			<div class="col-md-3">
				<div class="input-group">
					<span class="input-group-addon">故障名称</span>
					<input type="text" id="queryDevMaintainNameLike" name="queryDevMaintainNameLike" class="form-control" placeholder="">
				</div>
			</div>
			<div class="col-md-3">
				<div class="input-group">
					<span class="input-group-addon">故障状态</span>
					<select id="queryDevMaintainStatus" name="queryDevMaintainStatus" class="form-control">
						<option value="">请选择</option>
					</select>
				</div>
			</div>
			<div class="col-md-3">
				<button class="btn btn-success" type="button" id="searchBtn"><i class="icon-search"></i>搜索</button>
			</div>
		</div>
	</div>
</div>
<div class="search-box">
	&emsp;&emsp;&emsp;&emsp;
	<button class="btn btn-success " type="button" data-toggle="modal" data-target="#addModal" id="addBtn"><i class="icon-plus"></i>新增</button>
	&emsp;&emsp;&emsp;&emsp;
	<button class="btn btn-danger " type="button" data-toggle="modal" data-target="#deleteModal" id="deleteBtn"><i class="icon icon-trash"></i>删除</button>
	&nbsp;&nbsp;&nbsp;&nbsp;&emsp;&emsp;&emsp;&emsp;
	<button class="btn " type="button" id="detailBtn" data-toggle="modal" data-target="#detailModal"><i class="icon icon-info"></i>详情</button>
</div>
<hr>
<div id="listDataGrid" class="datagrid"></div>
<div id="listPager" class="pager" data-ride="pager"></div>

<div class="modal fade" id="addModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
				<h4 class="modal-title">添加设备故障信息</h4>
			</div>
			<form id="addForm" name="addForm">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">故障名称</span>
								<input type="text" class="form-control"  id="addDevMaintainName" name="addDevMaintainName" placeholder="设备名称">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备号</span>
								<select  id="addMerDev_Code"  name="addMerDev_Code" class="form-control">
								<option value="">请选择</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
					<div class="col-md-6">
						<div class="input-group">
								<span class="input-group-addon">所属地区</span>
								<input type="hidden" name="addArea_Code" id="addArea_Code" value="" />
								<input id="citySel" name="citySel" type="text" readonly value="" style="width:180px;"  />
								<div id="menuContent"  style="display:none; ">
									<ul id="addtreeDemo" class="ztree" style="margin-top:0; position: absolute;z-index: 999; height: 130px;"></ul>
								</div>
							</div>
							</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">联系人名称</span>
								<input type="text" class="form-control"  id="addContactName" name="addContactName">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">联系人电话</span>
								<input type="text" class="form-control"  id="addContactTel" name="addContactTel">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">故障时间</span>
								<input type="date" class="form-control"  id="addFaultTime" name="addFaultTime">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="input-group">
								<span class="input-group-addon">描述</span>
								<textarea id="addRemark" name="addRemark"  cols="60" rows="5" class="form-control"></textarea>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="input-group">
								<span class="input-group-addon">故障附件路径</span>
								<div id="AttachPath" class="uploader">
									<div class="file-list" data-drag-placeholder="请拖拽文件到此处"></div>
									&nbsp;&nbsp;&nbsp;
									<button type="button" class="btn btn-primary uploader-btn-browse">
										<i class="icon icon-cloud-upload"></i> 选择文件
									</button>
								</div>
							<input type="text" name="addAttachPath" style="width: 400px;" id="addAttachPath" value="" />
							</div>
						</div>

					</div>
				</div>
				<input type="hidden" id="DevMaintainId" name="DevMaintainId">
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="submit" class="btn btn-primary" id="addSaveBtn" name="addSaveBtn">保存</button>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="modal fade" id="detailModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">关闭</span></button>
				<h4 class="modal-title">设备型号详情</h4>
			</div>
			<form id="detailForm" name="detailForm">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">故障编码</span>
								<input type="text" class="form-control" readonly id="detailDevMaintain_Code" name="detailDevMaintain_Code">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">故障名称</span>
								<input type="text" class="form-control" readonly id="detailDevMaintainName" name="detailDevMaintainName" placeholder="设备名称">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">故障状态</span>
								<input type="text" id="detailDevMaintainStatus" readonly name="detailDevMaintainStatus" class="form-control">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备投放编码</span>
								<input type="text" id="detailMerDev_Code" readonly name="detailMerDev_Code" class="form-control">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">设备编号</span>
								<input type="text" class="form-control" readonly id="detailDevInf_Code" name="detailDevInf_Code">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">所属机构</span>
								<input type="text" id="detailOrg_Code" readonly name="detailOrg_Code" class="form-control">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">所属代理商</span>
								<input type="text" class="form-control" readonly id="detailAgeInf_Code" name="detailAgeInf_Code">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">商户号</span>
								<input type="text" class="form-control" readonly id="detailMerInf_Code" name="detailMerInf_Code">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">所属地区</span>
								<input type="text" class="form-control" readonly id="detailArea_Code" name="detailArea_Code">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">联系人名称</span>
								<input type="text" class="form-control" readonly id="detailContactName" name="detailContactName">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">联系人电话</span>
								<input type="text" class="form-control" readonly id="detailContactTel" name="detailContactTel">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">故障时间</span>
								<input type="text" class="form-control" readonly id="detailFaultTime" name="detailFaultTime">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">故障解决时间</span>
								<input type="text" class="form-control"  readonly id="detailSolveTime" name="detailSolveTime">
							</div>
						</div>
						<div class="col-md-12">
							<div class="input-group">
								<span class="input-group-addon">故障附件路径</span>
								<input type="text" class="form-control" readonly id="detailAttachPath" name="detailAttachPath">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">维修者</span>
								<input type="text" class="form-control" readonly id="detailRepairName" name="detailRepairName">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">维修开始时间</span>
								<input type="text" class="form-control" readonly  id="detailRepairBeginTime" name="detailRepairBeginTime">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">描述</span>
								<textarea id="detailRemark" name="detailRemark" readonly cols="60" rows="5" class="form-control"></textarea>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">故障解决描述</span>
								<textarea id="detailSolveRemark" name="detailSolveRemark" readonly cols="60" rows="5" class="form-control"></textarea>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="input-group">
								<span class="input-group-addon">故障解决附件路径</span>
							<input type="text" class="form-control" readonly name="detailSolveAttachPath" id="detailSolveAttachPath" value="" />
							</div>
						</div>
				
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">创建人</span>
								<input type="text" class="form-control" readonly id="detailCrtUserId" name="detailCrtUserId">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">创建日期</span>
								<input type="text" class="form-control" readonly id="detailCrtDate" name="detailCrtDate">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">修改人</span>
								<input type="text" class="form-control" readonly id="detailUptUserId" name="detailUptUserId">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">最后修改日期</span>
								<input type="text" class="form-control" readonly id="detailLastUptDate" name="detailLastUptDate">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- 删除确认 -->
<div class="modal fade" id="deleteModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">删除设备故障</h4>
			</div>
			<div class="modal-body">
				<p>您确定要删除此设备故障吗?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary" id="deleteSaveBtn">确定</button>
			</div>
		</div>
	</div>
</div>
<script src="${pageContext.request.contextPath}/js/jquery.ztree.core.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/DevMaintain.js?ver=1"></script>
