<%@ page language="java" pageEncoding="UTF-8"%>
<link href="${pageContext.request.contextPath}/css/zui.datagrid.css"
	rel="stylesheet">
<script src="${pageContext.request.contextPath}/js/zui.datagrid.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery.validate.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/localization/messages_zh.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/staorder.js?version=1"></script>

<div class="panel">
	<div class="panel-heading">查询订单</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="input-group">
					<span class="input-group-addon">订单创建年份</span> <select
						class="form-control" id="querycrtyear"
						name="querycrtyear">
						<option value="">请选择</option>
					</select>
				</div>
			</div>
			<div class="col-md-1">
				<button class="btn btn-success" type="button" id="searchBtn">
					<i class="icon-search"></i>搜索
				</button>
			</div>
			<div class="col-md-2">
				<div class="input-group">
					<input type="text" class="form-control" id="mernum"
									name="mernum" readonly="readonly">
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<canvas id="staordLineChart" width="1000" height="400"></canvas>
		</div>
	</div>
</div>
<div class="datagrid">
	<div id="listDataGrid" class="datagrid" data-ride="datagrid"></div>

	<div id="listPager" class="pager" data-ride="pager"></div>
</div>