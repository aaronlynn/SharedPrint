<%@ page language="java" pageEncoding="UTF-8"%>
<div class="container-fluid">
    <div id="container" style="min-width:400px;height:400px"></div>
	<div class="row">
		<div class="col-md-12">
			<canvas id="myLineChart" width="1000" height="400"></canvas>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<canvas id="myBarChart" width="1000" height="400"></canvas>
		</div>
	</div>
</div>
<script src="${pageContext.request.contextPath}/js/highcharts.js"></script>
<script src="${pageContext.request.contextPath}/js/zui.chart.js"></script>
<script src="${pageContext.request.contextPath}/js/adminMainCenter.js?version=1"></script>
