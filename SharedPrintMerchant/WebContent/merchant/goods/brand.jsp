<%@ page language="java" pageEncoding="UTF-8"%>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/zTreeStyle.css"
	type="text/css">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery.ztree.core.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery.ztree.excheck.js"></script>
<link href="${pageContext.request.contextPath}/css/zui.datagrid.css"
	rel="stylesheet">
<script src="${pageContext.request.contextPath}/js/zui.datagrid.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery.validate.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/localization/messages_zh.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/brand.js?version=1"></script>
<!--商品品牌管理  -->
<div class="panel">
	<div class="panel-heading">查询商品品牌</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon">商品品牌</span> <input type="text"
						id="queryGoodsCategoryLike" name="queryGoodsCategoryLike" class="form-control"
						placeholder="">
				</div>
			</div>
			<div class="col-md-4">
				<button class="btn btn-success" type="button" id="searchBtn">
					<i class="icon-search"></i>搜索
				</button>
			</div>
		</div>
	</div>
</div>
<div class="datagrid">
	<div class="search-box">
		&nbsp;&nbsp;&nbsp;&nbsp;
		<button class="btn btn-primary " type="button" id="addBtn">
			<i class="icon icon-trash"></i>增加
		</button>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<button class="btn btn-warning " type="button" id="updateBtn">
			<i class="icon icon-info"></i>修改
		</button>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<button class="btn btn-danger " type="button" id="deleteBtn">
			<i class="icon icon-trash"></i>删除
		</button>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<button class="btn " type="button" id="detailBtn">
			<i class="icon icon-info"></i>详情
		</button>
	</div>

	<div id="listDataGrid" class="datagrid" data-ride="datagrid"></div>

	<div id="listPager" class="pager" data-ride="pager"></div>
</div>
<!-- 添加 -->
<div class="modal fade" id="addModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">新增商品品牌</h4>
			</div>
			<form id="addForm">
				<div style="margin-top: 10px;"></div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">商品品牌名称<font color="red">*</font></span>
								<input type="text" class="form-control" id="addCategoryName"
									name="addCategoryName">
							</div>
						</div>
					</div>
					<div style="margin-top: 10px;"></div>
					<div class="row">
						<div class="col-md-3">
							<div class="input-group ">
								<span class="input-group-addon">上级商品品牌名称<font color="red">*</font></span>
								<div class="col-md-6">
									<input name="catName" id="catName" type="text"  class="form-control"
										style="width: 200px;" value="" onfocus="showMenu()"
										onclick="showMenu()" />
										<div id="menuContent" class="menuContent" style="display: none;">
									        <ul id="cateTree" class="ztree selectZtree" style="margin-top: 0;
									        background-color:white;width:240px; height:130px; z-index:999;
									         overflow: auto;"></ul>
								      </div>
										 <span id="catIdTip"></span> <input
										type="hidden" name="catId" id="catId" />
								</div>
							</div>
						</div>
						
					</div>
					<div style="margin-top: 10px;"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">排序值<font color="red">*</font></span>
								<input type="text" class="form-control" id="addSortNo"
									name="addSortNo">
							</div>
						</div>
					</div>
					<div style="margin-top: 10px;"></div>
                     <div class="row">
						<div class="col-md-12">
							<div class="input-group ">
								<span class="input-group-addon">商品品牌描述<font color="red">*</font></span>
								<textarea class="form-control" id="addCategoryDescription"
									name="addCategoryDescription"></textarea>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<input type="hidden" id="action" name="action" value="add">
						<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
						<button type="submit" class="btn btn-primary" id="addSave">保存</button>
					</div>
				</div>
			</form>

		</div>
	</div>
</div>
<!-- 修改页面 -->
<div class="modal fade" id="updateModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">修改商品类别</h4>
			</div>

			<form id="updateForm">
				<div class="modal-body">
					<div class="row with-padding">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">商品品牌名称<font color="red">*</font></span>
								<input type="text" class="form-control" id="updateCategoryName"
									name="updateCategoryName" readonly="readonly">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">上级商品品牌名称<font color="red">*</font></span>
								<div class="col-md-3">
									<input name="updatecatName" id="updatecatName" type="text"  class="form-control"
										style="width: 140px;" value="" onfocus="showMenu1()"
										onclick="showMenu1()" />
										<div id="updatemenuContent" class="menuContent" style="display: none;">
									        <ul id="cateTree1" class="ztree selectZtree" style="margin-top: 0;
									        background-color:white; height:230px;width:140px; z-index:999;
									         overflow: auto;"></ul>
								      </div>
										 <span id="catIdTip"></span> <input
										type="hidden" name="updatecatId" id="updatecatId" />
								</div>
							</div>
						</div>
					</div>
					<div class="row with-padding">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">排序值<font color="red">*</font></span>
								<input type="text" class="form-control" id=updateCategorySortNo
									name="updateCategorySortNo">
							</div>
						</div>

					</div>
					<div class="row with-padding">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">商品品牌描述<font color="red">*</font></span>
								<textarea class="form-control" id="updateCategoryDescript"
									name="updateCategoryDescript"></textarea>
							</div>
						</div>

					</div>


				</div>
				<div class="modal-footer">
					<input type="hidden" id="updateCategoryId" name="updateCategoryId" />
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="submit" class="btn btn-primary" id="updateSave">保存</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- 删除确认 -->
<div class="modal fade" id="deleteModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">删除商品品牌</h4>
			</div>
			<div class="modal-body">
				<p>您确定要删除此商品品牌信息吗?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary" id="deleteCategoryBtn">确定</button>
			</div>
		</div>
	</div>
</div>
<!-- 详情页面  -->
<div class="modal fade" id="detailModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">商品品牌详情</h4>
			</div>
			<form id="detailForm">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">商品品牌编号</span> <span
									class="form-control" id="detailCategoryCode"></span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">商品品牌名称</span> <span
									class="form-control" id="detailCategoryName"></span>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px;"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">所属上级品牌</span> <span
									class="form-control" id="detailParentName"></span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">排序码</span> <span
									class="form-control" id="detailSortNo"></span>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px;"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">商品品牌描述</span> <span
									class="form-control" id="detailDescription"></span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">操作时间</span> <span
									class="form-control" id="detailOprDate"></span>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px;"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">操作者编码</span> <span
									class="form-control" id="detailOprId"></span>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px;"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default " data-dismiss="modal">关闭</button>
				</div>
			</form>
		</div>
	</div>
</div>