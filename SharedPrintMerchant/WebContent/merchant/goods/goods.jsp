<%@ page language="java" pageEncoding="UTF-8"%>
<head>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/zTreeStyle.css"
	type="text/css">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery.ztree.core.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery.ztree.excheck.js"></script>
<link href="${pageContext.request.contextPath}/css/zui.uploader.css">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js//zui.uploader.js?version=1"></script>
<link href="${pageContext.request.contextPath}/css/zui.datagrid.css"
	rel="stylesheet">
<script src="${pageContext.request.contextPath}/js/zui.datagrid.js"></script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery.validate.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/localization/messages_zh.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/goods.js?version=1"></script>
</head>
<div class="panel">
	<div class="panel-heading">查询商品信息</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon">所属商品类别</span> <input type="text"
						id="queryGoodsCategory" name="queryGoodsCategory"
						class="form-control" placeholder="">
				</div>
			</div>
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon">所属商品品牌</span> <input type="text"
						id="queryGoodsBrand" name="queryGoodsBrand" class="form-control"
						placeholder="">
				</div>
			</div>
		</div>
		<div style="margin-top: 10px;"></div>
		<div class="row">
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon">最低价格</span> <input type="text"
						id="queryMinGoodsPrice" name="queryMinGoodsPrice"
						class="form-control" placeholder="">
				</div>
			</div>
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon">最高价格</span> <input type="text"
						id="queryMaxGoodsPrice" name="queryMaxGoodsPrice"
						class="form-control" placeholder="">
				</div>
			</div>
		</div>
		<div style="margin-top: 10px;"></div>
		<div class="row">
			<div class="col-md-4">
				<div class="input-group">
					<span class="input-group-addon">商品名称</span> <input type="text"
						id="queryGoodsNameLike" name="queryGoodsNameLike"
						class="form-control" placeholder="">
				</div>
			</div>
			<div class="col-md-4">
				<button class="btn btn-success" type="button" id="searchBtn">
					<i class="icon-search"></i>搜索
				</button>
			</div>
		</div>
	</div>
</div>
<div class="datagrid">
	<div class="search-box">
		&nbsp;&nbsp;&nbsp;&nbsp;
		<button class="btn btn-primary " type="button" id="addBtn">
			<i class="icon icon-trash"></i>增加
		</button>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<button class="btn btn-warning " type="button" id="updateBtn">
			<i class="icon icon-info"></i>修改
		</button>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<button class="btn btn-danger " type="button" id="deleteBtn">
			<i class="icon icon-trash"></i>删除
		</button>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<button class="btn " type="button" id="detailBtn">
			<i class="icon icon-info"></i>详情
		</button>
	</div>
	<div id="listDataGrid" class="datagrid" data-ride="datagrid"></div>
	<div id="listPager" class="pager" data-ride="pager"></div>
</div>
<!-- 添加 -->
<div class="modal fade" id="addModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">新增商品</h4>
			</div>
			<form id="addForm">
				<div style="margin-top: 10px;"></div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">商品名称<font color="red">*</font></span>
								<input type="text" class="form-control" id="addGoodsName"
									name="addGoodsName">
							</div>
						</div>
					</div>
					<div style="margin-top: 10px;"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">所属商品品牌<font color="red">*</font></span>
								<select class="form-control" id="addParentBrand"
									name="addParentBrand">
									<option value="">请选择</option>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">所属商品类别<font color="red">*</font></span>
								<select class="form-control" id="addParentCategory"
									name="addParentCategory">
									<option value="">请选择</option>
								</select>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px;"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">商品价格<font color="red">*</font></span>
								<input type="text" class="form-control" id="addPrice"
									name="addPrice">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">排序值<font color="red">*</font></span>
								<input type="text" class="form-control" id="addSortNo"
									name="addSortNo">
							</div>
						</div>
					</div>
					<div style="margin-top: 10px;"></div>
					<div class="row">
						<div class="col-md-12">
							<div class="input-group ">
								<span class="input-group-addon">商品描述<font color="red">*</font></span>
								<textarea class="form-control" id="addGoodsDescription"
									name="addGoodsDescription"></textarea>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">图片材料一</span>

								<div id="Material1" class="uploader">
									<div class="file-list" data-drag-placeholder="请拖拽文件到此处"></div>
									&nbsp;&nbsp;&nbsp;
									<button type="button"
										class="btn btn-primary uploader-btn-browse">
										<i class="icon icon-cloud-upload"></i> 选择文件
									</button>
								</div>

							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">图片材料二</span>

								<div id="Material2" class="uploader">
									<div class="file-list" data-drag-placeholder="请拖拽文件到此处"></div>
									&nbsp;&nbsp;&nbsp;
									<button type="button"
										class="btn btn-primary uploader-btn-browse">
										<i class="icon icon-cloud-upload"></i> 选择文件
									</button>
								</div>

							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">图片材料三</span>

								<div id="Material3" class="uploader">
									<div class="file-list" data-drag-placeholder="请拖拽文件到此处"></div>
									&nbsp;&nbsp;&nbsp;
									<button type="button"
										class="btn btn-primary uploader-btn-browse">
										<i class="icon icon-cloud-upload"></i> 选择文件
									</button>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">图片材料四</span>

								<div id="Material4" class="uploader">
									<div class="file-list" data-drag-placeholder="请拖拽文件到此处"></div>
									&nbsp;&nbsp;&nbsp;
									<button type="button"
										class="btn btn-primary uploader-btn-browse">
										<i class="icon icon-cloud-upload"></i> 选择文件
									</button>
								</div>

							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">图片材料五</span>

								<div id="Material5" class="uploader">
									<div class="file-list" data-drag-placeholder="请拖拽文件到此处"></div>
									&nbsp;&nbsp;&nbsp;
									<button type="button"
										class="btn btn-primary uploader-btn-browse">
										<i class="icon icon-cloud-upload"></i> 选择文件
									</button>
								</div>

							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">图片材料六</span>

								<div id="Material6" class="uploader">
									<div class="file-list" data-drag-placeholder="请拖拽文件到此处"></div>
									&nbsp;&nbsp;&nbsp;
									<button type="button"
										class="btn btn-primary uploader-btn-browse">
										<i class="icon icon-cloud-upload"></i> 选择文件
									</button>
								</div>

							</div>
						</div>
					</div>

					<div class="modal-footer">
						<input type="hidden" id="addMaterial1" name="addMaterial1">
						<input type="hidden" id="addMaterial2" name="addMaterial2">
						<input type="hidden" id="addMaterial3" name="addMaterial3">
						<input type="hidden" id="addMaterial4" name="addMaterial4">
						<input type="hidden" id="addMaterial5" name="addMaterial5">
						<input type="hidden" id="addMaterial6" name="addMaterial6">
						<input type="hidden" id="action" name="action" value="add">
						<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
						<button type="submit" class="btn btn-primary" id="addSave">保存</button>
					</div>
				</div>
			</form>

		</div>
	</div>
</div>
<!-- 修改-->
<div class="modal fade" id="updateModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">修改商品</h4>
			</div>
			<form id="updateForm">
				<div style="margin-top: 10px;"></div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">商品编号<font color="red">*</font></span>
								<input type="text" class="form-control" id="updateGoodsCode"
									name="updateGoodsCode" readonly="readonly">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">商品名称<font color="red">*</font></span>
								<input type="text" class="form-control" id="updateGoodsName"
									name="updateGoodsName" readonly="readonly">
							</div>
						</div>
					</div>
					<div style="margin-top: 10px;"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">所属商品品牌<font color="red">*</font></span>
								<select class="form-control" id="updateParentBrand"
									name="updateParentBrand">
									<option value="">请选择</option>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">所属商品类别<font color="red">*</font></span>
								<select class="form-control" id="updateParentCategory"
									name="updateParentCategory">
									<option value="">请选择</option>
								</select>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px;"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">商品价格<font color="red">*</font></span>
								<input type="text" class="form-control" id="updatePrice"
									name="updatePrice">
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">排序值<font color="red">*</font></span>
								<input type="text" class="form-control" id="updateSortNo"
									name="updateSortNo">
							</div>
						</div>
					</div>
					<div style="margin-top: 10px;"></div>
					<div class="row">
						<div class="col-md-12">
							<div class="input-group ">
								<span class="input-group-addon">商品描述<font color="red">*</font></span>
								<textarea class="form-control" id="updateGoodsDescription"
									name="updateGoodsDescription"></textarea>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">图片材料一</span>

								<div id="Material7" class="uploader">
									<div class="file-list" data-drag-placeholder="请拖拽文件到此处"></div>
									&nbsp;&nbsp;&nbsp;
									<button type="button"
										class="btn btn-primary uploader-btn-browse">
										<i class="icon icon-cloud-upload"></i> 选择文件
									</button>
								</div>

							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">图片材料二</span>

								<div id="Material8" class="uploader">
									<div class="file-list" data-drag-placeholder="请拖拽文件到此处"></div>
									&nbsp;&nbsp;&nbsp;
									<button type="button"
										class="btn btn-primary uploader-btn-browse">
										<i class="icon icon-cloud-upload"></i> 选择文件
									</button>
								</div>

							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">图片材料三</span>

								<div id="Material9" class="uploader">
									<div class="file-list" data-drag-placeholder="请拖拽文件到此处"></div>
									&nbsp;&nbsp;&nbsp;
									<button type="button"
										class="btn btn-primary uploader-btn-browse">
										<i class="icon icon-cloud-upload"></i> 选择文件
									</button>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">图片材料四</span>

								<div id="Material10" class="uploader">
									<div class="file-list" data-drag-placeholder="请拖拽文件到此处"></div>
									&nbsp;&nbsp;&nbsp;
									<button type="button"
										class="btn btn-primary uploader-btn-browse">
										<i class="icon icon-cloud-upload"></i> 选择文件
									</button>
								</div>

							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">图片材料五</span>

								<div id="Material11" class="uploader">
									<div class="file-list" data-drag-placeholder="请拖拽文件到此处"></div>
									&nbsp;&nbsp;&nbsp;
									<button type="button"
										class="btn btn-primary uploader-btn-browse">
										<i class="icon icon-cloud-upload"></i> 选择文件
									</button>
								</div>

							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<span class="input-group-addon">图片材料六</span>

								<div id="Material12" class="uploader">
									<div class="file-list" data-drag-placeholder="请拖拽文件到此处"></div>
									&nbsp;&nbsp;&nbsp;
									<button type="button"
										class="btn btn-primary uploader-btn-browse">
										<i class="icon icon-cloud-upload"></i> 选择文件
									</button>
								</div>

							</div>
						</div>
					</div>

					<div class="modal-footer">
						<input type="hidden" id="updateGoodsId" name="updateGoodsId" /> <input
							type="hidden" id="updateMaterial1" name="updateMaterial1"> <input
							type="hidden" id="updateMaterial2" name="updateMaterial2"> <input
							type="hidden" id="updateMaterial3" name="updateMaterial3"> <input
							type="hidden" id="updateMaterial4" name="updateMaterial4"> <input
							type="hidden" id="updateMaterial5" name="updateMaterial5"> <input
							type="hidden" id="updateMaterial6" name="updateMaterial6"> <input
							type="hidden" id="action" name="action" value="update">
						<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
						<button type="submit" class="btn btn-primary" id="updateSave">保存</button>
					</div>
				</div>
			</form>

		</div>
	</div>
</div>
<!-- 删除确认 -->
<div class="modal fade" id="deleteModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">删除商品信息</h4>
			</div>
			<div class="modal-body">
				<p>您确定要删除此商品信息吗?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary" id="deleteGoodsBtn">确定</button>
			</div>
		</div>
	</div>
</div>
<!-- 详情页面  -->
<div class="modal fade" id="detailModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span><span class="sr-only">关闭</span>
				</button>
				<h4 class="modal-title">商品信息详情</h4>
			</div>
			<form id="detailForm">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">商品编号</span> <span
									class="form-control" id="detailGoodsCode"></span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">商品名称</span> <span
									class="form-control" id="detailGoodsName"></span>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px;"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">所属类别名称</span> <span
									class="form-control" id="CategoryName"></span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">所属品牌名称</span> <span
									class="form-control" id="BrandName"></span>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px;"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">商品价格</span> <span
									class="form-control" id="Price"></span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">商品描述</span> <span
									class="form-control" id="Goodsdetail"></span>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px;"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">排序码</span> <span
									class="form-control" id="SortNo"></span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">操作者Id</span> <span
									class="form-control" id="detailOprId"></span>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px;"></div>
					<div class="row">
						<div class="col-md-6">
							<div class="input-group ">
								<span class="input-group-addon">操作时间</span> <span
									class="form-control" id="detailOprDate"></span>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px;"></div>
					<div class="row">
						<div class="col-md-12">
							<div class="input-group ">
								<a href="${pageContext.request.contextPath}/images/c_0001.jpg"
									data-toggle="lightbox" data-group="image-group-1"><img
									src="${pageContext.request.contextPath}/images/c_0001.jpg"
									class="img-rounded" alt=""></a> <a
									href="${pageContext.request.contextPath}/images/c_0002.jpg"
									data-toggle="lightbox" data-group="image-group-1"><img
									src="${pageContext.request.contextPath}/images/c_0002.jpg"
									class="img-rounded" alt=""></a> <a
									href="${pageContext.request.contextPath}/images/c_0003.jpg"
									data-toggle="lightbox" data-group="image-group-1"><img
									src="${pageContext.request.contextPath}/images/c_0003.jpg"
									class="img-rounded" alt=""></a> <a
									href="${pageContext.request.contextPath}/images/c_0004.jpg"
									data-toggle="lightbox" data-group="image-group-1"><img
									src="${pageContext.request.contextPath}/images/c_0004.jpg"
									class="img-rounded" alt=""></a> <a
									href="${pageContext.request.contextPath}/images/c_0001.jpg"
									data-toggle="lightbox" data-group="image-group-1"><img
									src="${pageContext.request.contextPath}/images/c_0001.jpg"
									class="img-rounded" alt=""></a> <a
									href="${pageContext.request.contextPath}/images/c_0002.jpg"
									data-toggle="lightbox" data-group="image-group-1"><img
									src="${pageContext.request.contextPath}/images/c_0002.jpg"
									class="img-rounded" alt=""></a>
							</div>
						</div>
					</div>
					<div style="margin-top: 10px;"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default " data-dismiss="modal">关闭</button>
				</div>
			</form>
		</div>
	</div>
</div>
