<%@ page language="java"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zh-cn">
  <head>
    <meta charset="utf-8">
    <!-- 兼容各种浏览器 -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <!-- 屏幕自适应 -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>商户系统登录</title>
    <!-- zui -->
    <link href="css/zui.css" rel="stylesheet">
    <link href="css/login.css?version=1" rel="stylesheet">
  </head>
  <body>
  <div>
      <h1 style="color: white;font-style:oblique;font-size: 70px;margin-left: 30px;margin-top: 50px;font-family:STKaiti">商户管理系统</h1>
  </div>
	<div class="panel loginPanel" style="margin-top: 90px">
	  <div class="panel-heading">
	     用户登录
	  </div>
	  <div class="panel-body">
	    <div class="input-control has-icon-left">
		  <input id="userNameId"  type="text" class="form-control" placeholder="用户名">
		  <label for="userNameId" class="input-control-icon-left"><i class="icon icon-user "></i></label>
		</div>
	  </div>
	  <div class="panel-body">
	    <div class="input-control has-icon-right">
		  <input id="passwdId" type="password" class="form-control" placeholder="密码">
		  <label for="passwdId" class="input-control-icon-right"><i class="icon icon-key"></i></label>
		</div>
	  </div>
	  <div class="panel-body">
	  	<button id="loginBtn" class="btn btn-block btn-primary" type="button">登录</button>
	  </div>
	  <div class="panel-footer">
	    版本所有：XX公司
	  </div>
	</div>
    <!-- jQuery (ZUI中的Javascript组件依赖于jQuery) -->
    <script src="js/jquery1.9.1.js"></script>
    <!-- ZUI Javascript组件 -->
    <script src="js/zui.js"></script>
    <script src="js/config.js"></script>	
    <script src="js/login.js?version=1"></script>	
  </body>
</html>