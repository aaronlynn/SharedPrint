//后台请求地址
var adminQueryUrl = 'http://localhost:8080/SharedPrintService';

// 通用的增删改查
var queryPageListMethod = '/queryPageList.action';
var initAddMethod = '/initAdd.action';
var addMethod = '/add.action';
var initUpdateMethod = '/initUpdate.action';
var updateMethod = '/update.action';
var deleteMethod = '/delete.action';
var detailMethod = '/detail.action';
var resetPwdMethod = '/resetPwd.action';

// 获取菜单服务
var menuService = '/api/menuInfo';
// 获取商户系统左侧菜单
var queryMerchantLeftMenuList = '/queryMerchantLeftMenuList.action';

// 获取商品类别
var categoryService = "/api/categoryInfo"
var queryCategory = "/queryCategory.action"
var initAddCategoryMethod = "/initAddMethod.action"
var deleteCategoryMethod = "/deleteCategory.action"
var detailCategoryMethod = "/detailCategory.action"
var validCategoryName = '/validCategoryName.action'

// 获取商品品牌
var brandService = "/api/brandInfo"
var queryBrand = "/queryBrand.action"
var deleteBrandMethod = "/deleteBrand.action"
var detailBrandMethod = "/detailBrand.action"
var initAddBrandMethod = "/initAddMethod.action"
var validBrandName = "/validBrandName.action"

// 获取商品信息
var goodsService = "/api/info"
var queryInfo = "/queryInfo.action"
var deleteGoodsMethod = "/deleteGoods.action"
var detailInfoMethod = "/detailInfo.action"
var validGoodsName = "/validGoodsName.action"

// 订单查询
var agentOrderService = '/api/agentOrderInfo';// 商户 、代理商订单管理统一由此进
var queryOrder = '/agentQueryOrderInfo.action';
var querychart = '/querychart.action'

// 获取用户方法
var merchantUserService = '/api/merchantUserInfo';
var loginMethod = '/merchantLogin.action';

// 订单查询
var agentOrderService = '/api/agentOrderInfo';
var queryOrder = '/agentQueryOrderInfo.action';

// 获取数据字典服务
var dicsService = '/api/dics';
var dicsMethod = '/getDicsList.action';

// 获取文件服务
var fileService = '/FileServlet';
var uploadMethod = '?action=addUserHeadPic'

// 获取统计服务
var StaordService = '/api/Staord';// 订单

// 获取商户服务
var merService = '/api/merInfo';

// 获取商户用户服务
var merUserService = '/api/merUserInfo';
var checkOriPwd = '/checkOriPwd.action';
var updatePwdMethod = '/updatePwd.action'; // 修改密码
