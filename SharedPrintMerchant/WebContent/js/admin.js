
$(document).ready(function(){
	var windowHeight = $(document).height();
	console.log('windowHeight='+windowHeight);
	//设置默认高度
	$("#adminMain2").css("min-height",windowHeight-40);
	$("#leftMenuTree").css("min-height",windowHeight-40);
	
	$("#loginOut").click(function(){
		$.zui.store.clear();  
		location.href='../login.jsp';
	});
	$("#trueName").html($.zui.store.get("trueName"));
	//动态获取左边菜单数据
	var queryLeftMenuUrl = adminQueryUrl + menuService + queryMerchantLeftMenuList;
	var token = $.zui.store.get("token");
	var userId = $.zui.store.get("userId");
	var param = "";
	console.log('token='+token+",userId="+userId);
	$.ajax({
		url:queryLeftMenuUrl,
		type:'post',
		beforeSend:function(xhr){
			xhr.setRequestHeader("token",token);
			xhr.setRequestHeader("userId",userId);
		},
		header:{"token":token,"userId":userId},
		data:param,
		dataType:"JSON",
		success:function(result){
			console.log(result);
			if (result.success == '1'){
				// 获取 tree 实例
				var myTree = $('#leftMenuTree').data('zui.tree');
				var menuList = result.menuList;
				var resultHtml = "";
				for(var i=0;i<menuList.length;i++){
					if (typeof(menuList[i].menuParentId)=='undefined'){
						resultHtml += '<li>';
						resultHtml += '<a href="#"  style="color:black;"><i class="icon icon-list"></i>'+menuList[i].menuName+'</a>';
						console.log('menuList[i].menuParentId='+menuList[i].menuCode);
						resultHtml += '<ul>';
						for(var j=0;j<menuList.length;j++){
							if (menuList[j].menuParentId==menuList[i].menuCode){
								var url = "'"+menuList[j].url+"'";
								resultHtml += '<li><a href="javascript:;"  style="color:black;"  onclick="reloadMainRight('+url+')"'
								+' ><i class="icon icon-circle-blank"></i>'+menuList[j].menuName+'</a></li>'
						    }
						}
						resultHtml += '</ul>';
						resultHtml += '</li>';
					}
				}
				$("#leftMenuTree").html(resultHtml);
			}
			else {
				new $.zui.Messager('获取数据出错', {
				    type: 'warning', // 定义颜色主题
				    placement: 'center' // 定义显示位置
				}).show();
			}
		},
		error:function(result){
			new $.zui.Messager('系统繁忙,请稍候再试!', {
			    type: 'warning', // 定义颜色主题
			    placement: 'center' // 定义显示位置
			}).show();
		}
	});
});

//重新加载右边页面
function reloadMainRight(url){
	$("#mainRight").empty();
	$("#mainRight").load(url);
}