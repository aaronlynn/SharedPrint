$(document).ready(function(){
	var currentPage = 1;
	var pageSize = 10;
	var token = $.zui.store.get("token");
	var userId = $.zui.store.get("userId");
	var merInfId = $.zui.store.get("merInfId");
	
	var queryListFun = function(){
		//获取列表数据
		var queryListUrl = adminQueryUrl + agentOrderService + queryOrder;
		
		var param = "";
		//搜索参数
		if ($("#OrderCode").val()!=''){
			param += "&OrderCode="+$("#OrderCode").val();
		}
		if ($("#OrderStatus").val()!=''){
			param += "&OrderStatus="+$("#OrderStatus").val();
		}
		if ($("#querycrtyear").val()!=''){
			param += "&querycrtyear="+$("#querycrtyear").val();
		}
		if ($("#querycrtmonth").val()!=''){
			param += "&querycrtmonth="+$("#querycrtmonth").val();
		}
		if (currentPage>0){
			param += "&currentPage="+(currentPage-1);
		}
		param += "&pageSize="+pageSize;
		param += "&currentPage="+(currentPage-1);
		
		console.log('queryListUrl='+queryListUrl);
		console.log('token='+token+",userId="+userId);
		console.log('param='+param);
		$.ajax({
			url:queryListUrl,
			type:'post',
			beforeSend:function(xhr){
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",userId);
				xhr.setRequestHeader("merchantId",merInfId);//辨别为商户用户
			},
			header:{"token":token,"merchantId":merInfId,"userId":userId},
			data:param,
			dataType:"JSON",
			success:function(result){
				console.log(result.success);
				console.log(result.pageList);
				if (result.success== '1'){
					
					$('#listDataGrid').empty();
					$('#listDataGrid').data('zui.datagrid',null);
					$('#listPager').empty();
					$('#listPager').data('zui.pager',null);
					$("#querycrtyear").empty();
					$("#querycrtyear").append('<option value="">请选择</option>');
					for(var i=0; i <= 10;i++)
					{
						$("#querycrtyear").append("<option value='"+result.tenyear[i]+"'>"+result.tenyear[i]+"年</option>");
					}
					$("#querycrtmonth").empty();
					$("#querycrtmonth").append('<option value="">请选择</option>');
					for(var i=1; i <= 12;i++)
						{
							$("#querycrtmonth").append("<option value='"+i+"'>"+i+"月</option>");
						}
					$("#OrderStatus").empty();
					$("#OrderStatus").append('<option value="">请选择</option>');
					$.each(result.statusList, function(i, item){ 
						$("#OrderStatus").append("<option value='"+item.key_value+"'>"+item.key_name+"</option>");
					}); 
					$('#listDataGrid').datagrid({
						checkable: true,
					    checkByClickRow: true,
					    selectable: true,
					    dataSource: {
					        cols:[
					        	{name: 'Order_Code', label: '订单号', width: 132},
					            {name: 'clientName', label: '下单人名称', width: 132},
					            {name: 'Price', label: '订单金额', width: 132},
					            {name: 'Order_Status', label: '订单状态', width: 132},
					            {name: 'mobile', label: '手机号码', width: 132},
					            {name: 'crtDate', label: '操作时间', width: 109},
					        ],
					        cache:false, 
					        array:result.pageList
					    }
					 
					});	
					
					// 手动进行初始化
					$('#listPager').pager({
					    page: currentPage,
					    recPerPage:pageSize,
					    elements:['first_icon', 'prev_icon', 'pages', 'next_icon', 'last_icon', 'total_text','size_menu','goto'],
					    pageSizeOptions:[5,10,20,30,50,100],
					    recTotal: result.pageTotal
					});
				}
				else {
					new $.zui.Messager(result.msg, {
					    type: 'warning', // 定义颜色主题
					    placement: 'center' // 定义显示位置
					}).show();
				}
			},
			error:function(result){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning', // 定义颜色主题
				    placement: 'center' // 定义显示位置
				}).show();
			}
		});
	};
	//调起查询数据
	queryListFun();
	//查询
	$("#searchBtn").click(function(){
		queryListFun();
	});
	//监听分页，修改当前页，条数
	$('#listPager').on('onPageChange', function(e, state, oldState) {
		currentPage = state.page;
		pageSize = state.recPerPage;
		queryListFun();
	});
	
	//初始化详情
	$("#detailBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		if (selectedItems.length == 0) {
			new $.zui.Messager('请选择要查看的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else if (selectedItems.length > 1) {
			new $.zui.Messager('请只选择一条要查看的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else {
			$("#detailForm")[0].reset();
			var url = adminQueryUrl + agentOrderService + detailMethod;
			var param = 'OrderCode='+selectedItems[0].Order_Code;//请求到列表页面
			console.log('param='+param);
			$.ajax({
				url:url,
				type:'post',
				dataType:'JSON',
				beforeSend:function(xhr){//设置请求头信息
					xhr.setRequestHeader("token",token);
					xhr.setRequestHeader("userId",userId);
				},
				header:{'token':token,'userId':userId},
				data:param,
				success:function(data){
					console.log('data='+data);
					if (data.success=='1'){
						$('#detailModal').modal('show', 'fit');
						$("#showOrderCode").val(data.orderMap.Order_Code);
						$("#PerUserName").val(data.orderMap.clientName);
						$("#Refund").val(data.orderMap.Refund);
						$("#RefundDate").val(data.orderMap.refundDate);
						$("#ReceivedBy").val(data.orderMap.Received_By);
						$("#Phone").val(data.orderMap.Phone);
						$("#Address").val(data.orderMap.Address);
						$("#GoodsName").val(data.orderMap.Goods_Name);
						$("#GoodsCount").val(data.orderMap.Goods_Count);
						$("#GoodsTotalPrice").val(data.orderMap.Goods_Total_Price);
						$("#CategoryName").val(data.orderMap.Category_Name);
						$("#BrandName").val(data.orderMap.Brand_Name);
						$("#OrderRemark").html(data.orderMap.OrderRemark);
					}
				},
				error:function(e){
					new $.zui.Messager('系统繁忙,请稍候再试!', {
					    type: 'warning',
					    placement:'center'
					}).show();
				}
			});
		}
	});
});