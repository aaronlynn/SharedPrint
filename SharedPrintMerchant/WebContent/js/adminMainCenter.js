$(document).ready(function(){
	var chart = Highcharts.chart('container', {
		chart: {
			type: 'spline'
		},
		title: {
			text: '今年月订单量与盈利统计'
		},
		subtitle: {
			text: '数据来源: lan'
		},
		xAxis: {
			categories: ['一月', '二月', '三月', '四月', '五月', '六月',
						 '七月', '八月', '九月', '十月', '十一月', '十二月']
		},
		yAxis: {
			title: {
				text: ''
			},
			labels: {
				formatter: function () {
					return this.value + '°';
				}
			}
		},
		tooltip: {
			crosshairs: true,
			shared: true
		},
		plotOptions: {
			spline: {
				marker: {
					radius: 4,
					lineColor: '#666666',
					lineWidth: 1
				}
			}
		},
		series: [{
			name: '订单量',
			marker: {
				symbol: 'square'
			},
			data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, {
				y: 26.5,
				marker: {
					//symbol: 'url(https://code.highcharts.com.cn/highcharts/graphics/sun.png)'
				}
			}, 23.3, 18.3, 13.9, 9.6]
		}, {
			name: '盈利',
			marker: {
				symbol: 'diamond'
			},
			data: [{
				y: 3.9,
				marker: {
					//symbol: 'url(https://code.highcharts.com.cn/highcharts/graphics/snow.png)'
				}
			}, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
		}]
	});

	
	
	var data = {
	    // labels 数据包含依次在X轴上显示的文本标签
	    labels: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
	    datasets: [{
	        // 数据集名称，会在图例中显示
	        label: "红队",
	
	        // 颜色主题，可以是'#fff'、'rgb(255,0,0)'、'rgba(255,0,0,0.85)'、'red' 或 ZUI配色表中的颜色名称
	        // 或者指定为 'random' 来使用一个随机的颜色主题
	        color: "red",
	        // 也可以不指定颜色主题，使用下面的值来分别应用颜色设置，这些值会覆盖color生成的主题颜色设置
	        // fillColor: "rgba(220,220,220,0.2)",
	        // strokeColor: "rgba(220,220,220,1)",
	        // pointColor: "rgba(220,220,220,1)",
	        // pointStrokeColor: "#fff",
	        // pointHighlightFill: "#fff",
	        // pointHighlightStroke: "rgba(220,220,220,1)",
	
	        // 数据集
	        data: [65, 59, 80, 81, 56, 55, 40, 44, 55, 70, 30, 40]
	    }, {
	        label: "绿队",
	        color: "green",
	        data: [28, 48, 40, 19, 86, 27, 90, 60, 30, 44, 50, 66]
	    }]
	};
	
	var options = {}; // 图表配置项，可以留空来使用默认的配置
	
	var myLineChart = $("#myLineChart").lineChart(data, options);
	
	var data2 = {
	    labels: ["一月", "二月", "三月", "四月", "五月", "六月", "七月"],
	    datasets: [
	        {
	            label: "蓝队",
	            color: 'blue',
	            data: [65, 59, 80, 81, 56, 55, 40]
	        }, {
	            label: "绿队",
	            color: 'green',
	            data: [28, 48, 40, 19, 86, 27, 90]
	        }
	    ]
	};

	var options2 = {responsive: true}; // 图表配置项，可以留空来使用默认的配置
	var myBarChart = $('#myBarChart').barChart(data2, options);
});