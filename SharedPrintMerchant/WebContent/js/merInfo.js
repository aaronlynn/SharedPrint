$(document).ready(function(){
	var token = $.zui.store.get("token");
	var userId = $.zui.store.get("userId");
	var merInfID = $.zui.store.get("merInfId");
	console.log('token='+token+",userId="+userId);
	var queryListFun = function(){
		$("#detailForm")[0].reset();
		var url = adminQueryUrl + merService + detailMethod;
		var param = 'merSystemMerInfID='+merInfID;//请求到列表页面
		console.log("url="+adminQueryUrl + merService + detailMethod);
		console.log('param'+param);
		$.ajax({
			url:url,
			type:'post',
			dataType:'JSON',
			beforeSend:function(xhr){//设置请求头信息
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",+userId);
			},
			headers:{'token':token,'userId':userId},
			data:param,
			success:function(data){
				console.log('data='+data);
				if (data.success=='1'){
					$('#detailModal').modal('show', 'fit');		
					$.each(data.agentInfoList, function(i, item){ 
						if (data.merInfo.ageinfCode == item.AgeInf_Code){						
							$("#detailAgeInfCode").val(item.AgeInfName);
						}							
					});
					$.each(data.orgInfoList, function(i, item1){ 
						if (data.merInfo.orgCode == item1.Org_Code){						
							$("#detailOrgCode").val(item1.Org_Name);
						}							
					}); 
					$.each(data.areaInfoList, function(i, item2){ 
						if (data.merInfo.areaCode == item2.areaCode){
							$("#detailAreaCode").val(item2.areaName);
						}
					}); 
					$.each(data.dicsInfoList, function(i, item3){
						if(item3.dics_code=="MerInfType"){
							if (data.merInfo.merinftype == item3.key_value){
								$("#detailMerInfType").val(item3.key_name);	
								//(item3.key_name);
							}								
						}else if(item3.dics_code=="MerInfLevel"){
							if (data.merInfo.merinflevel == item3.key_value){
								$("#detailMerInfLevel").val(item3.key_name);
								//(item3.key_name);
							}		
						}else if(item3.dics_code=="MerInfIDType"){
							if (data.merInfo.idtype == item3.key_value){
								$("#detailIDType").val(item3.key_name);									
							}		
						}
						else if(item3.dics_code=="MerInfStatus"){
							if (data.merInfo.merinfstatus == item3.key_value){
								$("#detailMerInfStatus").val(item3.key_name);									
							}		
						}
					}); 
					var updateIDPath = data.merInfo.idpath;
					var updateBusiAddressPath = data.merInfo.busiaddresspath;
					//显示证件照片
					if (updateIDPath!=null && updateIDPath!='' && typeof(updateIDPath)!=''){
						var imgSrc = adminQueryUrl + "/" + updateIDPath;
						var imgHtml = '<a href="'+imgSrc+'" target="_blank"><img src="'+imgSrc+'" width="200px" height="200px" class="img-thumbnail" alt="缩略图"></a>';	
						$("#uploadDetailIDPathDiv").removeClass();
						$("#uploadDetailIDPathDiv").html(imgHtml);
						$("#detailIDPath").val(data.merInfo.idpath);
					}		
					//显示经营场所照片
					if (updateBusiAddressPath!=null && updateBusiAddressPath!='' && typeof(updateBusiAddressPath)!=''){
						var imgSrc = adminQueryUrl + "/" + updateBusiAddressPath;
						var imgHtml = '<a href="'+imgSrc+'" target="_blank"><img src="'+imgSrc+'" width="200px" height="200px" class="img-thumbnail" alt="缩略图"></a>';
						$("#uploadDetailBusiAddressPathDiv").removeClass();
						$("#uploadDetailBusiAddressPathDiv").html(imgHtml);
						$("#detailBusiAddressPath").val(data.merInfo.busiaddresspath);
					}
					$("#detailMerInfId").val(data.merInfo.merinfId);
					$("#detailMerInfCode").val(data.merInfo.merinfCode);
					$("#detailMerInfName").val(data.merInfo.merinfname);					
					$("#detailIDCode").val(data.merInfo.idcode);
					$("#detailAlias").val(data.merInfo.alias);
					$("#detailBusiAddress").val(data.merInfo.busiaddress);
					$("#detailContactor").val(data.merInfo.contactor);
					$("#detailContactTel").val(data.merInfo.contacttel);
					$("#detailEmail").val(data.merInfo.email);
					$("#detailQQ").val(data.merInfo.qq);
					$("#detailWeixin").val(data.merInfo.weixin);										
					$("#detailCliMgrCode").val(data.merInfo.climgrcode);
					$("#detailRemark").val(data.merInfo.remark);
				}
			},
			error:function(e){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning',
				    placement:'center'
				}).show();
			}
		});
	}
	queryListFun();
	
	var IDPathTempFile;
	var busiAddressPathTempFile;
	
	//详情页面
	$("#detailBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		if(selectedItems.length == 0) {
			new $.zui.Messager('请选择要查询的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}else if(selectedItems.length > 1){
			new $.zui.Messager('请只选择一条要查询的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}else {
			$("#detailForm")[0].reset();
			var url = adminQueryUrl + merService + detailMethod;
			var param = 'merInfId='+selectedItems[0].MerInf_ID;//请求到列表页面
			console.log("url="+adminQueryUrl + merService + detailMethod);
			console.log('param'+param);
			$.ajax({
				url:url,
				type:'post',
				dataType:'JSON',
				beforeSend:function(xhr){//设置请求头信息
					xhr.setRequestHeader("token",token);
					xhr.setRequestHeader("userId",+userId);
				},
				headers:{'token':token,'userId':userId},
				data:param,
				success:function(data){
					console.log('data='+data);
					if (data.success=='1'){
						$('#detailModal').modal('show', 'fit');		
						$.each(data.agentInfoList, function(i, item){ 
							if (data.merInfo.ageinfCode == item.AgeInf_Code){						
								$("#detailAgeInfCode").val(item.AgeInfName);
							}							
						});
						$.each(data.orgInfoList, function(i, item1){ 
							if (data.merInfo.orgCode == item1.Org_Code){						
								$("#detailOrgCode").val(item1.Org_Name);
							}							
						}); 
						$.each(data.areaInfoList, function(i, item2){ 
							if (data.merInfo.areaCode == item2.areaCode){
								$("#detailAreaCode").val(item2.areaName);
							}
						}); 
						$.each(data.dicsInfoList, function(i, item3){
							if(item3.dics_code=="MerInfType"){
								if (data.merInfo.merinftype == item3.key_value){
									$("#detailMerInfType").val(item3.key_name);	
									//(item3.key_name);
								}								
							}else if(item3.dics_code=="MerInfLevel"){
								if (data.merInfo.merinflevel == item3.key_value){
									$("#detailMerInfLevel").val(item3.key_name);
									//(item3.key_name);
								}		
							}else if(item3.dics_code=="MerInfIDType"){
								if (data.merInfo.idtype == item3.key_value){
									$("#detailIDType").val(item3.key_name);									
								}		
							}
							else if(item3.dics_code=="MerInfStatus"){
								if (data.merInfo.merinfstatus == item3.key_value){
									$("#detailMerInfStatus").val(item3.key_name);									
								}		
							}
						}); 
						var updateIDPath = data.merInfo.idpath;
						var updateBusiAddressPath = data.merInfo.busiaddresspath;
						//显示证件照片
						if (updateIDPath!=null && updateIDPath!='' && typeof(updateIDPath)!=''){
							var imgSrc = adminQueryUrl + "/" + updateIDPath;
							var imgHtml = '<a href="'+imgSrc+'" target="_blank"><img src="'+imgSrc+'" width="200px" height="200px" class="img-thumbnail" alt="缩略图"></a>';	
							$("#uploadDetailIDPathDiv").removeClass();
							$("#uploadDetailIDPathDiv").html(imgHtml);
							$("#detailIDPath").val(data.merInfo.idpath);
						}		
						//显示经营场所照片
						if (updateBusiAddressPath!=null && updateBusiAddressPath!='' && typeof(updateBusiAddressPath)!=''){
							var imgSrc = adminQueryUrl + "/" + updateBusiAddressPath;
							var imgHtml = '<a href="'+imgSrc+'" target="_blank"><img src="'+imgSrc+'" width="200px" height="200px" class="img-thumbnail" alt="缩略图"></a>';
							$("#uploadDetailBusiAddressPathDiv").removeClass();
							$("#uploadDetailBusiAddressPathDiv").html(imgHtml);
							$("#detailBusiAddressPath").val(data.merInfo.busiaddresspath);
						}
						$("#detailMerInfId").val(data.merInfo.merinfId);
						$("#detailMerInfCode").val(data.merInfo.merinfCode);
						$("#detailMerInfName").val(data.merInfo.merinfname);					
						$("#detailIDCode").val(data.merInfo.idcode);
						$("#detailAlias").val(data.merInfo.alias);
						$("#detailBusiAddress").val(data.merInfo.busiaddress);
						$("#detailContactor").val(data.merInfo.contactor);
						$("#detailContactTel").val(data.merInfo.contacttel);
						$("#detailEmail").val(data.merInfo.email);
						$("#detailQQ").val(data.merInfo.qq);
						$("#detailWeixin").val(data.merInfo.weixin);										
						$("#detailCliMgrCode").val(data.merInfo.climgrcode);
						$("#detailRemark").val(data.merInfo.remark);
					}
				},
				error:function(e){
					new $.zui.Messager('系统繁忙,请稍候再试!', {
					    type: 'warning',
					    placement:'center'
					}).show();
				}
			});
		}
	});
	//审核页面
	$("#checkBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		if(selectedItems.length == 0) {
			new $.zui.Messager('请选择要审核的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}else if(selectedItems.length > 1){
			new $.zui.Messager('请只选择一条要审核的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}else {
			$("#checkForm")[0].reset();
			var url = adminQueryUrl + merService + detailMethod;
			var param = 'merInfId='+selectedItems[0].MerInf_ID;//请求到列表页面
			console.log("url="+adminQueryUrl + merService + detailMethod);
			console.log('param'+param);
			$.ajax({
				url:url,
				type:'post',
				dataType:'JSON',
				beforeSend:function(xhr){//设置请求头信息
					xhr.setRequestHeader("token",token);
					xhr.setRequestHeader("userId",+userId);
				},
				headers:{'token':token,'userId':userId},
				data:param,
				success:function(data){
					console.log('data='+data);
					if (data.success=='1'){
						$('#checkModal').modal('show', 'fit');		
						$.each(data.agentInfoList, function(i, item){ 
							if (data.merInfo.ageinfCode == item.AgeInf_Code){						
								$("#checkAgeInfCode").val(item.AgeInfName);
							}							
						});
						$.each(data.orgInfoList, function(i, item1){ 
							if (data.merInfo.orgCode == item1.Org_Code){						
								$("#checkOrgCode").val(item1.Org_Name);
							}							
						}); 
						$.each(data.areaInfoList, function(i, item2){ 
							if (data.merInfo.areaCode == item2.areaCode){
								$("#checkAreaCode").val(item2.areaName);
							}
						}); 
						$.each(data.dicsInfoList, function(i, item3){
							if(item3.dics_code=="MerInfType"){
								if (data.merInfo.merinftype == item3.key_value){
									$("#checkMerInfType").val(item3.key_name);	
									//(item3.key_name);
								}								
							}else if(item3.dics_code=="MerInfLevel"){
								if (data.merInfo.merinflevel == item3.key_value){
									$("#checkMerInfLevel").val(item3.key_name);
									//(item3.key_name);
								}		
							}else if(item3.dics_code=="MerInfIDType"){
								if (data.merInfo.idtype == item3.key_value){
									$("#checkIDType").val(item3.key_name);									
								}		
							}
							else if(item3.dics_code=="MerInfStatus"){
								if (data.merInfo.merinfstatus == item3.key_value){
									$("#checkMerInfStatus").val(item3.key_name);									
								}		
							}
						}); 
						var updateIDPath = data.merInfo.idpath;
						var updateBusiAddressPath = data.merInfo.busiaddresspath;
						//显示证件照片
						if (updateIDPath!=null && updateIDPath!='' && typeof(updateIDPath)!=''){
							var imgSrc = adminQueryUrl + "/" + updateIDPath;
							var imgHtml = '<a href="'+imgSrc+'" target="_blank"><img src="'+imgSrc+'" width="200px" height="200px" class="img-thumbnail" alt="缩略图"></a>';	
							$("#uploadcheckIDPathDiv").removeClass();
							$("#uploadcheckIDPathDiv").html(imgHtml);
							$("#checkIDPath").val(data.merInfo.idpath);
						}		
						//显示经营场所照片
						if (updateBusiAddressPath!=null && updateBusiAddressPath!='' && typeof(updateBusiAddressPath)!=''){
							var imgSrc = adminQueryUrl + "/" + updateBusiAddressPath;
							var imgHtml = '<a href="'+imgSrc+'" target="_blank"><img src="'+imgSrc+'" width="200px" height="200px" class="img-thumbnail" alt="缩略图"></a>';
							$("#uploadcheckBusiAddressPathDiv").removeClass();
							$("#uploadcheckBusiAddressPathDiv").html(imgHtml);
							$("#checkBusiAddressPath").val(data.merInfo.busiaddresspath);
						}
						$("#checkMerInfId").val(data.merInfo.merinfId);
						console.log($("#checkMerInfId").val());
						$("#checkMerInfCode").val(data.merInfo.merinfCode);
						$("#checkMerInfName").val(data.merInfo.merinfname);					
						$("#checkIDCode").val(data.merInfo.idcode);
						$("#checkAlias").val(data.merInfo.alias);
						$("#checkBusiAddress").val(data.merInfo.busiaddress);
						$("#checkContactor").val(data.merInfo.contactor);
						$("#checkContactTel").val(data.merInfo.contacttel);
						$("#checkEmail").val(data.merInfo.email);
						$("#checkQQ").val(data.merInfo.qq);
						$("#checkWeixin").val(data.merInfo.weixin);										
						$("#checkCliMgrCode").val(data.merInfo.climgrcode);
						$("#checkRemark").val(data.merInfo.remark);
					}
				},
				error:function(e){
					new $.zui.Messager('系统繁忙,请稍候再试!', {
					    type: 'warning',
					    placement:'center'
					}).show();
				}
			});
		}
	});
	//审核通过
	$("#checkPassBtn").click(function(){
		$("#checkResult").val("checkPass");
		console.log($("#checkResult").val());
		$("#checkForm").validate({
		rules:{			
			checkRemark:{
				"required":true,
			}
		},	
		submitHandler:function(form){	
			$.ajax({
				url:adminQueryUrl + merService + checkMethod,
				type:'post',
				dataType:'JSON',
				beforeSend:function(xhr){//设置请求头信息
					xhr.setRequestHeader("token",token);
					xhr.setRequestHeader("userId",userId);
				},
				headers:{'token':token,'userId':userId},
				data:$(form).serialize(),
				success:function(data){
					//('data='+data);
					if (data.success == '1') {
	                	new $.zui.Messager('审核成功!', {
	    				    type: 'success',
	    				    placement:'center'
	    				}).show();
	                	$('#checkModal').modal('hide', 'fit');
	                	queryListFun();
	                }
	                else{
	                	new $.zui.Messager(data.msg, {
	    				    type: 'warning',
	    				    placement:'center'
	    				}).show();
	                }
				},
				error:function(e){
					new $.zui.Messager('系统繁忙,请稍候再试!', {
    				    type: 'warning',
    				    placement:'center'
    				}).show();
				}
			});
		}
	})
  });
	//审核不通过
	$("#checkNotPassBtn").click(function(){
		$("#checkResult").val("checkNotPass");
		console.log($("#checkResult").val());
		$("#checkForm").validate({
		rules:{			
			checkRemark:{
				"required":true,
			}
		},	
		submitHandler:function(form){	
			$.ajax({
				url:adminQueryUrl + merService + checkMethod,
				type:'post',
				dataType:'JSON',
				beforeSend:function(xhr){//设置请求头信息
					xhr.setRequestHeader("token",token);
					xhr.setRequestHeader("userId",userId);
				},
				headers:{'token':token,'userId':userId},
				data:$(form).serialize(),
				success:function(data){
					//('data='+data);
					if (data.success == '1') {
	                	new $.zui.Messager('审核成功!', {
	    				    type: 'success',
	    				    placement:'center'
	    				}).show();
	                	$('#checkModal').modal('hide', 'fit');
	                	queryListFun();
	                }
	                else{
	                	new $.zui.Messager(data.msg, {
	    				    type: 'warning',
	    				    placement:'center'
	    				}).show();
	                }
				},
				error:function(e){
					new $.zui.Messager('系统繁忙,请稍候再试!', {
    				    type: 'warning',
    				    placement:'center'
    				}).show();
				}
			});
		}
	})
  });
	//解冻商户
	$("#thawBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		if(selectedItems.length == 0) {
			new $.zui.Messager('请选择要解冻的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}else if(selectedItems.length > 1){
			new $.zui.Messager('请只选择一条要解冻的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}else{
			$("#thawAndFreezeForm")[0].reset();
			$('#determineText').empty();
			var queryThawAndFreezeUrl = adminQueryUrl + merService + initFreezeMethod;
			var param = 'merInfId='+selectedItems[0].MerInf_ID;	
			$.ajax({
					url:queryThawAndFreezeUrl,
					type:'post',
					dataType:'JSON',
					beforeSend:function(xhr){
						xhr.setRequestHeader("token",token);
						xhr.setRequestHeader("userId",userId);
					},
					headers:{'token':token,'userId':userId},
					data:param,
					success:function(data){
						console.log('data='+data);
						if (data.success=='2'){
							$('#determineText').val("您确定要解冻此用户吗？");
							$('#thawAndFreezeModal').modal('show', 'fit');	
							$('#oprResult').val("解冻");
							$("#MerInfId").val(selectedItems[0].MerInf_ID);
						}else if(data.success=='1'){
							new $.zui.Messager('该商户状态已为正常，无需解冻', {
							    type: 'warning',
							    placement:'center'
							}).show();
						}else{
							new $.zui.Messager('系统繁忙,解冻失败!', {
							    type: 'warning',
							    placement:'center'
							}).show();
						}
					},
					error:function(e){
						new $.zui.Messager('系统繁忙,请稍候再试!', {
						    type: 'warning',
						    placement:'center'
						}).show();
					}			
			});
		}
   });
	//冻结商户
	$("#freezeBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		if(selectedItems.length == 0) {
			new $.zui.Messager('请选择要冻结的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}else if(selectedItems.length > 1){
			new $.zui.Messager('请只选择一条要冻结的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}else{
			$("#thawAndFreezeForm")[0].reset();
			$('#determineText').empty();
			var queryThawAndFreezeUrl = adminQueryUrl + merService + initFreezeMethod;
			var param = 'merInfId='+selectedItems[0].MerInf_ID;	
			$.ajax({
					url:queryThawAndFreezeUrl,
					type:'post',
					dataType:'JSON',
					beforeSend:function(xhr){
						xhr.setRequestHeader("token",token);
						xhr.setRequestHeader("userId",userId);
					},
					headers:{'token':token,'userId':userId},
					data:param,
					success:function(data){
						console.log('data='+data);
						if (data.success=='1'){
							$('#determineText').val("您确定要冻结此用户吗？");
							$('#thawAndFreezeModal').modal('show', 'fit');
							$('#oprResult').val("冻结");
							$("#MerInfId").val(selectedItems[0].MerInf_ID);
						}else if(data.success=='2'){
							new $.zui.Messager('该商户状态已为冻结，无需再次冻结！', {
							    type: 'warning',
							    placement:'center'
							}).show();
						}else{
							new $.zui.Messager('系统繁忙,解冻失败!', {
							    type: 'warning',
							    placement:'center'
							}).show();
						}
					},
					error:function(e){
						new $.zui.Messager('系统繁忙,请稍候再试!', {
						    type: 'warning',
						    placement:'center'
						}).show();
					}			
			});
		}
   });
	$("#determineBtn").click(function(){
		$("#thawAndFreezeForm").validate({
			submitHandler:function(form){				
				$.ajax({
					url:adminQueryUrl + merService + thawAndFreezeMethod,
					type:'post',
					dataType:'JSON',
					beforeSend:function(xhr){//设置请求头信息
						xhr.setRequestHeader("token",token);
						xhr.setRequestHeader("userId",userId);
					},
					headers:{'token':token,'userId':userId},
					data:$(form).serialize(),
					success:function(data){
						if (data.success == '1') {
		                	new $.zui.Messager('冻结成功!', {
		    				    type: 'success',
		    				    placement:'center'
		    				}).show();
		                	$('#thawAndFreezeModal').modal('hide', 'fit');
		                	queryListFun();
		                }else if(data.success == '2'){
		                	new $.zui.Messager('解冻成功!', {
		    				    type: 'success',
		    				    placement:'center'
		    				}).show();
		                	$('#thawAndFreezeModal').modal('hide', 'fit');
		                	queryListFun();
		                }
		                else{
		                	new $.zui.Messager(data.msg, {
		    				    type: 'warning',
		    				    placement:'center'
		    				}).show();
		                }
					},
					error:function(e){
						new $.zui.Messager('系统繁忙,请稍候再试!', {
	    				    type: 'warning',
	    				    placement:'center'
	    				}).show();
					}
				});
			}			
		});
	});
});

//变更证件图片
function updateIDPathImgChange(){
	$("#uploadIDPathDiv").css("display","none");
	$("#updateIDPathImgChangeBtn").css("display","none");
	$("#uploadUpdateIDPathDiv").css("display","block");
	$("#uploadUpdateIDPathDiv2").css("display","block");
	//$('#updateUploadHeadpicDiv').data('zui.uploader').destroy();
}


//变更经营场所图片
function updateBusiAddressPathImgChange(){
	$("#uploadBusiAddressPathDiv").css("display","none");
	$("#updateBusiAddressPathImgChangeBtn").css("display","none");
	$("#uploadUpdateBusiAddressPathDiv").css("display","block");
	$("#uploadUpdateBusiAddressPathDiv2").css("display","block");
	//$('#updateUploadHeadpicDiv').data('zui.uploader').destroy();
}