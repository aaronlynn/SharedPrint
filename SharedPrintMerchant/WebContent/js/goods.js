/*商品信息*/
$(document).ready(function(){
	var currentPage = 1;
	var pageSize = 10;
	var token = $.zui.store.get("token");
	var userId = $.zui.store.get("userId");
	var merInfId = $.zui.store.get("merInfId");
	
	var queryListFun = function(){
		// 获取列表数据
		var queryListUrl = adminQueryUrl + goodsService + queryInfo;
		
		var param = "";
		// 搜索参数
		if ($("#queryGoodsCategory").val()!=''){
			param += "&GoodsCategory="+$("#queryGoodsCategory").val();
			
		}
		if ($("#queryGoodsBrand").val()!='' ){
			param += "&GoodsBrand="+$("#queryGoodsBrand").val();
		}
		if ($("#queryMinGoodsPrice").val()!=''){
			param += "&MinGoodsPrice="+$("#queryMinGoodsPrice").val();
		}
		if ($("#queryMaxGoodsPrice").val()!=''){
			param += "&MaxGoodsPrice="+$("#queryMaxGoodsPrice").val();
		
		}
		if ($("#queryGoodsNameLike").val()!=''){
			param += "&GoodsNameLike="+$("#queryGoodsNameLike").val();
			
		}
		if (currentPage>0){
			param += "&currentPage="+(currentPage-1);
		}
		param += "&pageSize="+pageSize;
		param += "&currentPage="+(currentPage-1);
		
		console.log('queryListUrl='+queryListUrl);
		console.log('token='+token+",userId="+userId);
		console.log('param='+param);
		$.ajax({
			url:queryListUrl,
			type:'post',
			beforeSend:function(xhr){
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",userId);
				xhr.setRequestHeader("merInfId",merInfId);
			},
			header:{"token":token,"userId":userId,"merInfId":merInfId},
			data:param,
			dataType:"JSON",
			success:function(result){
				console.log(result.success);
				console.log(result.pageList);
				if (result.success== '1'){
					
					$('#listDataGrid').empty();
					$('#listDataGrid').data('zui.datagrid',null);
					$('#listPager').empty();
					$('#listPager').data('zui.pager',null);
					
					$('#listDataGrid').datagrid({
						checkable: true,
					    checkByClickRow: true,
					    selectable: true,
					    dataSource: {
					        cols:[
					            {name: 'goodsName', label: '商品名称', width: 132},
					            {name: 'categoryName', label: '所属商品类别', width: 109},
					            {name: 'brandName', label: '所属商品品牌', width: 132},
					            {name: 'price', label: '商品价格', width: 132},
					            {name: 'oprDate', label: '操作时间',width: 132},
					            {name: 'goodsId', label: '商品信息编号', hidden:true}
					        ],
					        cache:false,
					        array:result.pageList
					    }
					 
					});	
					
					// 手动进行初始化
					$('#listPager').pager({
					    page: currentPage,
					    recPerPage:pageSize,
					    elements:['first_icon', 'prev_icon', 'pages', 'next_icon', 'last_icon', 'total_text','size_menu','goto'],
					    pageSizeOptions:[5,10,20,30,50,100],
					    recTotal: result.pageTotal
					});
				}
				else {
					new $.zui.Messager(result.msg, {
					    type: 'warning', // 定义颜色主题
					    placement: 'center' // 定义显示位置
					}).show();
				}
			},
			error:function(result){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning', // 定义颜色主题
				    placement: 'center' // 定义显示位置
				}).show();
			}
		});
	};
	// 调起查询数据
	queryListFun();
	// 查询
	$("#searchBtn").click(function(){
		queryListFun();
	});
	// 监听分页，修改当前页，条数
	$('#listPager').on('onPageChange', function(e, state, oldState) {
		currentPage = state.page;
		pageSize = state.recPerPage;
		queryListFun();
	});
	
	//初始化添加
	$("#addBtn").click(function(){
		$("#addForm")[0].reset();
		var url = adminQueryUrl + categoryService + initAddCategoryMethod;
		var url2 = adminQueryUrl + brandService + initAddBrandMethod;
		var param = '';//请求到列表页面
		$.ajax({
			url:url2,
			type:'post',
			dataType:'JSON',
			beforeSend:function(xhr){//设置请求头信息
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",userId);
			},
			headers:{'token':token,'userId':userId},
			data:param,
			success:function(data){
				console.log('data.categoryList='+data.categoryList);
				if (data.success=='1'){
					$('#addModal').modal('show', 'fit');
					// 品牌
					$("#addParentBrand").empty();
					$.each(data.categoryList, function(i, item){ 
							$("#addParentBrand").append("<option value='"+item.Category_ID+"'>"+item.categoryName+"</option>");   
						});
				}
			},
			error:function(e){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning',
				    placement:'center'
				}).show();
			}
		});
		$.ajax({
			url:url,
			type:'post',
			dataType:'JSON',
			beforeSend:function(xhr){//设置请求头信息
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",userId);
			},
			headers:{'token':token,'userId':userId},
			data:param,
			success:function(data){
				console.log('data.categoryList='+data.categoryList);
				if (data.success=='1'){
					$('#addModal').modal('show', 'fit');
					$("#addParentCategory").empty();
					$.each(data.categoryList, function(i, item){ 
							$("#addParentCategory").append("<option value='"+item.Category_ID+"'>"+item.categoryName+"</option>");     
						});
				}
			},
			error:function(e){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning',
				    placement:'center'
				}).show();
			}
		});
	});
	//添加保存
	$("#addSave").click(function(){
		var validLoginNameUrl = adminQueryUrl + goodsService + validGoodsName;
		$("#addForm").validate({
			rules:{
				addGoodsName:{
					"required":true,
					"remote":{
						url:validLoginNameUrl,
						type:'get',
						beforeSend:function(xhr){//设置请求头信息
							xhr.setRequestHeader("token",token);
							xhr.setRequestHeader("userId",userId);
							xhr.setRequestHeader("merInfId",merInfId);
						},
						headers:{'token':token,'userId':userId,'merInfId':merInfId},
						dataType:'json',
						data:{
							'addGoodsName':function(){
								return $("#addGoodsName").val();
							},
						}
					}
				},
				addParentBrand:{
					"required":true
				},
				addParentCategory:{
					"required":true
				},
				addPrice:{
					"required":true
				},
			},
			messages:{
				addGoodsName:{
					"remote":"同级下商品已经存在"
				}
			},
			errorPlacement:function(error,element){
				error.appendTo(element.parent().parent());
			},
			submitHandler:function(form){
				$.ajax({
					url:adminQueryUrl + goodsService + addMethod,
					type:'post',
					dataType:'JSON',
					beforeSend:function(xhr){//设置请求头信息
						xhr.setRequestHeader("token:'"+token+"'");
						xhr.setRequestHeader("userId:'"+userId+"'");
						xhr.setRequestHeader("merInfId",merInfId);
					},
					headers:{'token':token,'userId':userId,'merInfId':merInfId},
					data:$(form).serialize(),
					success:function(data){
						console.log('data='+data);
						if (data.success == '1') {
		                	new $.zui.Messager('添加成功!', {
		    				    type: 'success',
		    				    placement:'center'
		    				}).show();
		                	$('#addModal').modal('hide', 'fit');
		                	queryListFun();
		                }
		                else{
		                	new $.zui.Messager(data.msg, {
		    				    type: 'warning',
		    				    placement:'center'
		    				}).show();
		                }
					},
					error:function(e){
						new $.zui.Messager('系统繁忙,请稍候再试!', {
	    				    type: 'warning',
	    				    placement:'center'
	    				}).show();
					}
				});
			}
		});
	});
	
	//图片文件处理
	var tempFile ;//用于清除刚上传的文件
	  //附件上传
	$('#Material1').uploader({// 当选择文件后立即自动进行上传操作
		filters:{
		    // 只允许上传图片或图标（.ico）
		    mime_types: [
		    	{title: '文件', extensions: 'png,jpg'}
		    ],
		    // 最大上传文件为 1MB
		    max_file_size: '1mb',
		    // 不允许上传重复文件
		    prevent_duplicates: true
		},
		previewImageSize:{width: 200, height: 200},
	    deleteActionOnDone: function(file, doRemoveFile) {
	          doRemoveFile();
	          $("#addMaterial1").val("");
	      	  $("#Material1").removeAttr("src");
	     },
		limitFilesCount:1,
		autoUpload: true, 
		url: adminQueryUrl+fileService +"?action=addGoodsPic", // 文件上传提交地址
	    responseHandler: function(responseObject, file) {
	    	addTempFile = file;
	    	if (responseObject!=null && responseObject.response!=null){
	    		var resultJson = JSON.parse(responseObject.response);
	    	
	    		if (resultJson.success!=null && resultJson.success!=''
		    			&& typeof(resultJson.success)!='undefined'
	    				&& resultJson.fileName!=''
		    			&& typeof(resultJson.fileName)!='undefined'){
	    			if (resultJson.success=='1' ){
	    				$("#addMaterial1").val(resultJson.fileName);
	    			}
	    		}
	    	}
	    }
	});
	$('#Material2').uploader({// 当选择文件后立即自动进行上传操作
		filters:{
		    // 只允许上传图片或图标（.ico）
		    mime_types: [
		    	{title: '文件', extensions: 'png,jpg'}
		    ],
		    // 最大上传文件为 1MB
		    max_file_size: '1mb',
		    // 不允许上传重复文件
		    prevent_duplicates: true
		},
		previewImageSize:{width: 200, height: 200},
	    deleteActionOnDone: function(file, doRemoveFile) {
	          doRemoveFile();
	          $("#addMaterial2").val("");
	      	  $("#Material2").removeAttr("src");
	     },
		limitFilesCount:1,
		autoUpload: true, 
		url: adminQueryUrl+fileService +"?action=addGoodsPic", // 文件上传提交地址
	    responseHandler: function(responseObject, file) {
	    	addTempFile = file;
	    	if (responseObject!=null && responseObject.response!=null){
	    		var resultJson = JSON.parse(responseObject.response);
	  
	    		if (resultJson.success!=null && resultJson.success!=''
		    			&& typeof(resultJson.success)!='undefined'
	    				&& resultJson.fileName!=''
		    			&& typeof(resultJson.fileName)!='undefined'){
	    			if (resultJson.success=='1' ){
	    				$("#addMaterial2").val(resultJson.fileName);
	    		
	    			}
	    		}
	    	}
	    }
	});
	
	$('#Material3').uploader({// 当选择文件后立即自动进行上传操作
		filters:{
		    // 只允许上传图片或图标（.ico）
		    mime_types: [
		    	{title: '文件', extensions: 'png,jpg'}
		    ],
		    // 最大上传文件为 1MB
		    max_file_size: '1mb',
		    // 不允许上传重复文件
		    prevent_duplicates: true
		},
		previewImageSize:{width: 200, height: 200},
	    deleteActionOnDone: function(file, doRemoveFile) {
	          doRemoveFile();
	          $("#addMaterial3").val("");
	      	  $("#Material3").removeAttr("src");
	     },
		limitFilesCount:1,
		autoUpload: true, 
		url: adminQueryUrl+fileService +"?action=addGoodsPic", // 文件上传提交地址
	    responseHandler: function(responseObject, file) {
	    	addTempFile = file;
	    	if (responseObject!=null && responseObject.response!=null){
	    		var resultJson = JSON.parse(responseObject.response);
	    		if (resultJson.success!=null && resultJson.success!=''
		    			&& typeof(resultJson.success)!='undefined'
	    				&& resultJson.fileName!=''
		    			&& typeof(resultJson.fileName)!='undefined'){
	    			if (resultJson.success=='1' ){
	    				$("#addMaterial3").val(resultJson.fileName);
	    				
	    			}
	    		}
	    	}
	    }
	});
	
	$('#Material4').uploader({// 当选择文件后立即自动进行上传操作
		filters:{
		    // 只允许上传图片或图标（.ico）
		    mime_types: [
		    	{title: '文件', extensions: 'png,jpg'}
		    ],
		    // 最大上传文件为 1MB
		    max_file_size: '1mb',
		    // 不允许上传重复文件
		    prevent_duplicates: true
		},
		previewImageSize:{width: 200, height: 200},
	    deleteActionOnDone: function(file, doRemoveFile) {
	          doRemoveFile();
	          $("#addMaterial4").val("");
	      	  $("#Material4").removeAttr("src");
	     },
		limitFilesCount:1,
		autoUpload: true, 
		url: adminQueryUrl+fileService +"?action=addGoodsPic", // 文件上传提交地址
	    responseHandler: function(responseObject, file) {
	    	addTempFile = file;
	    	if (responseObject!=null && responseObject.response!=null){
	    		var resultJson = JSON.parse(responseObject.response);
	    		
	    		if (resultJson.success!=null && resultJson.success!=''
		    			&& typeof(resultJson.success)!='undefined'
	    				&& resultJson.fileName!=''
		    			&& typeof(resultJson.fileName)!='undefined'){
	    			if (resultJson.success=='1' ){
	    				$("#addMaterial4").val(resultJson.fileName);
	    				
	    			}
	    		}
	    	}
	    }
	});
	
	$('#Material5').uploader({// 当选择文件后立即自动进行上传操作
		filters:{
		    // 只允许上传图片或图标（.ico）
		    mime_types: [
		    	{title: '文件', extensions: 'png,jpg'}
		    ],
		    // 最大上传文件为 1MB
		    max_file_size: '1mb',
		    // 不允许上传重复文件
		    prevent_duplicates: true
		},
		previewImageSize:{width: 200, height: 200},
	    deleteActionOnDone: function(file, doRemoveFile) {
	          doRemoveFile();
	          $("#addMaterial5").val("");
	      	  $("#Material5").removeAttr("src");
	     },
		limitFilesCount:1,
		autoUpload: true, 
		url: adminQueryUrl+fileService +"?action=addGoodsPic", // 文件上传提交地址
	    responseHandler: function(responseObject, file) {
	    	addTempFile = file;
	    	if (responseObject!=null && responseObject.response!=null){
	    		var resultJson = JSON.parse(responseObject.response);
	    
	    		if (resultJson.success!=null && resultJson.success!=''
		    			&& typeof(resultJson.success)!='undefined'
	    				&& resultJson.fileName!=''
		    			&& typeof(resultJson.fileName)!='undefined'){
	    			if (resultJson.success=='1' ){
	    				$("#addMaterial5").val(resultJson.fileName);
	    				
	    			}
	    		}
	    	}
	    }
	});
	
	$('#Material6').uploader({// 当选择文件后立即自动进行上传操作
		filters:{
		    // 只允许上传图片或图标（.ico）
		    mime_types: [
		    	{title: '文件', extensions: 'png,jpg'}
		    ],
		    // 最大上传文件为 1MB
		    max_file_size: '1mb',
		    // 不允许上传重复文件
		    prevent_duplicates: true
		},
		previewImageSize:{width: 200, height: 200},
	    deleteActionOnDone: function(file, doRemoveFile) {
	          doRemoveFile();
	          $("#addMaterial6").val("");
	      	  $("#Material6").removeAttr("src");
	     },
		limitFilesCount:1,
		autoUpload: true, 
		url: adminQueryUrl+fileService +"?action=addGoodsPic", // 文件上传提交地址
	    responseHandler: function(responseObject, file) {
	    	addTempFile = file;
	    	if (responseObject!=null && responseObject.response!=null){
	    		var resultJson = JSON.parse(responseObject.response);
	    		
	    		if (resultJson.success!=null && resultJson.success!=''
		    			&& typeof(resultJson.success)!='undefined'
	    				&& resultJson.fileName!=''
		    			&& typeof(resultJson.fileName)!='undefined'){
	    			if (resultJson.success=='1' ){
	    				$("#addMaterial6").val(resultJson.fileName);
	    				
	    			}
	    		}
	    	}
	    }
	});
	//修改
	  //附件上传
	$('#Material7').uploader({// 当选择文件后立即自动进行上传操作
		filters:{
		    // 只允许上传图片或图标（.ico）
		    mime_types: [
		    	{title: '文件', extensions: 'png,jpg'}
		    ],
		    // 最大上传文件为 1MB
		    max_file_size: '1mb',
		    // 不允许上传重复文件
		    prevent_duplicates: true
		},
		previewImageSize:{width: 200, height: 200},
	    deleteActionOnDone: function(file, doRemoveFile) {
	          doRemoveFile();
	          $("#updateMaterial1").val("");
	      	  $("#Material7").removeAttr("src");
	     },
		limitFilesCount:1,
		autoUpload: true, 
		url: adminQueryUrl+fileService +"?action=addGoodsPic", // 文件上传提交地址
	    responseHandler: function(responseObject, file) {
	    	addTempFile = file;
	    	if (responseObject!=null && responseObject.response!=null){
	    		var resultJson = JSON.parse(responseObject.response);
	    	
	    		if (resultJson.success!=null && resultJson.success!=''
		    			&& typeof(resultJson.success)!='undefined'
	    				&& resultJson.fileName!=''
		    			&& typeof(resultJson.fileName)!='undefined'){
	    			if (resultJson.success=='1' ){
	    				$("#updateMaterial1").val(resultJson.fileName);
	    			}
	    		}
	    	}
	    }
	});
	$('#Material8').uploader({// 当选择文件后立即自动进行上传操作
		filters:{
		    // 只允许上传图片或图标（.ico）
		    mime_types: [
		    	{title: '文件', extensions: 'png,jpg'}
		    ],
		    // 最大上传文件为 1MB
		    max_file_size: '1mb',
		    // 不允许上传重复文件
		    prevent_duplicates: true
		},
		previewImageSize:{width: 200, height: 200},
	    deleteActionOnDone: function(file, doRemoveFile) {
	          doRemoveFile();
	          $("#updateMaterial2").val("");
	      	  $("#Material8").removeAttr("src");
	     },
		limitFilesCount:1,
		autoUpload: true, 
		url: adminQueryUrl+fileService +"?action=addGoodsPic", // 文件上传提交地址
	    responseHandler: function(responseObject, file) {
	    	addTempFile = file;
	    	if (responseObject!=null && responseObject.response!=null){
	    		var resultJson = JSON.parse(responseObject.response);
	  
	    		if (resultJson.success!=null && resultJson.success!=''
		    			&& typeof(resultJson.success)!='undefined'
	    				&& resultJson.fileName!=''
		    			&& typeof(resultJson.fileName)!='undefined'){
	    			if (resultJson.success=='1' ){
	    				$("#updateMaterial2").val(resultJson.fileName);
	    		
	    			}
	    		}
	    	}
	    }
	});
	
	$('#Material9').uploader({// 当选择文件后立即自动进行上传操作
		filters:{
		    // 只允许上传图片或图标（.ico）
		    mime_types: [
		    	{title: '文件', extensions: 'png,jpg'}
		    ],
		    // 最大上传文件为 1MB
		    max_file_size: '1mb',
		    // 不允许上传重复文件
		    prevent_duplicates: true
		},
		previewImageSize:{width: 200, height: 200},
	    deleteActionOnDone: function(file, doRemoveFile) {
	          doRemoveFile();
	          $("#updateMaterial3").val("");
	      	  $("#Material9").removeAttr("src");
	     },
		limitFilesCount:1,
		autoUpload: true, 
		url: adminQueryUrl+fileService +"?action=addGoodsPic", // 文件上传提交地址
	    responseHandler: function(responseObject, file) {
	    	addTempFile = file;
	    	if (responseObject!=null && responseObject.response!=null){
	    		var resultJson = JSON.parse(responseObject.response);
	    		if (resultJson.success!=null && resultJson.success!=''
		    			&& typeof(resultJson.success)!='undefined'
	    				&& resultJson.fileName!=''
		    			&& typeof(resultJson.fileName)!='undefined'){
	    			if (resultJson.success=='1' ){
	    				$("#updateMaterial3").val(resultJson.fileName);
	    				
	    			}
	    		}
	    	}
	    }
	});
	
	$('#Material10').uploader({// 当选择文件后立即自动进行上传操作
		filters:{
		    // 只允许上传图片或图标（.ico）
		    mime_types: [
		    	{title: '文件', extensions: 'png,jpg'}
		    ],
		    // 最大上传文件为 1MB
		    max_file_size: '1mb',
		    // 不允许上传重复文件
		    prevent_duplicates: true
		},
		previewImageSize:{width: 200, height: 200},
	    deleteActionOnDone: function(file, doRemoveFile) {
	          doRemoveFile();
	          $("#updateMaterial4").val("");
	      	  $("#Material10").removeAttr("src");
	     },
		limitFilesCount:1,
		autoUpload: true, 
		url: adminQueryUrl+fileService +"?action=addGoodsPic", // 文件上传提交地址
	    responseHandler: function(responseObject, file) {
	    	addTempFile = file;
	    	if (responseObject!=null && responseObject.response!=null){
	    		var resultJson = JSON.parse(responseObject.response);
	    		
	    		if (resultJson.success!=null && resultJson.success!=''
		    			&& typeof(resultJson.success)!='undefined'
	    				&& resultJson.fileName!=''
		    			&& typeof(resultJson.fileName)!='undefined'){
	    			if (resultJson.success=='1' ){
	    				$("#updateMaterial4").val(resultJson.fileName);
	    				
	    			}
	    		}
	    	}
	    }
	});
	
	$('#Material11').uploader({// 当选择文件后立即自动进行上传操作
		filters:{
		    // 只允许上传图片或图标（.ico）
		    mime_types: [
		    	{title: '文件', extensions: 'png,jpg'}
		    ],
		    // 最大上传文件为 1MB
		    max_file_size: '1mb',
		    // 不允许上传重复文件
		    prevent_duplicates: true
		},
		previewImageSize:{width: 200, height: 200},
	    deleteActionOnDone: function(file, doRemoveFile) {
	          doRemoveFile();
	          $("#updateMaterial5").val("");
	      	  $("#Material11").removeAttr("src");
	     },
		limitFilesCount:1,
		autoUpload: true, 
		url: adminQueryUrl+fileService +"?action=addGoodsPic", // 文件上传提交地址
	    responseHandler: function(responseObject, file) {
	    	addTempFile = file;
	    	if (responseObject!=null && responseObject.response!=null){
	    		var resultJson = JSON.parse(responseObject.response);
	    
	    		if (resultJson.success!=null && resultJson.success!=''
		    			&& typeof(resultJson.success)!='undefined'
	    				&& resultJson.fileName!=''
		    			&& typeof(resultJson.fileName)!='undefined'){
	    			if (resultJson.success=='1' ){
	    				$("#updateMaterial5").val(resultJson.fileName);
	    				
	    			}
	    		}
	    	}
	    }
	});
	
	$('#Material12').uploader({// 当选择文件后立即自动进行上传操作
		filters:{
		    // 只允许上传图片或图标（.ico）
		    mime_types: [
		    	{title: '文件', extensions: 'png,jpg'}
		    ],
		    // 最大上传文件为 1MB
		    max_file_size: '1mb',
		    // 不允许上传重复文件
		    prevent_duplicates: true
		},
		previewImageSize:{width: 200, height: 200},
	    deleteActionOnDone: function(file, doRemoveFile) {
	          doRemoveFile();
	          $("#updateMaterial6").val("");
	      	  $("#Material12").removeAttr("src");
	     },
		limitFilesCount:1,
		autoUpload: true, 
		url: adminQueryUrl+fileService +"?action=addGoodsPic", // 文件上传提交地址
	    responseHandler: function(responseObject, file) {
	    	addTempFile = file;
	    	if (responseObject!=null && responseObject.response!=null){
	    		var resultJson = JSON.parse(responseObject.response);
	    		
	    		if (resultJson.success!=null && resultJson.success!=''
		    			&& typeof(resultJson.success)!='undefined'
	    				&& resultJson.fileName!=''
		    			&& typeof(resultJson.fileName)!='undefined'){
	    			if (resultJson.success=='1' ){
	    				$("#updateMaterial6").val(resultJson.fileName);
	    				
	    			}
	    		}
	    	}
	    }
	});
	//初始化修改
	$("#updateBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		if (selectedItems.length == 0) {
			new $.zui.Messager('请选择要修改的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else if (selectedItems.length > 1) {
			new $.zui.Messager('请只选择一条要修改的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else {
			$("#updateForm")[0].reset();
			var url3 = adminQueryUrl + categoryService + initAddCategoryMethod;
			var url2 = adminQueryUrl + brandService + initAddBrandMethod;
			var param1 = '';//请求到列表页面
			$.ajax({
				url:url2,
				type:'post',
				dataType:'JSON',
				beforeSend:function(xhr){//设置请求头信息
					xhr.setRequestHeader("token",token);
					xhr.setRequestHeader("userId",userId);
				},
				headers:{'token':token,'userId':userId},
				data:param1,
				success:function(data){
					console.log('data.categoryList='+data.categoryList);
					if (data.success=='1'){
						$('#updateModal').modal('show', 'fit');
						// 品牌
						$("#updateParentBrand").empty();
						$.each(data.categoryList, function(i, item){ 
								$("#updateParentBrand").append("<option value='"+item.Category_ID+"'>"+item.categoryName+"</option>");   
							});
					}
				},
				error:function(e){
					new $.zui.Messager('系统繁忙,请稍候再试!', {
					    type: 'warning',
					    placement:'center'
					}).show();
				}
			});
			$.ajax({
				url:url3,
				type:'post',
				dataType:'JSON',
				beforeSend:function(xhr){//设置请求头信息
					xhr.setRequestHeader("token",token);
					xhr.setRequestHeader("userId",userId);
				},
				headers:{'token':token,'userId':userId},
				data:param1,
				success:function(data){
					console.log('data.categoryList='+data.categoryList);
					if (data.success=='1'){
						$('#updateModal').modal('show', 'fit');
						$("#updateParentCategory").empty();
						$.each(data.categoryList, function(i, item){ 
								$("#updateParentCategory").append("<option value='"+item.Category_ID+"'>"+item.categoryName+"</option>");     
							});
					}
				},
				error:function(e){
					new $.zui.Messager('系统繁忙,请稍候再试!', {
					    type: 'warning',
					    placement:'center'
					}).show();
				}
			});
			$("#updateGoodsId").val(selectedItems[0].goodsId);
			var url = adminQueryUrl + goodsService+initUpdateMethod;
			var param = 'updategoodsId='+selectedItems[0].goodsId;//请求到列表页面
			console.log('param='+param);
			$.ajax({
				url:url,
				type:'post',
				dataType:'JSON',
				beforeSend:function(xhr){//设置请求头信息
					xhr.setRequestHeader("token:'"+token+"'");
					xhr.setRequestHeader("userId:'"+userId+"'");
				},
				headers:{'token':token,'userId':userId},
				data:param,
				success:function(data){
					console.log('data='+data);
					if (data.success=='1'){
						$('#updateModal').modal('show', 'fit');
						$("#updateGoodsCode").val(data.goodsMap.goodsCode);
						$("#updateGoodsName").val(data.goodsMap.goodsName);
						$("#updatePrice").val(data.goodsMap.price);
						$("#updateSortNo").val(data.goodsMap.sortNo);
						$("#updateGoodsDescription").val(data.goodsMap.remark);
						}
				},
				error:function(e){
					new $.zui.Messager('系统繁忙,请稍候再试!', {
					    type: 'warning',
					    placement:'center'
					}).show();
				}
			});
		}
	});
	//修改保存
	$("#updateSave").click(function(){
		$("#updateForm").validate({
			rules:{
				updateParentBrand:{
					"required":true
				},
				updateParentCategory:{
					"required":true
				},
				updatePrice:{
					"required":true
				},
			},
			errorPlacement:function(error,element){
				error.appendTo(element.parent().parent());
			},
			submitHandler:function(form){
					$.ajax({
					url:adminQueryUrl + goodsService+updateMethod,
					type:'post',
					dataType:'JSON',
					beforeSend:function(xhr){//设置请求头信息
						xhr.setRequestHeader("token",token);
						xhr.setRequestHeader("userId",userId);
						xhr.setRequestHeader("merInfId",merInfId);
					},
					headers:{'token':token,'userId':userId,'merInfId':merInfId},
					data:$(form).serialize(),
					success:function(data){
						console.log('data='+data);
						if (data.success == '1') {
		                	new $.zui.Messager('修改成功!', {
		                		type: 'success',
		    				    placement:'center'
		    				}).show();
		                	$('#updateModal').modal('hide', 'fit');
		                	queryListFun();
		                }
		                else{
		                	new $.zui.Messager(data.msg, {
		    				    type: 'warning',
		    				    placement:'center'
		    				}).show();
		                }
					},
					error:function(e){
						new $.zui.Messager('系统繁忙,请稍候再试!', {
	    				    type: 'warning',
	    				    placement:'center'
	    				}).show();
					}
				});
			}
		});
	});
	//显示删除确认框
	$("#deleteBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		if (selectedItems.length == 0) {
			new $.zui.Messager('请选择要删除的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else if (selectedItems.length > 1) {
			new $.zui.Messager('请只选择一条要删除的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else {
			$('#deleteModal').modal('show', 'fit');
		}
	});
	//删除菜单
	$("#deleteGoodsBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		var url = adminQueryUrl + goodsService + deleteGoodsMethod;
		var param = 'goodsId='+selectedItems[0].goodsId;//请求到列表页面
		console.log('url='+url);
		console.log('param2='+param);
		$.ajax({
			url:url,
			type:'post',
			dataType:'JSON',
			beforeSend:function(xhr){//设置请求头信息
				xhr.setRequestHeader("token",token);
				xhr.setRequestHeader("userId",userId);
			},
			header:{'token':token,'userId':userId},
			data:param,
			success:function(data){
				console.log('data='+data);
				$('#deleteModal').modal('hide', 'fit');
				if (data.success=='1'){
	                	new $.zui.Messager('删除成功!', {
	    				    type: 'success',
	    				    placement:'center'
	    				}).show();
	                	queryListFun();
				}
                else{
                	new $.zui.Messager(data.msg, {
    				    type: 'warning',
    				    placement:'center'
    				}).show();
				}
			},
			error:function(e){
				new $.zui.Messager('系统繁忙,请稍候再试!', {
				    type: 'warning',
				    placement:'center'
				}).show();
			}
		});
	});
	// 初始化详情
	$("#detailBtn").click(function(){
		// 获取数据表格实例
		var listDataGrid = $('#listDataGrid').data('zui.datagrid');
		// 获取当前已选中的行数据项
		var selectedItems = listDataGrid.getCheckItems();
		if (selectedItems.length == 0) {
			new $.zui.Messager('请选择要查看的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else if (selectedItems.length > 1) {
			new $.zui.Messager('请只选择一条要查看的记录', {
			    type: 'warning',
			    placement:'center'
			}).show();
		}
		else {
			$("#detailForm")[0].reset();
			var url = adminQueryUrl + goodsService + detailInfoMethod;
			var param = 'goodsId='+selectedItems[0].goodsId;// 请求到列表页面
			console.log('param='+param);
			$.ajax({
				url:url,
				type:'post',
				dataType:'JSON',
				beforeSend:function(xhr){// 设置请求头信息
					xhr.setRequestHeader("token",token);
					xhr.setRequestHeader("userId",userId);
				},
				header:{'token':token,'userId':userId},
				data:param,
				success:function(data){
					console.log('data='+data);
					if (data.success=='1'){
						$('#detailModal').modal('show', 'fit');
						
						$("#detailGoodsCode").html(data.goodsMap.goodsCode);
						$("#detailGoodsName").html(data.goodsMap.goodsName);
						$("#CategoryName").html(data.goodsMap.categoryName);
						$("#BrandName").html(data.goodsMap.brandName);
						$("#Price").html(data.goodsMap.price);
						$("#Goodsdetail").html(data.goodsMap.remark);
						$("#SortNo").html(data.goodsMap.sortNo);
						$("#detailOprId").html(data.goodsMap.oprId);
						$("#detailOprDate").html(data.goodsMap.oprDate);
					}
				},
				error:function(e){
					new $.zui.Messager('系统繁忙,请稍候再试!', {
					    type: 'warning',
					    placement:'center'
					}).show();
				}
			});
		}
	});
	
});