$(document).ready(function(){
	var token = $.zui.store.get("token");
	var userId = $.zui.store.get("userId");
	var merUserId = $.zui.store.get("userId");
	console.log('token='+token+",userId="+userId);
	var queryListFun = function(){
		$("#updatePwdForm")[0].reset();	
		$('#updatePwdModal').modal('show', 'fit');	
		$('#merUserId').val(merUserId);
	}
	queryListFun();
	
	$("#updateSave").click(function(){
		var checkPwd = adminQueryUrl + merUserService + checkOriPwd;
		$("#updatePwdForm").validate({
			rules:{			
				originalPwd:{
					"required":true,
					"remote":{
						url:checkPwd,
						type:'post',
						dataType:'json',
						beforeSend:function(xhr){//设置请求头信息
							xhr.setRequestHeader("token",token);
							xhr.setRequestHeader("userId",userId);
						},
						headers:{'token':token,'userId':userId},
						data:{
							'merUserId':function(){
								return merUserId;
							},
							'originalPwd':function(){
								return $("#originalPwd").val();
							}
						}
					}
				},
				updateNewPwd:{
					"required":true,
				},
				determinePwd:{
					"required":true,
				}
			},
			messages:{
				originalPwd:{
					"remote":"原密码输入错误"
				}
			},
			submitHandler:function(form){	
				$.ajax({
					url:adminQueryUrl + merUserService + updatePwdMethod,
					type:'post',
					dataType:'JSON',
					beforeSend:function(xhr){//设置请求头信息
						xhr.setRequestHeader("token",token);
						xhr.setRequestHeader("userId",userId);
					},
					headers:{'token':token,'userId':userId},
					data:$(form).serialize(),
					success:function(data){
						//('data='+data);
						if (data.success == '1') {
		                	new $.zui.Messager('修改密码成功!', {
		    				    type: 'success',
		    				    placement:'center'
		    				}).show();
		                	$('#updatePwdModal').modal('hide', 'fit');
		                }
		                else{
		                	new $.zui.Messager(data.msg, {
		    				    type: 'warning',
		    				    placement:'center'
		    				}).show();
		                }
					},
					error:function(e){
						new $.zui.Messager('系统繁忙,请稍候再试!', {
	    				    type: 'warning',
	    				    placement:'center'
	    				}).show();
					}
				});
			}
		})
	});
});
