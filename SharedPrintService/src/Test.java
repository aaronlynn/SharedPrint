import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.sharedprint.dics.service.DicsInfoService;

public class Test {


	public static void main(String[] args) {
		
		ApplicationContext ac = new ClassPathXmlApplicationContext("com/sharedprint/config/applicationContext.xml");
		DicsInfoService dicsInfoService = (DicsInfoService) ac.getBean("DicsInfoService");
		List<Map<String, Object>> dicsList = new ArrayList<Map<String,Object>>();
		Map<String, Object> searchMap = new HashMap<String, Object>();
        searchMap.put("AgeUserStatus", "AgeUserStatus");
		dicsList  = dicsInfoService.findDicsList(searchMap);
		for (Map<String, Object> map : dicsList) {
	    	   System.out.println(map.get("dicsName"));
		   }
	}

}
