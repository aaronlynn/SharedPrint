package com.sharedprint.dics.service;

import java.util.List;
import java.util.Map;

public interface DicsInfoService {

	List<Map<String, Object>> findDicsList(Map<String, Object> searchMap);
}
