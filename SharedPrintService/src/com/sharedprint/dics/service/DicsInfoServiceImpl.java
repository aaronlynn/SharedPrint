package com.sharedprint.dics.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sharedprint.dics.mapper.DicsInfoMapper;

@Service
public class DicsInfoServiceImpl implements DicsInfoService {

	@Autowired
	private DicsInfoMapper dicsInfoMapper;

	@Override
	public List<Map<String, Object>> findDicsList(Map<String, Object> searchMap) {
		
		return dicsInfoMapper.findDicsList(searchMap);
	}
	
}
