package com.sharedprint.dics.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sharedprint.dics.service.DicsInfoService;

/**
 * 数据字典的控制器
 */
@Controller
@RequestMapping("/api/dics")
public class DicsController {
	@Autowired
	private DicsInfoService dicsInfoService;
	
	/**
	 * 获取代理商列表数据
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/getDicsList")
	@ResponseBody
	public Map<String, Object> getDicsList(HttpServletRequest req)
	{
		String dicsCode = req.getParameter("dicsCode");
		Map<String, Object> dicsSearchMap = new HashMap<String, Object>();
		List<Map<String, Object>> dicsList = new ArrayList<Map<String,Object>>();
		dicsSearchMap.put("dicsCode", dicsCode);
		dicsList = dicsInfoService.findDicsList(dicsSearchMap);
		
		Map<String, Object> resultMap  = new HashMap<String, Object>();
		resultMap.put("dicsMap",dicsList);
		resultMap.put("success", 1);
		return resultMap;
	}
}
