package com.sharedprint.dics.mapper;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.sharedprint.dics.pojo.DicsInfo;

public interface DicsInfoMapper {
    int deleteByPrimaryKey(Integer dicsId);

    int insert(DicsInfo record);

    int insertSelective(DicsInfo record);

    DicsInfo selectByPrimaryKey(Integer dicsId);

    int updateByPrimaryKeySelective(DicsInfo record);

    int updateByPrimaryKey(DicsInfo record);

    //获取数据字典
	List<Map<String, Object>> findDicsList(Map<String, Object> searchMap);
}