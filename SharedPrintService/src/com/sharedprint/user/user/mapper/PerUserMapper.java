package com.sharedprint.user.user.mapper;

import java.util.List;
import java.util.Map;

import com.sharedprint.user.user.pojo.PerUser;

public interface PerUserMapper {
    int deleteByPrimaryKey(Integer peruserId);

    int insert(PerUser record);

    int insertSelective(PerUser record);

    PerUser selectByPrimaryKey(Integer peruserId);

    int updateByPrimaryKeySelective(PerUser record);

    int updateByPrimaryKey(PerUser record);
    /**
	 * 获取用户信息（登录）
	 * 
	 * @param searchMap
	 * @return
	 */
	List<Map<String, Object>> findUserList(Map<String, Object> searchMap);
}