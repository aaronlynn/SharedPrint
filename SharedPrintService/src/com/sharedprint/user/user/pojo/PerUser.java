package com.sharedprint.user.user.pojo;

import java.util.Date;

public class PerUser {
    private Integer peruserId;

    private String peruserCode;

    private String perusername;

    private String loginname;

    private String loginpwd;

    private String paymentpwd;

    private String peruserstatus;

    private String headpicpath;

    private String mobile;

    private String pwdquestion;

    private String pwdanswer;

    private String initpwdflag;

    private String firstloginflag;

    private String realstatus;

    private Date crtdate;

    private Date lastuptdate;

    public Integer getPeruserId() {
        return peruserId;
    }

    public void setPeruserId(Integer peruserId) {
        this.peruserId = peruserId;
    }

    public String getPeruserCode() {
        return peruserCode;
    }

    public void setPeruserCode(String peruserCode) {
        this.peruserCode = peruserCode == null ? null : peruserCode.trim();
    }

    public String getPerusername() {
        return perusername;
    }

    public void setPerusername(String perusername) {
        this.perusername = perusername == null ? null : perusername.trim();
    }

    public String getLoginname() {
        return loginname;
    }

    public void setLoginname(String loginname) {
        this.loginname = loginname == null ? null : loginname.trim();
    }

    public String getLoginpwd() {
        return loginpwd;
    }

    public void setLoginpwd(String loginpwd) {
        this.loginpwd = loginpwd == null ? null : loginpwd.trim();
    }

    public String getPaymentpwd() {
        return paymentpwd;
    }

    public void setPaymentpwd(String paymentpwd) {
        this.paymentpwd = paymentpwd == null ? null : paymentpwd.trim();
    }

    public String getPeruserstatus() {
        return peruserstatus;
    }

    public void setPeruserstatus(String peruserstatus) {
        this.peruserstatus = peruserstatus == null ? null : peruserstatus.trim();
    }

    public String getHeadpicpath() {
        return headpicpath;
    }

    public void setHeadpicpath(String headpicpath) {
        this.headpicpath = headpicpath == null ? null : headpicpath.trim();
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public String getPwdquestion() {
        return pwdquestion;
    }

    public void setPwdquestion(String pwdquestion) {
        this.pwdquestion = pwdquestion == null ? null : pwdquestion.trim();
    }

    public String getPwdanswer() {
        return pwdanswer;
    }

    public void setPwdanswer(String pwdanswer) {
        this.pwdanswer = pwdanswer == null ? null : pwdanswer.trim();
    }

    public String getInitpwdflag() {
        return initpwdflag;
    }

    public void setInitpwdflag(String initpwdflag) {
        this.initpwdflag = initpwdflag == null ? null : initpwdflag.trim();
    }

    public String getFirstloginflag() {
        return firstloginflag;
    }

    public void setFirstloginflag(String firstloginflag) {
        this.firstloginflag = firstloginflag == null ? null : firstloginflag.trim();
    }

    public String getRealstatus() {
        return realstatus;
    }

    public void setRealstatus(String realstatus) {
        this.realstatus = realstatus == null ? null : realstatus.trim();
    }

    public Date getCrtdate() {
        return crtdate;
    }

    public void setCrtdate(Date crtdate) {
        this.crtdate = crtdate;
    }

    public Date getLastuptdate() {
        return lastuptdate;
    }

    public void setLastuptdate(Date lastuptdate) {
        this.lastuptdate = lastuptdate;
    }
}