package com.sharedprint.user.user.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sharedprint.merchant.merDev.pojo.MerDev;
import com.sharedprint.user.user.mapper.PerUserMapper;
import com.sharedprint.user.user.pojo.PerUser;


@Service
@Transactional
public class PerUserServiceImpl implements PerUserService{
	@Autowired
	private PerUserMapper perUserMapper ;

	@Override
	public List<Map<String, Object>> findUserList(Map<String, Object> searchMap) {
		return perUserMapper.findUserList(searchMap);
	}

	@Override
	public int insertPerUser(PerUser perUser) {
		return perUserMapper.insertSelective(perUser);
	}

	@Override
	public int modifyUser(PerUser perUser) {
		return perUserMapper.updateByPrimaryKeySelective(perUser);
	}
}
