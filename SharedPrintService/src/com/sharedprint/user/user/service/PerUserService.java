package com.sharedprint.user.user.service;

import java.util.List;
import java.util.Map;

import com.sharedprint.user.user.pojo.PerUser;

public interface PerUserService {
	/**
	 * 获取用户信息
	 * 
	 * @param searchMap
	 * @return
	 */
	List<Map<String, Object>> findUserList(Map<String, Object> searchMap);
	/**
	 * 用户注册
	 * @param perUser
	 * @return
	 */
	int insertPerUser(PerUser perUser);
	/**
	 * 修改密码
	 * @param perUser
	 * @return
	 */
	int modifyUser(PerUser perUser);
}
