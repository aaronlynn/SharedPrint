package com.sharedprint.user.user.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sharedprint.merchant.merDev.pojo.MerDev;
import com.sharedprint.user.user.pojo.PerUser;
import com.sharedprint.user.user.service.PerUserService;
import com.sharedprint.util.RandomCode;
import com.sharedprint.util.Token;

import sun.misc.Perf.GetPerfAction;
@Controller
@RequestMapping("/api/PerUser")
public class PerUserController {
	
	private static final Logger logger = Logger.getLogger("PerUserController");
	@Autowired
	private PerUserService perUserService;
/**
 * 用户注册
 * @param request
 * @return
 */
	@RequestMapping("/PerUserRegister.action")
	@ResponseBody
	public Map<String, Object> register(HttpServletRequest request) {
		String name = request.getParameter("name");
		String passwd = request.getParameter("password");
		String peruserCode = RandomCode.getRandomCode();

		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		if (name != null && !"".equals(name.trim()) && passwd != null && !"".equals(passwd.trim())) {
			PerUser perUser = new PerUser();
			perUser.setLoginname(name);
			perUser.setPerusername(name);
			perUser.setLoginpwd(passwd);
			perUser.setPeruserCode(peruserCode);
			int success = 0;
			success = perUserService.insertPerUser(perUser);
			if (success > 0) {
				resultMap.put("success", "1");
			} else {
				resultMap.put("msg", "注册失败");
			}
		}else {
			resultMap.put("msg", "请上传相关参数");
		}
		return resultMap;
	}
	/**
	 * 验证
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/validate.action")
	@ResponseBody
	public boolean validateDevFactory(HttpServletRequest req) {
		String name = req.getParameter("name");
		
		boolean result = false;
		Map<String, Object> searchMap = new HashMap<String, Object>();
		if (name != null && !"".equals(name.trim())) {
			searchMap.put("name", name);
		}
		List<Map<String, Object>> count = perUserService.findUserList(searchMap);
		if (count.size() == 0) {
			result = true;
		}
		System.err.println("count="+count+"result="+result);
		return result;
	}
	/**
	 * 修改密码验证
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/validatePwd.action")
	@ResponseBody
	public boolean validatePwd(HttpServletRequest req) {
		String userId = req.getParameter("userId");
		String passwd = req.getParameter("oldPassword");
		boolean result = false;
		Map<String, Object> searchMap = new HashMap<String, Object>();
		if (userId != null && !"".equals(userId.trim())) {
			searchMap.put("userId", userId);
			searchMap.put("passwd", passwd);
		}
		List<Map<String, Object>> count = perUserService.findUserList(searchMap);
		if (count.size() > 0) {
			result = true;
		}
		System.err.println("count="+count+"result="+result);
		return result;
	}
	/**
	 * 修改密码和密码
	 * @param req
	 * @param resp
	 * @return
	 * @throws IOException
	 */
	@RequestMapping("/modifyUser.action")
	@ResponseBody
	public Map<String, Object> saveUpdate(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String userId = req.getParameter("userId");
		String userID = req.getParameter("userID");
		String newPassword = req.getParameter("newPassword");
		String userName = req.getParameter("userName");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		PerUser perUser = new PerUser();
		if (userId != null && !"".equals(userId.trim()) && newPassword != null && !"".equals(newPassword.trim())) {
			
			perUser.setPeruserId(Integer.parseInt(userId) );
			perUser.setLoginpwd(newPassword);
			
		}else if (userID != null && !"".equals(userID.trim()) && userName != null && !"".equals(userName.trim())) {
			perUser.setPerusername(userName);
			perUser.setPeruserId(Integer.parseInt(userID) );
		}
		else {
			resultMap.put("msg", "请上传相关参数");
		}
		int success = perUserService.modifyUser(perUser);
		if (success > 0) {
			resultMap.put("success", "1");
		} else {
			resultMap.put("msg", "修改失败");
		}
		return resultMap;
	}
	/**
	 * 客户用户登录
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/PerUserLogin.action")
	@ResponseBody
	public Map<String, Object> login(HttpServletRequest request) {
		String userName = request.getParameter("username");
		String passwd = request.getParameter("upwd");
		System.err.println("到了");

		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		if (userName != null && !"".equals(userName.trim()) && passwd != null && !"".equals(passwd.trim())) {
			Map<String, Object> searchMap = new HashMap<String, Object>();
			searchMap.put("userName", userName);
			searchMap.put("passwd", passwd);
			List<Map<String, Object>> userList = perUserService.findUserList(searchMap);
			if (userList != null && userList.size() == 1) {
				Map<String, Object> userMap = userList.get(0);
				String token = Token.getTokenString(request.getSession());
				resultMap.put("success", "1");
				resultMap.put("token", token);
				resultMap.put("userId", userMap.get("PerUser_ID"));
				resultMap.put("trueName", userMap.get("PerUserName"));
				resultMap.put("LoginName", userMap.get("LoginName"));
				resultMap.put("PerUserCode", userMap.get("PerUser_Code"));
				resultMap.put("PerUserStatus", userMap.get("PerUserStatus"));
			}
		}
		return resultMap;
	}
}
