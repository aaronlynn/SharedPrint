package com.sharedprint.merchant.merDev.mapper;

import java.util.List;
import java.util.Map;

import com.sharedprint.merchant.merDev.pojo.MerDev;

public interface MerDevMapper {
    int deleteByPrimaryKey(Integer merdevId);

    int insert(MerDev record);

    int insertSelective(MerDev record);

    MerDev selectByPrimaryKey(Integer merdevId);

    int updateByPrimaryKeySelective(MerDev record);

    int updateByPrimaryKey(MerDev record);

	List<Map<String, Object>> findMerDevInfoList(Map<String, Object> searchMap);

	int findMerDevInfoCount(Map<String, Object> searchMap);

	String findDevinfCode(String merdevCode);
}