package com.sharedprint.merchant.merDev.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sharedprint.dics.service.DicsInfoService;
import com.sharedprint.merchant.merDev.pojo.MerDev;
import com.sharedprint.merchant.merDev.service.MerDevService;
import com.sharedprint.merchant.user.service.MerchantUserInfoService;
import com.sharedprint.system.area.service.AreaInfoService;
import com.sharedprint.system.devFactory.service.DevFactoryInfoService;
import com.sharedprint.system.devInfo.pojo.DevInfo;
import com.sharedprint.system.devInfo.service.DevInfoService;
import com.sharedprint.system.devModel.pojo.DevModel;
import com.sharedprint.system.devModel.service.DevModelInfoService;
import com.sharedprint.system.merchant.service.MerInfoService;
import com.sharedprint.system.organization.service.OrgInfoService;
import com.sharedprint.util.RandomCode;

/**
 * 设备管理的控制器
 * 
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/api/merDev")
public class MerDevController {

	@Autowired
	private DevInfoService devInfoService;
	@Autowired
	private DicsInfoService dicsInfoService;
	@Autowired
	private MerchantUserInfoService merchantuserInfoService;
	@Autowired
	private MerDevService merDevInfoService;
	@Autowired
	private AreaInfoService areaInfoService;
	@Autowired
	private MerInfoService merInfoService;
	
	/**
	 * 获取设备列表数据
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/queryPageList")
	@ResponseBody
	public Map<String, Object> queryDevList(HttpServletRequest req) {
		String queryMerDevNameLike = req.getParameter("queryMerDevNameLike");//名称
		String queryMerDevCode = req.getParameter("queryMerDevCode");//编码
		String currentPage = req.getParameter("currentPage");
		String pageSize = req.getParameter("pageSize");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		System.err.println("userId="+userId);
		Map<String, Object> searchMap = new HashMap<String, Object>();
		Map<String, Object> merUserMap = new HashMap<String, Object>();
	    List<Map<String, Object>> merDevList = new ArrayList<Map<String, Object>>();
	    List<Map<String, Object>> merUserList = new ArrayList<Map<String, Object>>();
		int totalCount = 0;
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {
			if (currentPage == null || "".equals(currentPage.trim())) {
				currentPage = "0";
			}
			if (pageSize == null || "".equals(pageSize.trim())) {
				pageSize = "10";
			}
			int currentpages = 0;
			if ((currentPage != null || !"".equals(currentPage.trim()))) {
				currentpages = Integer.parseInt(currentPage);
				currentpages = currentpages * Integer.parseInt(pageSize);
			}
			searchMap.put("MerUser_ID", userId);
			merUserList = merchantuserInfoService.findUserList(searchMap);
			merUserMap = merUserList.get(0);
			searchMap.put("currentPage", currentpages);
			searchMap.put("pageSize", Integer.parseInt(pageSize));
			searchMap.put("queryMerDevNameLike", queryMerDevNameLike);
			searchMap.put("queryMerDevCode", queryMerDevCode);
			searchMap.put("MerInf_Code", merUserMap.get("MerInf_Code"));
			System.err.println("MerInf_Code="+merUserMap.get("MerInf_Code"));
			totalCount = merDevInfoService.findMerDevInfoCount(searchMap);// 总记录数
			if (totalCount > 0) {
				merDevList = merDevInfoService.findMerDevInfoList(searchMap);
			}
		}
		// list转json str
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("currentPage", Integer.parseInt(currentPage) + 1);
		resultMap.put("pageTotal", totalCount);
		resultMap.put("pageList", merDevList);
		resultMap.put("success", "1");

		return resultMap;
	}
	/**
	 * 验证
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/validate")
	@ResponseBody
	public boolean validateDevFactory(HttpServletRequest req) {
		String addDevInf_Code = req.getParameter("addDevInf_Code");
		String updateDevInf_Code = req.getParameter("updateDevInf_Code");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		String excludeId = req.getParameter("excludeId");
		System.err.println("后台接受修改的excludeId="+excludeId+"---updateDevInf_Code="+updateDevInf_Code);
		System.out.println("token=" + token + ",userId=" + userId );
		boolean result = false;
		Map<String, Object> searchMap = new HashMap<String, Object>();
		if (excludeId != null && !"".equals(excludeId.trim())) {
			searchMap.put("excludeId", excludeId);
			searchMap.put("updateDevInf_Code", updateDevInf_Code);
		}
		if (addDevInf_Code != null && !"".equals(addDevInf_Code.trim())) {
			searchMap.put("addDevInf_Code", addDevInf_Code);
		}
		int count = merDevInfoService.findMerDevInfoCount(searchMap);
		if (count == 0) {
			result = true;
		}
		System.err.println("count="+count+"result="+result);
		return result;
	}
	// 初始化添加
		@RequestMapping("/initAdd")
		@ResponseBody
		public Map<String, Object> initAdd(HttpServletRequest req, HttpServletResponse resp) throws IOException {
			String token = req.getHeader("token");
			String userId = req.getHeader("userId");
			System.out.println("token=" + token + ",userId=" + userId );

			Map<String, Object> resultMap = new HashMap<String, Object>();
			List<Map<String, Object>> devInfoList = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> areaList = new ArrayList<Map<String, Object>>();
			if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {
				Map<String, Object> searchMap = new HashMap<String, Object>();
				devInfoList = devInfoService.findDevInfoList(searchMap);
				areaList = areaInfoService.findAreaInfoList(searchMap);
			}
			resultMap.put("devInfoList", devInfoList);
			resultMap.put("areaList", areaList);
			resultMap.put("success", "1");
			return resultMap;

		}
	/**
	 * 保存商户设备！
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public Map<String, Object> saveAdd(HttpServletRequest req) {
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		System.out.println(token + userId);
		Map<String, Object> searchMap = new HashMap<String, Object>();
		Map<String, Object> merUserMap = new HashMap<String, Object>();
		List<Map<String, Object>> merInfoList = new ArrayList<Map<String, Object>>();
		searchMap.put("MerUser_ID", userId);
		merInfoList = merInfoService.findMerInfoList(searchMap);
		merUserMap = merInfoList.get(0);
		
		String merdevCode = RandomCode.getRandomCode();
		String merdevname = req.getParameter("addMerDevName");
		String orgCode = (String)merUserMap.get("Org_Code");
		System.err.println("orgCode="+orgCode);
		String ageinfCode = (String)merUserMap.get("AgeInf_Code");
		String merinfCode = (String)merUserMap.get("MerInf_Code");
		String areaCode = req.getParameter("addArea_Code");
		String devinfCode = req.getParameter("addDevInf_Code");
		String devhwver = req.getParameter("addDevHwVer");
		String devappver = req.getParameter("addDevAppVer");
		String devparamver = req.getParameter("addDevParamVer");
		String devotherver = req.getParameter("addDevOtherVer");
		String longitude = req.getParameter("addLongitude");
		String latitude = req.getParameter("addLatitude");
		String deliveryaddress = req.getParameter("addDeliveryAddress");
		
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {

			MerDev merDev = new MerDev();
			merDev.setMerdevCode(merdevCode);
			merDev.setMerdevname(merdevname);
			merDev.setOrgCode(orgCode);
			merDev.setAgeinfCode(ageinfCode);
			merDev.setMerinfCode(merinfCode);
			merDev.setAreaCode(areaCode);
			merDev.setDevinfCode(devinfCode);
			merDev.setDevhwver(devhwver);
			merDev.setDevappver(devappver);
			merDev.setDevparamver(devparamver);
			merDev.setDevotherver(devotherver);
			merDev.setLongitude(longitude);
			merDev.setLatitude(latitude);
			merDev.setDeliveryaddress(deliveryaddress);
			merDev.setCrtuserid(Integer.parseInt(userId));
			
			int success = 0;
			success = merDevInfoService.insertMerDevInfo(merDev);
			if (success > 0) {
				resultMap.put("success", "1");
			} else {
				resultMap.put("msg", "添加失败");
			}
		} else {
			resultMap.put("msg", "请上传相关参数");
		}
		return resultMap;
	}

	// 初始化前台修改设备厂商数据
	@RequestMapping("/initUpdate")
	@ResponseBody
	public Map<String, Object> initUpdate(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		String DevInfoId = req.getParameter("DevInfoId");
		System.out.println("token=" + token + ",userId=" + userId + ",DevInfoId=" + DevInfoId);

		Map<String, Object> merDevMap = new HashMap<String, Object>();
		Map<String, Object> resultMap = new HashMap<String, Object>();
		
		List<Map<String, Object>> MerDevStatusList = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> DevWorkStatusList = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> devInfoList = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> areaList = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> merDevList = new ArrayList<Map<String, Object>>();
		
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim()) && DevInfoId != null
				&& !"".equals(DevInfoId.trim())) {
			Map<String, Object> searchMap = new HashMap<String, Object>();
			areaList = areaInfoService.findAreaInfoList(searchMap);
			devInfoList = devInfoService.findDevInfoList(searchMap);
			searchMap.put("dicsCode", "MerDevStatus");
			MerDevStatusList = dicsInfoService.findDicsList(searchMap);
			searchMap.put("dicsCode", "DevWorkStatus");
			DevWorkStatusList = dicsInfoService.findDicsList(searchMap);
			searchMap.put("MerDevId", DevInfoId);
			System.err.println("要修改的ID="+DevInfoId);
			merDevList = merDevInfoService.findMerDevInfoList(searchMap);
			merDevMap = merDevList.get(0);
			System.err.println("merDevMap="+merDevMap);
		}
		resultMap.put("merDevMap", merDevMap);
		resultMap.put("MerDevStatusList", MerDevStatusList);
		resultMap.put("DevWorkStatusList", DevWorkStatusList);
		resultMap.put("devInfoList", devInfoList);
		resultMap.put("areaList", areaList);
		resultMap.put("success", "1");
		return resultMap;

	}

	/*
	 * 保存修改前台设备型号
	 */
	@RequestMapping("/update")
	@ResponseBody
	public Map<String, Object> saveUpdate(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String devinfCode = req.getParameter("updateDevInf_Code");
		String merdevname = req.getParameter("updateMerDevName");
		String areaCode = req.getParameter("updateArea_Code");
		String devhwver = req.getParameter("updateDevHwVer");
		String devappver = req.getParameter("updateDevAppVer");
		String devparamver = req.getParameter("updateDevParamVer");
		String devotherver = req.getParameter("updateDevOtherVer");
		String longitude = req.getParameter("updateLongitude");
		String latitude = req.getParameter("updateLatitude");
		String deliveryaddress = req.getParameter("updateDeliveryAddress");
		String DevInfoId = req.getParameter("excludeId");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		System.out.println("token:"+token +"userID："+ userId+"DevInfoId:"+DevInfoId);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {

			MerDev merDev = new MerDev();
			merDev.setMerdevId(Integer.parseInt(DevInfoId));
			merDev.setMerdevname(merdevname);
			merDev.setAreaCode(areaCode);
			merDev.setDevinfCode(devinfCode);
			merDev.setDevhwver(devhwver);
			merDev.setDevappver(devappver);
			merDev.setDevparamver(devparamver);
			merDev.setDevotherver(devotherver);
			merDev.setLongitude(longitude);
			merDev.setLatitude(latitude);
			merDev.setDeliveryaddress(deliveryaddress);
			merDev.setUptuserid(Integer.parseInt(userId));
			int success = merDevInfoService.updateMerDevInfo(merDev);
			if (success > 0) {
				resultMap.put("success", "1");
			} else {
				resultMap.put("msg", "修改失败");
			}
		} else {
			resultMap.put("msg", "请上传相关参数");
		}
		return resultMap;
	}

	// 前台设备型号详情
	@RequestMapping("/detail")
	@ResponseBody
	public Map<String, Object> detailFront(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		return initUpdate(req,resp);
	}

	// 删除前台菜单数据
	@RequestMapping("/delete")
	@ResponseBody
	public Map<String, Object> delete(HttpServletRequest req, HttpServletResponse resp)  {
		String DevInfo_ID = req.getParameter("DevInfo_ID");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");

		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		if (DevInfo_ID != null && !"".equals(DevInfo_ID.trim()) && token != null && !"".equals(token.trim()) && userId != null
				&& !"".equals(userId.trim())) {
			int success = merDevInfoService.delMerDevInfo(Integer.parseInt(DevInfo_ID));
			if (success > 0) {
				resultMap.put("success", "1");
			} else {
				resultMap.put("msg", "删除失败!");
			}
		}
		return resultMap;
	}
}
