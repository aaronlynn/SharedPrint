package com.sharedprint.merchant.merDev.pojo;

import java.util.Date;

public class MerDev {
    private Integer merdevId;

    private String merdevCode;

    private String merdevname;

    private String merdevstatus;

    private String orgCode;

    private String areaCode;

    private String ageinfCode;

    private String merinfCode;

    private String devinfCode;

    private String devworkstatus;

    private String sn;

    private String devhwver;

    private String devappver;

    private String devparamver;

    private String devotherver;

    private String deliveryaddress;

    private String longitude;

    private String latitude;

    private Integer crtuserid;

    private Date crtdate;

    private Integer uptuserid;

    private Date lastuptdate;

    private Integer chkuserid;

    private Date chkdate;

    private String chkremark;

    public Integer getMerdevId() {
        return merdevId;
    }

    public void setMerdevId(Integer merdevId) {
        this.merdevId = merdevId;
    }

    public String getMerdevCode() {
        return merdevCode;
    }

    public void setMerdevCode(String merdevCode) {
        this.merdevCode = merdevCode == null ? null : merdevCode.trim();
    }

    public String getMerdevname() {
        return merdevname;
    }

    public void setMerdevname(String merdevname) {
        this.merdevname = merdevname == null ? null : merdevname.trim();
    }

    public String getMerdevstatus() {
        return merdevstatus;
    }

    public void setMerdevstatus(String merdevstatus) {
        this.merdevstatus = merdevstatus == null ? null : merdevstatus.trim();
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode == null ? null : orgCode.trim();
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode == null ? null : areaCode.trim();
    }

    public String getAgeinfCode() {
        return ageinfCode;
    }

    public void setAgeinfCode(String ageinfCode) {
        this.ageinfCode = ageinfCode == null ? null : ageinfCode.trim();
    }

    public String getMerinfCode() {
        return merinfCode;
    }

    public void setMerinfCode(String merinfCode) {
        this.merinfCode = merinfCode == null ? null : merinfCode.trim();
    }

    public String getDevinfCode() {
        return devinfCode;
    }

    public void setDevinfCode(String devinfCode) {
        this.devinfCode = devinfCode == null ? null : devinfCode.trim();
    }

    public String getDevworkstatus() {
        return devworkstatus;
    }

    public void setDevworkstatus(String devworkstatus) {
        this.devworkstatus = devworkstatus == null ? null : devworkstatus.trim();
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn == null ? null : sn.trim();
    }

    public String getDevhwver() {
        return devhwver;
    }

    public void setDevhwver(String devhwver) {
        this.devhwver = devhwver == null ? null : devhwver.trim();
    }

    public String getDevappver() {
        return devappver;
    }

    public void setDevappver(String devappver) {
        this.devappver = devappver == null ? null : devappver.trim();
    }

    public String getDevparamver() {
        return devparamver;
    }

    public void setDevparamver(String devparamver) {
        this.devparamver = devparamver == null ? null : devparamver.trim();
    }

    public String getDevotherver() {
        return devotherver;
    }

    public void setDevotherver(String devotherver) {
        this.devotherver = devotherver == null ? null : devotherver.trim();
    }

    public String getDeliveryaddress() {
        return deliveryaddress;
    }

    public void setDeliveryaddress(String deliveryaddress) {
        this.deliveryaddress = deliveryaddress == null ? null : deliveryaddress.trim();
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude == null ? null : longitude.trim();
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude == null ? null : latitude.trim();
    }

    public Integer getCrtuserid() {
        return crtuserid;
    }

    public void setCrtuserid(Integer crtuserid) {
        this.crtuserid = crtuserid;
    }

    public Date getCrtdate() {
        return crtdate;
    }

    public void setCrtdate(Date crtdate) {
        this.crtdate = crtdate;
    }

    public Integer getUptuserid() {
        return uptuserid;
    }

    public void setUptuserid(Integer uptuserid) {
        this.uptuserid = uptuserid;
    }

    public Date getLastuptdate() {
        return lastuptdate;
    }

    public void setLastuptdate(Date lastuptdate) {
        this.lastuptdate = lastuptdate;
    }

    public Integer getChkuserid() {
        return chkuserid;
    }

    public void setChkuserid(Integer chkuserid) {
        this.chkuserid = chkuserid;
    }

    public Date getChkdate() {
        return chkdate;
    }

    public void setChkdate(Date chkdate) {
        this.chkdate = chkdate;
    }

    public String getChkremark() {
        return chkremark;
    }

    public void setChkremark(String chkremark) {
        this.chkremark = chkremark == null ? null : chkremark.trim();
    }
}