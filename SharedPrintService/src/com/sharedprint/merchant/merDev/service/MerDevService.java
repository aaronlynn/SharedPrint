package com.sharedprint.merchant.merDev.service;

import java.util.List;
import java.util.Map;

import com.sharedprint.merchant.merDev.pojo.MerDev;

public interface MerDevService {

	/**
	 * 获取设备列表
	 * @param searchMap
	 * @return
	 */
	List<Map<String, Object>> findMerDevInfoList(Map<String, Object> searchMap);
	
	/**
	 * 获取设备数量
	 * @param searchMap
	 * @return
	 */
	int findMerDevInfoCount(Map<String, Object> searchMap);

	/**
	 * 新增设备
	 * @param merDev
	 * @return
	 */
	int insertMerDevInfo(MerDev merDev);
	/**
	 * 更新设备
	 * @param merDev
	 * @return
	 */
	int updateMerDevInfo(MerDev merDev);
	/**
	 * 删除设备
	 * @param MerDevInfoID
	 * @return
	 */
	int delMerDevInfo(int MerDevInfoID);
	/**
	 * 根据设备号查找设备编号
	 * @param merdevCode
	 * @return
	 */
	String findDevinfCode(String merdevCode);

	
}
