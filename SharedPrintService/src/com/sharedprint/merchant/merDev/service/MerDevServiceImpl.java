package com.sharedprint.merchant.merDev.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sharedprint.merchant.merDev.mapper.MerDevMapper;
import com.sharedprint.merchant.merDev.pojo.MerDev;

@Service
public class MerDevServiceImpl implements MerDevService {

	@Autowired
	private MerDevMapper merDevMapper;
	
	@Override
	public List<Map<String, Object>> findMerDevInfoList(Map<String, Object> searchMap) {
		return merDevMapper.findMerDevInfoList(searchMap);
	}

	@Override
	public int findMerDevInfoCount(Map<String, Object> searchMap) {
		
		return merDevMapper.findMerDevInfoCount(searchMap);
	}

	@Override
	public int insertMerDevInfo(MerDev merDev) {
		return merDevMapper.insertSelective(merDev);
	}

	@Override
	public int updateMerDevInfo(MerDev merDev) {
	
		return merDevMapper.updateByPrimaryKeySelective(merDev);
	}

	@Override
	public int delMerDevInfo(int MerDevInfoId) {
		return merDevMapper.deleteByPrimaryKey(MerDevInfoId);
	}

	@Override
	public String findDevinfCode(String merdevCode) {
		
		return merDevMapper.findDevinfCode(merdevCode);
	}



}
