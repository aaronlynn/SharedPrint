package com.sharedprint.merchant.fileCenter.mapper;

import java.util.List;
import java.util.Map;

import com.sharedprint.merchant.fileCenter.pojo.PerFile;

public interface PerFileMapper {
    int deleteByPrimaryKey(Integer perfileId);

    int insert(PerFile record);

    int insertSelective(PerFile record);

    PerFile selectByPrimaryKey(Integer perfileId);

    int updateByPrimaryKeySelective(PerFile record);

    int updateByPrimaryKey(PerFile record);

	List<Map<String, Object>> findFileList(Map<String, Object> searchMap);

	int findFileCount(Map<String, Object> searchMap);
}