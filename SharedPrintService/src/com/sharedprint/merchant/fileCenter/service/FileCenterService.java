package com.sharedprint.merchant.fileCenter.service;

import java.util.List;
import java.util.Map;

import com.sharedprint.merchant.fileCenter.pojo.PerFile;

public interface FileCenterService {

	/**
	 * 获取文件列表
	 * @param searchMap
	 * @return
	 */
	List<Map<String, Object>> findFileList(Map<String, Object> searchMap);
	
	/**
	 * 获取文件数量
	 * @param searchMap
	 * @return
	 */
	int findFileCount(Map<String, Object> searchMap);

	/**
	 * 新增文件
	 * @param perFile
	 * @return
	 */
	int insertFile(PerFile perFile);
	/**
	 * 删除文件
	 * @param PerFileID
	 * @return
	 */
	int delMerDevInfo(int PerFileID);

	
}
