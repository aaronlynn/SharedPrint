package com.sharedprint.merchant.fileCenter.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sharedprint.merchant.fileCenter.mapper.PerFileMapper;
import com.sharedprint.merchant.fileCenter.pojo.PerFile;
import com.sharedprint.merchant.merDev.mapper.MerDevMapper;
import com.sharedprint.merchant.merDev.pojo.MerDev;

@Service
public class FileCenterServiceImpl implements FileCenterService {

	@Autowired
	private PerFileMapper perFileMapper;
	
	@Override
	public List<Map<String, Object>> findFileList(Map<String, Object> searchMap) {
		return perFileMapper.findFileList(searchMap);
	}

	@Override
	public int findFileCount(Map<String, Object> searchMap) {
		
		return perFileMapper.findFileCount(searchMap);
	}

	@Override
	public int insertFile(PerFile perFile) {
		return perFileMapper.insertSelective(perFile);
	}


	@Override
	public int delMerDevInfo(int MerDevInfoId) {
		return perFileMapper.deleteByPrimaryKey(MerDevInfoId);
	}




}
