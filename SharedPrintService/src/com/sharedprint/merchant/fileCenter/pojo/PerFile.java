package com.sharedprint.merchant.fileCenter.pojo;

import java.util.Date;

public class PerFile {
    private Integer perfileId;

    private String peruserCode;

    private String filename;

    private String filepath;

    private String filetype;

    private String remark;

    private Integer oprId;

    private Date oprDate;

    public Integer getPerfileId() {
        return perfileId;
    }

    public void setPerfileId(Integer perfileId) {
        this.perfileId = perfileId;
    }

    public String getPeruserCode() {
        return peruserCode;
    }

    public void setPeruserCode(String peruserCode) {
        this.peruserCode = peruserCode == null ? null : peruserCode.trim();
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename == null ? null : filename.trim();
    }

    public String getFilepath() {
        return filepath;
    }

    public void setFilepath(String filepath) {
        this.filepath = filepath == null ? null : filepath.trim();
    }

    public String getFiletype() {
        return filetype;
    }

    public void setFiletype(String filetype) {
        this.filetype = filetype == null ? null : filetype.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Integer getOprId() {
        return oprId;
    }

    public void setOprId(Integer oprId) {
        this.oprId = oprId;
    }

    public Date getOprDate() {
        return oprDate;
    }

    public void setOprDate(Date oprDate) {
        this.oprDate = oprDate;
    }
}