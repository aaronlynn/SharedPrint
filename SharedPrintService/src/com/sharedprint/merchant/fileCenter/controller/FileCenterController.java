package com.sharedprint.merchant.fileCenter.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sharedprint.dics.service.DicsInfoService;
import com.sharedprint.merchant.fileCenter.pojo.PerFile;
import com.sharedprint.merchant.fileCenter.service.FileCenterService;
import com.sharedprint.merchant.merDev.pojo.MerDev;
import com.sharedprint.merchant.merDev.service.MerDevService;
import com.sharedprint.merchant.user.service.MerchantUserInfoService;
import com.sharedprint.system.area.service.AreaInfoService;
import com.sharedprint.system.devFactory.service.DevFactoryInfoService;
import com.sharedprint.system.devInfo.pojo.DevInfo;
import com.sharedprint.system.devInfo.service.DevInfoService;
import com.sharedprint.system.devModel.pojo.DevModel;
import com.sharedprint.system.devModel.service.DevModelInfoService;
import com.sharedprint.system.merchant.service.MerInfoService;
import com.sharedprint.system.organization.service.OrgInfoService;
import com.sharedprint.user.user.service.PerUserService;
import com.sharedprint.util.RandomCode;

/**
 * 设备管理的控制器
 * 
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/api/fileCenter")
public class FileCenterController {

	@Autowired
	private PerUserService perUserService;
	@Autowired
	private FileCenterService fileCenterService;
	/**
	 * 获取文件列表数据
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/queryPageList")
	@ResponseBody
	public Map<String, Object> queryDevList(HttpServletRequest req) {
		String queryFileNameLike = req.getParameter("queryFileNameLike");//名称
		String currentPage = req.getParameter("currentPage");
		String pageSize = req.getParameter("pageSize");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		System.err.println("userId="+userId);
		Map<String, Object> searchMap = new HashMap<String, Object>();
		Map<String, Object> perUserMap = new HashMap<String, Object>();
	    List<Map<String, Object>> fileList = new ArrayList<Map<String, Object>>();
	    List<Map<String, Object>> perUserList = new ArrayList<Map<String, Object>>();
		int totalCount = 0;
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {
			if (currentPage == null || "".equals(currentPage.trim())) {
				currentPage = "0";
			}
			if (pageSize == null || "".equals(pageSize.trim())) {
				pageSize = "10";
			}
			int currentpages = 0;
			if ((currentPage != null || !"".equals(currentPage.trim()))) {
				currentpages = Integer.parseInt(currentPage);
				currentpages = currentpages * Integer.parseInt(pageSize);
			}
			searchMap.put("userId", userId);
			perUserList = perUserService.findUserList(searchMap);
			perUserMap = perUserList.get(0);
			searchMap.put("currentPage", currentpages);
			searchMap.put("pageSize", Integer.parseInt(pageSize));
			searchMap.put("queryFileNameLike", queryFileNameLike);
			searchMap.put("PerUser_Code", perUserMap.get("PerUser_Code"));
			System.err.println("PerUser_Code="+perUserMap.get("PerUser_Code"));
			totalCount = fileCenterService.findFileCount(searchMap);// 总记录数
			if (totalCount > 0) {
				fileList = fileCenterService.findFileList(searchMap);
			}
		}
		// list转json str
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("currentPage", Integer.parseInt(currentPage) + 1);
		resultMap.put("pageTotal", totalCount);
		resultMap.put("pageList", fileList);
		resultMap.put("success", "1");

		return resultMap;
	}
	/**
	 * 保存文件！
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public Map<String, Object> saveAdd(HttpServletRequest req) {
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		System.out.println(token + userId);
		String 	filename = req.getParameter("addFileName");
		String 	filepath = req.getParameter("addFilePath");
		String 	filetype = req.getParameter("addFileType");
		String 	remark = req.getParameter("addRemark");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		Map<String, Object> searchMap = new HashMap<String, Object>();
		Map<String, Object> perUserMap = new HashMap<String, Object>();
		searchMap.put("userId", userId);
		perUserMap = perUserService.findUserList(searchMap).get(0);
		resultMap.put("success", "0");
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {

			PerFile perFile = new PerFile();
			perFile.setFilename(filename);
			perFile.setFilepath(filepath);
			perFile.setFiletype(filetype);
			perFile.setOprId(Integer.parseInt(userId));
			perFile.setRemark(remark);
			perFile.setPeruserCode((String)perUserMap.get("PerUser_Code"));
			System.err.println("添加PerUser_Code="+perUserMap.get("PerUser_Code"));
			int success = 0;
			success = fileCenterService.insertFile(perFile);
			if (success > 0) {
				resultMap.put("success", "1");
			} else {
				resultMap.put("msg", "添加失败");
			}
		} else {
			resultMap.put("msg", "请上传相关参数");
		}
		return resultMap;
	}

	// 初始化修改和详情的数据
	@RequestMapping("/initUpdate")
	@ResponseBody
	public Map<String, Object> initUpdate(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		String PerFile_ID = req.getParameter("PerFile_ID");
		System.out.println("token=" + token + ",userId=" + userId + ",PerFile_ID=" + PerFile_ID);

		Map<String, Object> fileMap = new HashMap<String, Object>();
		Map<String, Object> resultMap = new HashMap<String, Object>();
		
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim()) && PerFile_ID != null
				&& !"".equals(PerFile_ID.trim())) {
			Map<String, Object> searchMap = new HashMap<String, Object>();
			searchMap.put("FileId", PerFile_ID);
			fileMap = fileCenterService.findFileList(searchMap).get(0);
		}
		resultMap.put("fileMap", fileMap);
		resultMap.put("success", "1");
		return resultMap;

	}


	// 前台设备型号详情
	@RequestMapping("/detail")
	@ResponseBody
	public Map<String, Object> detailFront(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		return initUpdate(req,resp);
	}

	// 删除前台菜单数据
	@RequestMapping("/delete")
	@ResponseBody
	public Map<String, Object> delete(HttpServletRequest req, HttpServletResponse resp)  {
		String DevInfo_ID = req.getParameter("DevInfo_ID");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");

		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		if (DevInfo_ID != null && !"".equals(DevInfo_ID.trim()) && token != null && !"".equals(token.trim()) && userId != null
				&& !"".equals(userId.trim())) {
			int success = fileCenterService.delMerDevInfo(Integer.parseInt(DevInfo_ID));
			if (success > 0) {
				resultMap.put("success", "1");
			} else {
				resultMap.put("msg", "删除失败!");
			}
		}
		return resultMap;
	}
}
