package com.sharedprint.merchant.user.mapper;

import java.util.List;
import java.util.Map;

import com.sharedprint.merchant.user.pojo.MerchantUserInfo;

public interface MerchantUserInfoMapper {
    int deleteByPrimaryKey(Integer meruserId);

    int insert(MerchantUserInfo record);

    int insertSelective(MerchantUserInfo record);

    MerchantUserInfo selectByPrimaryKey(Integer meruserId);

    int updateByPrimaryKeySelective(MerchantUserInfo record);

    int updateByPrimaryKey(MerchantUserInfo record);
    
    /**
	 * 获取用户信息（登录）
	 * 
	 * @param searchMap
	 * @return
	 */
	List<Map<String, Object>> findUserList(Map<String, Object> searchMap);

}