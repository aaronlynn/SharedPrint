package com.sharedprint.merchant.user.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sharedprint.merchant.user.service.MerchantUserInfoService;
import com.sharedprint.util.Token;
@Controller
@RequestMapping("/api/merchantUserInfo")
public class MerchantUserInfoController {
	private static final Logger logger = Logger.getLogger("AgentUserInfoController");
	@Autowired
	private MerchantUserInfoService merchantuserInfoService;
	
	/**
	 * 商户用户登录
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/merchantLogin.action")
	@ResponseBody
	public Map<String, Object> login(HttpServletRequest request) {
		String userName = request.getParameter("username");
		String passwd = request.getParameter("upwd");

		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		if (userName != null && !"".equals(userName.trim()) && passwd != null && !"".equals(passwd.trim())) {
			Map<String, Object> searchMap = new HashMap<String, Object>();
			searchMap.put("userName", userName);
			searchMap.put("passwd", passwd);
			List<Map<String, Object>> userList = merchantuserInfoService.findUserList(searchMap);
			if (userList != null && userList.size() == 1) {
				Map<String, Object> userMap = userList.get(0);
				String token = Token.getTokenString(request.getSession());
				resultMap.put("success", "1");
				resultMap.put("token", token);
				resultMap.put("userId", userMap.get("MerUser_ID"));
				resultMap.put("trueName", userMap.get("MerUserName"));
				resultMap.put("userName", userMap.get("LoginName"));
				resultMap.put("merInfId", userMap.get("MerInf_Code"));
			}
		}
		return resultMap;
	}
}
