package com.sharedprint.merchant.user.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sharedprint.merchant.user.mapper.MerchantUserInfoMapper;

@Service
@Transactional
public class MerchantUserInfoServiceImpl implements MerchantUserInfoService {
	@Autowired
	private MerchantUserInfoMapper merchantUserInfoMapper ;

	@Override
	public List<Map<String, Object>> findUserList(Map<String, Object> searchMap) {
		return merchantUserInfoMapper.findUserList(searchMap);
	}
}
