package com.sharedprint.merchant.user.service;

import java.util.List;
import java.util.Map;

public interface MerchantUserInfoService {
	/**
	 * 获取用户信息
	 * 
	 * @param searchMap
	 * @return
	 */
	List<Map<String, Object>> findUserList(Map<String, Object> searchMap);

}
