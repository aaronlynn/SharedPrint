package com.sharedprint.interceptor;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;

public class LoginInterceptor implements HandlerInterceptor {
	
	@Override
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {

	}
	/**
	 * 
	 */
	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
			throws Exception {
		
	}

	/**
	 * 进入pageList验证token
	 */
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object arg2) throws Exception {
		String token = request.getHeader("token");
		String userId = request.getHeader("userId");
		System.out.println("LoginInterceptor...loging登录拦截,token="+token+",userId="+userId);
		/*
		if (StringUtils.isEmpty(token) || StringUtils.isEmpty(userId)) {
			Map<String, Object> resultMap = new HashMap<String, Object>();
			resultMap.put("success", "0");
			String result = JSON.toJSONString(resultMap);
			response.getWriter().print(result);
			return false;
		}
		else {
			return true;
		}
		*/
		return true;
	}

}
