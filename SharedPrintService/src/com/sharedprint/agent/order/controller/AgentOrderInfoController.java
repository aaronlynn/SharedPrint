package com.sharedprint.agent.order.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sharedprint.agent.order.service.AgentOrderInfoService;
import com.sharedprint.dics.service.DicsInfoService;

@Controller
@RequestMapping("/api/agentOrderInfo")
public class AgentOrderInfoController {
	@Autowired
	private AgentOrderInfoService agentOrderInfoService;
	@Autowired
	private DicsInfoService dicsInfoService;

	@RequestMapping("/agentQueryOrderInfo")
	@ResponseBody
	public Map<String, Object> queryOrderList(HttpServletRequest req) {
		String Crtyear = req.getParameter("querycrtyear");
		String Crtmonth = req.getParameter("querycrtmonth");
		String OrderCode = req.getParameter("OrderCode");
		String OrderStatus = req.getParameter("OrderStatus");
		String currentPage = req.getParameter("currentPage");
		String pageSize = req.getParameter("pageSize");
		String orderByValue = req.getParameter("orderByValue");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		String agentId = req.getHeader("agentId");
		String merchantId = req.getHeader("merchantId");
		String clientId = req.getHeader("clientId");
		List<Map<String, Object>> orderList = new ArrayList<Map<String, Object>>();
		int totalCount = 0;
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {
			if (currentPage == null || "".equals(currentPage.trim())) {
				currentPage = "0";
			}
			if (pageSize == null || "".equals(pageSize.trim())) {
				pageSize = "10";
			}
			Map<String, Object> searchMap = new HashMap<String, Object>();
			int currentpages = 0;
			if ((currentPage != null || !"".equals(currentPage.trim()))) {
				currentpages = Integer.parseInt(currentPage);
				currentpages = currentpages * Integer.parseInt(pageSize);
			}
			searchMap.put("dateyear", null);
			searchMap.put("datemonth", null);
			if (Crtyear != null && !"".equals(Crtyear.trim()) && !"undefined".equals(Crtyear.trim())) {
				searchMap.put("dateyear", Crtyear);
			}
			if (Crtmonth != null && !"".equals(Crtmonth.trim()) && !"undefined".equals(Crtmonth.trim())) {
				searchMap.put("datemonth", Crtmonth);
			}

			searchMap.put("currentPage", currentpages);
			searchMap.put("pageSize", Integer.parseInt(pageSize));
			searchMap.put("OrderStatus", OrderStatus);
			searchMap.put("OrderCode", OrderCode);

			if (agentId != null && !"".equals(agentId.trim())) {
				searchMap.put("agentId", agentId);//代理商用户
			}
			if (merchantId != null && !"".equals(merchantId.trim())) {
				searchMap.put("merchantId", merchantId);// 商户用户
			}
			if (clientId != null && !"".equals(clientId.trim())) {
				searchMap.put("clientId", clientId);// 消费者用户
			}
			totalCount = agentOrderInfoService.findOrderCount(searchMap);// 总记录数
			if (totalCount > 0) {
				if (orderByValue == null || "".equals(orderByValue.trim())) {
					searchMap.put("orderByOrderIdDesc", "1");
				}
				orderList = agentOrderInfoService.findOrderList(searchMap);
			}
		}
		// 获取当前年月
		Calendar cale = null;
		cale = Calendar.getInstance();
		int year = cale.get(Calendar.YEAR);
		int month = cale.get(Calendar.MONTH) + 1;

		int[] tenyear = new int[11];
		for (int i = 0; i <= 10; i++) {
			tenyear[i] = year - i;
		}
		// 获取数据字典
		Map<String, Object> dicsSearchMap = new HashMap<String, Object>();
		List<Map<String, Object>> dicsList = new ArrayList<Map<String, Object>>();
		dicsSearchMap.put("dicsCode", "OrderStatus");
		dicsList = dicsInfoService.findDicsList(dicsSearchMap);
		for (int i = 0; i < orderList.size(); i++) {
			for (int j = 0; j < dicsList.size(); j++) {
				if (orderList.get(i).get("Order_Status").toString()
						.equals(dicsList.get(j).get("key_value").toString())) {
					orderList.get(i).put("Order_Status", dicsList.get(j).get("key_name").toString());
				}
			}
		}

		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("currentPage", Integer.parseInt(currentPage) + 1);
		resultMap.put("pageTotal", totalCount);
		resultMap.put("pageList", orderList);
		resultMap.put("year", year);
		resultMap.put("month", month);
		resultMap.put("tenyear", tenyear);
		resultMap.put("statusList", dicsList);
		resultMap.put("success", "1");

		return resultMap;
	}

	// 订单详情
	@RequestMapping("/detail")
	@ResponseBody
	public Map<String, Object> detailFront(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		String OrderCode = req.getParameter("OrderCode");

		Map<String, Object> orderMap = new HashMap<String, Object>();
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<Map<String, Object>> orderList = new ArrayList<Map<String, Object>>();
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {
			Map<String, Object> searchMap = new HashMap<String, Object>();
			searchMap.put("OrderCode", OrderCode);
			orderList = agentOrderInfoService.findOrderdetailList(searchMap);
			orderMap = orderList.get(0);
		}
		resultMap.put("orderMap", orderMap);
		resultMap.put("success", "1");
		return resultMap;

	}

	// 得到代理商首页销售量盈利表
	@RequestMapping("/querychart")
	@ResponseBody
	public Map<String, Object> querychart(HttpServletRequest req) {
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		int[] monthCount = new int[13];// 每月记录数
		Integer[] priceCount = new Integer[13];// 每月营业额
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {
			Map<String, Object> searchMap = new HashMap<String, Object>();
			// 获取当前年月
			Calendar cale = null;
			cale = Calendar.getInstance();
			int year = cale.get(Calendar.YEAR);

			for (int i = 0; i < 12; i++) {
				searchMap.put("dateyear", year);
				searchMap.put("datemonth", i);
				monthCount[i] = agentOrderInfoService.findOrderChartCount(searchMap);// 总记录数
				priceCount[i] = agentOrderInfoService.findOrderPriceChartCount(searchMap);
			}
		}

		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("monthCount", monthCount);
		resultMap.put("priceCount", priceCount);
		resultMap.put("success", "1");

		return resultMap;
	}
}
