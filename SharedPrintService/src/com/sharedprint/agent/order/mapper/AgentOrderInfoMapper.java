package com.sharedprint.agent.order.mapper;

import java.util.List;
import java.util.Map;

import com.sharedprint.agent.order.pojo.AgentOrderInfo;

public interface AgentOrderInfoMapper {
	int deleteByPrimaryKey(Integer orderId);

	int insert(AgentOrderInfo record);

	int insertSelective(AgentOrderInfo record);

	AgentOrderInfo selectByPrimaryKey(Integer orderId);

	int updateByPrimaryKeySelective(AgentOrderInfo record);

	int updateByPrimaryKey(AgentOrderInfo record);

	/**
	 * 获取订单列表
	 * 
	 * @param searchMap
	 * @return
	 */
	public List<Map<String, Object>> findOrderList(Map<String, Object> searchMap);

	/**
	 * 获取订单条数
	 * 
	 * @param searchMap
	 * @return
	 */
	public int findOrderCount(Map<String, Object> searchMap);

	/**
	 * 获取订单列表
	 * 
	 * @param searchMap
	 * @return
	 */
	public List<Map<String, Object>> findOrderdetailList(Map<String, Object> searchMap);

	/**
	 * 查询数据图表数据
	 * 
	 * @param searchMap
	 * @return
	 */
	int findOrderChartCount(Map<String, Object> searchMap);
    /**
     * 查询营业额
     */
	Integer findOrderPriceChartCount(Map<String, Object> searchMap);

}