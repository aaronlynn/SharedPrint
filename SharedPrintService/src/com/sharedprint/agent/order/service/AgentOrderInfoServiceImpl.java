package com.sharedprint.agent.order.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sharedprint.agent.order.mapper.AgentOrderInfoMapper;

@Service
public class AgentOrderInfoServiceImpl implements AgentOrderInfoService {
	@Autowired
	private AgentOrderInfoMapper agentOrderInfoMapper;

	@Override
	public List<Map<String, Object>> findOrderList(Map<String, Object> searchMap) {
		return agentOrderInfoMapper.findOrderList(searchMap);
	}

	@Override
	public int findOrderCount(Map<String, Object> searchMap) {
		return agentOrderInfoMapper.findOrderCount(searchMap);
	}

	@Override
	public List<Map<String, Object>> findOrderdetailList(Map<String, Object> searchMap) {
		return agentOrderInfoMapper.findOrderdetailList(searchMap);
	}

	@Override
	public int findOrderChartCount(Map<String, Object> searchMap) {
		return agentOrderInfoMapper.findOrderChartCount(searchMap);
	}

	@Override
	public Integer findOrderPriceChartCount(Map<String, Object> searchMap) {
		return agentOrderInfoMapper.findOrderPriceChartCount(searchMap);
	}
}
