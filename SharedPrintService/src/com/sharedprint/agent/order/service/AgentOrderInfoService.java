package com.sharedprint.agent.order.service;

import java.util.List;
import java.util.Map;

public interface AgentOrderInfoService {
	/**
	 * 获取订单列表
	 * 
	 * @param searchMap
	 * @return
	 */
	List<Map<String, Object>> findOrderList(Map<String, Object> searchMap);

	/**
	 * 获取订单列表数量
	 * 
	 * @param searchMap
	 * @return
	 */
	int findOrderCount(Map<String, Object> searchMap);

	/**
	 * 获取订单详情列表
	 * 
	 * @param searchMap
	 * @return
	 */
	List<Map<String, Object>> findOrderdetailList(Map<String, Object> searchMap);

	/**
	 * 查询数据图表数据
	 * 
	 * @param searchMap
	 * @return
	 */
	int findOrderChartCount(Map<String, Object> searchMap);

	/**
	 * 查询营业额
	 * 
	 * @param searchMap
	 * @return
	 */
	Integer findOrderPriceChartCount(Map<String, Object> searchMap);
}
