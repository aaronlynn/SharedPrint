package com.sharedprint.agent.user.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sharedprint.agent.user.service.AgentUserService;
import com.sharedprint.util.Token;
@Controller
@RequestMapping("/api/agentUserInfo")
public class AgentUserInfoController {
	private static final Logger logger = Logger.getLogger("AgentUserInfoController");
	@Autowired
	private AgentUserService agentuserInfoService;

	
	/**
	 * 代理商用户登录
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/agentLogin.action")
	@ResponseBody
	public Map<String, Object> login(HttpServletRequest request) {
		String userName = request.getParameter("username");
		String passwd = request.getParameter("upwd");

		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		if (userName != null && !"".equals(userName.trim()) && passwd != null && !"".equals(passwd.trim())) {
			Map<String, Object> searchMap = new HashMap<String, Object>();
			searchMap.put("userName", userName);
			searchMap.put("passwd", passwd);
			List<Map<String, Object>> userList = agentuserInfoService.findUserList(searchMap);
			if (userList != null && userList.size() == 1) {
				Map<String, Object> userMap = userList.get(0);
				String token = Token.getTokenString(request.getSession());
				resultMap.put("success", "1");
				resultMap.put("token", token);
				resultMap.put("userId", userMap.get("AgeUser_ID"));
				resultMap.put("agentId", userMap.get("AgeInf_Code"));
				resultMap.put("trueName", userMap.get("AgeTrueName"));
				resultMap.put("LoginName", userMap.get("LoginName"));
			}
		}
		return resultMap;
	}
}
