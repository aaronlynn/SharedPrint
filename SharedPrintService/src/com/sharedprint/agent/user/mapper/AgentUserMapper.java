package com.sharedprint.agent.user.mapper;

import java.util.List;
import java.util.Map;

import com.sharedprint.agent.user.pojo.AgentUser;

public interface AgentUserMapper {
	int deleteByPrimaryKey(Integer ageuserId);

	int insert(AgentUser record);

	int insertSelective(AgentUser record);

	AgentUser selectByPrimaryKey(Integer ageuserId);

	int updateByPrimaryKeySelective(AgentUser record);

	int updateByPrimaryKey(AgentUser record);

	/**
	 * 获取用户信息（登录）
	 * 
	 * @param searchMap
	 * @return
	 */
	List<Map<String, Object>> findUserList(Map<String, Object> searchMap);
}