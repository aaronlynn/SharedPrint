package com.sharedprint.agent.user.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sharedprint.agent.user.mapper.AgentUserMapper;


@Service
@Transactional
public class AgentUserServiceImpl implements AgentUserService{
	@Autowired
	private AgentUserMapper agentUserInfoMapper ;

	@Override
	public List<Map<String, Object>> findUserList(Map<String, Object> searchMap) {
		return agentUserInfoMapper.findUserList(searchMap);
	}

}
