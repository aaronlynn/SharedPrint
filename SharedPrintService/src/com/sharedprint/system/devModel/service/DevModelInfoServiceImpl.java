package com.sharedprint.system.devModel.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sharedprint.system.devModel.mapper.DevModelMapper;
import com.sharedprint.system.devModel.pojo.DevModel;

@Service
public class DevModelInfoServiceImpl implements DevModelInfoService {

	@Autowired
	private DevModelMapper devModelMapper;
	
	@Override
	public List<Map<String, Object>> findDevModelInfoList(Map<String, Object> searchMap) {
		return devModelMapper.findDevModelList(searchMap);
	}

	@Override
	public int findDevModelInfoCount(Map<String, Object> searchMap) {
		
		return devModelMapper.findDevModelCount(searchMap);
	}

	@Override
	public int insertDevModelInfo(DevModel DevModel) {
		return devModelMapper.insertSelective(DevModel);
	}

	@Override
	public int updateDevModelInfo(DevModel DevModel) {
	
		return devModelMapper.updateByPrimaryKeySelective(DevModel);
	}

	@Override
	public int delDevModelInfo(int DevModelId) {
		return devModelMapper.deleteByPrimaryKey(DevModelId);
	}



}
