package com.sharedprint.system.devModel.service;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.devModel.pojo.DevModel;

public interface DevModelInfoService {

	/**
	 * 获取设备型号列表
	 * @param searchMap
	 * @return
	 */
	List<Map<String, Object>> findDevModelInfoList(Map<String, Object> searchMap);
	
	/**
	 * 获取设备型号数量
	 * @param searchMap
	 * @return
	 */
	int findDevModelInfoCount(Map<String, Object> searchMap);

	/**
	 * 新增设备型号
	 * @param devModel
	 * @return
	 */
	int insertDevModelInfo(DevModel devModel);
	/**
	 * 更新设备型号
	 * @param devModel
	 * @return
	 */
	int updateDevModelInfo(DevModel devModel);
	/**
	 * 删除设备型号
	 * @param DevModelID
	 * @return
	 */
	int delDevModelInfo(int DevModelID);

	
}
