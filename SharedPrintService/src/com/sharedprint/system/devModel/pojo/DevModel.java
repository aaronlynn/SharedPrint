package com.sharedprint.system.devModel.pojo;

import java.util.Date;

public class DevModel {
    private Integer devmodelId;

    private String devmodelCode;

    private String devmodelname;

    private String devfactoryCode;

    private Integer sortno;

    private String remark;

    private Integer oprId;

    private Date oprDate;

    public Integer getDevmodelId() {
        return devmodelId;
    }

    public void setDevmodelId(Integer devmodelId) {
        this.devmodelId = devmodelId;
    }

    public String getDevmodelCode() {
        return devmodelCode;
    }

    public void setDevmodelCode(String devmodelCode) {
        this.devmodelCode = devmodelCode == null ? null : devmodelCode.trim();
    }

    public String getDevmodelname() {
        return devmodelname;
    }

    public void setDevmodelname(String devmodelname) {
        this.devmodelname = devmodelname == null ? null : devmodelname.trim();
    }

    public String getDevfactoryCode() {
        return devfactoryCode;
    }

    public void setDevfactoryCode(String devfactoryCode) {
        this.devfactoryCode = devfactoryCode == null ? null : devfactoryCode.trim();
    }

    public Integer getSortno() {
        return sortno;
    }

    public void setSortno(Integer sortno) {
        this.sortno = sortno;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Integer getOprId() {
        return oprId;
    }

    public void setOprId(Integer oprId) {
        this.oprId = oprId;
    }

    public Date getOprDate() {
        return oprDate;
    }

    public void setOprDate(Date oprDate) {
        this.oprDate = oprDate;
    }
}