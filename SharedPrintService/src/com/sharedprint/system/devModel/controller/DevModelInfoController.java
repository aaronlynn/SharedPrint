package com.sharedprint.system.devModel.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sharedprint.dics.service.DicsInfoService;
import com.sharedprint.system.devFactory.service.DevFactoryInfoService;
import com.sharedprint.system.devInfo.service.DevInfoService;
import com.sharedprint.system.devModel.pojo.DevModel;
import com.sharedprint.system.devModel.service.DevModelInfoService;
import com.sharedprint.system.menu.service.MenuInfoService;
import com.sharedprint.system.role.pojo.RoleInfo;
import com.sharedprint.system.role.service.RoleInfoService;
import com.sharedprint.util.RandomCode;

/**
 * 设备型号管理的控制器
 * 
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/api/devModelInfo")
public class DevModelInfoController {

	@Autowired
	private DevFactoryInfoService devFactoryInfoService;
	@Autowired
	private DevModelInfoService devModelInfoService;
	@Autowired
	private DevInfoService devInfoService;
	 
	/**
	 * 获取设备厂商列表数据
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/queryPageList")
	@ResponseBody
	public Map<String, Object> queryDevModelList(HttpServletRequest req) {
		String DevModelNameLike = req.getParameter("queryDevModelNameLike");
		String queryDevFactory = req.getParameter("queryDevFactory");
		String currentPage = req.getParameter("currentPage");
		String pageSize = req.getParameter("pageSize");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		List<Map<String, Object>> devModelList = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> devFactoryList = new ArrayList<Map<String, Object>>();
		Map<String, Object> searchMap = new HashMap<String, Object>();
		int totalCount = 0;
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {
			if (currentPage == null || "".equals(currentPage.trim())) {
				currentPage = "0";
			}
			if (pageSize == null || "".equals(pageSize.trim())) {
				pageSize = "10";
			}
			int currentpages = 0;
			if ((currentPage != null || !"".equals(currentPage.trim()))) {
				currentpages = Integer.parseInt(currentPage);
				currentpages = currentpages * Integer.parseInt(pageSize);
			}

			searchMap.put("currentPage", currentpages);
			searchMap.put("pageSize", Integer.parseInt(pageSize));
			searchMap.put("DevModelNameLike", DevModelNameLike);
			System.out.print(DevModelNameLike+"===="+queryDevFactory);
			searchMap.put("queryDevFactory", queryDevFactory);
			devFactoryList = devFactoryInfoService.findDevFactoryInfoList(searchMap);
			totalCount = devModelInfoService.findDevModelInfoCount(searchMap);// 总记录数
			if (totalCount > 0) {
				devModelList = devModelInfoService.findDevModelInfoList(searchMap);
			}
		}
		// list转json str
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("currentPage", Integer.parseInt(currentPage) + 1);
		resultMap.put("pageTotal", totalCount);
		resultMap.put("devFactoryList", devFactoryList);
		resultMap.put("DevFactoryMap", searchMap);
		resultMap.put("pageList", devModelList);
		resultMap.put("success", "1");

		return resultMap;
	}
	
	/**
	 * 验证设备型号名称是否存在！
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/validateDevModelName")
	@ResponseBody
	public boolean validateDevModel(HttpServletRequest req) {
		String addDevModelName = req.getParameter("addDevModelName");
		String updateDevModelName = req.getParameter("updateDevModelName");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		String excludeDevModelId = req.getParameter("excludeDevModelId");
		System.err.println("后台接受修改的id和名称"+excludeDevModelId+updateDevModelName);
		System.out.println("token=" + token + ",userId=" + userId );
		boolean result = false;
		Map<String, Object> searchMap = new HashMap<String, Object>();
		if (excludeDevModelId != null && !"".equals(excludeDevModelId.trim())) {
			searchMap.put("excludeDevModelId", excludeDevModelId);
			searchMap.put("updateDevModelName", updateDevModelName);
		}
		if (addDevModelName != null && !"".equals(addDevModelName.trim())) {
			searchMap.put("addDevModelName", addDevModelName);
		}
		int count = devModelInfoService.findDevModelInfoCount(searchMap);
		if (count == 0) {
			result = true;
		}
		return result;
	}
	// 初始化添加
		@RequestMapping("/initAdd")
		@ResponseBody
		public Map<String, Object> initAdd(HttpServletRequest req, HttpServletResponse resp) throws IOException {
			String token = req.getHeader("token");
			String userId = req.getHeader("userId");
			System.out.println("token=" + token + ",userId=" + userId );

			Map<String, Object> resultMap = new HashMap<String, Object>();
			List<Map<String, Object>> addFactoryList = new ArrayList<Map<String, Object>>();
			if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {
				Map<String, Object> searchMap = new HashMap<String, Object>();
				addFactoryList = devFactoryInfoService.findDevFactoryInfoList(searchMap);
			}
			resultMap.put("addFactoryList", addFactoryList);
			resultMap.put("success", "1");
			return resultMap;

		}
	/**
	 * 保存新增型号！
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public Map<String, Object> addDevModel(HttpServletRequest req) {
		String devModelCode = RandomCode.getRandomCode();
		String devModelname = req.getParameter("addDevModelName");
		String devfactoryCode = req.getParameter("addDevFactory_Code");
		String remark = req.getParameter("addRemark");
		String sortno = req.getParameter("addSortNo");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		System.out.println(token + userId);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {

			DevModel devModel = new DevModel();
			devModel.setDevmodelCode(devModelCode);
			devModel.setDevmodelname(devModelname);
			devModel.setDevfactoryCode(devfactoryCode);
			devModel.setRemark(remark);
			devModel.setSortno(Integer.parseInt(sortno));
			devModel.setOprId(Integer.parseInt(userId));
			int success = 0;
			success = devModelInfoService.insertDevModelInfo(devModel);
			if (success > 0) {
				resultMap.put("success", "1");
			} else {
				resultMap.put("msg", "添加失败");
			}
		} else {
			resultMap.put("msg", "请上传相关参数");
		}
		return resultMap;
	}

	// 初始化前台修改设备厂商数据
	@RequestMapping("/initUpdate")
	@ResponseBody
	public Map<String, Object> initUpdate(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		String DevModelId = req.getParameter("DevModelId");
		System.out.println("token=" + token + ",userId=" + userId + ",devModelId=" + DevModelId);

		Map<String, Object> devModelMap = new HashMap<String, Object>();
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<Map<String, Object>> devModelList = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> devFactoryList = new ArrayList<Map<String, Object>>();
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim()) && DevModelId != null
				&& !"".equals(DevModelId.trim())) {
			Map<String, Object> searchMap = new HashMap<String, Object>();
			devFactoryList = devFactoryInfoService.findDevFactoryInfoList(searchMap);
			searchMap.put("DevModelId", DevModelId);
			System.err.println("要修改的ID"+DevModelId);
			devModelList = devModelInfoService.findDevModelInfoList(searchMap);
			devModelMap = devModelList.get(0);
			System.err.println("devModelMap="+devModelMap);
		}
		resultMap.put("devModelMap", devModelMap);
		resultMap.put("devFactoryList", devFactoryList);
		resultMap.put("success", "1");
		return resultMap;

	}

	/*
	 * 保存修改前台设备型号
	 */
	@RequestMapping("/update")
	@ResponseBody
	public Map<String, Object> saveUpdate(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String devModelname = req.getParameter("updateDevModelName");
		String devfactoryCode = req.getParameter("updateDevFactory_Code");
		String remark = req.getParameter("updateRemark");
		String sortno = req.getParameter("updateSortNo");
		String excludeDevModelId = req.getParameter("excludeDevModelId");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		System.out.println("token:"+token +"userID："+ userId+"excludeDevModelId:"+excludeDevModelId);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {

			DevModel devModel = new DevModel();
			devModel.setDevmodelId(Integer.parseInt(excludeDevModelId));
			devModel.setDevmodelname(devModelname);
			devModel.setDevfactoryCode(devfactoryCode);
			devModel.setRemark(remark);
			devModel.setSortno(Integer.parseInt(sortno));
			devModel.setOprId(Integer.parseInt(userId));
			int success = devModelInfoService.updateDevModelInfo(devModel);
			if (success > 0) {
				resultMap.put("success", "1");
			} else {
				resultMap.put("msg", "修改失败");
			}
		} else {
			resultMap.put("msg", "请上传相关参数");
		}
		return resultMap;
	}

	// 前台设备型号详情
	@RequestMapping("/detail")
	@ResponseBody
	public Map<String, Object> detailFront(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		return initUpdate(req,resp);
	}

	// 删除前台菜单数据
	@RequestMapping("/delete")
	@ResponseBody
	public Map<String, Object> delete(HttpServletRequest req, HttpServletResponse resp)  {
		String DevModel_ID = req.getParameter("DevModel_ID");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");

		Map<String, Object> resultMap = new HashMap<String, Object>();
		Map<String, Object> searchMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		if (DevModel_ID != null && !"".equals(DevModel_ID.trim()) && token != null && !"".equals(token.trim()) && userId != null
				&& !"".equals(userId.trim())) {
			searchMap.put("queryDevModel", DevModel_ID);
			int devNow = devInfoService.findDevInfoCount(searchMap);
			if (devNow>0) {
				resultMap.put("msg", "该型号下有设备，  不允许删除 !");
			}else {
				int success = devModelInfoService.delDevModelInfo(Integer.parseInt(DevModel_ID));
				if (success > 0) {
					resultMap.put("success", "1");
				} else {
					resultMap.put("msg", "删除失败!");
				}
			}
		}
		return resultMap;
	}
}
