package com.sharedprint.system.devModel.mapper;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.devModel.pojo.DevModel;

public interface DevModelMapper {
    int deleteByPrimaryKey(Integer devmodelId);

    int insert(DevModel record);

    int insertSelective(DevModel record);

    DevModel selectByPrimaryKey(Integer devmodelId);

    int updateByPrimaryKeySelective(DevModel record);

    int updateByPrimaryKey(DevModel record);

	List<Map<String, Object>> findDevModelList(Map<String, Object> searchMap);

	int findDevModelCount(Map<String, Object> searchMap);
}