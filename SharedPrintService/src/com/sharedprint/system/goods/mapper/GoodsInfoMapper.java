
package com.sharedprint.system.goods.mapper;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.goods.pojo.GoodsInfo;

public interface GoodsInfoMapper {
    int deleteByPrimaryKey(Integer goodsId);

    int insert(GoodsInfo record);

    int insertSelective(GoodsInfo record);

    GoodsInfo selectByPrimaryKey(Integer goodsId);

    int updateByPrimaryKeySelective(GoodsInfo record);

    int updateByPrimaryKey(GoodsInfo record);
    
	/**
   	 * 获取云打印商品品牌商品列表
   	 * @param searchMap
   	 * @return
   	 */
   	public List<Map<String,Object>> findBrandGoodsList(Map<String,Object> searchMap);
   	
	public List<Map<String,Object>> findGoodsNameLikeList(Map<String,Object> searchMap);
   	
   	/**
   	 * 获取品牌商品条数
   	 * @param searchMap
   	 * @return
   	 */
   	public int findBrandGoodsCount(Map<String,Object> searchMap);
   	
	/**
   	 * 获取云打印商品类别商品列表
   	 * @param searchMap
   	 * @return
   	 */
   	public List<Map<String,Object>> findCateGoodsLists(Map<String,Object> searchMap);
	/**
   	 * 获取云打印商品类别商品条数
   	 * @param searchMap
   	 * @return
   	 */
   	public int findCateGoodsCount(Map<String,Object> searchMap);
   	
	public int findNameLikeGoodsCount(Map<String,Object> searchMap);

	/**
	 * 获取商品列表
	 * 
	 * @param searchMap
	 * @return
	 */
	public List<Map<String, Object>> findGoodsList(Map<String, Object> searchMap);

	/**
	 * 获取商品条数
	 * 
	 * @param searchMap
	 * @return
	 */
	public int findGoodsCount(Map<String, Object> searchMap);

	/**
	 * 提交审核信息
	 * 
	 * @param goodsInfo
	 * @return
	 */
	int upcheckInfo(GoodsInfo goodsInfo);

	/**
	 * 添加商品
	 * 
	 * @param goodsInfo
	 * @return
	 */
	int saveInfo(GoodsInfo goodsInfo);

	/**
	 * 修改
	 * 
	 * @param goodsInfo
	 * @return
	 */
	int updateInfo(GoodsInfo goodsInfo);

	/**
	 * 删除
	 * 
	 * @param goodsId
	 * @return
	 */
	int delGoods(int goodsId);
}
