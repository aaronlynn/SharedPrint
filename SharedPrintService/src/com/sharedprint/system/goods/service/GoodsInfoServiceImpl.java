package com.sharedprint.system.goods.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sharedprint.system.goods.mapper.GoodsInfoMapper;
import com.sharedprint.system.goods.pojo.GoodsInfo;

@Service
public class GoodsInfoServiceImpl implements GoodsInfoService{
    @Autowired
    private GoodsInfoMapper goodsInfoMapper;
    @Override
	public int findGoodsInfoCount(Map<String, Object> searchMap) {
		return goodsInfoMapper.findGoodsCount(searchMap);
	}

	@Override
	public List<Map<String, Object>> findGoodsInfoList(Map<String, Object> searchMap) {
		return goodsInfoMapper.findGoodsList(searchMap);
	}

	@Override
	public int upcheckInfo(GoodsInfo goodsInfo) {
		return goodsInfoMapper.upcheckInfo(goodsInfo);
	}

	@Override
	public int saveInfo(GoodsInfo goodsInfo) {
		return goodsInfoMapper.saveInfo(goodsInfo);
	}

	@Override
	public int updateInfo(GoodsInfo goodsInfo) {
		return goodsInfoMapper.updateInfo(goodsInfo);
	}

	@Override
	public int delGoods(int goodsId) {
		return goodsInfoMapper.delGoods(goodsId);
	}

	@Override
	public List<Map<String, Object>> findBrandGoodsList(Map<String, Object> searchMap) {
		
		return goodsInfoMapper.findBrandGoodsList(searchMap);
	}

	@Override
	public List<Map<String, Object>> findCateGoodsList(Map<String, Object> searchMap) {
		
		return goodsInfoMapper.findCateGoodsLists(searchMap);
	}

	@Override
	public int findBrandGoodsCount(Map<String, Object> searchMap) {
		
		return goodsInfoMapper.findBrandGoodsCount(searchMap);
	}

	@Override
	public int findCateGoodsCount(Map<String, Object> searchMap) {
	
		return goodsInfoMapper.findCateGoodsCount(searchMap);
	}

	@Override
	public List<Map<String, Object>> findGoodsNameLikeList(Map<String, Object> searchMap) {
		
		return goodsInfoMapper.findGoodsNameLikeList(searchMap);
	}

	@Override
	public int findNameLikeGoodsCount(Map<String, Object> searchMap) {
		
		return goodsInfoMapper.findNameLikeGoodsCount(searchMap);
	}

	
    
}
