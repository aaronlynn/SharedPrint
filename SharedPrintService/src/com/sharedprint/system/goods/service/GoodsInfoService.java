package com.sharedprint.system.goods.service;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.goods.pojo.GoodsInfo;

public interface GoodsInfoService {
	/**
	 * 查询列表数量
	 * 
	 * @param searchMap
	 * @return
	 */
	int findGoodsInfoCount(Map<String, Object> searchMap);

	/**
	 * 查询列表
	 * 
	 * @param searchMap
	 * @return
	 */
	List<Map<String, Object>> findGoodsInfoList(Map<String, Object> searchMap);

	/**
	 * 提交审核信息
	 * 
	 * @param goodsInfo
	 * @return
	 */
	int upcheckInfo(GoodsInfo goodsInfo);

	/**
	 * 添加商品
	 * 
	 * @param goodsInfo
	 * @return
	 */
	int saveInfo(GoodsInfo goodsInfo);

	/**
	 * 修改信息
	 * 
	 * @param goodsInfo
	 * @return
	 */
	int updateInfo(GoodsInfo goodsInfo);

	/**
	 * 删除
	 * 
	 * @param parseInt
	 * @return
	 */
	int delGoods(int goodsId);

	List<Map<String, Object>> findBrandGoodsList(Map<String, Object> searchMap);
	List<Map<String, Object>> findCateGoodsList(Map<String, Object> searchMap);
	int findBrandGoodsCount(Map<String, Object> searchMap);
	int findCateGoodsCount(Map<String, Object> searchMap);
	List<Map<String, Object>> findGoodsNameLikeList(Map<String, Object> searchMap);
	int findNameLikeGoodsCount(Map<String, Object> searchMap);
	

}
