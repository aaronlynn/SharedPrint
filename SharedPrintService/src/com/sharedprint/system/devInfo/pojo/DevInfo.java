package com.sharedprint.system.devInfo.pojo;

import java.util.Date;

public class DevInfo {
    private Integer devinfId;

    private String devinfCode;

    private String devinfname;

    private String devinfstatus;

    private String orgCode;

    private String devfactoryCode;

    private String devmodelCode;

    private String devhwver;

    private String devappver;

    private String devparamver;

    private String devotherver;

    private String devimage1;

    private String devimage2;

    private String devimage3;

    private String devimage4;

    private Integer oprId;

    private Date oprDate;

    public Integer getDevinfId() {
        return devinfId;
    }

    public void setDevinfId(Integer devinfId) {
        this.devinfId = devinfId;
    }

    public String getDevinfCode() {
        return devinfCode;
    }

    public void setDevinfCode(String devinfCode) {
        this.devinfCode = devinfCode == null ? null : devinfCode.trim();
    }

    public String getDevinfname() {
        return devinfname;
    }

    public void setDevinfname(String devinfname) {
        this.devinfname = devinfname == null ? null : devinfname.trim();
    }

    public String getDevinfstatus() {
        return devinfstatus;
    }

    public void setDevinfstatus(String devinfstatus) {
        this.devinfstatus = devinfstatus == null ? null : devinfstatus.trim();
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode == null ? null : orgCode.trim();
    }

    public String getDevfactoryCode() {
        return devfactoryCode;
    }

    public void setDevfactoryCode(String devfactoryCode) {
        this.devfactoryCode = devfactoryCode == null ? null : devfactoryCode.trim();
    }

    public String getDevmodelCode() {
        return devmodelCode;
    }

    public void setDevmodelCode(String devmodelCode) {
        this.devmodelCode = devmodelCode == null ? null : devmodelCode.trim();
    }

    public String getDevhwver() {
        return devhwver;
    }

    public void setDevhwver(String devhwver) {
        this.devhwver = devhwver == null ? null : devhwver.trim();
    }

    public String getDevappver() {
        return devappver;
    }

    public void setDevappver(String devappver) {
        this.devappver = devappver == null ? null : devappver.trim();
    }

    public String getDevparamver() {
        return devparamver;
    }

    public void setDevparamver(String devparamver) {
        this.devparamver = devparamver == null ? null : devparamver.trim();
    }

    public String getDevotherver() {
        return devotherver;
    }

    public void setDevotherver(String devotherver) {
        this.devotherver = devotherver == null ? null : devotherver.trim();
    }

    public String getDevimage1() {
        return devimage1;
    }

    public void setDevimage1(String devimage1) {
        this.devimage1 = devimage1 == null ? null : devimage1.trim();
    }

    public String getDevimage2() {
        return devimage2;
    }

    public void setDevimage2(String devimage2) {
        this.devimage2 = devimage2 == null ? null : devimage2.trim();
    }

    public String getDevimage3() {
        return devimage3;
    }

    public void setDevimage3(String devimage3) {
        this.devimage3 = devimage3 == null ? null : devimage3.trim();
    }

    public String getDevimage4() {
        return devimage4;
    }

    public void setDevimage4(String devimage4) {
        this.devimage4 = devimage4 == null ? null : devimage4.trim();
    }

    public Integer getOprId() {
        return oprId;
    }

    public void setOprId(Integer oprId) {
        this.oprId = oprId;
    }

    public Date getOprDate() {
        return oprDate;
    }

    public void setOprDate(Date oprDate) {
        this.oprDate = oprDate;
    }
}