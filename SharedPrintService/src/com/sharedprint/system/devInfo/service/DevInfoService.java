package com.sharedprint.system.devInfo.service;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.devInfo.pojo.DevInfo;

public interface DevInfoService {

	/**
	 * 获取设备列表
	 * @param searchMap
	 * @return
	 */
	List<Map<String, Object>> findDevInfoList(Map<String, Object> searchMap);
	
	/**
	 * 获取设备数量
	 * @param searchMap
	 * @return
	 */
	int findDevInfoCount(Map<String, Object> searchMap);

	/**
	 * 新增设备
	 * @param devInfo
	 * @return
	 */
	int insertDevInfo(DevInfo devInfo);
	/**
	 * 更新设备
	 * @param devInfo
	 * @return
	 */
	int updateDevInfo(DevInfo devInfo);
	/**
	 * 删除设备
	 * @param DevInfoID
	 * @return
	 */
	int delDevInfo(int DevInfoID);

	
}
