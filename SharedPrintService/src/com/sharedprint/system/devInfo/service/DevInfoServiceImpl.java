package com.sharedprint.system.devInfo.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sharedprint.system.devInfo.mapper.DevInfoMapper;
import com.sharedprint.system.devInfo.pojo.DevInfo;

@Service
public class DevInfoServiceImpl implements DevInfoService {

	@Autowired
	private DevInfoMapper devInfoMapper;
	
	@Override
	public List<Map<String, Object>> findDevInfoList(Map<String, Object> searchMap) {
		return devInfoMapper.findDevInfoList(searchMap);
	}

	@Override
	public int findDevInfoCount(Map<String, Object> searchMap) {
		
		return devInfoMapper.findDevInfoCount(searchMap);
	}

	@Override
	public int insertDevInfo(DevInfo devInfo) {
		return devInfoMapper.insertSelective(devInfo);
	}

	@Override
	public int updateDevInfo(DevInfo devInfo) {
	
		return devInfoMapper.updateByPrimaryKeySelective(devInfo);
	}

	@Override
	public int delDevInfo(int DevInfoId) {
		return devInfoMapper.deleteByPrimaryKey(DevInfoId);
	}



}
