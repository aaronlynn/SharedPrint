package com.sharedprint.system.devInfo.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sharedprint.dics.service.DicsInfoService;
import com.sharedprint.system.devFactory.service.DevFactoryInfoService;
import com.sharedprint.system.devInfo.pojo.DevInfo;
import com.sharedprint.system.devInfo.service.DevInfoService;
import com.sharedprint.system.devModel.pojo.DevModel;
import com.sharedprint.system.devModel.service.DevModelInfoService;
import com.sharedprint.system.merchant.service.MerDevInfoService;
import com.sharedprint.system.organization.service.OrgInfoService;
import com.sharedprint.util.RandomCode;

/**
 * 设备型号管理的控制器
 * 
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/api/devInfo")
public class DevInfoController {

	@Autowired
	private DevFactoryInfoService devFactoryInfoService;
	@Autowired
	private DevModelInfoService devModelInfoService;
	@Autowired
	private DevInfoService devInfoService;
	@Autowired
	private DicsInfoService dicsInfoService;
	@Autowired
	private OrgInfoService orgInfoService;
	@Autowired
	private MerDevInfoService merDevInfoService;
	 
	
	/**
	 * 获取设备厂商列表数据
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/queryPageList")
	@ResponseBody
	public Map<String, Object> queryDevModelList(HttpServletRequest req) {
		String queryDevInfNameLike = req.getParameter("queryDevInfNameLike");//名称
		String queryDevFactory = req.getParameter("queryDevFactory");//厂商
		String queryDevInfCode = req.getParameter("queryDevInfCode");//编码
		String queryDevModel = req.getParameter("queryDevModel");//型号
		String queryDevInfStatus = req.getParameter("queryDevInfStatus");//状态
		String currentPage = req.getParameter("currentPage");
		String pageSize = req.getParameter("pageSize");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		Map<String, Object> searchMap = new HashMap<String, Object>();
	    List<Map<String, Object>> devInfoList = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> devModelList = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> devFactoryList = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> statusList = new ArrayList<Map<String, Object>>();
		
		int totalCount = 0;
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {
			if (currentPage == null || "".equals(currentPage.trim())) {
				currentPage = "0";
			}
			if (pageSize == null || "".equals(pageSize.trim())) {
				pageSize = "10";
			}
			int currentpages = 0;
			if ((currentPage != null || !"".equals(currentPage.trim()))) {
				currentpages = Integer.parseInt(currentPage);
				currentpages = currentpages * Integer.parseInt(pageSize);
			}

			searchMap.put("currentPage", currentpages);
			searchMap.put("pageSize", Integer.parseInt(pageSize));
			searchMap.put("queryDevInfNameLike", queryDevInfNameLike);
			searchMap.put("queryDevFactory", queryDevFactory);
			searchMap.put("queryDevInfCode", queryDevInfCode);
			searchMap.put("queryDevModel", queryDevModel);
			searchMap.put("queryDevInfStatus", queryDevInfStatus);
			searchMap.put("dicsCode", "DevStatus");
			
			devFactoryList = devFactoryInfoService.findDevFactoryInfoList(searchMap);
			devModelList = devModelInfoService.findDevModelInfoList(searchMap);
			statusList = dicsInfoService.findDicsList(searchMap);
			
			totalCount = devInfoService.findDevInfoCount(searchMap);// 总记录数
			if (totalCount > 0) {
				devInfoList = devInfoService.findDevInfoList(searchMap);
			}
		}
		// list转json str
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("currentPage", Integer.parseInt(currentPage) + 1);
		resultMap.put("pageTotal", totalCount);
		
		resultMap.put("devFactoryList", devFactoryList);
		resultMap.put("devModelList", devModelList);
		resultMap.put("statusList", statusList);
		
		resultMap.put("searchMap", searchMap);
		resultMap.put("pageList", devInfoList);
		resultMap.put("success", "1");

		return resultMap;
	}
	
	/**
	 * 验证设备型号名称是否存在！
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/validateDevInfoName")
	@ResponseBody
	public boolean validateDevInfo(HttpServletRequest req) {
		String addDevInfoName = req.getParameter("addDevInfName");
		System.err.println("要添加的设备名称"+addDevInfoName);
		String updateDevInfoName = req.getParameter("updateDevInfName");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		String excludeDevInfoId = req.getParameter("excludeDevInfoId");
		System.err.println("后台接受修改的id和名称"+excludeDevInfoId+updateDevInfoName);
		System.out.println("验证token=" + token + ",userId=" + userId );
		boolean result = false;
		Map<String, Object> searchMap = new HashMap<String, Object>();
		if (excludeDevInfoId != null && !"".equals(excludeDevInfoId.trim())) {
			searchMap.put("excludeDevInfoId", excludeDevInfoId);
			searchMap.put("updateDevInfoName", updateDevInfoName);
		}
		if (addDevInfoName != null && !"".equals(addDevInfoName.trim())) {
			searchMap.put("addDevInfoName", addDevInfoName);
		}
		int count = devInfoService.findDevInfoCount(searchMap);
		if (count == 0) {
			result = true;
		}
		return result;
	}
	// 初始化添加
		@RequestMapping("/initAdd")
		@ResponseBody
		public Map<String, Object> initAdd(HttpServletRequest req, HttpServletResponse resp) throws IOException {
			String token = req.getHeader("token");
			String userId = req.getHeader("userId");
			System.out.println("token=" + token + ",userId=" + userId );

			Map<String, Object> resultMap = new HashMap<String, Object>();
			List<Map<String, Object>> devModelList = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> devFactoryList = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> devOrgList = new ArrayList<Map<String, Object>>();
			if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {
				Map<String, Object> searchMap = new HashMap<String, Object>();
				devFactoryList = devFactoryInfoService.findDevFactoryInfoList(searchMap);
				devModelList = devModelInfoService.findDevModelInfoList(searchMap);
				devOrgList = orgInfoService.findOrgInfoList(searchMap);
			}
			resultMap.put("devFactoryList", devFactoryList);
			resultMap.put("devModelList", devModelList);
			resultMap.put("devOrgList", devOrgList);
			resultMap.put("success", "1");
			return resultMap;

		}
	/**
	 * 保存新增型号！
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public Map<String, Object> addDevModel(HttpServletRequest req) {
		String devinfCode = RandomCode.getRandomCode();
		String devinfname = req.getParameter("addDevInfName");
		String devfactoryCode = req.getParameter("addDevFactory_Code");
		String devmodelCode = req.getParameter("addDevModel_Code");
		String orgCode = req.getParameter("addOrg_Code");
		String devhwver = req.getParameter("addDevHwVer");
		String devappver = req.getParameter("addDevAppVer");
		String devparamver = req.getParameter("addDevParamVer");
		String devotherver = req.getParameter("addDevOtherVer");
		String devimage1 = req.getParameter("devimage1");
		
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		System.out.println(token + userId);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {

			DevInfo devInfo = new DevInfo();
			devInfo.setDevinfname(devinfname);
			devInfo.setDevinfCode(devinfCode);
			devInfo.setDevmodelCode(devmodelCode);
			devInfo.setOrgCode(orgCode);
			devInfo.setDevfactoryCode(devfactoryCode);
			devInfo.setDevhwver(devhwver);
			devInfo.setDevappver(devappver);
			devInfo.setDevparamver(devparamver);
			devInfo.setDevotherver(devotherver);
			devInfo.setDevimage1(devimage1);
			devInfo.setOprId(Integer.parseInt(userId));
			int success = 0;
			success = devInfoService.insertDevInfo(devInfo);
			if (success > 0) {
				resultMap.put("success", "1");
			} else {
				resultMap.put("msg", "添加失败");
			}
		} else {
			resultMap.put("msg", "请上传相关参数");
		}
		return resultMap;
	}

	// 初始化前台修改设备厂商数据
	@RequestMapping("/initUpdate")
	@ResponseBody
	public Map<String, Object> initUpdate(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		String DevInfoId = req.getParameter("DevInfoId");
		System.out.println("token=" + token + ",userId=" + userId + ",DevInfoId=" + DevInfoId);

		Map<String, Object> devInfoMap = new HashMap<String, Object>();
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<Map<String, Object>> devStatusList = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> devOrgList = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> devModelList = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> devFactoryList = new ArrayList<Map<String, Object>>();
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim()) && DevInfoId != null
				&& !"".equals(DevInfoId.trim())) {
			List<Map<String, Object>> devInfoList = new ArrayList<Map<String, Object>>();
			Map<String, Object> searchMap = new HashMap<String, Object>();
			devFactoryList = devFactoryInfoService.findDevFactoryInfoList(searchMap);
			devModelList = devModelInfoService.findDevModelInfoList(searchMap);
			devOrgList = orgInfoService.findOrgInfoList(searchMap);
			searchMap.put("dicsCode", "DevStatus");
			devStatusList = dicsInfoService.findDicsList(searchMap);
			
			searchMap.put("DevInfoId", DevInfoId);
			System.err.println("要修改的ID"+DevInfoId);
			devInfoList = devInfoService.findDevInfoList(searchMap);
			devInfoMap = devInfoList.get(0);
			System.err.println("devInfoMap="+devInfoMap);
		}
		resultMap.put("devInfoMap", devInfoMap);
		resultMap.put("devFactoryList", devFactoryList);
		resultMap.put("devModelList", devModelList);
		resultMap.put("devOrgList", devOrgList);
		resultMap.put("devStatusList", devStatusList);
		resultMap.put("success", "1");
		return resultMap;

	}

	/*
	 * 保存修改前台设备型号
	 */
	@RequestMapping("/update")
	@ResponseBody
	public Map<String, Object> saveUpdate(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String devinfname = req.getParameter("updateDevInfName");
		String devfactoryCode = req.getParameter("updateDevFactory_Code");
		String devmodelCode = req.getParameter("updateDevModel_Code");
		String orgCode = req.getParameter("updateOrg_Code");
		String devhwver = req.getParameter("updateDevHwVer");
		String devappver = req.getParameter("updateDevAppVer");
		String devparamver = req.getParameter("updateDevParamVer");
		String devotherver = req.getParameter("updateDevOtherVer");
		String updatedevimage1 = req.getParameter("updatedevimage1");
		String excludeDevInfoId = req.getParameter("excludeDevInfoId");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		System.out.println("token:"+token +"userID："+ userId+"excludeDevInfoId:"+excludeDevInfoId);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {

			DevInfo devInfo = new DevInfo();
			devInfo.setDevinfId(Integer.parseInt(excludeDevInfoId));
			devInfo.setDevinfname(devinfname);
			devInfo.setDevmodelCode(devmodelCode);
			devInfo.setOrgCode(orgCode);
			devInfo.setDevfactoryCode(devfactoryCode);
			devInfo.setDevhwver(devhwver);
			devInfo.setDevappver(devappver);
			devInfo.setDevparamver(devparamver);
			devInfo.setDevotherver(devotherver);
			devInfo.setDevimage1(updatedevimage1);
			devInfo.setOprId(Integer.parseInt(userId));
			int success = devInfoService.updateDevInfo(devInfo);
			if (success > 0) {
				resultMap.put("success", "1");
			} else {
				resultMap.put("msg", "修改失败");
			}
		} else {
			resultMap.put("msg", "请上传相关参数");
		}
		return resultMap;
	}

	// 前台设备型号详情
	@RequestMapping("/detail")
	@ResponseBody
	public Map<String, Object> detailFront(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		return initUpdate(req,resp);
	}

	// 删除前台菜单数据
	@RequestMapping("/delete")
	@ResponseBody
	public Map<String, Object> delete(HttpServletRequest req, HttpServletResponse resp)  {
		String DevInfo_ID = req.getParameter("DevInfo_ID");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");

		Map<String, Object> resultMap = new HashMap<String, Object>();
		Map<String, Object> searchMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		if (DevInfo_ID != null && !"".equals(DevInfo_ID.trim()) && token != null && !"".equals(token.trim()) && userId != null
				&& !"".equals(userId.trim())) {
			searchMap.put("DevInfo_ID", DevInfo_ID);
			int MerNow = merDevInfoService.findMerDevInfoCount(searchMap);
			if (MerNow>0) {
				resultMap.put("msg", "该设备被商户占用， 不允许删除!");
			}else {
				int success = devInfoService.delDevInfo(Integer.parseInt(DevInfo_ID));
				if (success > 0) {
					resultMap.put("success", "1");
				} else {
					resultMap.put("msg", "删除失败!");
				}
			}
		}
		return resultMap;
	}
}
