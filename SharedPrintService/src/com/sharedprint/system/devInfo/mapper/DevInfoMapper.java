package com.sharedprint.system.devInfo.mapper;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.devInfo.pojo.DevInfo;

public interface DevInfoMapper {
    int deleteByPrimaryKey(Integer devinfId);

    int insert(DevInfo record);

    int insertSelective(DevInfo record);

    DevInfo selectByPrimaryKey(Integer devinfId);

    int updateByPrimaryKeySelective(DevInfo record);

    int updateByPrimaryKey(DevInfo record);

	List<Map<String, Object>> findDevInfoList(Map<String, Object> searchMap);

	int findDevInfoCount(Map<String, Object> searchMap);
}