package com.sharedprint.system.category.mapper;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.category.pojo.CategoryInfo;

public interface CategoryInfoMapper {
	 /**
     * 删除商品类别
     * @param categoryId
     * @return
     */
    int deleteByPrimaryKey(Integer categoryId);

    int insert(CategoryInfo record);

    int insertSelective(CategoryInfo record);
   
    CategoryInfo selectByPrimaryKey(Integer categoryId);
   
    /**
     * 更新商品类别
     * @param record
     * @return
     */
    int updateByPrimaryKeySelective(CategoryInfo record);

    int updateByPrimaryKey(CategoryInfo record);
    /**
   	 * 获取地区列表
   	 * @param searchMap
   	 * @return
   	 */
   	public List<Map<String,Object>> findCategoryList(Map<String,Object> searchMap);
   	
   	/**
   	 * 获取地区条数
   	 * @param searchMap
   	 * @return
   	 */
   	public int findCategoryCount(Map<String,Object> searchMap);
    
   	/**
   	 * 添加保存
   	 * @param categoryInfo
   	 * @return
   	 */
	int saveInfo(CategoryInfo categoryInfo);
    /**
     * 修改信息
     * @param categoryInfo
     * @return
     */
	int updateInfo(CategoryInfo categoryInfo);
}