package com.sharedprint.system.category.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sharedprint.system.area.pojo.AreaInfo;
import com.sharedprint.system.area.service.AreaInfoService;
import com.sharedprint.system.category.pojo.CategoryInfo;
import com.sharedprint.system.category.service.CategoryInfoService;
import com.sharedprint.system.user.pojo.UserInfo;
/**
 * 商品类别的控制器
 */
@Controller
@RequestMapping("/api/categoryInfo")
public class CategoryInfoController {
	private static final Logger logger = Logger.getLogger("CategoryInfoController");
		@Autowired
		private CategoryInfoService categoryInfoService;
         /**
		 * 获取地区列表数据
		 * 
		 * @param request
		 * @return
		 */
		@RequestMapping("/queryCategory")
		@ResponseBody
		public Map<String, Object> queryCategory(HttpServletRequest req) {
			String goodsCategoryLike = req.getParameter("GoodsCategoryLike");

			String currentPage = req.getParameter("currentPage");
			String pageSize = req.getParameter("pageSize");
			String orderByValue = req.getParameter("orderByValue");
			String token = req.getHeader("token");
			String userId = req.getHeader("userId");
			String merchantId = req.getHeader("merchantId");//商户ID
			List<Map<String, Object>> categoryList = new ArrayList<Map<String, Object>>();
			int totalCount = 0;
			if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {
				if (currentPage == null || "".equals(currentPage.trim())) {
					currentPage = "0";
				}
				if (pageSize == null || "".equals(pageSize.trim())) {
					pageSize = "10";
				}
				Map<String, Object> searchMap = new HashMap<String, Object>();
				int currentpages = 0;
				if ((currentPage != null || !"".equals(currentPage.trim()))) {
					currentpages = Integer.parseInt(currentPage);
					currentpages = currentpages * Integer.parseInt(pageSize);
				}

				searchMap.put("currentPage", currentpages);
				searchMap.put("pageSize", Integer.parseInt(pageSize));
				searchMap.put("goodsCategoryLike", goodsCategoryLike);
				searchMap.put("merchantId", merchantId);

				totalCount = categoryInfoService.findCategoryInfoCount(searchMap);// 总记录数
				if (totalCount > 0) {
					if (orderByValue == null || "".equals(orderByValue.trim())) {
						searchMap.put("orderByAreaIdDesc", "1");
					}
					categoryList = categoryInfoService.findCategoryInfoList(searchMap);
				}
			}
			Map<String, Object> resultMap = new HashMap<String, Object>();
			resultMap.put("currentPage", Integer.parseInt(currentPage) + 1);
			resultMap.put("pageTotal", totalCount);
			resultMap.put("pageList", categoryList);
			resultMap.put("success", "1");

			return resultMap;
		}
		
		//获取上级类别菜单树型下拉框
		@RequestMapping("/initAddMethod")
		@ResponseBody
		public Map<String, Object> initAddMethod(HttpServletRequest req, HttpServletResponse resp)  {
			String token = req.getHeader("token");
			String userId = req.getHeader("userId");

			Map<String, Object> resultMap = new HashMap<String, Object>();
			List<Map<String, Object>> categoryList = new ArrayList<Map<String, Object>>();
			resultMap.put("success", "0");
			if (token != null && !"".equals(token.trim()) && userId != null
					&& !"".equals(userId.trim())) {
				Map<String, Object> searchMap = new HashMap<String, Object>();
				categoryList = categoryInfoService.findCategoryInfoList(searchMap);
			}
			resultMap.put("categoryList", categoryList);
			resultMap.put("success", "1");
			return resultMap;
		}
		/**
		 * 验证类别名称
		 * 
		 * @param req
		 * @param resp
		 * @throws IOException
		 */
		@RequestMapping("/validCategoryName")
		@ResponseBody
		public boolean validLoginName(HttpServletRequest req){
			String categoryName = req.getParameter("addCategoryName");
			String catId = req.getParameter("catId");
			String token = req.getHeader("token");
			String userId = req.getHeader("userId");
			logger.info("catId=" + catId + ",token=" + token + ",userId=" + userId + ",categoryName=" + categoryName);
			boolean result = false;
			if (categoryName != null && !"".equals(categoryName.trim())) {
				Map<String, Object> searchMap = new HashMap<String, Object>();
				searchMap.put("catId", catId);
				searchMap.put("categoryName", categoryName);
				int count = categoryInfoService.findCategoryInfoCount(searchMap);
				if (count == 0) {
					result = true;
				}
			}
			return result;
		}
		/**
		 * 保存类别信息
		 * 
		 * @param req
		 * @param resp
		 * @throws IOException
		 */
		@RequestMapping("/add")
		@ResponseBody
		public Map<String, Object> add(HttpServletRequest req) {
			String addCategoryName = req.getParameter("addCategoryName");
			String catName = req.getParameter("catName");
			String catId = req.getParameter("catId");
			String addSortNo = req.getParameter("addSortNo");
			String addCategoryDescription = req.getParameter("addCategoryDescription");

			String token = req.getHeader("token");
			String userId = req.getHeader("userId");
			String merInfId = req.getHeader("merInfId");
			Map<String, Object> resultMap = new HashMap<String, Object>();
			resultMap.put("success", "0");
			if (catId != null && !"".equals(catId.trim()) && addSortNo != null && !"".equals(addSortNo.trim()) && addCategoryName != null && !"".equals(addCategoryName.trim()) && catName != null
					&& !"".equals(catName.trim()) &&  token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {
				
				CategoryInfo categoryInfo = new CategoryInfo();
				categoryInfo.setCategoryName(addCategoryName);
				categoryInfo.setCategoryParentId(Integer.parseInt(catId));
				categoryInfo.setRemark(addCategoryDescription);
				categoryInfo.setSortNo(Integer.parseInt(addSortNo));
				categoryInfo.setOprId(Integer.parseInt(userId));
				categoryInfo.setMerInfId(Integer.parseInt(merInfId));
				try {
					int success = categoryInfoService.saveInfo(categoryInfo);
					if (success > 0) {
						resultMap.put("success", "1");
					} else {
						resultMap.put("msg", "保存失败");
					}
				} catch (Exception e) {
					logger.info("保存异常=" + e.getMessage());
					e.printStackTrace();
					resultMap.put("msg", "保存异常");
				}
			} else {
				resultMap.put("msg", "请上传相关参数");
			}
			return resultMap;
		}
		/**
		 * 获取修改相关信息
		 * 
		 * @param req
		 * @param resp
		 * @throws IOException
		 * @throws ServletException
		 */
		@RequestMapping("/initUpdate")
		@ResponseBody
		public Map<String, Object> initUpdate(HttpServletRequest req) {
			String token = req.getHeader("token");
			String userId = req.getHeader("userId");
			String updateCategoryId = req.getParameter("updateCategoryId");
			logger.info("token=" + token + ",userId=" + userId + ",updateCategoryId=" + updateCategoryId);

			Map<String, Object> categoryMap = new HashMap<String, Object>();
			Map<String, Object> resultMap = new HashMap<String, Object>();
			List<Map<String, Object>> categoryList = new ArrayList<Map<String, Object>>();
			if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim()) && userId != null
					&& !"".equals(userId.trim())) {
				Map<String, Object> searchMap = new HashMap<String, Object>();
				searchMap.put("updateCategoryId", updateCategoryId);
				categoryList = categoryInfoService.findCategoryInfoList(searchMap);
				categoryMap = categoryList.get(0);
				categoryList = categoryInfoService.findCategoryInfoList(null);
			}
			resultMap.put("categoryList", categoryList);
			resultMap.put("categoryMap", categoryMap);
			resultMap.put("success", "1");
			return resultMap;
		}
		/**
		 * 修改信息
		 * 
		 * @param req
		 * @param resp
		 * @throws IOException
		 */
		@RequestMapping("/update")
		@ResponseBody
		public Map<String, Object> update(HttpServletRequest req) {
			String updateCategoryName = req.getParameter("updateCategoryName");
			String updateCategoryId = req.getParameter("updateCategoryId");
			String updatecatName = req.getParameter("updatecatName");
			String updatecatId = req.getParameter("updatecatId");
			String updateCategorySortNo = req.getParameter("updateCategorySortNo");
			String updateCategoryDescript = req.getParameter("updateCategoryDescript");
			String token = req.getHeader("token");
			String userId = req.getHeader("userId");
			Map<String, Object> resultMap = new HashMap<String, Object>();
			resultMap.put("success", "0");

			if (updateCategoryName != null && !"".equals(updateCategoryName.trim()) && updatecatName != null
					&& !"".equals(updatecatName.trim()) && updateCategorySortNo != null
					&& !"".equals(updateCategorySortNo.trim())) {
				CategoryInfo categoryInfo = new CategoryInfo();
				categoryInfo.setCategoryName(updateCategoryName);
				categoryInfo.setCategoryParentId(Integer.parseInt(updatecatId));
				categoryInfo.setRemark(updateCategoryDescript);
				categoryInfo.setSortNo(Integer.parseInt(updateCategorySortNo));
				categoryInfo.setOprId(Integer.parseInt(userId));
				categoryInfo.setCategoryId(Integer.parseInt(updateCategoryId));
				try {
					int success = categoryInfoService.updateInfo(categoryInfo);
					if (success > 0) {
						resultMap.put("success", "1");
					} else {
						resultMap.put("msg", "修改失败");
					}
				} catch (Exception e) {
					e.printStackTrace();
					logger.info("修改异常=" + e.getMessage());
					resultMap.put("msg", "修改异常");
				}
			} else {
				resultMap.put("msg", "请上传相关参数");
			}
			return resultMap;
		}

		// 删除前台菜单数据
		@RequestMapping("/deleteCategory")
		@ResponseBody
		public Map<String, Object> delete(HttpServletRequest req, HttpServletResponse resp)  {
			String categoryId = req.getParameter("categoryId");
			String token = req.getHeader("token");
			String userId = req.getHeader("userId");

			Map<String, Object> resultMap = new HashMap<String, Object>();
			resultMap.put("success", "0");
			if (categoryId != null && !"".equals(categoryId.trim()) && token != null && !"".equals(token.trim()) && userId != null
					&& !"".equals(userId.trim())) {
				 //判断是否有下级
				int success = categoryInfoService.delCategoryInfo(Integer.parseInt(categoryId));
				if (success > 0) {
					resultMap.put("success", "1");
				} else {
					resultMap.put("msg", "该菜单存在子菜单,不允许删除!");
				}
			}
			return resultMap;
		}
		
		/**
		 * 前台地区详情
		 * @param req
		 * @param resp
		 * @return
		 * @throws IOException
		 */
		@RequestMapping("/detailCategory")
		@ResponseBody
		public Map<String, Object> detailFront(HttpServletRequest req, HttpServletResponse resp) throws IOException {
			String token = req.getHeader("token");
			String userId = req.getHeader("userId");
			String categoryId = req.getParameter("categoryId");

			Map<String, Object> categoryMap = new HashMap<String, Object>();
			Map<String, Object> resultMap = new HashMap<String, Object>();
			List<Map<String, Object>> categoryList = new ArrayList<Map<String, Object>>();
			if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {
				Map<String, Object> searchMap = new HashMap<String, Object>();
				searchMap.put("categoryId", categoryId);
				categoryList =categoryInfoService.findCategoryInfoList(searchMap);
				categoryMap = categoryList.get(0);
			}
			resultMap.put("categoryMap", categoryMap);
			resultMap.put("success", "1");
			return resultMap;

		}
	}

