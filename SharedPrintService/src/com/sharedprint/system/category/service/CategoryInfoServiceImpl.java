package com.sharedprint.system.category.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sharedprint.system.category.mapper.CategoryInfoMapper;
import com.sharedprint.system.category.pojo.CategoryInfo;
@Service
public class CategoryInfoServiceImpl implements CategoryInfoService{
	@Autowired
	private CategoryInfoMapper categoryInfoMapper;
	@Override
	public int findCategoryInfoCount(Map<String, Object> searchMap) {
		return categoryInfoMapper.findCategoryCount(searchMap);
	}
	@Override
	public List<Map<String, Object>> findCategoryInfoList(Map<String, Object> searchMap) {
		return categoryInfoMapper.findCategoryList(searchMap);
	}
	
	@Override
	public int delCategoryInfo(int categoryId) {
		return categoryInfoMapper.deleteByPrimaryKey(categoryId);
	}
	@Override
	public int saveInfo(CategoryInfo categoryInfo) {
		return categoryInfoMapper.saveInfo(categoryInfo);
	}
	@Override
	public int updateInfo(CategoryInfo categoryInfo) {
		return categoryInfoMapper.updateInfo(categoryInfo);
	}

}
