package com.sharedprint.system.category.service;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.category.pojo.CategoryInfo;


public interface CategoryInfoService {
	/**
	 * 获取地区列表数量
	 * @param searchMap
	 * @return
	 *
	 **/
	int findCategoryInfoCount(Map<String, Object> searchMap);

     /**
	 * 获取商品类别列表
	 * @param searchMap
	 * @return
	 */
	List<Map<String, Object>> findCategoryInfoList(Map<String, Object> searchMap);

	
	/**
	 * 删除地区
	 * @param menuInfo
	 * @return
	 */
	int delCategoryInfo(int categoryId);
    
	/**
     * 添加保存
     * @param categoryInfo
     * @return
     */
	int saveInfo(CategoryInfo categoryInfo);
    /**
     * 修改信息
     * @param categoryInfo
     * @return
     */
	int updateInfo(CategoryInfo categoryInfo);

}
