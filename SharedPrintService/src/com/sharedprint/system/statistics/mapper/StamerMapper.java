package com.sharedprint.system.statistics.mapper;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.statistics.pojo.Stamer;

public interface StamerMapper {
    int deleteByPrimaryKey(Integer merinfId);

    int insert(Stamer record);

    int insertSelective(Stamer record);

    Stamer selectByPrimaryKey(Integer merinfId);

    int updateByPrimaryKeySelective(Stamer record);

    int updateByPrimaryKey(Stamer record);
    /**
   	 * 获取商户列表
   	 * @param searchMap
   	 * @return
   	 */
   	public List<Map<String,Object>> findStamerList(Map<String,Object> searchMap);
   	
   	/**
   	 * 获取商户条数
   	 * @param searchMap
   	 * @return
   	 */
   	public int findStamerCount(Map<String,Object> searchMap);
}