package com.sharedprint.system.statistics.mapper;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.statistics.pojo.Staord;

public interface StaordMapper {
    int deleteByPrimaryKey(Integer orderId);

    int insert(Staord record);

    int insertSelective(Staord record);

    Staord selectByPrimaryKey(Integer orderId);

    int updateByPrimaryKeySelective(Staord record);

    int updateByPrimaryKey(Staord record);
    /**
   	 * 获取订单列表
   	 * @param searchMap
   	 * @return
   	 */
   	public List<Map<String,Object>> findStaordList(Map<String,Object> searchMap);
   	
   	/**
   	 * 获取订单条数
   	 * @param searchMap
   	 * @return 
   	 */
   	public int findStaordCount(Map<String,Object> searchMap);
}