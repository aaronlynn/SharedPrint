package com.sharedprint.system.statistics.mapper;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.statistics.pojo.Staper;

public interface StaperMapper {
    int deleteByPrimaryKey(Integer peruserId);

    int insert(Staper record);

    int insertSelective(Staper record);

    Staper selectByPrimaryKey(Integer peruserId);

    int updateByPrimaryKeySelective(Staper record);

    int updateByPrimaryKey(Staper record);
    /**
   	 * 获取客户列表
   	 * @param searchMap
   	 * @return
   	 */
   	public List<Map<String,Object>> findStaperList(Map<String,Object> searchMap);
   	
   	/**
   	 * 获取客户条数
   	 * @param searchMap
   	 * @return
   	 */
   	public int findStaperCount(Map<String,Object> searchMap);
}