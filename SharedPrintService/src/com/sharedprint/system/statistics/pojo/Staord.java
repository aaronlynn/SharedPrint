package com.sharedprint.system.statistics.pojo;

import java.util.Date;

public class Staord {
    private Integer orderId;

    private String orderCode;

    private String orderStatus;

    private Integer price;

    private Integer payerId;

    private String merinfCode;

    private String ageinfCode;

    private String orgCode;

    private Date crtdate;

    private Date lastuptdate;

    private String refund;

    private Date refunddate;

    private String orderremark;

    private String auditor;

    private Date auditdate;

    private String auditremark;

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode == null ? null : orderCode.trim();
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus == null ? null : orderStatus.trim();
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getPayerId() {
        return payerId;
    }

    public void setPayerId(Integer payerId) {
        this.payerId = payerId;
    }

    public String getMerinfCode() {
        return merinfCode;
    }

    public void setMerinfCode(String merinfCode) {
        this.merinfCode = merinfCode == null ? null : merinfCode.trim();
    }

    public String getAgeinfCode() {
        return ageinfCode;
    }

    public void setAgeinfCode(String ageinfCode) {
        this.ageinfCode = ageinfCode == null ? null : ageinfCode.trim();
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode == null ? null : orgCode.trim();
    }

    public Date getCrtdate() {
        return crtdate;
    }

    public void setCrtdate(Date crtdate) {
        this.crtdate = crtdate;
    }

    public Date getLastuptdate() {
        return lastuptdate;
    }

    public void setLastuptdate(Date lastuptdate) {
        this.lastuptdate = lastuptdate;
    }

    public String getRefund() {
        return refund;
    }

    public void setRefund(String refund) {
        this.refund = refund == null ? null : refund.trim();
    }

    public Date getRefunddate() {
        return refunddate;
    }

    public void setRefunddate(Date refunddate) {
        this.refunddate = refunddate;
    }

    public String getOrderremark() {
        return orderremark;
    }

    public void setOrderremark(String orderremark) {
        this.orderremark = orderremark == null ? null : orderremark.trim();
    }

    public String getAuditor() {
        return auditor;
    }

    public void setAuditor(String auditor) {
        this.auditor = auditor == null ? null : auditor.trim();
    }

    public Date getAuditdate() {
        return auditdate;
    }

    public void setAuditdate(Date auditdate) {
        this.auditdate = auditdate;
    }

    public String getAuditremark() {
        return auditremark;
    }

    public void setAuditremark(String auditremark) {
        this.auditremark = auditremark == null ? null : auditremark.trim();
    }
}