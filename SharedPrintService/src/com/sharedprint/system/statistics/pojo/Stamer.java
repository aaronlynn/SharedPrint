package com.sharedprint.system.statistics.pojo;

import java.util.Date;

public class Stamer {
    private Integer merinfId;

    private String merinfCode;

    private String merinfname;

    private String idtype;

    private String idcode;

    private String merinftype;

    private String merinfstatus;

    private String merinflevel;

    private String alias;

    private String orgCode;

    private String ageinfCode;

    private String areaCode;

    private String busiaddress;

    private String contactor;

    private String contacttel;

    private String email;

    private String qq;

    private String weixin;

    private String remark;

    private String idpath;

    private String busiaddresspath;

    private String climgrcode;

    private Integer crtuserid;

    private Date crtdate;

    private Integer uptuserid;

    private Date lastuptdate;

    private Integer chkuserid;

    private Date chkdate;

    private String chkremark;

    public Integer getMerinfId() {
        return merinfId;
    }

    public void setMerinfId(Integer merinfId) {
        this.merinfId = merinfId;
    }

    public String getMerinfCode() {
        return merinfCode;
    }

    public void setMerinfCode(String merinfCode) {
        this.merinfCode = merinfCode == null ? null : merinfCode.trim();
    }

    public String getMerinfname() {
        return merinfname;
    }

    public void setMerinfname(String merinfname) {
        this.merinfname = merinfname == null ? null : merinfname.trim();
    }

    public String getIdtype() {
        return idtype;
    }

    public void setIdtype(String idtype) {
        this.idtype = idtype == null ? null : idtype.trim();
    }

    public String getIdcode() {
        return idcode;
    }

    public void setIdcode(String idcode) {
        this.idcode = idcode == null ? null : idcode.trim();
    }

    public String getMerinftype() {
        return merinftype;
    }

    public void setMerinftype(String merinftype) {
        this.merinftype = merinftype == null ? null : merinftype.trim();
    }

    public String getMerinfstatus() {
        return merinfstatus;
    }

    public void setMerinfstatus(String merinfstatus) {
        this.merinfstatus = merinfstatus == null ? null : merinfstatus.trim();
    }

    public String getMerinflevel() {
        return merinflevel;
    }

    public void setMerinflevel(String merinflevel) {
        this.merinflevel = merinflevel == null ? null : merinflevel.trim();
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias == null ? null : alias.trim();
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode == null ? null : orgCode.trim();
    }

    public String getAgeinfCode() {
        return ageinfCode;
    }

    public void setAgeinfCode(String ageinfCode) {
        this.ageinfCode = ageinfCode == null ? null : ageinfCode.trim();
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode == null ? null : areaCode.trim();
    }

    public String getBusiaddress() {
        return busiaddress;
    }

    public void setBusiaddress(String busiaddress) {
        this.busiaddress = busiaddress == null ? null : busiaddress.trim();
    }

    public String getContactor() {
        return contactor;
    }

    public void setContactor(String contactor) {
        this.contactor = contactor == null ? null : contactor.trim();
    }

    public String getContacttel() {
        return contacttel;
    }

    public void setContacttel(String contacttel) {
        this.contacttel = contacttel == null ? null : contacttel.trim();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq == null ? null : qq.trim();
    }

    public String getWeixin() {
        return weixin;
    }

    public void setWeixin(String weixin) {
        this.weixin = weixin == null ? null : weixin.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getIdpath() {
        return idpath;
    }

    public void setIdpath(String idpath) {
        this.idpath = idpath == null ? null : idpath.trim();
    }

    public String getBusiaddresspath() {
        return busiaddresspath;
    }

    public void setBusiaddresspath(String busiaddresspath) {
        this.busiaddresspath = busiaddresspath == null ? null : busiaddresspath.trim();
    }

    public String getClimgrcode() {
        return climgrcode;
    }

    public void setClimgrcode(String climgrcode) {
        this.climgrcode = climgrcode == null ? null : climgrcode.trim();
    }

    public Integer getCrtuserid() {
        return crtuserid;
    }

    public void setCrtuserid(Integer crtuserid) {
        this.crtuserid = crtuserid;
    }

    public Date getCrtdate() {
        return crtdate;
    }

    public void setCrtdate(Date crtdate) {
        this.crtdate = crtdate;
    }

    public Integer getUptuserid() {
        return uptuserid;
    }

    public void setUptuserid(Integer uptuserid) {
        this.uptuserid = uptuserid;
    }

    public Date getLastuptdate() {
        return lastuptdate;
    }

    public void setLastuptdate(Date lastuptdate) {
        this.lastuptdate = lastuptdate;
    }

    public Integer getChkuserid() {
        return chkuserid;
    }

    public void setChkuserid(Integer chkuserid) {
        this.chkuserid = chkuserid;
    }

    public Date getChkdate() {
        return chkdate;
    }

    public void setChkdate(Date chkdate) {
        this.chkdate = chkdate;
    }

    public String getChkremark() {
        return chkremark;
    }

    public void setChkremark(String chkremark) {
        this.chkremark = chkremark == null ? null : chkremark.trim();
    }
}