package com.sharedprint.system.statistics.service;

import java.util.List;
import java.util.Map;

public interface StamerService {
	/**
	 * 获取商户列表
	 * @param searchMap
	 * @return
	 */
	List<Map<String, Object>> findStamerList(Map<String, Object> searchMap);
	
	/**
	 * 获取商户列表数量
	 * @param searchMap
	 * @return
	 */
	int findStamerCount(Map<String, Object> searchMap);
}
