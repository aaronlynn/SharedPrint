package com.sharedprint.system.statistics.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sharedprint.system.statistics.mapper.StaordMapper;

@Service
public class StaordServiceImpl implements StaordService{

	@Autowired
	private StaordMapper staordMapper;
	@Override
	public List<Map<String, Object>> findStaordList(Map<String, Object> searchMap) {
		return staordMapper.findStaordList(searchMap);
	}
	@Override
	public int findStaordCount(Map<String, Object> searchMap) {
		return staordMapper.findStaordCount(searchMap);
	}
}
