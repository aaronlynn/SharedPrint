package com.sharedprint.system.statistics.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sharedprint.system.statistics.mapper.StamerMapper;

@Service
public class StamerServiceImpl implements StamerService{
	@Autowired
	private StamerMapper stamerMapper;
	@Override
	public List<Map<String, Object>> findStamerList(Map<String, Object> searchMap) {
		return stamerMapper.findStamerList(searchMap);
	}

	@Override
	public int findStamerCount(Map<String, Object> searchMap) {
		return stamerMapper.findStamerCount(searchMap);
	}
}
