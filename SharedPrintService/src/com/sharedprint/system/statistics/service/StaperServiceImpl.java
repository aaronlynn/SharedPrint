package com.sharedprint.system.statistics.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sharedprint.system.statistics.mapper.StaperMapper;

@Service
public class StaperServiceImpl implements StaperService{
	@Autowired
	private StaperMapper staperMapper;
	@Override
	public List<Map<String, Object>> findStaperList(Map<String, Object> searchMap) {
		return staperMapper.findStaperList(searchMap);
	}
	@Override
	public int findStaperCount(Map<String, Object> searchMap) {
		return staperMapper.findStaperCount(searchMap);
	}
}
