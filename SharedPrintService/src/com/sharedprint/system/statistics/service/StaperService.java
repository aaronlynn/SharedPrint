package com.sharedprint.system.statistics.service;

import java.util.List;
import java.util.Map;

public interface StaperService {
	/**
	 * 获取商户列表
	 * @param searchMap
	 * @return
	 */
	List<Map<String, Object>> findStaperList(Map<String, Object> searchMap);
	
	/**
	 * 获取商户列表数量
	 * @param searchMap
	 * @return
	 */
	int findStaperCount(Map<String, Object> searchMap);
}
