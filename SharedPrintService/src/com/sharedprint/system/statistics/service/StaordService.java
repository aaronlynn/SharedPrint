package com.sharedprint.system.statistics.service;

import java.util.List;
import java.util.Map;

public interface StaordService {
	/**
	 * 获取订单列表
	 * @param searchMap
	 * @return
	 */
	List<Map<String, Object>> findStaordList(Map<String, Object> searchMap);
	
	/**
	 * 获取订单列表数量
	 * @param searchMap
	 * @return
	 */
	int findStaordCount(Map<String, Object> searchMap);
}
