package com.sharedprint.system.statistics.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sharedprint.system.agent.service.AgentInfoService;
import com.sharedprint.system.statistics.service.StamerService;
import com.sharedprint.system.statistics.service.StaordService;

/**
 *商户统计的控制器
 */
@Controller
@RequestMapping("/api/Staord")
public class StaordController {

	@Autowired
	private StaordService staordService;
	@Autowired
	private AgentInfoService agentInfoService;
	@Autowired
	private StamerService stamerInfoService;

	@RequestMapping("/queryPageList")
	@ResponseBody
	public Map<String, Object> queryStaordList(HttpServletRequest req) {
		String Crtyear = req.getParameter("querycrtyear");
		String queryAgeName = req.getParameter("queryAgeName");
		String queryMerInfCode = req.getParameter("queryMerInfCode");
		String currentPage = req.getParameter("currentPage");
		String pageSize = req.getParameter("pageSize");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		String AgeuserId = req.getHeader("AgeuserId");
		String MeruserId = req.getHeader("MeruserId");
		
		List<Map<String, Object>> agentList = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> merList = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> staordList = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> staordmonthList = new ArrayList<Map<String, Object>>();
		

		
		int totalCount = 0;
		int type=0;
		int[] allmonthCount=new int[13];//每月总记录数
		int[] monthCount=new int[13];//代理商每月记录数
		int[] yearCount=new int[13];//年份
		int[] Count=new int[13];//订单总笔数
		int[] Price=new int[13];//订单总金额
		Price[0]= 0;
		//获取当前年月
		Calendar cale = null;  
        cale = Calendar.getInstance();  
        int year = cale.get(Calendar.YEAR); 
        for (int i = 0; i < 11; i++) {
			yearCount[i]=year-i;
		}
        
		if (token != null && !"".equals(token.trim())) {
			Map<String, Object> searchMap = new HashMap<String, Object>();
			
			if (currentPage == null || "".equals(currentPage.trim())) {
				currentPage = "0";
			}
			if (pageSize == null || "".equals(pageSize.trim())) {
				pageSize = "10";
			}
			int currentpages = 0;
			if ((currentPage != null || !"".equals(currentPage.trim()))) {
				currentpages = Integer.parseInt(currentPage);
				currentpages = currentpages * Integer.parseInt(pageSize);
			}
			
			searchMap.put("currentPage", currentpages);
			searchMap.put("pageSize", Integer.parseInt(pageSize));	
			searchMap.put("AgeInfID", AgeuserId);
			searchMap.put("MeruserId", MeruserId);
			
			
			if(queryMerInfCode != null && !"".equals(queryMerInfCode.trim())&& !"undefined".equals(queryMerInfCode.trim())) {
				searchMap.put("MerInfCode",queryMerInfCode);
			}//选定商户 如果没选 则默认所有
			merList=stamerInfoService.findStamerList(searchMap);
			if(queryAgeName != null && !"".equals(queryAgeName.trim())&& !"undefined".equals(queryAgeName.trim())) {
				searchMap.put("ageInfName", queryAgeName);
			}//选定代理商 如果没选 则默认所有
			agentList =agentInfoService.findAgentInfoList(searchMap);//查询所有代理商号
			
			searchMap.put("dateyear", year);
			if(Crtyear != null && !"".equals(Crtyear.trim())&& !"undefined".equals(Crtyear.trim())) {
				searchMap.put("dateyear", Crtyear);
			}//年份选定 如果没有选择则默认今年
			for (int i = 0; i < 12; i++) {
				searchMap.put("datemonth", i+1);
				allmonthCount[i]=staordService.findStaordCount(searchMap);// 每月订单记录数
			}
			if((AgeuserId != null && !"".equals(AgeuserId.trim()))||(MeruserId != null && !"".equals(MeruserId.trim()))){
				for (int i = 0; i < merList.size(); i++) {
					for(int j=0;j<12;j++) {
						searchMap.put("datemonth", j+1);
						monthCount[j]=staordService.findStaordCount(searchMap);// 每月订单记录数
						searchMap.put("MerInfCode", merList.get(i).get("MerInf_Code"));
						Count[j]= staordService.findStaordCount(searchMap);// 每月每个商户的订单记录数
						Price[j]=0;
						if(Count[j]>0) {
							staordList= staordService.findStaordList(searchMap);
							for (int q = 0; q < staordList.size(); q++) {
								Price[j]+=Integer.parseInt(staordList.get(q).get("Price").toString());
							//每月每个商户的订单总金额
							}
						}
						Map<String, Object> staordmonthMap = new HashMap<String, Object>();
						staordmonthMap.put("month", j+1);
						staordmonthMap.put("MerInfName", merList.get(i).get("MerInfName"));
						staordmonthMap.put("monthCount", monthCount[j]);
						staordmonthMap.put("Price", Price[j]);
						staordmonthList.add(type,staordmonthMap);
						type++;
					}
				}
			}
			else if(userId != null && !"".equals(userId.trim())) {
				for (int i = 0; i < agentList.size(); i++) {
					for(int j=0;j<12;j++) {
						searchMap.put("datemonth", j+1);
						monthCount[j]=staordService.findStaordCount(searchMap);// 每月订单记录数
						searchMap.put("AgeInfCode", agentList.get(i).get("AgeInf_Code"));
						Count[j]= staordService.findStaordCount(searchMap);// 每月每个代理商的订单记录数
						Price[j]=0;
						if(Count[j]>0) {
							staordList= staordService.findStaordList(searchMap);
							for (int q = 0; q < staordList.size(); q++) {
								Price[j]+=Integer.parseInt(staordList.get(q).get("Price").toString());
							//每月每个代理商的订单总金额
							}
						}
						Map<String, Object> staordmonthMap = new HashMap<String, Object>();
						staordmonthMap.put("month", j+1);
						staordmonthMap.put("AgeInfName", agentList.get(i).get("AgeInfName"));
						staordmonthMap.put("monthCount", monthCount[j]);
						staordmonthMap.put("Price", Price[j]);
						staordmonthList.add(type,staordmonthMap);
						type++;
					}
				}
			}
			

			totalCount = staordService.findStaordCount(searchMap);// 总记录数
			if (totalCount > 0) {
				staordList = staordService.findStaordList(searchMap);
				for(int i=0;i<agentList.size();i++) {
					searchMap.put("AgeInfCode", agentList.get(i).get("AgeInf_Code"));
					Count[i]= staordService.findStaordCount(searchMap);// 总记录数
				}
			}
			
		}


		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("currentPage", Integer.parseInt(currentPage) + 1);
		resultMap.put("pageTotal", totalCount);
		resultMap.put("allmonthCount", allmonthCount);
		resultMap.put("yearCount", yearCount);
		resultMap.put("Count", Count);
		resultMap.put("staordmonthList", staordmonthList);
		resultMap.put("pageList", staordList);
		resultMap.put("success", "1");

		return resultMap;
	}
}
