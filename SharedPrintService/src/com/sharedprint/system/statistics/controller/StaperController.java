package com.sharedprint.system.statistics.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sharedprint.dics.service.DicsInfoService;
import com.sharedprint.system.statistics.service.StaperService;

/**
 * 商户统计的控制器
 */
@Controller
@RequestMapping("/api/Staper")
public class StaperController {

	@Autowired
	private StaperService staperService;
	@Autowired
	private DicsInfoService dicsInfoService;

	@RequestMapping("/queryPageList")
	@ResponseBody
	public Map<String, Object> queryStaperList(HttpServletRequest req) {
		String Crtyear = req.getParameter("querycrtyear");
		String currentPage = req.getParameter("currentPage");
		String pageSize = req.getParameter("pageSize");
		String orderByValue = req.getParameter("orderByValue");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		List<Map<String, Object>> staperList = new ArrayList<Map<String, Object>>();
		int totalCount = 0;
		int[] monthCount = new int[13];// 每月记录数
		int[] yearCount = new int[13];
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {
			if (currentPage == null || "".equals(currentPage.trim())) {
				currentPage = "0";
			}
			if (pageSize == null || "".equals(pageSize.trim())) {
				pageSize = "10";
			}
			Map<String, Object> searchMap = new HashMap<String, Object>();
			int currentpages = 0;
			if ((currentPage != null || !"".equals(currentPage.trim()))) {
				currentpages = Integer.parseInt(currentPage);
				currentpages = currentpages * Integer.parseInt(pageSize);
			}
			searchMap.put("dateyear", null);
			searchMap.put("datemonth", null);
			if (Crtyear != null && !"".equals(Crtyear.trim()) && !"undefined".equals(Crtyear.trim())) {
				searchMap.put("dateyear", Crtyear);
			}

			searchMap.put("currentPage", currentpages);
			searchMap.put("pageSize", Integer.parseInt(pageSize));

			// 获取当前年月
			Calendar cale = null;
			cale = Calendar.getInstance();
			int year = cale.get(Calendar.YEAR);

			for (int i = 0; i < 12; i++) {
				yearCount[i] = year - i;
				searchMap.put("datemonth", i);
				monthCount[i] = staperService.findStaperCount(searchMap);// 总记录数
			}
			searchMap.put("datemonth", null);
			totalCount = staperService.findStaperCount(searchMap);// 总记录数
			if (totalCount > 0) {
				if (orderByValue == null || "".equals(orderByValue.trim())) {
					searchMap.put("orderByAreaIdDesc", "1");
				}
				staperList = staperService.findStaperList(searchMap);
			}
		}
		// 获取数据字典
		Map<String, Object> dicsSearchMap = new HashMap<String, Object>();
		List<Map<String, Object>> dicsList = new ArrayList<Map<String, Object>>();
		dicsSearchMap.put("dicsCode", "AgeUserStatus");
		dicsList = dicsInfoService.findDicsList(dicsSearchMap);
		for (int i = 0; i < staperList.size(); i++) {
			for (int j = 0; j < dicsList.size(); j++) {
				if (staperList.get(i).get("PerUserStatus").toString()
						.equals(dicsList.get(j).get("key_value").toString())) {
					staperList.get(i).put("PerUserStatus", dicsList.get(j).get("key_name").toString());
				}
			}
		}

		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("currentPage", Integer.parseInt(currentPage) + 1);
		resultMap.put("pageTotal", totalCount);
		resultMap.put("monthCount", monthCount);
		resultMap.put("yearCount", yearCount);
		resultMap.put("pageList", staperList);
		resultMap.put("success", "1");

		return resultMap;
	}
}
