package com.sharedprint.system.devFactory.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sharedprint.system.devFactory.mapper.DevFactoryMapper;
import com.sharedprint.system.devFactory.pojo.DevFactory;

@Service
public class DevFactoryInfoServiceImpl implements DevFactoryInfoService {

	@Autowired
	private DevFactoryMapper devFactoryMapper;
	
	@Override
	public List<Map<String, Object>> findDevFactoryInfoList(Map<String, Object> searchMap) {
		return devFactoryMapper.findDevFactoryList(searchMap);
	}

	@Override
	public int findDevFactoryInfoCount(Map<String, Object> searchMap) {
		
		return devFactoryMapper.findDevFactoryCount(searchMap);
	}

	@Override
	public int insertDevFactoryInfo(DevFactory devFactory) {
		return devFactoryMapper.insertSelective(devFactory);
	}

	@Override
	public int updateDevFactoryInfo(DevFactory devFactory) {
	
		return devFactoryMapper.updateByPrimaryKeySelective(devFactory);
	}

	@Override
	public int delDevFactoryInfo(int devFactoryId) {
		return devFactoryMapper.deleteByPrimaryKey(devFactoryId);
	}



}
