package com.sharedprint.system.devFactory.service;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.devFactory.pojo.DevFactory;

public interface DevFactoryInfoService {

	/**
	 * 获取设备厂商列表
	 * @param searchMap
	 * @return
	 */
	List<Map<String, Object>> findDevFactoryInfoList(Map<String, Object> searchMap);
	
	/**
	 * 获取设备厂商数量
	 * @param searchMap
	 * @return
	 */
	int findDevFactoryInfoCount(Map<String, Object> searchMap);

	/**
	 * 新增设备厂商
	 * @param devFactory
	 * @return
	 */
	int insertDevFactoryInfo(DevFactory devFactory);
	/**
	 * 更新设备厂商
	 * @param devFactory
	 * @return
	 */
	int updateDevFactoryInfo(DevFactory devFactory);
	/**
	 * 删除设备厂商
	 * @param devFactory
	 * @return
	 */
	int delDevFactoryInfo(int DevFactoryID);

	
}
