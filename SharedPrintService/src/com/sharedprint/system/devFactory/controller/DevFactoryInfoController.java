package com.sharedprint.system.devFactory.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sharedprint.dics.service.DicsInfoService;
import com.sharedprint.system.devFactory.pojo.DevFactory;
import com.sharedprint.system.devFactory.service.DevFactoryInfoService;
import com.sharedprint.system.devInfo.service.DevInfoService;
import com.sharedprint.system.menu.service.MenuInfoService;
import com.sharedprint.system.role.pojo.RoleInfo;
import com.sharedprint.system.role.service.RoleInfoService;
import com.sharedprint.util.RandomCode;

/**
 * 角色管理的控制器
 * 
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/api/devFactoryInfo")
public class DevFactoryInfoController {

	@Autowired
	private DicsInfoService dicsInfoService;
	@Autowired
	private DevFactoryInfoService devFactoryInfoService;
	@Autowired
	private DevInfoService devInfoService;
	private Map<String, Object> dicsMap = new HashMap<String, Object>();
	
	 
	/**
	 * 获取设备厂商列表数据
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/queryPageList")
	@ResponseBody
	public Map<String, Object> queryDevFactoryList(HttpServletRequest req) {
		String DevFactoryNameLike = req.getParameter("DevFactoryNameLike");

		String currentPage = req.getParameter("currentPage");
		String pageSize = req.getParameter("pageSize");
		String orderByValue = req.getParameter("orderByValue");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		List<Map<String, Object>> devFactoryList = new ArrayList<Map<String, Object>>();
		int totalCount = 0;
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {
			if (currentPage == null || "".equals(currentPage.trim())) {
				currentPage = "0";
			}
			if (pageSize == null || "".equals(pageSize.trim())) {
				pageSize = "10";
			}
			Map<String, Object> searchMap = new HashMap<String, Object>();
			int currentpages = 0;
			if ((currentPage != null || !"".equals(currentPage.trim()))) {
				currentpages = Integer.parseInt(currentPage);
				currentpages = currentpages * Integer.parseInt(pageSize);
			}

			searchMap.put("currentPage", currentpages);
			searchMap.put("pageSize", Integer.parseInt(pageSize));
			searchMap.put("DevFactoryNameLike", DevFactoryNameLike);

			totalCount = devFactoryInfoService.findDevFactoryInfoCount(searchMap);// 总记录数
			if (totalCount > 0) {
				if (orderByValue == null || "".equals(orderByValue.trim())) {
					searchMap.put("orderByDevFactoryIdDesc", "1");
				}
				devFactoryList = devFactoryInfoService.findDevFactoryInfoList(searchMap);
			}
		}
		// list转json str
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("currentPage", Integer.parseInt(currentPage) + 1);
		resultMap.put("pageTotal", totalCount);
		resultMap.put("pageList", devFactoryList);
		resultMap.put("success", "1");

		return resultMap;
	}
	
	/**
	 * 验证设备厂商名称是否存在！
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/validateDevFactoryName")
	@ResponseBody
	public boolean validateDevFactory(HttpServletRequest req) {
		String addDevFactoryName = req.getParameter("addDevFactoryName");
		String updateDevFactoryName = req.getParameter("updateDevFactoryName");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		String excludeDevFactoryId = req.getParameter("excludeDevFactoryId");
		System.err.println("后台接受修改的id和名称"+excludeDevFactoryId+updateDevFactoryName);
		System.out.println("token=" + token + ",userId=" + userId );
		boolean result = false;
		Map<String, Object> searchMap = new HashMap<String, Object>();
		if (excludeDevFactoryId != null && !"".equals(excludeDevFactoryId.trim())) {
			searchMap.put("excludeDevFactoryId", excludeDevFactoryId);
			searchMap.put("updateDevFactoryName", updateDevFactoryName);
		}
		if (addDevFactoryName != null && !"".equals(addDevFactoryName.trim())) {
			searchMap.put("addDevFactoryName", addDevFactoryName);
		}
		int count = devFactoryInfoService.findDevFactoryInfoCount(searchMap);
		if (count == 0) {
			result = true;
		}
		return result;
	}

	/**
	 * 保存新增角色！
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public Map<String, Object> addDevFactory(HttpServletRequest req) {
		String devfactoryCode = RandomCode.getRandomCode();
		String devfactoryname = req.getParameter("addDevFactoryName");
		String weburl = req.getParameter("addWebURL");
		String contactname = req.getParameter("addContactName");
		String contacttel = req.getParameter("addContactTel");
		String address = req.getParameter("addAddress");
		String remark = req.getParameter("addRemark");
		String sortno = req.getParameter("addSortNo");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		System.out.println(token + userId);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {

			DevFactory devFactory = new DevFactory();
			devFactory.setDevfactoryCode(devfactoryCode);
			devFactory.setDevfactoryname(devfactoryname);
			devFactory.setWeburl(weburl);
			devFactory.setContactname(contactname);
			devFactory.setContacttel(contacttel);
			devFactory.setAddress(address);
			devFactory.setRemark(remark);
			devFactory.setSortno(Integer.parseInt(sortno));
			devFactory.setOprId(Integer.parseInt(userId));
			int success = 0;
			success = devFactoryInfoService.insertDevFactoryInfo(devFactory);
			if (success > 0) {
				resultMap.put("success", "1");
			} else {
				resultMap.put("msg", "添加失败");
			}
		} else {
			resultMap.put("msg", "请上传相关参数");
		}
		return resultMap;
	}

	// 初始化前台修改设备厂商数据
	@RequestMapping("/initUpdate")
	@ResponseBody
	public Map<String, Object> initUpdate(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		String DevFactoryId = req.getParameter("DevFactoryId");
		System.out.println("token=" + token + ",userId=" + userId + ",devFactoryId=" + DevFactoryId);

		Map<String, Object> devFactoryMap = new HashMap<String, Object>();
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<Map<String, Object>> devFactoryList = new ArrayList<Map<String, Object>>();
		dicsMap.put("dicsCode", "DevFactoryStatus");
		List<Map<String, Object>> dicsList = dicsInfoService.findDicsList(dicsMap);
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim()) && DevFactoryId != null
				&& !"".equals(DevFactoryId.trim())) {
			Map<String, Object> searchMap = new HashMap<String, Object>();
			searchMap.put("DevFactoryId", DevFactoryId);
			System.err.println("要修改的ID"+DevFactoryId);
			devFactoryList = devFactoryInfoService.findDevFactoryInfoList(searchMap);
			devFactoryMap = devFactoryList.get(0);
		}
		resultMap.put("devFactoryMap", devFactoryMap);
		resultMap.put("dicsList", dicsList);
		resultMap.put("success", "1");
		return resultMap;

	}

	/*
	 * 保存修改前台角色
	 */
	@RequestMapping("/update")
	@ResponseBody
	public Map<String, Object> saveUpdate(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		//String status = req.getParameter("newStatus");//预留冻结功能
		String devfactoryname = req.getParameter("updateDevFactoryName");
		String weburl = req.getParameter("updateWebURL");
		String contactname = req.getParameter("updateContactName");
		String contacttel = req.getParameter("updateContactTel");
		String address = req.getParameter("updateAddress");
		String remark = req.getParameter("updateRemark");
		String sortno = req.getParameter("updateSortNo");
		String excludeDevFactoryId = req.getParameter("excludeDevFactoryId");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		System.out.println("token:"+token +"userID："+ userId+"excludeDevFactoryId:"+excludeDevFactoryId);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {

			DevFactory devFactory = new DevFactory();
			//devFactory.setStatus(status);
			devFactory.setDevfactoryId(Integer.parseInt(excludeDevFactoryId));
			devFactory.setDevfactoryname(devfactoryname);
			devFactory.setWeburl(weburl);
			devFactory.setContactname(contactname);
			devFactory.setContacttel(contacttel);
			devFactory.setAddress(address);
			devFactory.setRemark(remark);
			devFactory.setSortno(Integer.parseInt(sortno));
			devFactory.setOprId(Integer.parseInt(userId));
			int success = devFactoryInfoService.updateDevFactoryInfo(devFactory);
			if (success > 0) {
				resultMap.put("success", "1");
			} else {
				resultMap.put("msg", "修改失败");
			}
		} else {
			resultMap.put("msg", "请上传相关参数");
		}
		return resultMap;
	}

	// 前台角色详情
	@RequestMapping("/detail")
	@ResponseBody
	public Map<String, Object> detailFront(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		return initUpdate(req,resp);
	}

	// 删除前台菜单数据
	@RequestMapping("/delete")
	@ResponseBody
	public Map<String, Object> delete(HttpServletRequest req, HttpServletResponse resp)  {
		String DevFactory_ID = req.getParameter("DevFactory_ID");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");

		Map<String, Object> resultMap = new HashMap<String, Object>();
		Map<String, Object> searchMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		if (DevFactory_ID != null && !"".equals(DevFactory_ID.trim()) && token != null && !"".equals(token.trim()) && userId != null
				&& !"".equals(userId.trim())) {
			searchMap.put("queryDevFactory", DevFactory_ID);
			int devNow = devInfoService.findDevInfoCount(searchMap);
			if (devNow>0) {
				resultMap.put("msg", "该厂商时下有设备， 不允许删除 !");
			}else {
				int success = devFactoryInfoService.delDevFactoryInfo(Integer.parseInt(DevFactory_ID));
				if (success > 0) {
					resultMap.put("success", "1");
				} else {
					resultMap.put("msg", "删除失败!");
				}
			}
		}
		return resultMap;
	}
}
