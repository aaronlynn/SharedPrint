package com.sharedprint.system.devFactory.pojo;

import java.util.Date;

public class DevFactory {
    private Integer devfactoryId;

    private String devfactoryCode;

    private String devfactoryname;

    private String status;

    private String weburl;

    private String contactname;

    private String contacttel;

    private String address;

    private Integer sortno;

    private String remark;

    private Integer oprId;

    private Date oprDate;

    public Integer getDevfactoryId() {
        return devfactoryId;
    }

    public void setDevfactoryId(Integer devfactoryId) {
        this.devfactoryId = devfactoryId;
    }

    public String getDevfactoryCode() {
        return devfactoryCode;
    }

    public void setDevfactoryCode(String devfactoryCode) {
        this.devfactoryCode = devfactoryCode == null ? null : devfactoryCode.trim();
    }

    public String getDevfactoryname() {
        return devfactoryname;
    }

    public void setDevfactoryname(String devfactoryname) {
        this.devfactoryname = devfactoryname == null ? null : devfactoryname.trim();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public String getWeburl() {
        return weburl;
    }

    public void setWeburl(String weburl) {
        this.weburl = weburl == null ? null : weburl.trim();
    }

    public String getContactname() {
        return contactname;
    }

    public void setContactname(String contactname) {
        this.contactname = contactname == null ? null : contactname.trim();
    }

    public String getContacttel() {
        return contacttel;
    }

    public void setContacttel(String contacttel) {
        this.contacttel = contacttel == null ? null : contacttel.trim();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public Integer getSortno() {
        return sortno;
    }

    public void setSortno(Integer sortno) {
        this.sortno = sortno;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Integer getOprId() {
        return oprId;
    }

    public void setOprId(Integer oprId) {
        this.oprId = oprId;
    }

    public Date getOprDate() {
        return oprDate;
    }

    public void setOprDate(Date oprDate) {
        this.oprDate = oprDate;
    }
}