package com.sharedprint.system.devFactory.mapper;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.devFactory.pojo.DevFactory;

public interface DevFactoryMapper {
    int deleteByPrimaryKey(Integer devfactoryId);

    int insert(DevFactory record);

    int insertSelective(DevFactory record);

    DevFactory selectByPrimaryKey(Integer devfactoryId);

    int updateByPrimaryKeySelective(DevFactory record);

    int updateByPrimaryKey(DevFactory record);

	List<Map<String, Object>> findDevFactoryList(Map<String, Object> searchMap);

	int findDevFactoryCount(Map<String, Object> searchMap);
}