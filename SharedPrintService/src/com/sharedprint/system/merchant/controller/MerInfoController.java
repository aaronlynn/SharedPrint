package com.sharedprint.system.merchant.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.swing.plaf.basic.BasicInternalFrameTitlePane.IconifyAction;

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.apache.tomcat.util.http.fileupload.RequestContext;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.apache.tools.ant.taskdefs.Get;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sharedprint.dics.service.DicsInfoService;
import com.sharedprint.system.agent.service.AgentInfoService;
import com.sharedprint.system.area.service.AreaInfoService;
import com.sharedprint.system.merchant.pojo.MerInfo;
import com.sharedprint.system.merchant.service.MerInfoService;
import com.sharedprint.system.merchantuser.pojo.MerUserInfo;
import com.sharedprint.system.merchantuser.service.MerUserService;
import com.sharedprint.system.organization.pojo.OrgInfo;
import com.sharedprint.system.organization.service.OrgInfoService;

@Controller
@RequestMapping("/api/merInfo")
public class MerInfoController {
	//商户服务端
	@Autowired
	private MerInfoService merInfoService; 
	
	//数据字典服务端
	@Autowired
	private DicsInfoService dicsInfoService;
	
	//地区服务端
	@Autowired
	private AreaInfoService areaInfoService;
	
	//机构服务端
	@Autowired
	private OrgInfoService orgInfoService;
	
	//商户用户服务端
	@Autowired
	private MerUserService merUserService;
	
	//代理商服务端
	@Autowired
	private AgentInfoService agentInfoService;
	
	//生成随机数
	public static String getRandomNum(){
	    Random r = new Random();
	    String string=String.valueOf(r.nextInt(900000)+100000);
	    return string;//(int)(Math.random()*999999)
	}
	
	//获取当前时间
	public static Date getNowTime() throws ParseException {
		Date now = new Date(); 
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		String date = dateFormat.format(now); 
		return dateFormat.parse(date);
	}
	//获取代理商号
	private String getAgentCode(HttpServletRequest req) {
		String loginName = req.getParameter("loginName");  //代理商登录账号
		String agentCode = null;//代理商号
		Map<String, Object> searchMap = new HashMap<String, Object>();
		// 判断登录的代理商编号
		if (loginName != null && loginName.trim() != "") {
			searchMap.put("loginName", loginName);
			List<Map<String, Object>> agentList = agentInfoService.findAgentInfoList(searchMap);
			agentCode = agentList.get(0).get("AgeInf_Code").toString();
		}
		return agentCode;
	}
	
	//获取商户主键
	private String getMerInfID(HttpServletRequest req) {
		String merInfCode = req.getParameter("merSystemMerInfID");
		String merInfID = null;//商户主键
		Map<String, Object> searchMap = new HashMap<String, Object>();
		if (merInfCode !=null) {
			searchMap.put("merInfCode", merInfCode);
			List<Map<String, Object>> merInfoList = merInfoService.findMerInfoList(searchMap);
			merInfID = merInfoList.get(0).get("MerInf_ID").toString();
		}
		return merInfID;
	}
	@RequestMapping("/queryPageList")
	@ResponseBody
	private Map<String, Object> findMerInfoList(HttpServletRequest req) {		
		//查询条件
		String queryCondition = req.getParameter("queryCondition");
		String queryMerInfStatus = req.getParameter("queryMerInfStatus");
		String queryOrgName = req.getParameter("queryOrgCode");
		String loginName = req.getParameter("loginName");  //代理商登录账号
		System.out.println("loginName="+loginName);
		//查询条件
		String pageSize = req.getParameter("pageSize");
		String currentPage = req.getParameter("currentPage");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		String agentCode = getAgentCode(req);//代理商号
		System.out.println("token=" + token + ",userId="+ userId);
		//searchMap数据库查询条件
		Map<String, Object> searchMap = new HashMap<String, Object>();
		searchMap.put("queryCondition", queryCondition);
		searchMap.put("queryMerInfStatus", queryMerInfStatus);
		searchMap.put("orgName", queryOrgName);
		if (agentCode !=null) {
			searchMap.put("AgeInfCode", agentCode);
		}
		//查询条件所属机构编号
		if(queryOrgName != null&&queryOrgName.trim() != "") {
			List<Map<String, Object>> orgList = orgInfoService.findOrgCodeList(searchMap);
			System.out.println(orgList.get(0).get("Org_Code").toString());
			String queryOrgCode =  orgList.get(0).get("Org_Code").toString();
			searchMap.put("queryOrgCode", queryOrgCode);
		}	
		searchMap.put("pageSize", Integer.parseInt(pageSize));
		int currentRecord = Integer.parseInt(currentPage)*Integer.parseInt(pageSize);
		searchMap.put("currentRecord", currentRecord);
		System.out.println("pageSize="+pageSize+",currentRecord="+currentRecord);
		
		List<Map<String, Object>> merList = merInfoService.findMerInfoList(searchMap);
		for (Map<String, Object> map : merList) {
			if (map.get("LastUptDate")!=null) {
				String oprDate = map.get("LastUptDate").toString();
				map.put("oprDate", oprDate);
			}
		}
		int totalCount = merInfoService.findMerInfoCount(searchMap); 
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		if(merList.size() > 0){
			resultMap.put("success", "1");			
			resultMap.put("listDataGrid", merList);
			resultMap.put("pageTotal", totalCount);
		}
		System.out.println("resultMap11="+resultMap);
		return resultMap;
	}
	
	
	/**
	 * 初始化添加商户
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/initAdd")
	@ResponseBody
	private Map<String, Object> initAdd(HttpServletRequest req){
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		String loginName = req.getParameter("loginName");  //代理商登录账号
		String agentCode = getAgentCode(req);//代理商号
		System.out.println("agentCode="+agentCode);
		
		Map<String, Object> searchMap = new HashMap<String, Object>();
		if (agentCode != null) {
			searchMap.put("ageInfCode", agentCode);
		}
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<Map<String, Object>> orgInfoList = orgInfoService.findOrgInfoList(searchMap);
		List<Map<String, Object>> dicsInfoList = dicsInfoService.findDicsList(searchMap);
		List<Map<String, Object>> areaInfoList = areaInfoService.findAreaInfoList(searchMap);
		List<Map<String, Object>> agentInfoList = agentInfoService.findAgentInfoList(searchMap);
		
		if(orgInfoList.size()>0&&dicsInfoList.size()>0&&areaInfoList.size()>0) {
			resultMap.put("success", "1");
			resultMap.put("orgInfoList", orgInfoList);
			resultMap.put("dicsInfoList", dicsInfoList);
			resultMap.put("areaInfoList", areaInfoList);
			resultMap.put("agentInfoList", agentInfoList);
			if (agentCode !=null) {
				String agentName = agentInfoList.get(0).get("AgeInfName").toString();
				System.out.println("agentName="+agentName);
				resultMap.put("AgeInfName", agentName);
				resultMap.put("AgeInfCode", agentCode);
			}
		}else {
			resultMap.put("success", "0");
		}
		return resultMap;
	}
	
	/**
	 * 判断商户号是否重复
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/validateMerCode")
	@ResponseBody
	private Boolean validMerCode (HttpServletRequest req){
		// TODO Auto-generated method stub
		String merInfCode = req.getParameter("merInfCode");
		String excludeMerId = req.getParameter("merInfId");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		System.out.println("token=" + token + ",userId=" + userId + ",exclude="+excludeMerId);
		boolean result = false;
		if (merInfCode != null && !"".equals(merInfCode.trim())) {
			Map<String, Object> searchMap = new HashMap<String, Object>();
			searchMap.put("merInfCode", merInfCode);
			if (excludeMerId !=null && !"".equals(excludeMerId.trim())) {
				searchMap.put("excludeMerId", excludeMerId);
			}
			int count = merInfoService.findMerInfoCount(searchMap);
			if (count == 0) {
				result = true;
			}
		}
		return result;
	}
	
	/**
	 * 判断商户名称是否重复
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/validateMerName")
	@ResponseBody
	private Boolean validMerName(HttpServletRequest req){
		// TODO Auto-generated method stub
		String merInfName = req.getParameter("merInfName");
		String excludeMerId = req.getParameter("merInfId");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		System.out.println("token=" + token + ",userId=" + userId + ",exclude="+excludeMerId);
		boolean result = false;
		if (merInfName != null && !"".equals(merInfName.trim())) {
			Map<String, Object> searchMap = new HashMap<String, Object>();
			searchMap.put("merInfName", merInfName);
			if (excludeMerId !=null && !"".equals(excludeMerId.trim())) {
				searchMap.put("excludeMerId", excludeMerId);
			}
			int count = merInfoService.findMerInfoCount(searchMap);
			if (count == 0) {
				result = true;
			}
		}
		return result;
	}
	
	@RequestMapping("/add")
	@ResponseBody
	private Map<String, Object> addMerInfo(HttpServletRequest req) throws ParseException {
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		System.out.println("token=" + token + ",userId=" + userId);
		String merInfName = req.getParameter("addMerInfName");
		String IDType = req.getParameter("addIDType");
		String IDCode = req.getParameter("addIDCode");
		String merInfType= req.getParameter("addMerInfType");
		String ageInfCode = req.getParameter("addAgeInfCode");
		String merInfLevel = req.getParameter("addMerInfLevel");		
		String alias = req.getParameter("addAlias");
		String orgCode = req.getParameter("addOrgCode");
		String areaCode = req.getParameter("addAreaCode");
		String busiAddress = req.getParameter("addBusiAddress");
		String contactor = req.getParameter("addContactor");
		String contactTel = req.getParameter("addContactTel");
		String email = req.getParameter("addEmail");
		String QQ = req.getParameter("addQQ");
		String weixin = req.getParameter("addWeixin");
		String IDPath = req.getParameter("addIDPath");
		String busiAddressPath = req.getParameter("addBusiAddressPath");
		String cliMgrCode = req.getParameter("addCliMgrCode");
		String remark = req.getParameter("addRemark");
		String merInfCode =getRandomNum();
		Map<String, Object> resultMap = new HashMap<String, Object>();
		if (token != null && !"".equals(token.trim())){
			//商户添加
			MerInfo merInfo = new MerInfo();
			merInfo.setMerinfCode(merInfCode);
			merInfo.setMerinfname(merInfName);
			merInfo.setIdcode(IDCode);
			merInfo.setIdtype(IDType);
			merInfo.setMerinflevel(merInfLevel);
			merInfo.setMerinftype(merInfType);
			merInfo.setAgeinfCode(ageInfCode);
			merInfo.setAlias(alias);
			merInfo.setAreaCode(areaCode);
			merInfo.setOrgCode(orgCode);
			merInfo.setBusiaddress(busiAddress);
			merInfo.setContactor(contactor);
			merInfo.setContacttel(contactTel);
			merInfo.setEmail(email);
			merInfo.setQq(QQ);
			merInfo.setWeixin(weixin);
			merInfo.setIdpath(IDPath);
			merInfo.setBusiaddresspath(busiAddressPath);
			merInfo.setClimgrcode(cliMgrCode);
			merInfo.setRemark(remark);
			merInfo.setCrtuserid(Integer.parseInt(userId));			 
			merInfo.setCrtdate(getNowTime());
			merInfo.setMerinfstatus("4");
			int success = merInfoService.insertMerInfo(merInfo);
			//商户用户添加
			MerUserInfo merUserInfo = new MerUserInfo();
			merUserInfo.setMerusername(contactor);
			merUserInfo.setMerinfCode(merInfCode);
			merUserInfo.setLoginname(IDCode);
			String merUserCode = getRandomNum();
			merUserInfo.setMeruserCode(merUserCode);
			merUserInfo.setLoginpwd("123456");
			merUserInfo.setMeruserstatus("1");
			merUserInfo.setFirstloginflag("0");
			merUserInfo.setInitpwdflag("0");
			merUserInfo.setOprId(Integer.parseInt(userId));
			merUserInfo.setOprDate(getNowTime());
			int successMerUser = merUserService.insertMerUser(merUserInfo);
			if(success > 0 && successMerUser > 0){
				resultMap.put("success", "1");
			}else{
				resultMap.put("msg", "保存失败");
			}	
		}else{
			resultMap.put("msg", "保存失败");
		}	
		System.out.println("resultMap="+resultMap);
		return resultMap;	
	}	
	/**
	 * 初始化修改商户
	 * 
	 * @param req
	 * @return
	 * @throws ParseException 
	 */
	@RequestMapping("/initUpdate")
	@ResponseBody
	private Map<String, Object> initUpdate(HttpServletRequest req) throws ParseException{
		String merInfId = req.getParameter("merInfId");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		String loginName = req.getParameter("loginName");  //代理商登录账号
		String agentCode = getAgentCode(req);//代理商号
		Map<String, Object> searchMap = new HashMap<String, Object>();
		Map<String, Object> resultMap = new HashMap<String, Object>();
		if (merInfId != null && merInfId.trim() !="") {			
			List<Map<String, Object>> orgInfoList = orgInfoService.findOrgInfoList(searchMap);
			List<Map<String, Object>> dicsInfoList = dicsInfoService.findDicsList(searchMap);
			List<Map<String, Object>> areaInfoList = areaInfoService.findAreaInfoList(searchMap);
			List<Map<String, Object>> agentInfoList = agentInfoService.findAgentInfoList(searchMap);
			MerInfo merInfo = merInfoService.findMerInfoDetail(Integer.parseInt(merInfId));
			if(orgInfoList.size()>0&&dicsInfoList.size()>0&&areaInfoList.size()>0
					&&merInfo !=null){		
				resultMap.put("success", "1");
				resultMap.put("orgInfoList", orgInfoList);
				resultMap.put("dicsInfoList", dicsInfoList);
				resultMap.put("areaInfoList", areaInfoList);
				resultMap.put("agentInfoList", agentInfoList);
				resultMap.put("merInfo", merInfo);
				if (agentCode !=null) {
					String agentName = agentInfoList.get(0).get("AgeInfName").toString();
					System.out.println("agentName222="+agentName);
					resultMap.put("AgeInfName", agentName);
				}
			}else {
				resultMap.put("success", "0");
			} 
		}
		return resultMap;
	}
	
	/**
	 * 修改商户
	 * 
	 * @param req
	 * @return
	 * @throws ParseException 
	 */
	@RequestMapping("/update")
	@ResponseBody
	private Map<String, Object> updateMerInfo(HttpServletRequest req) throws ParseException{
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		System.out.println("token=" + token + ",userId=" + userId);
		String merInfId = req.getParameter("updateMerInfId");
		String merInfCode = req.getParameter("updateMerInfCode");
		String merInfName = req.getParameter("updateMerInfName");
		String IDType = req.getParameter("updateIDType");
		String IDCode = req.getParameter("updateIDCode");
		String merInfType= req.getParameter("updateMerInfType");
		String ageInfCode = req.getParameter("updateAgeInfCode");
		String merInfLevel = req.getParameter("updateMerInfLevel");		
		String alias = req.getParameter("updateAlias");
		String orgCode = req.getParameter("updateOrgCode");
		String areaCode = req.getParameter("updateAreaCode");
		String busiAddress = req.getParameter("updateBusiAddress");
		String contactor = req.getParameter("updateContactor");
		String contactTel = req.getParameter("updateContactTel");
		String email = req.getParameter("updateEmail");
		String QQ = req.getParameter("updateQQ");
		String weixin = req.getParameter("updateWeixin");
		String IDPath = req.getParameter("updateIDPath");
		String busiAddressPath = req.getParameter("updateBusiAddressPath");
		String cliMgrCode = req.getParameter("updateCliMgrCode");
		String remark = req.getParameter("updateRemark");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		if (token != null && !"".equals(token.trim())){
			MerInfo merInfo = new MerInfo();
			merInfo.setMerinfId(Integer.parseInt(merInfId));
			merInfo.setMerinfCode(merInfCode);
			merInfo.setMerinfname(merInfName);
			merInfo.setIdcode(IDCode);
			merInfo.setIdtype(IDType);
			merInfo.setMerinflevel(merInfLevel);
			merInfo.setMerinftype(merInfType);
			merInfo.setAgeinfCode(ageInfCode);
			merInfo.setAlias(alias);
			merInfo.setAreaCode(areaCode);
			merInfo.setOrgCode(orgCode);
			merInfo.setBusiaddress(busiAddress);
			merInfo.setContactor(contactor);
			merInfo.setContacttel(contactTel);
			merInfo.setEmail(email);
			merInfo.setQq(QQ);
			merInfo.setWeixin(weixin);
			merInfo.setIdpath(IDPath);
			merInfo.setBusiaddresspath(busiAddressPath);
			merInfo.setClimgrcode(cliMgrCode);
			merInfo.setRemark(remark);
			merInfo.setUptuserid(Integer.parseInt(userId));
			Date now = new Date(); 
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			String crtDate = dateFormat.format(now); 
			merInfo.setLastuptdate(dateFormat.parse(crtDate));
			int success = merInfoService.updateMerInfo(merInfo);
			if(success > 0){
				resultMap.put("success", "1");
			}else{
				resultMap.put("msg", "系统繁忙，修改失败！");
			}	
		}else{
			resultMap.put("msg", "系统繁忙，修改失败！");
		}	
		System.out.println("resultMap="+resultMap);
		return resultMap;	
	}
	
	/**
	 * 删除商户
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/delete")
	@ResponseBody
	private Map<String, Object> deleteMerInfo(HttpServletRequest req){
		String merInfId = req.getParameter("merInfId");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		int success = merInfoService.deleteMerInfo(Integer.parseInt(merInfId));
		if(success != 0) {
			resultMap.put("success", "1");
		}else {
			resultMap.put("success", "0");
		}
		return resultMap;
	}
	
	/**
	 * 商户详情
	 * 
	 * @param req
	 * @return
	 * @throws ParseException 
	 */
	@RequestMapping("/detail")
	@ResponseBody
	private Map<String, Object> detailMerInfo(HttpServletRequest req) throws ParseException{
		String merInfId = null;
		String merSystemMerInfID = getMerInfID(req);
		if (req.getParameter("merInfId") ==null&&merSystemMerInfID !=null ){
			merInfId = merSystemMerInfID;
		}else {
			merInfId = req.getParameter("merInfId");
		}
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		Map<String, Object> searchMap = new HashMap<String, Object>();
		Map<String, Object> resultMap = new HashMap<String, Object>();
		if (merInfId != null && merInfId.trim() !="") {			
			List<Map<String, Object>> orgInfoList = orgInfoService.findOrgInfoList(searchMap);
			List<Map<String, Object>> dicsInfoList = dicsInfoService.findDicsList(searchMap);
			List<Map<String, Object>> areaInfoList = areaInfoService.findAreaInfoList(searchMap);
			List<Map<String, Object>> agentInfoList = agentInfoService.findAgentInfoList(searchMap);
			MerInfo merInfo = merInfoService.findMerInfoDetail(Integer.parseInt(merInfId));
			if(orgInfoList.size()>0&&dicsInfoList.size()>0&&areaInfoList.size()>0
					&&merInfo !=null){		
				resultMap.put("success", "1");
				resultMap.put("orgInfoList", orgInfoList);
				resultMap.put("dicsInfoList", dicsInfoList);
				resultMap.put("areaInfoList", areaInfoList);
				resultMap.put("agentInfoList", agentInfoList);
				resultMap.put("merInfo", merInfo);
			}else {
				resultMap.put("success", "0");
			} 
		}
		return resultMap;
	}
	
	/**
	 * 审核商户信息
	 * 
	 * @param req
	 * @return
	 * @throws ParseException 
	 */
	@RequestMapping("/check")
	@ResponseBody
	private Map<String, Object> checkMerInfo(HttpServletRequest req) throws ParseException{
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		String merInfId = req.getParameter("checkMerInfId");
		String checkResult = req.getParameter("checkResult");
		String checkRemark = req.getParameter("checkRemark");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		MerInfo merInfo = new MerInfo();
		if(checkResult!=null && "checkPass".equals(checkResult.trim())){
			merInfo.setMerinfstatus("1");
			System.err.println("11111");
		}else if(checkResult!=null && "checkNotPass".equals(checkResult.trim())){
			merInfo.setMerinfstatus("5");
			System.err.println("555555");
		}
		merInfo.setMerinfId(Integer.parseInt(merInfId));
		merInfo.setChkuserid(Integer.parseInt(userId));
		merInfo.setChkdate(getNowTime());
		merInfo.setChkremark(checkRemark);
		int success = merInfoService.updateMerInfo(merInfo);
		if(success > 0) {
			resultMap.put("success", "1");
		}else {
			resultMap.put("success", "0");
		}
		return resultMap;
	}
	
	/**
	 * 初始化商户冻结与解冻界面
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/initFreeze")
	@ResponseBody
	private Map<String, Object> initFreeze(HttpServletRequest req){
		String merInfId = req.getParameter("merInfId");
		Map<String, Object> searchMap = new HashMap<String, Object>();
		List<Map<String, Object>> dicsInfoList = dicsInfoService.findDicsList(searchMap);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		if (merInfId !=null&& merInfId.trim() !="") {
			String normalStatus = null; //正常商户状态
			String freezeStatus = null;//冻结用户状态
			MerInfo merInfo =merInfoService.findMerInfoDetail(Integer.parseInt(merInfId));
			for (Map<String, Object> map : dicsInfoList) {
				if(map.get("dics_code").equals("MerInfStatus") && map.get("key_name").equals("正常")) {
					normalStatus = map.get("key_value").toString();					
				}else if (map.get("dics_code").equals("MerInfStatus") && map.get("key_name").equals("冻结")) {
					freezeStatus = map.get("key_value").toString();
				}
			}
			if (normalStatus !=null&&freezeStatus!=null) {
				if(merInfo.getMerinfstatus().equals(normalStatus)) {
					resultMap.put("success", "1");//商户状态正常
				}else if (merInfo.getMerinfstatus().equals(freezeStatus)) {
					resultMap.put("success", "2");//商户状态已冻结
				}
			}else {
				resultMap.put("success", "0");
			}					
		}
		System.out.println("success="+resultMap.get("success"));
		return resultMap;
	}
	
	@RequestMapping("/thawAndFreezeMethod")
	@ResponseBody
	Map<String,Object> thawAndFreezeMer(HttpServletRequest req){
		String merInfId = req.getParameter("MerInfId");
		String oprResult = req.getParameter("oprResult");
		Map<String, Object> searchMap = new HashMap<String, Object>();
		List<Map<String, Object>> dicsInfoList = dicsInfoService.findDicsList(searchMap);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		int success = 0;
		String normalStatus = null; //正常商户状态
		String freezeStatus = null;//冻结用户状态
		MerInfo merInfo = new MerInfo();
		merInfo.setMerinfId(Integer.parseInt(merInfId));
		if (dicsInfoList !=null) {			
			for (Map<String, Object> map : dicsInfoList) {
				if(map.get("dics_code").equals("MerInfStatus") && map.get("key_name").equals("正常")) {
					normalStatus = map.get("key_value").toString();					
				}else if (map.get("dics_code").equals("MerInfStatus") && map.get("key_name").equals("冻结")) {
					freezeStatus = map.get("key_value").toString();
				}
			}
		}
		if (oprResult!=null && oprResult.equals("冻结")) {
			merInfo.setMerinfstatus(freezeStatus);
			success = merInfoService.updateMerInfo(merInfo);
		}else if (oprResult!=null && oprResult.equals("解冻")) {
			merInfo.setMerinfstatus(normalStatus);
			success = merInfoService.updateMerInfo(merInfo)+1;
		}else {
			resultMap.put("msg", "系统繁忙，操作失败！");
			resultMap.put("success", "0");
		}
		if (success == 1) {
			resultMap.put("success", "1"); //冻结成功
		}else if(success ==2) {
			resultMap.put("success", "2"); //解冻成功
		}else {
			resultMap.put("success", "0");
		}
		return resultMap;
	}
}
