package com.sharedprint.system.merchant.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sharedprint.dics.service.DicsInfoService;
import com.sharedprint.system.agent.service.AgentInfoService;
import com.sharedprint.system.merchant.mapper.MerDevInfoMapper;
import com.sharedprint.system.merchant.pojo.MerDevInfo;
import com.sharedprint.system.merchant.service.MerDevInfoService;
import com.sharedprint.system.merchant.service.MerInfoService;
import com.sharedprint.system.organization.service.OrgInfoService;

@Controller
@RequestMapping("/api/merDevInfo")
public class MerDevInfoController {
	@Autowired
	MerDevInfoService merDevInfoService;
	
	//数据字典服务端
	@Autowired
	DicsInfoService dicsInfoService;
	
	//商户服务端
	@Autowired
	MerInfoService merInfoService;
	
	//机构服务端
	@Autowired
	OrgInfoService orgInfoService;
	
	@Autowired
	AgentInfoService agentInfoService;
	
	// 获取当前时间
	public static Date getNowTime() throws ParseException {
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		String date = dateFormat.format(now);
		return dateFormat.parse(date);
	}
	
	// 获取代理商号
	private String getAgentCode(HttpServletRequest req) {
		String loginName = req.getParameter("loginName"); // 代理商登录账号
		String agentCode = null;// 代理商号
		Map<String, Object> searchMap = new HashMap<String, Object>();
		// 判断登录的代理商编号
		if (loginName != null && loginName.trim() != "") {
			searchMap.put("loginName", loginName);
			List<Map<String, Object>> agentList = agentInfoService.findAgentInfoList(searchMap);
			agentCode = agentList.get(0).get("AgeInf_Code").toString();
		}
		return agentCode;
	}
	@RequestMapping("/queryPageList")
	@ResponseBody
	private Map<String, Object> findMerDevInfoList(HttpServletRequest req) {		
		//查询条件
		String queryMerInfo = req.getParameter("queryMerInfo");
		String queryAgeInfName = null;
		if(req.getParameter("queryAgeInfCode")!=null) {
			queryAgeInfName = req.getParameter("queryAgeInfCode");
		}
		String queryOrgName = req.getParameter("queryOrgCode");
		String agentCode = getAgentCode(req);
		//查询条件
		String pageSize = req.getParameter("pageSize");
		String currentPage = req.getParameter("currentPage");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		System.out.println("token=" + token + ",userId="+ userId);
		Map<String, Object> searchMap = new HashMap<String, Object>();		
		List<Map<String, Object>> dicsInfoList = dicsInfoService.findDicsList(searchMap);//查询数据字典
		List<Map<String, Object>> merInfoList = merInfoService.findMerInfoList(searchMap);
		searchMap.put("queryCondition", queryMerInfo); //商户查询条件
		if (queryAgeInfName !=null) {
			searchMap.put("addAgeInfName", queryAgeInfName);
		}
		//searchMap.put("addAgeInfName", queryAgeInfName);
		System.err.println("addAgeInfName="+searchMap.get("addAgeInfName"));
		searchMap.put("queryOrgName", queryOrgName);
		if (agentCode !=null) {
			searchMap.put("AgentCode", agentCode);
		}
		if(queryOrgName != null&&queryOrgName.trim() != "") {  //机构查询
			List<Map<String, Object>> orgList = orgInfoService.findOrgCodeList(searchMap);
			//System.out.println(orgList.get(0).get("Org_Code").toString());
			String queryOrgCode =  orgList.get(0).get("Org_Code").toString();
			searchMap.put("queryOrgCode", queryOrgCode);
		}
		if(queryAgeInfName !=null && queryAgeInfName.trim()!="") {  //代理商查询
			List<Map<String, Object>> agentList = agentInfoService.findAgentInfoList(searchMap);
			String queryAgentCode = agentList.get(0).get("AgeInf_Code").toString();
			searchMap.put("queryAgentCode", queryAgentCode);
		}
		if (queryMerInfo !=null&&queryMerInfo.trim() != "") {  //商户查询
			List<Map<String, Object>> merList = merInfoService.findMerInfoList(searchMap);
			String queryMerCode = merList.get(0).get("MerInf_Code").toString();
			System.out.println("queryMerCode="+queryMerCode);
			searchMap.put("queryMerCode", queryMerCode);
		}
		searchMap.put("pageSize", Integer.parseInt(pageSize));
		int currentRecord = Integer.parseInt(currentPage)*Integer.parseInt(pageSize);
		searchMap.put("currentRecord", currentRecord);
		System.out.println("pageSize="+pageSize+",currentRecord="+currentRecord);
		List<Map<String, Object>> merDevList = merDevInfoService.findMerDevInfoList(searchMap);
		for (Map<String, Object> map : merDevList) {
			for (Map<String, Object> map2 : dicsInfoList) {
				if (map2.get("dics_code").equals("MerDevStatus")) {
					if (map.get("MerDevStatus").equals(map2.get("key_value").toString())) {
						map.put("merDevStatus", map2.get("key_name").toString()); //商户设备审核状态
					}
				}else if ((map2.get("dics_code").equals("DevWorkStatus"))) {
					if (map.get("DevWorkStatus").equals(map2.get("key_value").toString())) {
						map.put("devWorkStatus", map2.get("key_name").toString());//商户设备工作状态
					}
				}		
			}
			for (Map<String, Object> map3 : merInfoList) {
				if (map.get("MerInf_Code").equals(map3.get("MerInf_Code"))) {
					map.put("merInfName", map3.get("MerInfName").toString());
				}
			}
		}
		int totalCount = merDevInfoService.findMerDevInfoCount(searchMap);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		if(merDevList.size() > 0){
			resultMap.put("success", "1");			
			resultMap.put("listDataGrid", merDevList);
			resultMap.put("pageTotal", totalCount);
		}else {
			resultMap.put("success", "0");
			resultMap.put("msg", "系统繁忙，查询失败！");		
		}
		System.out.println("resultMap11="+resultMap);
		return resultMap;
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	private Map<String, Object> deleteMerDevInfo(HttpServletRequest request){
		String merDevId = request.getParameter("merDevInfId");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		int success=0;
		if (merDevId !=null) {
			success = merDevInfoService.deleteMerDevInfo(Integer.parseInt(merDevId));
		}
		if (success >0) {
			resultMap.put("success", "1");
		}else {
			resultMap.put("success", "0");
			resultMap.put("msg", "系统繁忙，删除错误！请稍后再试！");
		}
		return resultMap;
	}
	
	@RequestMapping("/detail")
	@ResponseBody
	private Map<String, Object> detailMerDevInfo(HttpServletRequest req){
		String merDevId = req.getParameter("merDevInfId");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		Map<String, Object> searchMap = new HashMap<String, Object>();		
		List<Map<String, Object>> dicsInfoList = dicsInfoService.findDicsList(searchMap);
		MerDevInfo merDevInfo=null;
		if (merDevId !=null) {
			merDevInfo = merDevInfoService.detailMerDevInfo(Integer.parseInt(merDevId));
			for (Map<String, Object> map : dicsInfoList) {
				if (map.get("dics_code").equals("MerDevStatus")) {
					if (merDevInfo.getMerdevstatus().equals(map.get("key_value").toString())) {
						resultMap.put("merDevStatus", map.get("key_name").toString()); //商户设备审核状态
					}
				}else if ((map.get("dics_code").equals("DevWorkStatus"))) {
					if (merDevInfo.getDevworkstatus().equals(map.get("key_value").toString())) {
						resultMap.put("devWorkStatus", map.get("key_name").toString());//商户设备工作状态
					}
				}		
			}
			searchMap.put("ageInfCode", merDevInfo.getAgeinfCode());
			List<Map<String, Object>> agentList = agentInfoService.findAgentInfoList(searchMap);
			if (agentList != null) {
				resultMap.put("ageInfName", agentList.get(0).get("AgeInfName"));
			}
		}
		if (merDevInfo !=null) {
			resultMap.put("success", "1");
			resultMap.put("merDevInfo", merDevInfo);
		}else {
			resultMap.put("success", "0");
			resultMap.put("msg", "系统繁忙，删除错误！请稍后再试！");
		}
		return resultMap;
	}
	
	/**
	 * 审核商户设备信息
	 * 
	 * @param req
	 * @return
	 * @throws ParseException 
	 */
	@RequestMapping("/check.action")
	@ResponseBody
	private Map<String, Object> checkMerDevInfo(HttpServletRequest req) throws ParseException{
		String checkResult = req.getParameter("checkResult");
		String checkSN = req.getParameter("checkSN");
		String merDevId = req.getParameter("checkMerDevId");
		String checkRemark = req.getParameter("checkRemark");
		System.out.println("checkResult="+checkResult+",checkSN="+checkSN+",merDevId="+merDevId+",checkRemark="+checkRemark);
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		Map<String, Object> searchMap = new HashMap<String, Object>();		
		List<Map<String, Object>> dicsInfoList = dicsInfoService.findDicsList(searchMap);
		String checkPass = null;
		String checkNotPass = null;
		int success = 0;
		MerDevInfo merDevInfo = new MerDevInfo();
		merDevInfo.setMerdevId(Integer.parseInt(merDevId));
		for (Map<String, Object> map : dicsInfoList) {
			if (map.get("dics_code").equals("MerDevStatus")) {
				if (map.get("key_name").equals("正常")) {
					checkPass = map.get("key_value").toString();
				}else if (map.get("key_name").equals("审核不通过")) {
					checkNotPass = map.get("key_value").toString();
				}
			}
		}
		if (token !=null && userId !=null) {
			merDevInfo.setChkuserid(Integer.parseInt(userId));
			merDevInfo.setChkdate(getNowTime());
			if (checkResult.equals("checkPass")) {
				merDevInfo.setMerdevstatus(checkPass);
			}else if (checkResult.equals("checkNotPass")) {
				merDevInfo.setMerdevstatus(checkNotPass);
			}
			if (merDevId != null ) {
				merDevInfo.setSn(checkSN);
				merDevInfo.setChkremark(checkRemark);
				success = merDevInfoService.updateMerDevInfo(merDevInfo);
			}			
			if (success > 0) {
				resultMap.put("success", "1");
			}else {
				resultMap.put("success", "0");
				resultMap.put("msg", "系统繁忙，审核失败！");
			}
		}else {
			resultMap.put("success", "0");
			resultMap.put("msg", "系统繁忙，审核失败！");
		}		
		return resultMap;
	}
}
