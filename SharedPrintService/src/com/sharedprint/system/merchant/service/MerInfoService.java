package com.sharedprint.system.merchant.service;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.merchant.pojo.MerInfo;

public interface MerInfoService {
	/**
     * 查询商户列表
     * 
     * @param searchMap
     * @return
     */
    List<Map<String, Object>> findMerInfoList(Map<String, Object> searchMap);
    
    /**
     * 查询商户数量
     * 
     * @param searchMap
     * @return
     */
    int findMerInfoCount(Map<String, Object> searchMap);
    
    /**
     * 插入商户数据
     * 
     * @param searchMap
     * @return
     */
    int insertMerInfo(MerInfo merinfo);
    
    /**
     * 查询商户详情
     * 
     * @param merInfId
     * @return
     */
    MerInfo findMerInfoDetail(int merInfId);
    
    /**
     * 删除商户
     * 
     * @param merInfId
     * @return
     */
    int deleteMerInfo(int merInfId);
    
    /**
     * 修改商户数据
     * 
     * @param merInfo
     * @return
     */
    int updateMerInfo(MerInfo merInfo);
}
