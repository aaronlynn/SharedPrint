package com.sharedprint.system.merchant.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sharedprint.system.merchant.mapper.MerDevInfoMapper;
import com.sharedprint.system.merchant.pojo.MerDevInfo;

@Service
public class MerDevInfoServiceImpl implements MerDevInfoService {
	@Autowired
	MerDevInfoMapper merDevInfoMapper;
	
	@Override
	public List<Map<String, Object>> findMerDevInfoList(Map<String, Object> searchMap) {
		// TODO Auto-generated method stub
		return merDevInfoMapper.findMerDevInfoList(searchMap);
	}

	@Override
	public int findMerDevInfoCount(Map<String, Object> searchMap) {
		// TODO Auto-generated method stub
		return merDevInfoMapper.findMerDevInfoCount(searchMap);
	}

	@Override
	public int deleteMerDevInfo(int merDevId) {
		// TODO Auto-generated method stub
		return merDevInfoMapper.deleteByPrimaryKey(merDevId);
	}

	@Override
	public MerDevInfo detailMerDevInfo(int merDevId) {
		// TODO Auto-generated method stub
		return merDevInfoMapper.selectByPrimaryKey(merDevId);
	}

	@Override
	public int updateMerDevInfo(MerDevInfo merDevInfo) {
		// TODO Auto-generated method stub
		return merDevInfoMapper.updateByPrimaryKeySelective(merDevInfo);
	}

	
}
