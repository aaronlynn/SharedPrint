package com.sharedprint.system.merchant.service;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.merchant.pojo.MerDevInfo;

public interface MerDevInfoService {
	/**
     * 查询商户设备列表
     * 
     * @param searchMap
     * @return
     */
    List<Map<String, Object>> findMerDevInfoList(Map<String, Object> searchMap);
    
    /**
     * 查询商户设备数量
     * 
     * @param searchMap
     * @return
     */
    int findMerDevInfoCount(Map<String, Object> searchMap);
    
    /**
     * 删除商户设备
     * 
     * @param searchMap
     * @return
     */
    int deleteMerDevInfo(int merDevId);
    
    /**
     * 修改商户设备信息
     * 
     * @param merDevInfo
     * @return
     */
    int updateMerDevInfo(MerDevInfo merDevInfo);
    
    /**
     * 查询商户设备信息
     * 
     * @param merDevId
     * @return
     */
    MerDevInfo detailMerDevInfo(int merDevId);
}
