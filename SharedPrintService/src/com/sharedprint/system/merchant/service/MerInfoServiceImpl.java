package com.sharedprint.system.merchant.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sharedprint.system.merchant.controller.MerInfoController;
import com.sharedprint.system.merchant.mapper.MerInfoMapper;
import com.sharedprint.system.merchant.pojo.MerInfo;

@Service
public class MerInfoServiceImpl implements MerInfoService {
	@Autowired
	private MerInfoMapper merInfoMapper;
	
	@Override
	public List<Map<String, Object>> findMerInfoList(Map<String, Object> searchMap) {
		// TODO Auto-generated method stub
		return merInfoMapper.findMerInfoList(searchMap);
	}

	@Override
	public int findMerInfoCount(Map<String, Object> searchMap) {
		// TODO Auto-generated method stub
		return merInfoMapper.findMerInfoCount(searchMap);
	}

	@Override
	public int insertMerInfo(MerInfo merInfo) {
		// TODO Auto-generated method stub
		return merInfoMapper.insertSelective(merInfo);
	}

	@Override
	public MerInfo findMerInfoDetail(int merInfId) {
		// TODO Auto-generated method stub
		return merInfoMapper.selectByPrimaryKey(merInfId);
	}

	@Override
	public int deleteMerInfo(int merInfId) {
		// TODO Auto-generated method stub
		return merInfoMapper.deleteByPrimaryKey(merInfId);
	}

	@Override
	public int updateMerInfo(MerInfo merInfo) {
		// TODO Auto-generated method stub
		return merInfoMapper.updateByPrimaryKeySelective(merInfo);
	}

}
