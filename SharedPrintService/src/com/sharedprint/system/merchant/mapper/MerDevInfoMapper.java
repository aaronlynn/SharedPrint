package com.sharedprint.system.merchant.mapper;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.merchant.pojo.MerDevInfo;

public interface MerDevInfoMapper {
    int deleteByPrimaryKey(Integer merdevId);

    int insert(MerDevInfo record);

    int insertSelective(MerDevInfo record);

    MerDevInfo selectByPrimaryKey(Integer merdevId);

    int updateByPrimaryKeySelective(MerDevInfo record);

    int updateByPrimaryKey(MerDevInfo record);
    
    /**
     * 查询商户设备列表
     * 
     * @param searchMap
     * @return
     */
    List<Map<String, Object>> findMerDevInfoList(Map<String, Object> searchMap);
    
    /**
     * 查询商户设备数量
     * 
     * @param searchMap
     * @return
     */
    int findMerDevInfoCount(Map<String, Object> searchMap);
}