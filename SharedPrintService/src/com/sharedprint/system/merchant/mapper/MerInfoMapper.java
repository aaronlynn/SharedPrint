package com.sharedprint.system.merchant.mapper;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.merchant.pojo.MerInfo;

public interface MerInfoMapper {
    int deleteByPrimaryKey(Integer merinfId);

    int insert(MerInfo record);

    int insertSelective(MerInfo record);

    MerInfo selectByPrimaryKey(Integer merinfId);

    int updateByPrimaryKeySelective(MerInfo record);

    int updateByPrimaryKey(MerInfo record);
    /**
     * 查询机构列表
     * 
     * @param searchMap
     * @return
     */
    List<Map<String, Object>> findMerInfoList(Map<String, Object> searchMap);
    
    /**
     * 查询机构数量
     * 
     * @param searchMap
     * @return
     */
    int findMerInfoCount(Map<String, Object> searchMap);
}