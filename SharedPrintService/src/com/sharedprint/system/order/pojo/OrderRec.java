package com.sharedprint.system.order.pojo;

import java.util.Date;

public class OrderRec {
    private Integer orderrecId;

    private String orderCode;

    private String receivedBy;

    private String phone;

    private String address;

    private Date oprDate;

    public Integer getOrderrecId() {
        return orderrecId;
    }

    public void setOrderrecId(Integer orderrecId) {
        this.orderrecId = orderrecId;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode == null ? null : orderCode.trim();
    }

    public String getReceivedBy() {
        return receivedBy;
    }

    public void setReceivedBy(String receivedBy) {
        this.receivedBy = receivedBy == null ? null : receivedBy.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public Date getOprDate() {
        return oprDate;
    }

    public void setOprDate(Date oprDate) {
        this.oprDate = oprDate;
    }
}