package com.sharedprint.system.order.mapper;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.order.pojo.Orderinfo;

public interface OrderinfoMapper {
    int deleteByPrimaryKey(Integer orderId);

    int insert(Orderinfo record);

    int insertSelective(Orderinfo record);

    Orderinfo selectByPrimaryKey(Integer orderId);

    int updateByPrimaryKeySelective(Orderinfo record);

    int updateByPrimaryKey(Orderinfo record);
    /**
   	 * 获取订单列表
   	 * @param searchMap
   	 * @return
   	 */
   	public List<Map<String,Object>> findOrderList(Map<String,Object> searchMap);
   	
   	/**
   	 * 获取订单条数
   	 * @param searchMap
   	 * @return
   	 */
   	public int findOrderCount(Map<String,Object> searchMap);
   	
    /**
   	 * 获取订单列表
   	 * @param searchMap
   	 * @return
   	 */
   	public List<Map<String,Object>> findOrderdetailList(Map<String,Object> searchMap);
   	
   	
}