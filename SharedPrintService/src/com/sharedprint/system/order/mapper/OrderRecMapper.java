package com.sharedprint.system.order.mapper;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.order.pojo.OrderRec;

public interface OrderRecMapper {
    int deleteByPrimaryKey(Integer orderrecId);

    int insert(OrderRec record);

    int insertSelective(OrderRec record);

    OrderRec selectByPrimaryKey(Integer orderrecId);

    int updateByPrimaryKeySelective(OrderRec record);

    int updateByPrimaryKey(OrderRec record);
    
    /**
	 * 查询订单收货表
	 * 
	 * @param searcMap
	 * @return
	 */
	List<Map<String, Object>> findOrderRecList(Map<String, Object> searcMap);
}