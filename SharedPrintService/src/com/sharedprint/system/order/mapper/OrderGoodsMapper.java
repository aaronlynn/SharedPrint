package com.sharedprint.system.order.mapper;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.order.pojo.OrderGoods;

public interface OrderGoodsMapper {
    int deleteByPrimaryKey(Integer ordergoodsId);

    int insert(OrderGoods record);

    int insertSelective(OrderGoods record);

    OrderGoods selectByPrimaryKey(Integer ordergoodsId);

    int updateByPrimaryKeySelective(OrderGoods record);

    int updateByPrimaryKey(OrderGoods record);
    
    /**
     * 查询购物车列表
     * 
     * @param searchMap
     * @return
     */
    List<Map<String, Object>> findOrderGoodsList(Map<String, Object> searchMap);
    
    /**
     * 查询购物车商品数量
     * 
     * @param searchMap
     * @return
     */
    int findOrderGoodsCount(Map<String, Object> searchMap);
}