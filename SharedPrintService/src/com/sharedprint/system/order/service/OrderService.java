package com.sharedprint.system.order.service;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.order.pojo.Orderinfo;

public interface OrderService {
	/**
	 * 获取订单列表
	 * @param searchMap
	 * @return
	 */
	List<Map<String, Object>> findOrderList(Map<String, Object> searchMap);
	
	/**
	 * 获取订单列表数量
	 * @param searchMap
	 * @return
	 */
	int findOrderCount(Map<String, Object> searchMap);
	/**
	 * 获取订单详情列表
	 * @param searchMap
	 * @return
	 */
	List<Map<String, Object>> findOrderdetailList(Map<String, Object> searchMap);
	/**
	 * 添加订单
	 * @param orderinfo
	 * @return
	 */
	int insertOrder(Orderinfo orderinfo);
	
	/**
	 * 通过订单号修改订单
	 * 
	 * @param OrderCode
	 * @return
	 */
	int updateOrder(Orderinfo orderinfo);
}
