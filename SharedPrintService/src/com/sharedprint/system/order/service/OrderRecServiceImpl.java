package com.sharedprint.system.order.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sharedprint.system.order.mapper.OrderRecMapper;
import com.sharedprint.system.order.pojo.OrderRec;

@Service
public class OrderRecServiceImpl implements OrderRecService {
	
	@Autowired
	OrderRecMapper orderRecMapper;
	
	@Override
	public int addOrderRec(OrderRec orderRec) {
		// TODO Auto-generated method stub
		return orderRecMapper.insertSelective(orderRec);
	}

	@Override
	public List<Map<String, Object>> findOrderRecList(Map<String, Object> searcMap) {
		// TODO Auto-generated method stub
		return orderRecMapper.findOrderRecList(searcMap);
	}

}
