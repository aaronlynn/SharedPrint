package com.sharedprint.system.order.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sharedprint.system.order.mapper.OrderGoodsMapper;
import com.sharedprint.system.order.pojo.OrderGoods;

@Service
public class OrderGoodsServiceImpl implements OrderGoodsService {
	@Autowired
	OrderGoodsMapper orderGoodsMapper;
	
	@Override
	public List<Map<String, Object>> findOrderGoodsList(Map<String, Object> searchMap) {
		// TODO Auto-generated method stub
		return orderGoodsMapper.findOrderGoodsList(searchMap);
	}

	@Override
	public int updateNumber(OrderGoods orderGoods) {
		// TODO Auto-generated method stub
		return orderGoodsMapper.updateByPrimaryKeySelective(orderGoods);
	}

	@Override
	public int addOrderGoods(OrderGoods orderGoods) {
		// TODO Auto-generated method stub
		return orderGoodsMapper.insertSelective(orderGoods);
	}

	@Override
	public int updateOrderGoods(OrderGoods orderGoods) {
		// TODO Auto-generated method stub
		return orderGoodsMapper.updateByPrimaryKeySelective(orderGoods);
	}

	@Override
	public int deleteOrderGoods(int orderGoodsId) {
		// TODO Auto-generated method stub
		return orderGoodsMapper.deleteByPrimaryKey(orderGoodsId);
	}

	@Override
	public int findOrderGoodsCount(Map<String, Object> searchMap) {
		// TODO Auto-generated method stub
		return orderGoodsMapper.findOrderGoodsCount(searchMap);
	}

}
