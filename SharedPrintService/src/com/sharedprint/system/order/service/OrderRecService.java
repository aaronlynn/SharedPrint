package com.sharedprint.system.order.service;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.order.pojo.OrderRec;

public interface OrderRecService {
	/**
	 * 添加收货信息
	 * 
	 * @param orderRec
	 * @return
	 */
	int addOrderRec(OrderRec orderRec);
	
	/**
	 * 查询订单收货表
	 * 
	 * @param searcMap
	 * @return
	 */
	List<Map<String, Object>> findOrderRecList(Map<String, Object> searcMap);
	
}
