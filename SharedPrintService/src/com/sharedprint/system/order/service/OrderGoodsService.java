package com.sharedprint.system.order.service;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.order.pojo.OrderGoods;

public interface OrderGoodsService {
	/**
     * 查询购物车列表
     * 
     * @param searchMap
     * @return
     */
    List<Map<String, Object>> findOrderGoodsList(Map<String, Object> searchMap);
    
    /**
     * 修改商品数量
     * 
     * @param orderGoodsId
     * @return
     */
    int updateNumber(OrderGoods orderGoods);
    
    /**
     * 添加订单商品
     * 
     * @param orderGoods
     * @return
     */
    int addOrderGoods(OrderGoods orderGoods);
    
    /**
     * 修改订单商品
     * 
     * @param orderGoods
     * @return
     */
    int updateOrderGoods(OrderGoods orderGoods);
    
    /**
     * 删除订单商品信息
     * 
     * @param orderGoodsId
     * @return
     */
    int deleteOrderGoods(int orderGoodsId);
    
    /**
     * 查询购物车商品数量
     * 
     * @param searchMap
     * @return
     */
    int findOrderGoodsCount(Map<String, Object> searchMap);
}
