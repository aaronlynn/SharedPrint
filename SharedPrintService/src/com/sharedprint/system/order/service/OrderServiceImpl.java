package com.sharedprint.system.order.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sharedprint.system.order.mapper.OrderinfoMapper;
import com.sharedprint.system.order.pojo.Orderinfo;

@Service
public class OrderServiceImpl implements OrderService{
	@Autowired
	private OrderinfoMapper orderinfoMapper;

	@Override
	public List<Map<String, Object>> findOrderList(Map<String, Object> searchMap) {
		return orderinfoMapper.findOrderList(searchMap);
	}

	@Override
	public int findOrderCount(Map<String, Object> searchMap) {
		return orderinfoMapper.findOrderCount(searchMap);
	}

	@Override
	public List<Map<String, Object>> findOrderdetailList(Map<String, Object> searchMap) {
		return orderinfoMapper.findOrderdetailList(searchMap);
	}

	@Override
	public int insertOrder(Orderinfo orderinfo) {
		return orderinfoMapper.insertSelective(orderinfo);
	}

	@Override
	public int updateOrder(Orderinfo orderinfo) {
		// TODO Auto-generated method stub
		return orderinfoMapper.updateByPrimaryKeySelective(orderinfo);
	}
}
