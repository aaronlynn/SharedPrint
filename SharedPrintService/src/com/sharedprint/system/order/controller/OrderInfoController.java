package com.sharedprint.system.order.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.alipay.config.AlipayConfig;
import com.sharedprint.dics.service.DicsInfoService;
import com.sharedprint.merchant.fileCenter.pojo.PerFile;
import com.sharedprint.system.goods.pojo.GoodsInfo;
import com.sharedprint.system.goods.service.GoodsInfoService;
import com.sharedprint.system.order.pojo.OrderGoods;
import com.sharedprint.system.order.pojo.OrderRec;
import com.sharedprint.system.order.pojo.Orderinfo;
import com.sharedprint.system.order.service.OrderGoodsService;
import com.sharedprint.system.order.service.OrderRecService;
import com.sharedprint.system.order.service.OrderService;
import com.sharedprint.util.RandomCode;

/**
 *商户统计的控制器
 */
@Controller
@RequestMapping("/api/orderInfo")
public class OrderInfoController {
	
	@Autowired
	private OrderService orderService;
	@Autowired
	private DicsInfoService dicsInfoService;
	@Autowired
	private OrderRecService orderRecService;
	@Autowired
	private OrderGoodsService orderGoodsService;
	@Autowired
	private GoodsInfoService goodsInfoService;
	
	// 获取当前时间
	public static Date getNowTime() throws ParseException {
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		String date = dateFormat.format(now);
		return dateFormat.parse(date);
	}

	@RequestMapping("/queryOrderList")
	@ResponseBody
	public Map<String, Object> queryOrderList(HttpServletRequest req) {
		String Crtyear = req.getParameter("querycrtyear");
		String Crtmonth = req.getParameter("querycrtmonth");
		String OrderCode = req.getParameter("OrderCode");
		String OrderStatus = req.getParameter("OrderStatus");
		String currentPage = req.getParameter("currentPage");
		String pageSize = req.getParameter("pageSize");
		String orderByValue = req.getParameter("orderByValue");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		List<Map<String, Object>> orderList = new ArrayList<Map<String, Object>>();
		int totalCount = 0;
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {
			if (currentPage == null || "".equals(currentPage.trim())) {
				currentPage = "0";
			}
			if (pageSize == null || "".equals(pageSize.trim())) {
				pageSize = "10";
			}
			Map<String, Object> searchMap = new HashMap<String, Object>();
			int currentpages = 0;
			if ((currentPage != null || !"".equals(currentPage.trim()))) {
				currentpages = Integer.parseInt(currentPage);
				currentpages = currentpages * Integer.parseInt(pageSize);
			}
			searchMap.put("dateyear", null);
			searchMap.put("datemonth", null);
			if(Crtyear != null && !"".equals(Crtyear.trim())&& !"undefined".equals(Crtyear.trim())) {
				searchMap.put("dateyear", Crtyear);			
			}
			if(Crtmonth != null && !"".equals(Crtmonth.trim())&& !"undefined".equals(Crtmonth.trim())) {
				searchMap.put("datemonth", Crtmonth);			
			}

			searchMap.put("currentPage", currentpages);
			searchMap.put("pageSize", Integer.parseInt(pageSize));
			searchMap.put("OrderStatus", OrderStatus);
			searchMap.put("OrderCode", OrderCode);
			
			totalCount = orderService.findOrderCount(searchMap);// 总记录数
			if (totalCount > 0) {
				if (orderByValue == null || "".equals(orderByValue.trim())) {
					searchMap.put("orderByOrderIdDesc", "1");
				}
				orderList = orderService.findOrderList(searchMap);
			}
		}
		

		//获取当前年月
		Calendar cale = null;  
        cale = Calendar.getInstance();  
        int year = cale.get(Calendar.YEAR);
        int month = cale.get(Calendar.MONTH) + 1;  
        
        int[] tenyear = new int[11];
        for(int i=0; i <= 10;i++)
        {
        	tenyear[i]=year-i;
        }
      //获取数据字典
      		Map<String, Object> dicsSearchMap = new HashMap<String, Object>();
      		List<Map<String, Object>> dicsList = new ArrayList<Map<String,Object>>();
      		dicsSearchMap.put("dicsCode", "OrderStatus");
      		dicsList = dicsInfoService.findDicsList(dicsSearchMap);
      		for(int i=0;i<orderList.size();i++)
      		{
      			for (int j = 0; j < dicsList.size(); j++) {
      				if (orderList.get(i).get("Order_Status").toString().equals(dicsList.get(j).get("key_value").toString())) {
      					orderList.get(i).put("Order_Status", dicsList.get(j).get("key_name").toString());
      				}
      			}
      		}

		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("currentPage", Integer.parseInt(currentPage) + 1);
		resultMap.put("pageTotal", totalCount);
		resultMap.put("pageList", orderList);
		resultMap.put("year", year);
		resultMap.put("month", month);
		resultMap.put("tenyear", tenyear);
		resultMap.put("statusList", dicsList);
		resultMap.put("success", "1");

		return resultMap;
	}
	/**
	 * 提交订单！
	 * 
	 * @param req
	 * @return
	 * @throws ParseException 
	 */
	@RequestMapping("/addOrder")
	@ResponseBody
	public Map<String, Object> saveOrder(HttpServletRequest req) throws ParseException {
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		String orderCode = RandomCode.getRandomCode();
		String price = req.getParameter("price");
		String orderremark = req.getParameter("OrderRemark");
		String address = req.getParameter("address");//详细地址
		String post = req.getParameter("post");
		String receiver = req.getParameter("receiver");//收件人
		String mobile = req.getParameter("mobile");//收件人电话
		String orderGoodsIds = req.getParameter("orderGoodsIds");
		String[] orderGoodsId = null;
		if(orderGoodsIds !=null) {
			orderGoodsId = orderGoodsIds.split(","); //订单商品id
		}		
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		int orderSuccess = 0;
		int orderRecSuccess = 0;
		int orderGoodsSuccess = 0;
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {
			Map<String, Object> searchMap = new HashMap<String, Object>();
			searchMap.put("dicsCode", "OrderStatus");
			List<Map<String, Object>> dicsInfoiList = dicsInfoService.findDicsList(searchMap);
			String orderStatus = null;
			for (Map<String, Object> map : dicsInfoiList) {
				if (map.get("key_name").equals("未付款")) {
					orderStatus = map.get("key_value").toString();//查询到订单未付款状态码  0
				}
			}
			Orderinfo orderinfo = new Orderinfo();
			orderinfo.setOrderStatus(orderStatus);
			orderinfo.setOrderCode(orderCode);
			orderinfo.setPrice(Integer.parseInt(price));
			orderinfo.setPayerId(Integer.parseInt(userId));
			orderinfo.setOrderremark(orderremark);			
			orderSuccess = orderService.insertOrder(orderinfo);
			//购物车商品添加订单号
			OrderGoods orderGoods = new OrderGoods();
			if (orderSuccess > 0) {
				if (orderGoodsIds !=null) {
					for (String oGid : orderGoodsId) {
						orderGoods.setOrdergoodsId(Integer.parseInt(oGid));
						orderGoods.setOrderCode(orderCode);
						orderGoodsSuccess = orderGoodsService.updateOrderGoods(orderGoods);
					}
					searchMap.put("orderGoodsId", orderGoodsId[0]);
				}				
				//获取第一条订单商品信息
				Map<String, Object> orderGoodsMap = orderGoodsService.findOrderGoodsList(searchMap).get(0);
				String goodsName = orderGoodsMap.get("Goods_Name").toString();
				//
				OrderRec orderRec = new OrderRec();
				orderRec.setAddress(address);
				orderRec.setOrderCode(orderCode);
				orderRec.setPhone(mobile);
				orderRec.setReceivedBy(receiver);
				orderRec.setOprDate(getNowTime());
				orderRecSuccess = orderRecService.addOrderRec(orderRec);
				System.err.println("orderRecSuccess="+orderRecSuccess+",orderGoodsSuccess="+orderGoodsSuccess);
				if (orderRecSuccess >0 ) {
					resultMap.put("success", "1");
					resultMap.put("orderCode", orderCode);
					resultMap.put("goodsName", goodsName);
					//判断选择的订单商品数量
					if (orderGoodsIds !=null) {
						if (orderGoodsId.length>1) {
							resultMap.put("number", "2");
						}else {
							resultMap.put("number", "1");
						}
					}	
				}
			} else {
				resultMap.put("success", "0");
				resultMap.put("msg", "提交订单失败");
			}
		} else {
			resultMap.put("success", "0");
			resultMap.put("msg", "请上传相关参数");
		}
		return resultMap;
	}
	
	@RequestMapping("/imdAddOrder")
	@ResponseBody
	public Map<String, Object> imdAddOrder(HttpServletRequest req) throws ParseException {
		String userId = req.getHeader("userId");
		String goodsId = req.getParameter("goodsId");
		int goodsNumber = Integer.parseInt(req.getParameter("goodsNumber"));
		String orderCode = RandomCode.getRandomCode();
		String price = req.getParameter("price");
		String orderremark = req.getParameter("OrderRemark");
		String address = req.getParameter("address");//详细地址
		String post = req.getParameter("post");
		String receiver = req.getParameter("receiver");//收件人
		String mobile = req.getParameter("mobile");//收件人电话
		
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		int orderSuccess = 0;
		int orderRecSuccess = 0;
		int orderGoodsSuccess = 0;
		if (userId != null && !"".equals(userId.trim())) {
			Map<String, Object> searchMap = new HashMap<String, Object>();
			searchMap.put("dicsCode", "OrderStatus");
			List<Map<String, Object>> dicsInfoiList = dicsInfoService.findDicsList(searchMap);
			String orderStatus = null;
			for (Map<String, Object> map : dicsInfoiList) {
				if (map.get("key_name").equals("未付款")) {
					orderStatus = map.get("key_value").toString();//查询到订单未付款状态码  0
				}
			}
			Orderinfo orderinfo = new Orderinfo();
			orderinfo.setOrderStatus(orderStatus);
			orderinfo.setOrderCode(orderCode);
			orderinfo.setPrice(Integer.parseInt(price));
			orderinfo.setPayerId(Integer.parseInt(userId));
			orderinfo.setOrderremark(orderremark);			
			orderSuccess = orderService.insertOrder(orderinfo);
			if (orderSuccess > 0) {
				//立即购买生成订单商品
				OrderGoods imdOrderGoods = new OrderGoods();
				searchMap.put("goodsId", goodsId);					
				Map<String, Object> goodsMap = goodsInfoService.findGoodsInfoList(searchMap).get(0);
				System.err.println(goodsMap);
				int goodsPrice = Integer.parseInt(goodsMap.get("price").toString());
				imdOrderGoods.setGoodsId(Integer.parseInt(goodsId));
				imdOrderGoods.setGoodsCount(goodsNumber);
				imdOrderGoods.setGoodsName(goodsMap.get("goodsName").toString());
				imdOrderGoods.setGoodsUnitPrice(goodsPrice);
				imdOrderGoods.setPicture1(goodsMap.get("Picture1").toString());
				imdOrderGoods.setGoodsTotalPrice(goodsPrice*goodsNumber);
				imdOrderGoods.setOrderCode(orderCode);
				imdOrderGoods.setPeruserId(Integer.parseInt(userId));
				imdOrderGoods.setOprDate(getNowTime());
				orderGoodsSuccess = orderGoodsService.addOrderGoods(imdOrderGoods);
				if (orderGoodsSuccess >0) {					
					resultMap.put("goodsName", goodsMap.get("goodsName").toString());
				}
				//生成订单收货信息
				OrderRec orderRec = new OrderRec();
				orderRec.setAddress(address);
				orderRec.setOrderCode(orderCode);
				orderRec.setPhone(mobile);
				orderRec.setReceivedBy(receiver);
				orderRec.setOprDate(getNowTime());
				orderRecSuccess = orderRecService.addOrderRec(orderRec);
				if (orderRecSuccess >0) {
					resultMap.put("orderCode", orderCode);
					resultMap.put("success", "1");
					resultMap.put("number", "1");
				}else {
					resultMap.put("success", "0");
				}				
			}else {
				resultMap.put("success", "0");
			}
		}else {
			resultMap.put("success", "0");
		}
		return resultMap;
	}
	// 客户详情
	@RequestMapping("/detail")
	@ResponseBody
	public Map<String, Object> detailFront(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		String OrderCode = req.getParameter("OrderCode");

		Map<String, Object> orderMap = new HashMap<String, Object>();
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<Map<String, Object>> orderList = new ArrayList<Map<String, Object>>();
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {
			Map<String, Object> searchMap = new HashMap<String, Object>();
			searchMap.put("OrderCode", OrderCode);
			orderList = orderService.findOrderdetailList(searchMap);
			orderMap = orderList.get(0);
		}
		resultMap.put("orderMap", orderMap);
		resultMap.put("success", "1");
		return resultMap;

	}
	
	// 支付宝接口
	@SuppressWarnings("unused")
	@RequestMapping("/alipay.action")
	private void aliyPay(HttpServletRequest request, HttpServletResponse resp) throws AlipayApiException, IOException {
		String flag = request.getParameter("flag");
		// ↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
		// 获得初始化的AlipayClient
		AlipayClient alipayClient = new DefaultAlipayClient(AlipayConfig.gatewayUrl, AlipayConfig.app_id,
				AlipayConfig.merchant_private_key, "json", AlipayConfig.charset, AlipayConfig.alipay_public_key,
				AlipayConfig.sign_type);
		// 设置请求参数
		AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
		if (flag !=null) {
			alipayRequest.setReturnUrl(AlipayConfig.order_url);
		}else {
			alipayRequest.setReturnUrl(AlipayConfig.return_url);
		}
		alipayRequest.setNotifyUrl(AlipayConfig.notify_url);
		// 商户订单号，商户网站订单系统中唯一订单号，必填
		String out_trade_no = new String(request.getParameter("WIDout_trade_no").getBytes("ISO-8859-1"), "UTF-8");
		// 付款金额，必填
		String total_amount = new String(request.getParameter("WIDtotal_amount").getBytes("ISO-8859-1"), "UTF-8");
		// 订单名称，必填
		String subject = new String(request.getParameter("WIDsubject").getBytes("ISO-8859-1"), "UTF-8");
		// 商品描述，可空
		String body = new String(request.getParameter("WIDbody").getBytes("ISO-8859-1"), "UTF-8");

		alipayRequest.setBizContent("{\"out_trade_no\":\"" + out_trade_no + "\"," + "\"total_amount\":\"" + total_amount
				+ "\"," + "\"subject\":\"" + subject + "\"," + "\"body\":\"" + body + "\","
				+ "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");

		// 若想给BizContent增加其他可选请求参数，以增加自定义超时时间参数timeout_express来举例说明
		// alipayRequest.setBizContent("{\"out_trade_no\":\""+ out_trade_no +"\","
		// + "\"total_amount\":\""+ total_amount +"\","
		// + "\"subject\":\""+ subject +"\","
		// + "\"body\":\""+ body +"\","
		// + "\"timeout_express\":\"10m\","
		// + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");
		// 请求参数可查阅【电脑网站支付的API文档-alipay.trade.page.pay-请求参数】章节

		// 请求
		String result = alipayClient.pageExecute(alipayRequest).getBody();

		// 输出
		resp.setContentType("text/html;charset=utf-8");
		resp.getWriter().append(result);
	}
	
	@RequestMapping("/queryOrderRec")
	@ResponseBody
	private Map<String, Object> queryOrderRec(HttpServletRequest request) {
		String orderCode = request.getParameter("orderCode");
		Map<String, Object> searchMap = new HashMap<String, Object>();
		Map<String, Object> resultMap = new HashMap<String, Object>();
		int orderSuccess = 0;//修改定单状态标记
		if (orderCode !=null) {
			searchMap.put("orderCode", orderCode);
			searchMap.put("dicsCode", "OrderStatus");
			List<Map<String, Object>> dicsInfoiList = dicsInfoService.findDicsList(searchMap);
			String orderStatus = null;
			for (Map<String, Object> map : dicsInfoiList) {
				if (map.get("key_name").equals("已付款")) {
					orderStatus = map.get("key_value").toString();//查询到订单已付款状态码  1
				}
			}
			//修改订单状态为已付款
			Orderinfo orderinfo = new Orderinfo();
			orderinfo.setOrderCode(orderCode);
			orderinfo.setOrderStatus(orderStatus);
			orderSuccess = orderService.updateOrder(orderinfo);
			
			Map<String, Object> orderRecMap = orderRecService.findOrderRecList(searchMap).get(0);
			Map<String, Object> orderInfoMap = orderService.findOrderList(searchMap).get(0);
			if (orderRecMap !=null && orderSuccess > 0) {
				resultMap.put("success", "1");
				resultMap.put("orderRecMap", orderRecMap);
				resultMap.put("orderInfoMap", orderInfoMap);
			}else {
				resultMap.put("success", "0");
			}
		}else {
			resultMap.put("success", "0");
		}
		return resultMap;
	}
	
	//查询已购买的宝贝
	@RequestMapping("/queryBoughtGoods")
	@ResponseBody
	private Map<String, Object> queryBoughtGoods(HttpServletRequest request) {
		String userId = request.getParameter("userId");
		Map<String, Object> searchMap = new HashMap<String, Object>();
		Map<String, Object> resultMap = new HashMap<String, Object>();		
		if (userId !=null) {
			List<Map<String, Object>> orderGoodsList = new ArrayList<Map<String,Object>>();
			//查询订单号
			searchMap.put("userId", userId);
			List<Map<String, Object>> orderList = orderService.findOrderList(searchMap);
			searchMap.put("dicsCode", "OrderStatus");
			List<Map<String, Object>> dicsInfoiList = dicsInfoService.findDicsList(searchMap);
			String orderStatus = null;
			for (Map<String, Object> map : dicsInfoiList) {
				if (map.get("key_name").equals("已付款")) {
					orderStatus = map.get("key_value").toString();//查询到订单已付款状态码  1
				}
			}
			//查询已付款的订单号
			for (Map<String, Object> map : orderList) {      
				if (map.get("Order_Status").equals(orderStatus)) { //判断订单已付款
					searchMap.put("orderCode", map.get("Order_Code"));
					System.err.println("订单"+map.get("Order_Code"));
					List<Map<String, Object>> orderGoodsList2 = orderGoodsService.findOrderGoodsList(searchMap);
					for (Map<String, Object> map2 : orderGoodsList2) {
						orderGoodsList.add(map2);//添加订单商品信息到列表
						System.err.println("订单商品"+map2.get("Ordergoods_ID"));
					}
				}				
			}
			if (orderList !=null) {
				resultMap.put("success", "1");
				resultMap.put("orderGoodsList", orderGoodsList);
			}else {
				resultMap.put("success", "0");
			}
		}else {
			resultMap.put("success", "0");
		}
		return resultMap;
	}
	/**
	 * 购物车商品订单付款
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/payOrder")
	@ResponseBody
	private Map<String, Object> payOrder(HttpServletRequest request){
		String OrderCode = request.getParameter("OrderCode");
		Map<String, Object> searchMap = new HashMap<String, Object>();
		Map<String, Object> resultMap = new HashMap<String, Object>();
		if (OrderCode !=null) {
			searchMap.put("orderCode", OrderCode);
			searchMap.put("dicsCode", "OrderStatus");
			List<Map<String, Object>> dicsInfoiList = dicsInfoService.findDicsList(searchMap);
			String orderStatus = null;
			for (Map<String, Object> map : dicsInfoiList) {
				if (map.get("key_name").equals("已付款")) {
					orderStatus = map.get("key_value").toString();//查询到订单已付款状态码  1
				}
			}
			Map<String, Object> orderMap = orderService.findOrderList(searchMap).get(0);
			//获取订单内的订单商品			
			List<Map<String, Object>> orderGoodsList = orderGoodsService.findOrderGoodsList(searchMap);
			String goodsName = orderGoodsList.get(0).get("Goods_Name").toString();			
			if (orderMap.get("Order_Status").equals(orderStatus)) {
				resultMap.put("success", "0");
				resultMap.put("msg", "该订单已付款，请勿重复付款！");
			}else {
				resultMap.put("success", "1");
				resultMap.put("totalPrice", orderMap.get("price"));
				resultMap.put("orderCode", OrderCode);
				resultMap.put("goodsName", goodsName);
				if (orderGoodsList.size()>1) {
					resultMap.put("number", "2");
				}else {
					resultMap.put("number", "1");
				}
			}
		}else {
			resultMap.put("success", "0");
			resultMap.put("msg", "系统繁忙，请稍后再试！");
		}
		return resultMap;
	}
	
	/**
	 * 文件订单付款
	 * 
	 * @param request
	 * @return
	 * @throws ParseException 
	 */
	@RequestMapping("/payFileOrder")
	@ResponseBody
	private Map<String, Object> payFileOrder(HttpServletRequest request) throws ParseException{
		String orderCode = RandomCode.getRandomCode();//自动生成订单号
		String price = request.getParameter("price");
		String OrderRemark = request.getParameter("OrderRemark");
		String userId = request.getHeader("userId");
		String orderName = request.getParameter("printFileName");
		Map<String, Object> searchMap = new HashMap<String, Object>();
		Map<String, Object> resultMap = new HashMap<String, Object>();
		int success = 0;
		int orderGoodsSuccess = 0;
		if (orderCode !=null) {
			Orderinfo orderinfo = new Orderinfo();
			orderinfo.setOrderCode(orderCode);			
			orderinfo.setPrice(Integer.parseInt(price));
			orderinfo.setPayerId(Integer.parseInt(userId));
			searchMap.put("dicsCode", "OrderStatus");
			List<Map<String, Object>> dicsInfoiList = dicsInfoService.findDicsList(searchMap);
			String orderStatus = null;
			for (Map<String, Object> map : dicsInfoiList) {
				if (map.get("key_name").equals("未付款")) {
					orderStatus = map.get("key_value").toString();//查询到订单未付款状态码  1
				}
			}
			orderinfo.setOrderremark(OrderRemark);
			orderinfo.setOrderStatus(orderStatus);
			System.err.println(orderinfo.getCrtdate());
			success = orderService.insertOrder(orderinfo);
			
			//生成订单商品信息
			OrderGoods orderGoods = new OrderGoods();
			orderGoods.setGoodsName(orderName);
			orderGoods.setGoodsTotalPrice(Integer.parseInt(price));
			orderGoods.setPeruserId(Integer.parseInt(userId));
			orderGoods.setOrderCode(orderCode);
			orderGoodsSuccess = orderGoodsService.addOrderGoods(orderGoods);
			//获取订单内的订单商品
			if (success >0 && orderGoodsSuccess >0) {
				resultMap.put("success", "1");
				resultMap.put("totalPrice", price);
				resultMap.put("orderCode", orderCode);
				resultMap.put("goodsName", orderName);
				resultMap.put("orderRemark", OrderRemark);
				resultMap.put("number", "1");
			}			
		}else {
			resultMap.put("success", "0");
			resultMap.put("msg", "系统繁忙，请稍后再试！");
		}
		return resultMap;
	}
	
	/**
	 * 文件订单付款成功
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/payedFileSuccess")
	@ResponseBody
	private Map<String, Object> payedFileSuccess(HttpServletRequest request) {
		String orderCode = request.getParameter("orderCode");
		Map<String, Object> searchMap = new HashMap<String, Object>();
		Map<String, Object> resultMap = new HashMap<String, Object>();
		int orderSuccess = 0;//修改定单状态标记
		if (orderCode !=null) {
			searchMap.put("orderCode", orderCode);
			searchMap.put("dicsCode", "OrderStatus");
			List<Map<String, Object>> dicsInfoiList = dicsInfoService.findDicsList(searchMap);
			String orderStatus = null;
			for (Map<String, Object> map : dicsInfoiList) {
				if (map.get("key_name").equals("已付款")) {
					orderStatus = map.get("key_value").toString();//查询到订单已付款状态码  1
				}
			}
			//修改订单状态为已付款
			Orderinfo orderinfo = new Orderinfo();
			orderinfo.setOrderCode(orderCode);
			orderinfo.setOrderStatus(orderStatus);
			orderSuccess = orderService.updateOrder(orderinfo);			
			Map<String, Object> orderInfoMap = orderService.findOrderList(searchMap).get(0);
			if (orderSuccess > 0) {
				resultMap.put("success", "1");
				resultMap.put("orderInfoMap", orderInfoMap);
			}else {
				resultMap.put("success", "0");
			}
		}else {
			resultMap.put("success", "0");
		}
		return resultMap;
	}
}
