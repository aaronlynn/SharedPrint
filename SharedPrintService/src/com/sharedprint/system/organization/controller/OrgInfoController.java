package com.sharedprint.system.organization.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sharedprint.system.organization.pojo.OrgInfo;
import com.sharedprint.system.organization.service.OrgInfoService;

@Controller
@RequestMapping("/api/orgInfo")
public class OrgInfoController {
	@Autowired
	private OrgInfoService orgInfoService;
	
	/**
	 * 查询机构All列表
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/getAllOrgList")
	@ResponseBody
	private Map<String, Object> getAllOrgList(HttpServletRequest req){
		
		List<Map<String, Object>> orgList = new ArrayList<Map<String,Object>>();
		Map<String, Object> searchMap = new HashMap<String, Object>();
		orgList  = orgInfoService.getAllOrgList(searchMap);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("orgList", orgList);
		resultMap.put("success", 1);
		return resultMap;
		
	}
	
	
	/**
	 * 查询机构分页列表
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/queryPageList")
	@ResponseBody
	private Map<String, Object> queryPageList(HttpServletRequest req){
		String OrgNameLike = req.getParameter("OrgNameLike");
		String OrgCode = req.getParameter("OrgCode");
		String pageSize = req.getParameter("pageSize");
		String currentPage = req.getParameter("currentPage");
		String menuId = req.getParameter("id");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		System.out.println("menuId=" + menuId + ",token=" + token + ",userId="+ userId);
		Map<String, Object> searchMap = new HashMap<String, Object>();
		searchMap.put("OrgName", OrgNameLike);
		searchMap.put("OrgCode", OrgCode);
		searchMap.put("pageSize", Integer.parseInt(pageSize));
		int currentRecord = Integer.parseInt(currentPage)*Integer.parseInt(pageSize);
		searchMap.put("currentRecord", currentRecord);
		System.out.println("pageSize="+pageSize+",currentRecord="+currentRecord);
		//searchMap.put("orderBySortNoAsc", "1");
		List<Map<String, Object>> orgList = orgInfoService.findOrgInfoList(searchMap);
		int totalCount = orgInfoService.findOrgInfoCount(searchMap); 
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		if (orgList.size() > 0) {
			List<Map<String, Object>> resultOrgList = new ArrayList<Map<String, Object>>();
			for (int i = 0; i < orgList.size(); i++) {
				// 查询出一级目录
				Map<String, Object> tempParentMap = orgList.get(i);
				if (tempParentMap.get("Org_Parent_Code") != null
						&& tempParentMap.get("Org_Parent_Code")  == "0") {
					Map<String, Object> resultParentMap = new HashMap<String, Object>();
					resultParentMap.put("Org_Name", tempParentMap.get("Org_Name"));

					// 查询子集
					List<Map<String, Object>> resultChildrenList = new ArrayList<Map<String, Object>>();
					for (int j = 0; j < orgList.size(); j++) {
						Map<String, Object> tempChildrenMap = orgList.get(j);	
						if (tempChildrenMap.get("Org_Parent_Code").equals(tempParentMap
								.get("Org_Code")) ) {
							Map<String, Object> resultChildrenMap = new HashMap<String, Object>();
							resultChildrenMap.put("parentName",tempChildrenMap.get("parentName"));
							resultChildrenList.add(resultChildrenMap);
						}
					}
					resultParentMap.put("children", resultChildrenList);
					resultOrgList.add(resultParentMap);
				}
			}
			if (resultOrgList.size() > 0) {				
				resultMap.put("orgInfoList", resultOrgList);
				resultMap.put("success", "1");
			}
		}
		if(orgList.size() > 0){
			resultMap.put("success", "1");			
			resultMap.put("orgListDataGrid", orgList);
			resultMap.put("pageTotal", totalCount);
		}
		System.out.println("orgList.size()="+orgList.size());
		System.out.println("resultMap11="+resultMap);
		return resultMap;
	}
	@RequestMapping("/initAdd")
	@ResponseBody
	private Map<String, Object> initAdd(HttpServletRequest req) {
		Map<String, Object> searchMap = new HashMap<String, Object>();
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<Map<String, Object>> orgInfoList = orgInfoService.findOrgInfoList(searchMap);
		if (orgInfoList != null) {
			resultMap.put("orgParentList", orgInfoList);
			resultMap.put("success", "1");
		}
		return resultMap;
	}
	/**
	 * 判断机构编码是否重复
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/validateOrgCode")
	@ResponseBody
	private Boolean validOrgCode (HttpServletRequest req){
		// TODO Auto-generated method stub
		String orgCode = req.getParameter("orgCode");
		String excludeOrgId = req.getParameter("excludeOrgId");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		System.out.println("token=" + token + ",userId=" + userId + ",orgCode=" + orgCode +",exclude="+excludeOrgId);
		boolean result = false;
		if (orgCode != null && !"".equals(orgCode.trim())) {
			Map<String, Object> searchMap = new HashMap<String, Object>();
			searchMap.put("orgCode", orgCode);
			if (excludeOrgId !=null && !"".equals(excludeOrgId.trim())) {
				searchMap.put("excludeOrgId", excludeOrgId);
			}
			int count = orgInfoService.findOrgInfoCount(searchMap);
			if (count == 0) {
				result = true;
			}
		}
		return result;
	}
	
	/**
	 * 判断机构名称是否重复
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/validateOrgName")
	@ResponseBody
	private Boolean validOrgName (HttpServletRequest req){
		String orgName = req.getParameter("orgName");
		String excludeOrgId = req.getParameter("excludeOrgId");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		System.out.println("token=" + token + ",userId=" + userId + ",orgName=" + orgName +",exclude="+excludeOrgId);
		boolean result = false;
		if (orgName != null && !"".equals(orgName.trim())) {		
			Map<String, Object> searchMap = new HashMap<String, Object>();
			searchMap.put("orgName", orgName);
			if (excludeOrgId !=null && !"".equals(excludeOrgId.trim())) {
				searchMap.put("excludeOrgId", excludeOrgId);
			}
			int count = orgInfoService.findOrgInfoCount(searchMap);
			if (count == 0) {
				result = true;
			}
		}
		return result;	
	}
	
	@RequestMapping("/add")
	@ResponseBody
	private Map<String, Object> add(HttpServletRequest req) {
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		System.out.println("token=" + token + ",userId=" + userId);
		String orgCode = req.getParameter("addOrgCode");
		String orgName = req.getParameter("addOrgName");
		String orgAreaBelong = req.getParameter("addOrgAreaBelong");
		String orgConnPerson = req.getParameter("addOrgConnPerson");
		String orgConnTelephone= req.getParameter("addOrgConnTelephone");
		String orgAddress = req.getParameter("addOrgAddress");
		String orgParentCode = req.getParameter("addOrgParentCode");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		if (token != null && !"".equals(token.trim())){
			OrgInfo orgInfo = new OrgInfo();
			orgInfo.setOrgCode(orgCode);
			orgInfo.setOrgName(orgName);
			orgInfo.setOrgParentCode(orgParentCode);
			orgInfo.setContactor(orgConnPerson);
			orgInfo.setAreaCode(orgAreaBelong);
			orgInfo.setMobile(orgConnTelephone);
			orgInfo.setAddress(orgAddress);
			orgInfo.setOprId(Integer.parseInt(userId));
			orgInfo.setStatus("1");
			int success = orgInfoService.insertOrgInfo(orgInfo);
			if(success > 0){
				resultMap.put("success", "1");
			}else{
				resultMap.put("msg", "保存失败111");
			}	
		}else{
			resultMap.put("msg", "保存失败2");
		}	
		System.out.println("resultMap="+resultMap);
		return resultMap;
	}
	
	/**
	 * 修改机构信息预加载
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/initUpdate")
	@ResponseBody
	private Map<String, Object> initUpdate(HttpServletRequest req) {
		Map<String, Object> searchMap = new HashMap<String, Object>();
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<Map<String, Object>> orgList = orgInfoService.findOrgInfoList(searchMap);
		if (orgList.size() >0) {
			resultMap.put("orgList",orgList);
		}else {
			System.out.println("列表查询失败");
		}
		String orgId = req.getParameter("orgId");
		searchMap.put("orgId", Integer.parseInt(orgId));
		OrgInfo orgInfo = orgInfoService.findOrgInfo(Integer.parseInt(orgId));
		System.out.println("orgInfo="+orgInfo);
		resultMap.put("orgInfo",orgInfo);
		resultMap.put("success", "1");
		return resultMap;
	}
	
	@RequestMapping("/update")
	@ResponseBody
	private Map<String, Object> updateOrgInfo(HttpServletRequest req) {
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		System.out.println("token=" + token + ",userId=" + userId);
		String orgId = req.getParameter("updateOrgId");
		String orgCode = req.getParameter("updateOrgCode");
		String orgName = req.getParameter("updateOrgName");
		String orgAreaBelong = req.getParameter("updateOrgAreaBelong");
		String orgConnPerson = req.getParameter("updateOrgConnPerson");
		String orgConnTelephone= req.getParameter("updateOrgConnTelephone");
		String orgAddress = req.getParameter("updateOrgAddress");
		String orgParentCode = req.getParameter("updateOrgParentCode");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		if (token != null && !"".equals(token.trim())){
			OrgInfo orgInfo = new OrgInfo();
			orgInfo.setOrgId(Integer.parseInt(orgId));
			orgInfo.setOrgCode(orgCode);
			orgInfo.setOrgName(orgName);
			orgInfo.setOrgParentCode(orgParentCode);
			orgInfo.setContactor(orgConnPerson);
			orgInfo.setAreaCode(orgAreaBelong);
			orgInfo.setMobile(orgConnTelephone);
			orgInfo.setAddress(orgAddress);
			orgInfo.setOprId(Integer.parseInt(userId));
			orgInfo.setStatus("1");
			int success = orgInfoService.updateOrgInfo(orgInfo);
			System.out.println("result="+success);
			if(success > 0){
				resultMap.put("success", "1");
			}else{
				resultMap.put("msg", "修改失败1");
			}	
		}else{
			resultMap.put("msg", "修改失败2");
		}	
		System.out.println("resultMap="+resultMap);
		return resultMap;
	}
	
	/**
	 * 删除机构
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/delete")
	@ResponseBody
	private Map<String, Object> deleteOrgInfo(HttpServletRequest req) {
		String orgId= req.getParameter("orgId");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		System.out.println("orgCode="+orgId);
		if(orgId !=null&&""!=orgId.trim()){
			int result = orgInfoService.deleteOrgInfo(Integer.parseInt(orgId));
			if(result >0){
				resultMap.put("success", "1");
			}else {
				resultMap.put("success", "0");
			}
		}else {
			resultMap.put("success", "0");
		}
		return resultMap;
	}
	
	/**
	 * 查询机构详情
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/detail")
	@ResponseBody
	private Map<String, Object> detailOrgInfo(HttpServletRequest req) {
		Map<String, Object> searchMap = new HashMap<String, Object>();
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<Map<String, Object>> orgList = orgInfoService.findOrgInfoList(searchMap);
		if (orgList.size() >0) {
			resultMap.put("orgList",orgList);
		}else {
			System.out.println("列表查询失败");
		}
		String orgId = req.getParameter("orgId");
		searchMap.put("orgId", Integer.parseInt(orgId));
		OrgInfo orgInfo = orgInfoService.findOrgInfo(Integer.parseInt(orgId));
		System.out.println("orgInfo="+orgInfo);
		resultMap.put("orgInfo",orgInfo);
		resultMap.put("success", "1");
		return resultMap;
	}
	
}
