package com.sharedprint.system.organization.mapper;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.organization.pojo.OrgInfo;

public interface OrgInfoMapper {
    int deleteByPrimaryKey(Integer orgId);

    int insert(OrgInfo record);

    int insertSelective(OrgInfo record);

    OrgInfo selectByPrimaryKey(Integer orgId);

    int updateByPrimaryKeySelective(OrgInfo record);

    int updateByPrimaryKey(OrgInfo record);
    
    /**
     * 查询机构列表
     * 
     * @param searchMap
     * @return
     */
    List<Map<String, Object>> findOrgInfoList(Map<String, Object> searchMap);
    
    /**
     * 查询机构数量
     * 
     * @param searchMap
     * @return
     */
    int findOrgInfoCount(Map<String, Object> searchMap);
    /**
     * 查询机构Code
     * 
     * @param searchMap
     * @return
     */
    List<Map<String, Object>> findOrgCodeList(Map<String, Object> searchMap);
    /**
     * 查询机构Code数量
     * 
     * @param searchMap
     * @return
     */
    int findOrgCodeCount(Map<String, Object> searchMap);
    /**
     * 查询机构所有列表
     */
    List<Map<String, Object>> getAllOrgList(Map<String, Object> searchMap);
}