package com.sharedprint.system.organization.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sharedprint.system.organization.mapper.OrgInfoMapper;
import com.sharedprint.system.organization.pojo.OrgInfo;

@Service
public class OrgInfoServiceImpl implements OrgInfoService {
	@Autowired
	private OrgInfoMapper orgInfoMapper;
	
	@Override
	public List<Map<String, Object>> findOrgInfoList(Map<String, Object> searchMap) {
		// TODO Auto-generated method stub
		return orgInfoMapper.findOrgInfoList(searchMap);
	}

	@Override
	public int findOrgInfoCount(Map<String, Object> searchMap) {
		// TODO Auto-generated method stub
		return orgInfoMapper.findOrgInfoCount(searchMap);
	}

	@Override
	public int insertOrgInfo(OrgInfo orgInfo) {
		// TODO Auto-generated method stub
		return orgInfoMapper.insertSelective(orgInfo);
	}

	@Override
	public int updateOrgInfo(OrgInfo orgInfo) {
		// TODO Auto-generated method stub
		return orgInfoMapper.updateByPrimaryKeySelective(orgInfo);
	}

	@Override
	public OrgInfo findOrgInfo(int orgId) {
		// TODO Auto-generated method stub
		return orgInfoMapper.selectByPrimaryKey(orgId);
	}

	@Override
	public int deleteOrgInfo(int orgId) {
		// TODO Auto-generated method stub
		return orgInfoMapper.deleteByPrimaryKey(orgId);
	}

	@Override
	public List<Map<String, Object>> findOrgCodeList(Map<String, Object> searchMap) {
		
		return orgInfoMapper.findOrgCodeList(searchMap);
	}

	@Override
	public int findOrgCodeListCount(Map<String, Object> searchMap) {
		
		return orgInfoMapper.findOrgCodeCount(searchMap);
	}

	@Override
	public List<Map<String, Object>> getAllOrgList(Map<String, Object> searchMap) {
		
		return orgInfoMapper.getAllOrgList(searchMap);
	}

}
