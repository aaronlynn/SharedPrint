package com.sharedprint.system.organization.service;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.organization.pojo.OrgInfo;

public interface OrgInfoService {
	
	/**
	 * 查询机构列表
	 * 
	 * @param searchMap
	 * @return
	 */
	public List<Map<String, Object>> findOrgInfoList(Map<String, Object> searchMap);
	
	/**
	 * 查询机构数量
	 * 
	 * @param searchMap
	 * @return
	 */
	public int findOrgInfoCount(Map<String, Object> searchMap);
	
	/**
	 *添加机构
	 *  
	 * @param orgInfo
	 * @return
	 */
	public int insertOrgInfo(OrgInfo orgInfo);
	
	/**
	 * 修改机构
	 * 
	 * @param orgInfo
	 * @return
	 */
	public int updateOrgInfo(OrgInfo orgInfo); 
	
	/**
	 * 查询机构详细信息
	 * 
	 * @param orgId
	 * @return
	 */
	public OrgInfo findOrgInfo(int orgId); 
	
	/**
	 * 删除机构
	 * 
	 * @param orgId
	 * @return
	 */
	public int deleteOrgInfo(int orgId);
	
	/**
	 * 代理商通过机构查询机构名称
	 */
	public List<Map<String, Object>> findOrgCodeList(Map<String, Object> searchMap);
	/**
	 * 代理商通过机构查询机构名称是否存在
	 */
	public int findOrgCodeListCount(Map<String, Object> searchMap);
	/**
	 * 代理商通过获取机构所有列表
	 */
	public List<Map<String, Object>> getAllOrgList(Map<String, Object> s);
}

