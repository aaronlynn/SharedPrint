package com.sharedprint.system.brand.pojo;

import java.util.Date;

public class BrandInfo {
	private Integer brandId;

    private String brandName;

    private Integer brandParentId;

    private String remark;

    private Integer sortNo;

    private Integer oprId;

    private Date oprDate;
    
    private Integer merInfId;

    public Integer getMerInfId() {
		return merInfId;
	}

	public void setMerInfId(Integer merInfId) {
		this.merInfId = merInfId;
	}

	public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName == null ? null : brandName.trim();
    }

    public Integer getBrandParentId() {
        return brandParentId;
    }

    public void setBrandParentId(Integer brandParentId) {
        this.brandParentId = brandParentId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public Integer getOprId() {
        return oprId;
    }

    public void setOprId(Integer oprId) {
        this.oprId = oprId;
    }

    public Date getOprDate() {
        return oprDate;
    }

    public void setOprDate(Date oprDate) {
        this.oprDate = oprDate;
    }
}