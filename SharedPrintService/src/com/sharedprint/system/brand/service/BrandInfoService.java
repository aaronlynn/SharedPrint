package com.sharedprint.system.brand.service;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.brand.pojo.BrandInfo;
import com.sharedprint.system.category.pojo.CategoryInfo;

public interface BrandInfoService {
	/**
	 * 获取地区列表数量
	 * 
	 * @param searchMap
	 * @return
	 *
	 **/
	int findBrandInfoCount(Map<String, Object> searchMap);

	/**
	 * 获取商品品牌列表
	 * 
	 * @param searchMap
	 * @return
	 */
	List<Map<String, Object>> findBrandInfoList(Map<String, Object> searchMap);

	/**
	 * 删除商品品牌
	 * 
	 * @param menuInfo
	 * @return
	 */
	int delBrandInfo(int categoryId);

	/**
	 * 保存新增商品品牌
	 * 
	 * @param brandInfo
	 * @return
	 */
	int saveInfo(BrandInfo brandInfo);

	/**
	 * 修改信息
	 * 
	 * @param categoryInfo
	 * @return
	 */
	int updateInfo(BrandInfo brandInfo);

}
