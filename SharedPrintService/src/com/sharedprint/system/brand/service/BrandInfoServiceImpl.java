package com.sharedprint.system.brand.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sharedprint.system.brand.mapper.BrandInfoMapper;
import com.sharedprint.system.brand.pojo.BrandInfo;
import com.sharedprint.system.category.mapper.CategoryInfoMapper;
import com.sharedprint.system.category.pojo.CategoryInfo;

@Service
public class BrandInfoServiceImpl implements BrandInfoService{
	@Autowired
	private BrandInfoMapper brandInfoMapper;
	@Override
	public int findBrandInfoCount(Map<String, Object> searchMap) {
		return brandInfoMapper.findBrandCount(searchMap);
	}
	@Override
	public List<Map<String, Object>> findBrandInfoList(Map<String, Object> searchMap) {
		return brandInfoMapper.findBrandList(searchMap);
	}
	@Override
	public int delBrandInfo(int categoryId) {
		return brandInfoMapper.deleteByPrimaryKey(categoryId);
	}
	@Override
	public int saveInfo(BrandInfo brandInfo) {
		return brandInfoMapper.saveInfo(brandInfo);
	}
	@Override
	public int updateInfo(BrandInfo brandInfo) {
		return brandInfoMapper.updateInfo(brandInfo);
	}  
}
