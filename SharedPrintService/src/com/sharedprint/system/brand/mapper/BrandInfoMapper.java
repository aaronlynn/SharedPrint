package com.sharedprint.system.brand.mapper;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.brand.pojo.BrandInfo;


public interface BrandInfoMapper {

	/**
     * 删除商品类别
     * @param categoryId
     * @return
     */
    int deleteByPrimaryKey(Integer categoryId);

    int insert(BrandInfo record);

    int insertSelective(BrandInfo record);
   
    BrandInfo selectByPrimaryKey(Integer categoryId);
 
    int updateByPrimaryKeySelective(BrandInfo record);

    int updateByPrimaryKey(BrandInfo record);
    /**
   	 * 获取品牌列表
   	 * @param searchMap
   	 * @return
   	 */
   	public List<Map<String,Object>> findBrandList(Map<String,Object> searchMap);
   	
   	
   	
   	/**
   	 * 获取品牌条数
   	 * @param searchMap
   	 * @return
   	 */
   	public int findBrandCount(Map<String,Object> searchMap);
    /**
     * 添加保存
     * @param brandInfo
     * @return
     */
	int saveInfo(BrandInfo brandInfo);
   /**
    * 修改信息
    * @param brandInfo
    * @return
    */
	int updateInfo(BrandInfo brandInfo);
}