package com.sharedprint.system.operationlog.pojo;

import java.util.Date;

public class OperationLog {
    private Integer operatationLogId;

    private Integer userId;

    private String operatelogtype;

    private String requestUrl;

    private String description;

    private String ip;

    private Integer oprId;

    private String oprDate;

    public Integer getOperatationLogId() {
        return operatationLogId;
    }

    public void setOperatationLogId(Integer operatationLogId) {
        this.operatationLogId = operatationLogId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getOperatelogtype() {
        return operatelogtype;
    }

    public void setOperatelogtype(String operatelogtype) {
        this.operatelogtype = operatelogtype == null ? null : operatelogtype.trim();
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl == null ? null : requestUrl.trim();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip == null ? null : ip.trim();
    }

    public Integer getOprId() {
        return oprId;
    }

    public void setOprId(Integer oprId) {
        this.oprId = oprId;
    }

    public String getOprDate() {
        return oprDate;
    }

    public void setOprDate(String oprDate) {
        this.oprDate = oprDate;
    }
}