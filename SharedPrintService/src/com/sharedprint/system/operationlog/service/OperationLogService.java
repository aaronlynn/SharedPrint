package com.sharedprint.system.operationlog.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sharedprint.system.operationlog.mapper.OperationLogMapper;
import com.sharedprint.system.operationlog.pojo.OperationLog;

@Service
public class OperationLogService {
	@Autowired
	private OperationLogMapper operationLogMapper;
    public int saveInfo(OperationLog operationLog)
    {
    	return operationLogMapper.insertSelective(operationLog);
    }
}
