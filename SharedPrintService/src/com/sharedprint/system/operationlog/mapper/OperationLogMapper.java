package com.sharedprint.system.operationlog.mapper;

import com.sharedprint.system.operationlog.pojo.OperationLog;

public interface OperationLogMapper {
    int deleteByPrimaryKey(Integer operatationLogId);

    int insert(OperationLog record);

    int insertSelective(OperationLog record);

    OperationLog selectByPrimaryKey(Integer operatationLogId);

    int updateByPrimaryKeySelective(OperationLog record);

    int updateByPrimaryKey(OperationLog record);
}