package com.sharedprint.system.agent.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sharedprint.system.agent.mapper.AgentInfoMapper;
import com.sharedprint.system.agent.pojo.AgentInfo;

@Service
public class AgentInfoServiceImpl implements AgentInfoService {

	@Autowired
	private AgentInfoMapper agentInfoMapper;
	@Override
	public List<Map<String, Object>> findAgentInfoList(Map<String, Object> searchMap) {
		
		return agentInfoMapper.findAgentList(searchMap);
	}

	@Override
	public int findAgentInfoCount(Map<String, Object> searchMap) {
		
		return agentInfoMapper.findAgentCount(searchMap);
	}

	@Override
	public int insertAgentInfo(AgentInfo agentInfo) {
		
		return agentInfoMapper.insertSelective(agentInfo);
	}

	@Override
	public int updateAgentInfo(AgentInfo agentInfo) {
		
		return agentInfoMapper.updateByPrimaryKeySelective(agentInfo);
	}

	@Override
	public int delAgentInfo(int agentId) {
		
		return agentInfoMapper.deleteByPrimaryKey(agentId);
	}

}
