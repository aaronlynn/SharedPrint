package com.sharedprint.system.agent.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sharedprint.system.agent.mapper.AgentUserInfoMapper;
import com.sharedprint.system.agent.pojo.AgentUserInfo;

@Service
public class AgentUserInfoServiceImpl implements AgentUserInfoService {

	@Autowired
	private AgentUserInfoMapper agentUserInfoMapper;
	@Override
	public List<Map<String, Object>> findAgentUserInfoList(Map<String, Object> searchMap) {
		
		return agentUserInfoMapper.findAgentUserList(searchMap);
	}

	@Override
	public int findAgentUserInfoCount(Map<String, Object> searchMap) {
		
		return agentUserInfoMapper.findAgentUserCount(searchMap);
	}

	@Override
	public int insertAgentUserInfo(AgentUserInfo agentUserInfo) {
		return agentUserInfoMapper.insertSelective(agentUserInfo);
	}

	@Override
	public int updateAgentUserInfo(AgentUserInfo agentUserInfo) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int delAgentUserInfo(int agentUserId) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int resetAgentUserPwd(Map<String, Object> resetMap) {
		
		return agentUserInfoMapper.resetPwdAgentUser(resetMap);
	}

	@Override
	public int isExitfindAgentUserCount(Map<String, Object> resetMap) {
		
		return agentUserInfoMapper.isExitfindAgentUserCount(resetMap);
	}

	@Override
	public int deleteByAgentCode(Map<String, Object> delMap) {
		
		return agentUserInfoMapper.deleteByAgentCode(delMap);
	}

	@Override
	public int isExitAgentUserPwdCount(Map<String, Object> searchMap) {
		
		return agentUserInfoMapper.isExitAgentUserPwdCount(searchMap);
	}

}
