package com.sharedprint.system.agent.service;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.agent.pojo.AgentUserInfo;



public interface AgentUserInfoService {

	/**
	 * 获取代理商用户列表
	 * @param searchMap
	 * @return
	 */
	List<Map<String, Object>> findAgentUserInfoList(Map<String, Object> searchMap);
	
	/**
	 * 获取代理商用户列表数量
	 * @param searchMap
	 * @return
	 */
	int findAgentUserInfoCount(Map<String, Object> searchMap);

	/**
	 * 新增代理商用户
	 * @param menuInfo
	 * @return
	 */
	int insertAgentUserInfo(AgentUserInfo agentUserInfo);
	/**
	 * 更新代理商用户
	 * @param menuInfo
	 * @return
	 */
	int updateAgentUserInfo(AgentUserInfo agentUserInfo);
	/**
	 * 删除代理商用户
	 * @param menuInfo
	 * @return
	 */
	int delAgentUserInfo(int agentUserId);
	/**
	 * 重置代理商用户密码
	 * @param menuInfo
	 * @return
	 */
	int resetAgentUserPwd(Map<String, Object> resetMap);
	/**
	 * 代理商用户是否存在
	 * @param resetMap
	 * @return
	 */
	int isExitfindAgentUserCount(Map<String, Object> resetMap);
	/**
	 * 删除代理商下的用户
	 * @param delMap
	 * @return
	 */
	int deleteByAgentCode(Map<String, Object> delMap);
	/**
	 * 判断代理商用户原密码是否存在
	 * @param searchMap
	 * @return
	 */
	int isExitAgentUserPwdCount(Map<String, Object> searchMap);
}
