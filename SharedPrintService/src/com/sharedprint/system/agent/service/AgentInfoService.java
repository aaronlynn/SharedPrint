package com.sharedprint.system.agent.service;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.agent.pojo.AgentInfo;


public interface AgentInfoService {

	/**
	 * 获取代理商列表
	 * @param searchMap
	 * @return
	 */
	List<Map<String, Object>> findAgentInfoList(Map<String, Object> searchMap);
	
	/**
	 * 获取代理商列表数量
	 * @param searchMap
	 * @return
	 */
	int findAgentInfoCount(Map<String, Object> searchMap);

	/**
	 * 新增代理商
	 * @param menuInfo
	 * @return
	 */
	int insertAgentInfo(AgentInfo agentInfo);
	/**
	 * 更新代理商
	 * @param menuInfo
	 * @return
	 */
	int updateAgentInfo(AgentInfo agentInfo);
	/**
	 * 删除代理商
	 * @param menuInfo
	 * @return
	 */
	int delAgentInfo(int agentId);
}
