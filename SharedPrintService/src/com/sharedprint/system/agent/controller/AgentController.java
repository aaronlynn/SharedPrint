package com.sharedprint.system.agent.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sharedprint.dics.service.DicsInfoService;
import com.sharedprint.system.agent.pojo.AgentInfo;
import com.sharedprint.system.agent.pojo.AgentUserInfo;
import com.sharedprint.system.agent.service.AgentInfoService;
import com.sharedprint.system.agent.service.AgentUserInfoService;
import com.sharedprint.system.area.service.AreaInfoService;
import com.sharedprint.system.organization.service.OrgInfoService;
import com.sharedprint.util.RandomCode;

/**
 * 代理商管理的控制器
 */
@Controller
@RequestMapping("/api/agentInfo")
public class AgentController {

	@Autowired
	private AreaInfoService areaInfoService;
	@Autowired
	private AgentUserInfoService agentUserService;
	@Autowired
	private AgentInfoService agentInfoService;
	@Autowired
	private OrgInfoService orgInfoService;
	@Autowired
	private DicsInfoService dicsInfoService;
	
	private Date date = new Date();
	private SimpleDateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd :HH:mm:ss");
	/**
	 * 获取代理商列表数据
	 * @param request
	 * @return
	 */
	@RequestMapping("/queryPageList")
	@ResponseBody
	public Map<String, Object> queryMenuList(HttpServletRequest req) {
		String queryAgentNameLike = req.getParameter("queryAgentNameLike");
		String queryAgentCode = req.getParameter("queryAgentCode");
		String queryAgentStatus = req.getParameter("queryAgentStatus");
		String queryAgentOrgName = req.getParameter("queryAgentOrgName");
		
		List<Map<String, Object>> orgList = new ArrayList<Map<String,Object>>();
		Map<String, Object> orgCodeMap= new HashMap<String, Object>();
		orgCodeMap.put("orgName", queryAgentOrgName);
		String orgCode = null;
		//获取orgCode
		int isExit = orgInfoService.findOrgCodeListCount(orgCodeMap);
		if ( isExit > 0) {
			orgList = orgInfoService.findOrgCodeList(orgCodeMap);
			orgCode = orgList.get(0).get("Org_Code").toString();
		}
		
		String currentPage = req.getParameter("currentPage");
		String pageSize = req.getParameter("pageSize");
		String orderByValue = req.getParameter("orderByValue");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		List<Map<String, Object>> pageList = new ArrayList<Map<String, Object>>();
		int totalCount = 0;
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {
			if (currentPage == null || "".equals(currentPage.trim())) {
				currentPage = "0";
			}
			if (pageSize == null || "".equals(pageSize.trim())) {
				pageSize = "10";
			}
			Map<String, Object> searchMap = new HashMap<String, Object>();
			int currentpages = 0;
			if ((currentPage != null || !"".equals(currentPage.trim()))) {
				currentpages = Integer.parseInt(currentPage);
				currentpages = currentpages * Integer.parseInt(pageSize);
			}

			searchMap.put("currentPage", currentpages);
			searchMap.put("pageSize", Integer.parseInt(pageSize));
			searchMap.put("ageInfName", queryAgentNameLike);
			searchMap.put("ageInfCode", queryAgentCode);
			searchMap.put("AgeInfStatus", queryAgentStatus);
			searchMap.put("Org_Code", orgCode);

			totalCount = agentInfoService.findAgentInfoCount(searchMap);// 总记录数
			if (totalCount > 0) {
				if (orderByValue == null || "".equals(orderByValue.trim())) {
					searchMap.put("orderByAreaIdDesc", "1");
				}
				pageList =agentInfoService .findAgentInfoList(searchMap);
			}
		}
		//获取数据字典
		Map<String, Object> dicsSearchMap = new HashMap<String, Object>();
		List<Map<String, Object>> dicsList = new ArrayList<Map<String,Object>>();
		dicsSearchMap.put("dicsCode", "AgeInfType");
		dicsList = dicsInfoService.findDicsList(dicsSearchMap);
		//修改代理商类型中文
		for(int i=0;i<pageList.size();i++)
		{
			for (int j = 0; j < dicsList.size(); j++) {
				if (pageList.get(i).get("AgeInfType").toString().equals(dicsList.get(j).get("key_value").toString())) {
					pageList.get(i).put("AgeInfType", dicsList.get(j).get("key_name").toString());
				}
			}
		}
		dicsSearchMap.put("dicsCode", "IDType");
		dicsList = dicsInfoService.findDicsList(dicsSearchMap);
		//修改证件类型中文
		for(int i=0;i<pageList.size();i++)
		{
			for (int j = 0; j < dicsList.size(); j++) {
				if (pageList.get(i).get("IDType").toString().equals(dicsList.get(j).get("key_value").toString())) {
					pageList.get(i).put("IDType", dicsList.get(j).get("key_name").toString());
				}
			}
		}
		dicsSearchMap.put("dicsCode", "AgeInfStatus");
		dicsList = dicsInfoService.findDicsList(dicsSearchMap);
		//修改代理商状态 中文
		for(int i=0;i<pageList.size();i++)
		{
			for (int j = 0; j < dicsList.size(); j++) {
				if (pageList.get(i).get("AgeInfStatus").toString().equals(dicsList.get(j).get("key_value").toString())) {
					pageList.get(i).put("AgeInfStatus", dicsList.get(j).get("key_name").toString());
				}
			}
		}
		dicsSearchMap.put("dicsCode", "AgeInfLevel");
		dicsList = dicsInfoService.findDicsList(dicsSearchMap);
		//修改代理商状态 中文
		for(int i=0;i<pageList.size();i++)
		{
			for (int j = 0; j < dicsList.size(); j++) {
				if (pageList.get(i).get("AgeInfLevel").toString().equals(dicsList.get(j).get("key_value").toString())) {
					pageList.get(i).put("AgeInfLevel", dicsList.get(j).get("key_name").toString());
				}
			}
		}
		Map<String, Object> resultMap = new HashMap<String, Object>();
		//代理商状态数据字典
		dicsSearchMap.put("dicsCode", "AgeInfStatus");
		dicsList = dicsInfoService.findDicsList(dicsSearchMap);
		resultMap.put("dicsAgeInfStatusMap", dicsList);
		
		
		resultMap.put("currentPage", Integer.parseInt(currentPage) + 1);
		resultMap.put("pageTotal", totalCount);
		resultMap.put("pageList", pageList);
		resultMap.put("success", "1");

		return resultMap;
	}

	/**
	 * 验证代理商名称是否存在！
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/validAgentName")
	@ResponseBody
	public boolean validateAreaName(HttpServletRequest req) {
		String validAgentName = req.getParameter("AgeInfName");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		String excludeAgentId = req.getParameter("excludeAgentId");
		System.out.println("token=" + token + ",userId=" + userId + ",validAgentName=" + validAgentName
				+"excludeAgentId"+excludeAgentId);
		boolean result = false;
		if (validAgentName != null && !"".equals(validAgentName.trim())) {
			Map<String, Object> searchMap = new HashMap<String, Object>();
			searchMap.put("addAgeInfName", validAgentName);
			if (excludeAgentId != null && !"".equals(excludeAgentId.trim())) {
				searchMap.put("excludeAreaId", excludeAgentId);
			}
			int count = agentInfoService.findAgentInfoCount(searchMap);
			System.err.println("count="+count);
			if (count == 0) {
				result = true;
			}
		}
		return result;
	}

	/**
	 * 保存新增菜单！
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public Map<String, Object> addMenu(HttpServletRequest req) {
		String AgeInfName = req.getParameter("AgeInfName");
		String AgeInfType = req.getParameter("AgeInfType");
		String IDType = req.getParameter("IDType");
		String IDCode = req.getParameter("IDCode");
		String Email = req.getParameter("Email");
		String AgeInfLevel = req.getParameter("AgeInfLevel");
		String Alias = req.getParameter("Alias");
		String Org_Name = req.getParameter("Org_Name");
		String Area_Name = req.getParameter("Area_Name");
		String CliMgrCode = req.getParameter("CliMgrCode");
		String Contactor = req.getParameter("Contactor");
		String ContactTel = req.getParameter("ContactTel");
		String Remark = req.getParameter("Remark");
		String BusiAddress = req.getParameter("BusiAddress");
		String addMaterial = req.getParameter("addMaterial");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		System.out.println(token + userId);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {

			AgentInfo agentInfo = new AgentInfo();
			new RandomCode();
			String agentCode = RandomCode.getRandomCode();
			agentInfo.setAgeinfCode(agentCode);
			agentInfo.setAgeinfname(AgeInfName);
			agentInfo.setAgeinftype(AgeInfType);
			agentInfo.setIdtype(IDType);
			agentInfo.setIdcode(IDCode);
			agentInfo.setEmail(Email);
			agentInfo.setAgeinflevel(AgeInfLevel);
			agentInfo.setAlias(Alias);
			agentInfo.setOrgCode(Org_Name);
			agentInfo.setAreaCode(Area_Name);
			agentInfo.setClimgrcode(CliMgrCode);
			agentInfo.setContactor(Contactor);
			agentInfo.setContacttel(ContactTel);
			agentInfo.setRemark(Remark);
			agentInfo.setBusiaddress(BusiAddress);
			agentInfo.setMaterial(addMaterial);
			agentInfo.setCrtuserid(Integer.parseInt(userId));
			agentInfo.setCrtdate(dateFormat.format(date));
			
			int success1 = agentInfoService.insertAgentInfo(agentInfo);
			AgentUserInfo agentUserInfo =  new AgentUserInfo();
			agentUserInfo.setAgeinfCode(agentCode);
			agentUserInfo.setAgetruename(Contactor);
			agentUserInfo.setLoginname(ContactTel);
			//获取数据字典
			Map<String, Object> dicsSearchMap = new HashMap<String, Object>();
			List<Map<String, Object>> dicsList = new ArrayList<Map<String,Object>>();
			dicsSearchMap.put("dicsCode", "ResetPwd");
			dicsList = dicsInfoService.findDicsList(dicsSearchMap);
			agentUserInfo.setLoginpwd(dicsList.get(0).get("key_value").toString());
			agentUserInfo.setAgeuserstatus("1");
			agentUserInfo.setOprId(Integer.parseInt(userId));
			agentUserInfo.setOprDate(dateFormat.format(date));
			agentUserInfo.setFirstloginflag("0");
			agentUserInfo.setInitpwdflag("0");
			int success2 = agentUserService.insertAgentUserInfo(agentUserInfo);
			
			if (success1 > 0 && success2 > 0) {
				resultMap.put("success", "1");
				resultMap.put("msg", "代理商用户新增成功！");
			} else {
				resultMap.put("msg", "保存失败");
			}
		} else {
			resultMap.put("msg", "请上传相关参数");
		}
		return resultMap;
	}

	// 初始化前台修改代理商数据
	@RequestMapping("/initUpdate")
	@ResponseBody
	public Map<String, Object> initUpdate(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		String AgeInf_ID = req.getParameter("AgeInf_ID");
		System.out.println("token=" + token + ",userId=" + userId + ",AgeInf_ID=" + AgeInf_ID);

		Map<String, Object> pageMap = new HashMap<String, Object>();
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<Map<String, Object>> pageList = new ArrayList<Map<String, Object>>();
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim()) && AgeInf_ID != null
				&& !"".equals(AgeInf_ID.trim())) {
			Map<String, Object> searchMap = new HashMap<String, Object>();
			searchMap.put("ageInfID", AgeInf_ID);
			pageList = agentInfoService.findAgentInfoList(searchMap);
			pageMap = pageList.get(0);
		}
		Map<String, Object> dicsSearchMap = new HashMap<String, Object>();
		List<Map<String, Object>> dicsList = new ArrayList<Map<String,Object>>();
		dicsSearchMap.put("dicsCode", "AgeInfType");
		dicsList = dicsInfoService.findDicsList(dicsSearchMap);
		resultMap.put("dicsAgeInfType", dicsList);

		dicsSearchMap.put("dicsCode", "IDType");
		dicsList = dicsInfoService.findDicsList(dicsSearchMap);
		resultMap.put("dicsIDType", dicsList);
		
		dicsSearchMap.put("dicsCode", "AgeInfLevel");
		dicsList = dicsInfoService.findDicsList(dicsSearchMap);
		resultMap.put("dicsAgeInfLevel", dicsList);
		
		Map<String, Object> searchMap = new HashMap<String, Object>();
		List<Map<String, Object>> areaList = new ArrayList<Map<String,Object>>();
		int totalCount = areaInfoService.findAreaInfoCount(searchMap);// 总记录数
		if (totalCount > 0) {
			areaList = areaInfoService.findAreaInfoList(searchMap);
		}
		resultMap.put("areaList", areaList);
		
		List<Map<String, Object>> orgList = new ArrayList<Map<String,Object>>();
		orgList  = orgInfoService.getAllOrgList(searchMap);
		resultMap.put("orgList", orgList);
		
		resultMap.put("pageMap", pageMap);
		resultMap.put("success", "1");
		return resultMap;

	}

	/*
	 * 更新并保存前台代理商数据
	 */
	@RequestMapping("/update")
	@ResponseBody
	public Map<String, Object> saveUpdate(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		
		String AgeInfId = req.getParameter("updateAgentId");
		String AgeInfName = req.getParameter("updateAgeInfName");
		String AgeInfType = req.getParameter("updateAgeInfType");
		String IDType = req.getParameter("updateIDType");
		String IDCode = req.getParameter("updateIDCode");
		String Email = req.getParameter("updateEmail");
		String AgeInfLevel = req.getParameter("updateAgeInfLevel");
		String Alias = req.getParameter("updateAlias");
		String Org_Name = req.getParameter("updateOrg_Name");
		String Area_Name = req.getParameter("updateArea_Name");
		String CliMgrCode = req.getParameter("updateCliMgrCode");
		String Contactor = req.getParameter("updateContactor");
		String ContactTel = req.getParameter("updateContactTel");
		String Remark = req.getParameter("updateRemark");
		String BusiAddress = req.getParameter("updateBusiAddress");
		String addMaterial = req.getParameter("updateMaterial");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		System.out.println(token + userId);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {

			AgentInfo agentInfo = new AgentInfo();
			agentInfo.setAgeinfname(AgeInfName);
			agentInfo.setAgeinftype(AgeInfType);
			agentInfo.setIdtype(IDType);
			agentInfo.setIdcode(IDCode);
			agentInfo.setEmail(Email);
			agentInfo.setAgeinflevel(AgeInfLevel);
			agentInfo.setAlias(Alias);
			agentInfo.setOrgCode(Org_Name);
			agentInfo.setAreaCode(Area_Name);
			agentInfo.setClimgrcode(CliMgrCode);
			agentInfo.setContactor(Contactor);
			agentInfo.setContacttel(ContactTel);
			agentInfo.setRemark(Remark);
			agentInfo.setBusiaddress(BusiAddress);
			agentInfo.setMaterial(addMaterial);
			agentInfo.setUptuserid(Integer.parseInt(userId));
			agentInfo.setLastuptdate(dateFormat.format(date));
			agentInfo.setAgeinfId(Integer.parseInt(AgeInfId));
			
			int success = agentInfoService.updateAgentInfo(agentInfo);
			if (success > 0) {
				resultMap.put("success", "1");
			} else {
				resultMap.put("msg", "保存失败");
			}
		} else {
			resultMap.put("msg", "请上传相关参数");
		}
		return resultMap;
	}

	// 前台代理商详情
	@RequestMapping("/detail")
	@ResponseBody
	public Map<String, Object> detailFront(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		String AgeInf_ID = req.getParameter("AgeInf_ID");

		Map<String, Object> pageMap = new HashMap<String, Object>();
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<Map<String, Object>> pageList = new ArrayList<Map<String, Object>>();
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {
			Map<String, Object> searchMap = new HashMap<String, Object>();
			searchMap.put("ageInfID", AgeInf_ID);
			pageList = agentInfoService.findAgentInfoList(searchMap);
		}
		//获取数据字典
		Map<String, Object> dicsSearchMap = new HashMap<String, Object>();
		List<Map<String, Object>> dicsList = new ArrayList<Map<String,Object>>();
		dicsSearchMap.put("dicsCode", "AgeInfType");
		dicsList = dicsInfoService.findDicsList(dicsSearchMap);
		//修改代理商类型中文
		for(int i=0;i<pageList.size();i++)
		{
			for (int j = 0; j < dicsList.size(); j++) {
				if (pageList.get(i).get("AgeInfType").toString().equals(dicsList.get(j).get("key_value").toString())) {
					pageList.get(i).put("AgeInfType", dicsList.get(j).get("key_name").toString());
				}
			}
		}
		dicsSearchMap.put("dicsCode", "IDType");
		dicsList = dicsInfoService.findDicsList(dicsSearchMap);
		//修改证件类型中文
		for(int i=0;i<pageList.size();i++)
		{
			for (int j = 0; j < dicsList.size(); j++) {
				if (pageList.get(i).get("IDType").toString().equals(dicsList.get(j).get("key_value").toString())) {
					pageList.get(i).put("IDType", dicsList.get(j).get("key_name").toString());
				}
			}
		}
		dicsSearchMap.put("dicsCode", "AgeInfStatus");
		dicsList = dicsInfoService.findDicsList(dicsSearchMap);
		//修改代理商状态 中文
		for(int i=0;i<pageList.size();i++)
		{
			for (int j = 0; j < dicsList.size(); j++) {
				if (pageList.get(i).get("AgeInfStatus").toString().equals(dicsList.get(j).get("key_value").toString())) {
					pageList.get(i).put("AgeInfStatus", dicsList.get(j).get("key_name").toString());
				}
			}
		}
		dicsSearchMap.put("dicsCode", "AgeInfLevel");
		dicsList = dicsInfoService.findDicsList(dicsSearchMap);
		//修改代理商状态 中文
		for(int i=0;i<pageList.size();i++)
		{
			for (int j = 0; j < dicsList.size(); j++) {
				if (pageList.get(i).get("AgeInfLevel").toString().equals(dicsList.get(j).get("key_value").toString())) {
					pageList.get(i).put("AgeInfLevel", dicsList.get(j).get("key_name").toString());
				}
			}
		}
		dicsSearchMap.put("dicsCode", "AgentCheckStatus");
		dicsList = dicsInfoService.findDicsList(dicsSearchMap);
		resultMap.put("dicsAgeInfStatusMap", dicsList);
		pageMap = pageList.get(0);
		resultMap.put("pageMap", pageMap);
		resultMap.put("success", "1");
		return resultMap;

	}

	// 删除前台代理商数据
	@RequestMapping("/delete")
	@ResponseBody
	public Map<String, Object> delete(HttpServletRequest req, HttpServletResponse resp)  {
		String AgeInf_ID = req.getParameter("AgeInf_ID");
		String ageInfCode = req.getParameter("ageInfCode");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
        System.err.println("AgeInf_ID="+AgeInf_ID+"ageInfCode="+ageInfCode);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		if (AgeInf_ID != null && !"".equals(AgeInf_ID.trim()) && token != null && !"".equals(token.trim()) && userId != null
				&& !"".equals(userId.trim())) {
			 //判断是否有下级
			int success = agentInfoService.delAgentInfo(Integer.parseInt(AgeInf_ID));
			Map<String, Object> searchMap = new HashMap<String, Object>();
			searchMap.put("agentInfoCode", ageInfCode);
			int agentUserCount = agentUserService.isExitfindAgentUserCount(searchMap);
			//该代理商下有代理商用户？
			boolean succeesUser = false;
			if (agentUserCount > 0) {
				int succees2 = agentUserService.deleteByAgentCode(searchMap);
				if (succees2 > 0) {
					succeesUser = true;
				}
			}
			if (success > 0 && agentUserCount == 0) {
				resultMap.put("success", "1");
				resultMap.put("msg", "删除成功!");
			}
			else if (success > 0 && agentUserCount > 0) {
				if (succeesUser) {
					resultMap.put("success", "1");
					resultMap.put("msg", "删除成功，同时删除关联用户！");
				}
			}
			else {
				resultMap.put("msg", "该代理商存在代理商用户,不允许删除!");
			}
		}
		return resultMap;
	}
	// 审核代理商
	@RequestMapping("/checkAgent")
	@ResponseBody
	public Map<String, Object> checkAgent(HttpServletRequest req, HttpServletResponse resp)  {
		String AgeInfId = req.getParameter("checkAgentId");
		String checkStatus = req.getParameter("checkStatus");
		String checkAdvice = req.getParameter("checkAdvice");
		
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		System.out.println(token + userId);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {

			AgentInfo agentInfo = new AgentInfo();
			agentInfo.setChkuserid(Integer.parseInt(userId));
			agentInfo.setAgeinfstatus(checkStatus);
			agentInfo.setChkremark(checkAdvice);
			agentInfo.setChkdate(dateFormat.format(date));
			agentInfo.setAgeinfId(Integer.parseInt(AgeInfId));
			
			int success = agentInfoService.updateAgentInfo(agentInfo);
			if (success > 0) {
				resultMap.put("success", "1");
			} else {
				resultMap.put("msg", "提交审核失败");
			}
		} else {
			resultMap.put("msg", "请上传相关参数");
		}
		return resultMap;		
	}
	  //解冻代理商
		@RequestMapping("/unFreeze")
		@ResponseBody
		public Map<String, Object> unFreeze(HttpServletRequest req, HttpServletResponse resp)  {
			String AgeInfId = req.getParameter("AgeInf_ID");
			
			String token = req.getHeader("token");
			String userId = req.getHeader("userId");
			System.out.println(token + userId);
			Map<String, Object> resultMap = new HashMap<String, Object>();
			resultMap.put("success", "0");
			if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {

				Map<String, Object> searchMap = new HashMap<String, Object>();
				searchMap.put("ageInfID", AgeInfId);
				List<Map<String, Object>> isFreezeList = agentInfoService.findAgentInfoList(searchMap);
				
				AgentInfo agentInfo = new AgentInfo();
				agentInfo.setAgeinfId(Integer.parseInt(AgeInfId));
				//获取代理商状态数据字典
				Map<String, Object> dicsSearchMap = new HashMap<String, Object>();
				List<Map<String, Object>> dicsList = new ArrayList<Map<String,Object>>();
				dicsSearchMap.put("dicsCode", "AgeInfStatus");
				dicsList = dicsInfoService.findDicsList(dicsSearchMap);
				String freezeKeyValue = null;
				String normalKeyValue =null;
				for (int i = 0; i < dicsList.size(); i++) {
					if (dicsList.get(i).get("key_name").toString().equals("冻结")) {
						freezeKeyValue = dicsList.get(i).get("key_value").toString();
					}
				}
				for (int i = 0; i < dicsList.size(); i++) {
					if (dicsList.get(i).get("key_name").toString().equals("正常")) {
						normalKeyValue = dicsList.get(i).get("key_value").toString();
						agentInfo.setAgeinfstatus(normalKeyValue);
					}
				}
				int success = 0;
				//判断该记录是否为冻结
				if (freezeKeyValue.equals(isFreezeList.get(0).get("AgeInfStatus").toString()) ) {
					success = agentInfoService.updateAgentInfo(agentInfo);
					if (success > 0) {
						resultMap.put("success", "1");
						resultMap.put("msg", "解冻成功！");
					} else {
						resultMap.put("msg", "解冻失败！");
					}
				}
				else {
						resultMap.put("success", "0");
						resultMap.put("msg", "只有冻结状态才可以解冻！");
				}
				
			} else {
				resultMap.put("msg", "请上传相关参数");
			}
			return resultMap;		
		}
		
       //冻结代理商
		@RequestMapping("/freeze")
		@ResponseBody
		public Map<String, Object> Freeze(HttpServletRequest req, HttpServletResponse resp)  {
			String AgeInfId = req.getParameter("AgeInf_ID");
			
			String token = req.getHeader("token");
			String userId = req.getHeader("userId");
			System.out.println(token + userId);
			Map<String, Object> resultMap = new HashMap<String, Object>();
			resultMap.put("success", "0");
			if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {

				Map<String, Object> searchMap = new HashMap<String, Object>();
				searchMap.put("ageInfID", AgeInfId);
				List<Map<String, Object>> isFreezeList = agentInfoService.findAgentInfoList(searchMap);
				
				AgentInfo agentInfo = new AgentInfo();
				agentInfo.setAgeinfId(Integer.parseInt(AgeInfId));
				//获取代理商状态数据字典
				Map<String, Object> dicsSearchMap = new HashMap<String, Object>();
				List<Map<String, Object>> dicsList = new ArrayList<Map<String,Object>>();
				dicsSearchMap.put("dicsCode", "AgeInfStatus");
				dicsList = dicsInfoService.findDicsList(dicsSearchMap);
				String freezeKeyValue = null;
				String normalKeyValue =null;
				for (int i = 0; i < dicsList.size(); i++) {
					if (dicsList.get(i).get("key_name").toString().equals("正常")) {
						normalKeyValue = dicsList.get(i).get("key_value").toString();
					}
				}
				for (int i = 0; i < dicsList.size(); i++) {
					if (dicsList.get(i).get("key_name").toString().equals("冻结")) {
						freezeKeyValue = dicsList.get(i).get("key_value").toString();
						agentInfo.setAgeinfstatus(freezeKeyValue);
					}
				}
				int success = 0;
				//判断该记录是否为正常
				if (normalKeyValue.equals(isFreezeList.get(0).get("AgeInfStatus").toString()) ) {
					success = agentInfoService.updateAgentInfo(agentInfo);
					if (success > 0) {
						resultMap.put("success", "1");
						resultMap.put("msg", "冻结成功！");
					} else {
						resultMap.put("msg", "冻结失败！");
					}
				}
				else {
						resultMap.put("success", "0");
						resultMap.put("msg", "只有状态正常才可以冻结！");
				}
				
			} else {
				resultMap.put("msg", "请上传相关参数");
					}
					return resultMap;		
		}
		// 前台代理商系统管理详情
		@RequestMapping("/agentDetailMethod")
		@ResponseBody
		public Map<String, Object> AgrntDetailFront(HttpServletRequest req, HttpServletResponse resp) throws IOException {
			String token = req.getHeader("token");
			String userId = req.getHeader("userId");
			String loginName = req.getParameter("loginName");
			Map<String, Object> pageMap = new HashMap<String, Object>();
			Map<String, Object> resultMap = new HashMap<String, Object>();
			List<Map<String, Object>> pageList = new ArrayList<Map<String, Object>>();
			if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {
				Map<String, Object> searchMap = new HashMap<String, Object>();
				searchMap.put("loginName", loginName);
				//获取代理商code
				List<Map<String, Object>> agentUserList = new ArrayList<Map<String, Object>>();
				agentUserList = agentUserService.findAgentUserInfoList(searchMap);
				String agentInfoCode = agentUserList.get(0).get("AgeInf_Code").toString();
				System.err.println("agentInfoCode="+agentInfoCode);
				searchMap.put("ageInfCode", agentInfoCode);
				pageList = agentInfoService.findAgentInfoList(searchMap);
			}
			//获取数据字典
			Map<String, Object> dicsSearchMap = new HashMap<String, Object>();
			List<Map<String, Object>> dicsList = new ArrayList<Map<String,Object>>();
			dicsSearchMap.put("dicsCode", "AgeInfType");
			dicsList = dicsInfoService.findDicsList(dicsSearchMap);
			//修改代理商类型中文
			for(int i=0;i<pageList.size();i++)
			{
				for (int j = 0; j < dicsList.size(); j++) {
					if (pageList.get(i).get("AgeInfType").toString().equals(dicsList.get(j).get("key_value").toString())) {
						pageList.get(i).put("AgeInfType", dicsList.get(j).get("key_name").toString());
					}
				}
			}
			dicsSearchMap.put("dicsCode", "IDType");
			dicsList = dicsInfoService.findDicsList(dicsSearchMap);
			//修改证件类型中文
			for(int i=0;i<pageList.size();i++)
			{
				for (int j = 0; j < dicsList.size(); j++) {
					if (pageList.get(i).get("IDType").toString().equals(dicsList.get(j).get("key_value").toString())) {
						pageList.get(i).put("IDType", dicsList.get(j).get("key_name").toString());
					}
				}
			}
			dicsSearchMap.put("dicsCode", "AgeInfStatus");
			dicsList = dicsInfoService.findDicsList(dicsSearchMap);
			//修改代理商状态 中文
			for(int i=0;i<pageList.size();i++)
			{
				for (int j = 0; j < dicsList.size(); j++) {
					if (pageList.get(i).get("AgeInfStatus").toString().equals(dicsList.get(j).get("key_value").toString())) {
						pageList.get(i).put("AgeInfStatus", dicsList.get(j).get("key_name").toString());
					}
				}
			}
			dicsSearchMap.put("dicsCode", "AgeInfLevel");
			dicsList = dicsInfoService.findDicsList(dicsSearchMap);
			//修改代理商状态 中文
			for(int i=0;i<pageList.size();i++)
			{
				for (int j = 0; j < dicsList.size(); j++) {
					if (pageList.get(i).get("AgeInfLevel").toString().equals(dicsList.get(j).get("key_value").toString())) {
						pageList.get(i).put("AgeInfLevel", dicsList.get(j).get("key_name").toString());
					}
				}
			}
			dicsSearchMap.put("dicsCode", "AgentCheckStatus");
			dicsList = dicsInfoService.findDicsList(dicsSearchMap);
			resultMap.put("dicsAgeInfStatusMap", dicsList);
			pageMap = pageList.get(0);
			resultMap.put("pageMap", pageMap);
			resultMap.put("success", "1");
			return resultMap;

		}
}
