package com.sharedprint.system.agent.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sharedprint.dics.service.DicsInfoService;
import com.sharedprint.system.agent.service.AgentUserInfoService;
import com.sharedprint.system.organization.service.OrgInfoService;

/**
 * 代理商用户管理的控制器
 */
@Controller
@RequestMapping("/api/agentUserInfo")
public class AgentUserController {

	@Autowired
	private AgentUserInfoService agentUserInfoService;
	@Autowired
	private DicsInfoService dicsInfoService;
	private Date date = new Date();
	private SimpleDateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd :HH:mm:ss");
	/**
	 * 获取代理商用户列表数据
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/queryPageList.action")
	@ResponseBody
	public Map<String, Object> queryMenuList(HttpServletRequest req) {
		String queryAgentUserLoginName = req.getParameter("queryAgentUserLoginName");
		String queryAgentUserNameLike = req.getParameter("queryAgentUserNameLike");

		String currentPage = req.getParameter("currentPage");
		String pageSize = req.getParameter("pageSize");
		String orderByValue = req.getParameter("orderByValue");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		List<Map<String, Object>> pageList = new ArrayList<Map<String, Object>>();
		int totalCount = 0;
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {
			if (currentPage == null || "".equals(currentPage.trim())) {
				currentPage = "0";
			}
			if (pageSize == null || "".equals(pageSize.trim())) {
				pageSize = "10";
			}
			Map<String, Object> searchMap = new HashMap<String, Object>();
			int currentpages = 0;
			if ((currentPage != null || !"".equals(currentPage.trim()))) {
				currentpages = Integer.parseInt(currentPage);
				currentpages = currentpages * Integer.parseInt(pageSize);
			}

			searchMap.put("currentPage", currentpages);
			searchMap.put("pageSize", Integer.parseInt(pageSize));
			searchMap.put("loginName", queryAgentUserLoginName);
			searchMap.put("ageTrueName", queryAgentUserNameLike);

			totalCount = agentUserInfoService.findAgentUserInfoCount(searchMap);// 总记录数
			if (totalCount > 0) {
				if (orderByValue == null || "".equals(orderByValue.trim())) {
					searchMap.put("orderByAreaIdDesc", "1");
				}
				pageList = agentUserInfoService.findAgentUserInfoList(searchMap);
			}
		}
		//获取数据字典
		Map<String, Object> dicsSearchMap = new HashMap<String, Object>();
		List<Map<String, Object>> dicsList = new ArrayList<Map<String,Object>>();
		dicsSearchMap.put("dicsCode", "AgeUserStatus");
		dicsList = dicsInfoService.findDicsList(dicsSearchMap);
		//修改用户状态中文
		for(int i=0;i<pageList.size();i++)
		{
			for (int j = 0; j < dicsList.size(); j++) {
				if (pageList.get(i).get("AgeUserStatus").toString().equals(dicsList.get(j).get("key_value").toString())) {
					pageList.get(i).put("AgeUserStatus", dicsList.get(j).get("key_name").toString());
				}
			}
		}
		//修改登录密码状态为中文
		dicsSearchMap.put("dicsCode", "InitPwdFlag");
		dicsList = dicsInfoService.findDicsList(dicsSearchMap);
		for(int i=0;i<pageList.size();i++)
		{
			for (int j = 0; j < dicsList.size(); j++) {
				if (pageList.get(i).get("InitPwdFlag").toString().equals(dicsList.get(j).get("key_value").toString())) {
					pageList.get(i).put("InitPwdFlag", dicsList.get(j).get("key_name").toString());
				}
			}
		}
		//修改第一次登录标志为中文
		dicsSearchMap.put("dicsCode", "FirstLoginFlag");
		dicsList = dicsInfoService.findDicsList(dicsSearchMap);
		for(int i=0;i<pageList.size();i++)
		{
			for (int j = 0; j < dicsList.size(); j++) {
				if (pageList.get(i).get("FirstLoginFlag").toString().equals(dicsList.get(j).get("key_value").toString())) {
					pageList.get(i).put("FirstLoginFlag", dicsList.get(j).get("key_name").toString());
				}
			}
		}

		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("currentPage", Integer.parseInt(currentPage) + 1);
		resultMap.put("pageTotal", totalCount);
		resultMap.put("pageList", pageList);
		resultMap.put("success", "1");

		return resultMap;
	}

//	// 前台代理商用户详情
	@RequestMapping("/detail")
	@ResponseBody
	public Map<String, Object> detailFront(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		
		String ageUserID = req.getParameter("AgeUser_ID");

		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<Map<String, Object>> agentUserList = new ArrayList<Map<String, Object>>();
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {
			Map<String, Object> searchMap = new HashMap<String, Object>();
			
			searchMap.put("ageuserId", ageUserID);
			agentUserList = agentUserInfoService.findAgentUserInfoList(searchMap);
		}
		//获取数据字典
		Map<String, Object> dicsSearchMap = new HashMap<String, Object>();
		List<Map<String, Object>> dicsList = new ArrayList<Map<String,Object>>();
		dicsSearchMap.put("dicsCode", "AgeUserStatus");
		dicsList = dicsInfoService.findDicsList(dicsSearchMap);
		//修改用户状态中文
		for(int i=0;i<agentUserList.size();i++)
		{
			for (int j = 0; j < dicsList.size(); j++) {
				if (agentUserList.get(i).get("AgeUserStatus").toString().equals(dicsList.get(j).get("key_value").toString())) {
					agentUserList.get(i).put("AgeUserStatus", dicsList.get(j).get("key_name").toString());
				}
			}
		}
		//修改登录密码状态为中文
		dicsSearchMap.put("dicsCode", "InitPwdFlag");
		dicsList = dicsInfoService.findDicsList(dicsSearchMap);
		for(int i=0;i<agentUserList.size();i++)
		{
			for (int j = 0; j < dicsList.size(); j++) {
				if (agentUserList.get(i).get("InitPwdFlag").toString().equals(dicsList.get(j).get("key_value").toString())) {
					agentUserList.get(i).put("InitPwdFlag", dicsList.get(j).get("key_name").toString());
				}
			}
		}
		//修改第一次登录标志为中文
		dicsSearchMap.put("dicsCode", "FirstLoginFlag");
		dicsList = dicsInfoService.findDicsList(dicsSearchMap);
		for(int i=0;i<agentUserList.size();i++)
		{
			for (int j = 0; j < dicsList.size(); j++) {
				if (agentUserList.get(i).get("FirstLoginFlag").toString().equals(dicsList.get(j).get("key_value").toString())) {
					agentUserList.get(i).put("FirstLoginFlag", dicsList.get(j).get("key_name").toString());
				}
			}
		}
		resultMap.put("map", agentUserList.get(0));
		resultMap.put("success", "1");
		return resultMap;

	}

	// 重置密码
	@RequestMapping("/resetPwd")
	@ResponseBody
	public Map<String, Object> delete(HttpServletRequest req, HttpServletResponse resp)  {
		String ageUserID = req.getParameter("AgeUser_ID");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");

		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		if (ageUserID != null && !"".equals(ageUserID.trim()) && token != null && !"".equals(token.trim()) && userId != null
				&& !"".equals(userId.trim())) {
			Map<String, Object> dicsSearchMap = new HashMap<String, Object>();
			List<Map<String, Object>> dicsList = new ArrayList<Map<String,Object>>();
			dicsSearchMap.put("dicsCode", "ResetPwd");
			dicsList = dicsInfoService.findDicsList(dicsSearchMap);
			Map<String,Object> resetMap = new HashMap<String, Object>();
			resetMap.put("ageUserId", ageUserID);
			resetMap.put("ResetPwd", dicsList.get(0).get("key_value"));
			resetMap.put("initpwdflag", "1");
			int success = agentUserInfoService.resetAgentUserPwd(resetMap);
			if (success > 0) {
				resultMap.put("success", "1");
			} else {
				resultMap.put("msg", "用户密码重置失败!");
			}
		}
		return resultMap;
	}
	
	/**
	 * 验证原密码是否正确是否正确！
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/validModifyPwdMethod")
	@ResponseBody
	public boolean validateAreaName(HttpServletRequest req) {
		String oldPwd = req.getParameter("oldPwd");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		System.out.println("token=" + token + ",userId=" + userId + ",oldPwd=" + oldPwd
				);
		boolean result = true;
		if (oldPwd != null && !"".equals(oldPwd.trim())) {
			Map<String, Object> searchMap = new HashMap<String, Object>();
			searchMap.put("oldPwd", oldPwd);
			searchMap.put("AgeUser_ID", userId);
			
			int count = agentUserInfoService.isExitAgentUserPwdCount(searchMap);
			System.err.println("count="+count);
			if (count == 0) {
				result = false;
			}
		}
		return result;
	}
	// 修改密码
		@RequestMapping("/modifyPwd")
		@ResponseBody
		public Map<String, Object> modifyPwd(HttpServletRequest req, HttpServletResponse resp)  {
			String newPwd = req.getParameter("newPwd");
			String token = req.getHeader("token");
			String userId = req.getHeader("userId");

			Map<String, Object> resultMap = new HashMap<String, Object>();
			resultMap.put("success", "0");
			if (newPwd != null && !"".equals(newPwd.trim()) && token != null && !"".equals(token.trim()) && userId != null
					&& !"".equals(userId.trim())) {
				
				Map<String,Object> resetMap = new HashMap<String, Object>();
				resetMap.put("ResetPwd", newPwd);
				resetMap.put("ageUserId", userId);
				int success = agentUserInfoService.resetAgentUserPwd(resetMap);
				if (success > 0) {
					resultMap.put("success", "1");
				} else {
					resultMap.put("msg", "用户密码修改失败!");
				}
			}
			return resultMap;
		}
}
