package com.sharedprint.system.agent.pojo;


public class AgentInfo {
    private Integer ageinfId;

    private String ageinfCode;

    private String ageinfname;

    private String ageinftype;

    private String idtype;

    private String idcode;

    private String ageinfstatus;

    private String ageinflevel;

    private String alias;

    private String orgCode;

    private String areaCode;

    private String busiaddress;

    private String contactor;

    private String contacttel;

    private String email;

    private String material;

    private String remark;

    private String climgrcode;

    private Integer crtuserid;

    private String crtdate;

    private Integer uptuserid;

    private String lastuptdate;

    private Integer chkuserid;

    private String chkdate;

    private String chkremark;

    private String reserve1;

    private String reserve2;

    private String reserve3;

    public Integer getAgeinfId() {
        return ageinfId;
    }

    public void setAgeinfId(Integer ageinfId) {
        this.ageinfId = ageinfId;
    }

    public String getAgeinfCode() {
        return ageinfCode;
    }

    public void setAgeinfCode(String ageinfCode) {
        this.ageinfCode = ageinfCode == null ? null : ageinfCode.trim();
    }

    public String getAgeinfname() {
        return ageinfname;
    }

    public void setAgeinfname(String ageinfname) {
        this.ageinfname = ageinfname == null ? null : ageinfname.trim();
    }

    public String getAgeinftype() {
        return ageinftype;
    }

    public void setAgeinftype(String ageinftype) {
        this.ageinftype = ageinftype == null ? null : ageinftype.trim();
    }

    public String getIdtype() {
        return idtype;
    }

    public void setIdtype(String idtype) {
        this.idtype = idtype == null ? null : idtype.trim();
    }

    public String getIdcode() {
        return idcode;
    }

    public void setIdcode(String idcode) {
        this.idcode = idcode == null ? null : idcode.trim();
    }

    public String getAgeinfstatus() {
        return ageinfstatus;
    }

    public void setAgeinfstatus(String ageinfstatus) {
        this.ageinfstatus = ageinfstatus == null ? null : ageinfstatus.trim();
    }

    public String getAgeinflevel() {
        return ageinflevel;
    }

    public void setAgeinflevel(String ageinflevel) {
        this.ageinflevel = ageinflevel == null ? null : ageinflevel.trim();
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias == null ? null : alias.trim();
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode == null ? null : orgCode.trim();
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode == null ? null : areaCode.trim();
    }

    public String getBusiaddress() {
        return busiaddress;
    }

    public void setBusiaddress(String busiaddress) {
        this.busiaddress = busiaddress == null ? null : busiaddress.trim();
    }

    public String getContactor() {
        return contactor;
    }

    public void setContactor(String contactor) {
        this.contactor = contactor == null ? null : contactor.trim();
    }

    public String getContacttel() {
        return contacttel;
    }

    public void setContacttel(String contacttel) {
        this.contacttel = contacttel == null ? null : contacttel.trim();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material == null ? null : material.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getClimgrcode() {
        return climgrcode;
    }

    public void setClimgrcode(String climgrcode) {
        this.climgrcode = climgrcode == null ? null : climgrcode.trim();
    }

    public Integer getCrtuserid() {
        return crtuserid;
    }

    public void setCrtuserid(Integer crtuserid) {
        this.crtuserid = crtuserid;
    }

    public String getCrtdate() {
        return crtdate;
    }

    public void setCrtdate(String crtdate) {
        this.crtdate = crtdate;
    }

    public Integer getUptuserid() {
        return uptuserid;
    }

    public void setUptuserid(Integer uptuserid) {
        this.uptuserid = uptuserid;
    }

    public String getLastuptdate() {
        return lastuptdate;
    }

    public void setLastuptdate(String lastuptdate) {
        this.lastuptdate = lastuptdate;
    }

    public Integer getChkuserid() {
        return chkuserid;
    }

    public void setChkuserid(Integer chkuserid) {
        this.chkuserid = chkuserid;
    }

    public String getChkdate() {
        return chkdate;
    }

    public void setChkdate(String chkdate) {
        this.chkdate = chkdate;
    }

    public String getChkremark() {
        return chkremark;
    }

    public void setChkremark(String chkremark) {
        this.chkremark = chkremark == null ? null : chkremark.trim();
    }

    public String getReserve1() {
        return reserve1;
    }

    public void setReserve1(String reserve1) {
        this.reserve1 = reserve1 == null ? null : reserve1.trim();
    }

    public String getReserve2() {
        return reserve2;
    }

    public void setReserve2(String reserve2) {
        this.reserve2 = reserve2 == null ? null : reserve2.trim();
    }

    public String getReserve3() {
        return reserve3;
    }

    public void setReserve3(String reserve3) {
        this.reserve3 = reserve3 == null ? null : reserve3.trim();
    }
}