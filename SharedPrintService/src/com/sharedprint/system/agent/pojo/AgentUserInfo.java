package com.sharedprint.system.agent.pojo;


public class AgentUserInfo {
    private Integer ageuserId;

    private String agetruename;

    private String ageinfCode;

    private String loginname;

    private String loginpwd;

    private String ageuserstatus;

    private String pwdquestion;

    private String pwdanswer;

    private String initpwdflag;

    private String firstloginflag;

    private Integer oprId;

    private String  oprDate;

    public Integer getAgeuserId() {
        return ageuserId;
    }

    public void setAgeuserId(Integer ageuserId) {
        this.ageuserId = ageuserId;
    }

    public String getAgetruename() {
        return agetruename;
    }

    public void setAgetruename(String agetruename) {
        this.agetruename = agetruename == null ? null : agetruename.trim();
    }

    public String getAgeinfCode() {
        return ageinfCode;
    }

    public void setAgeinfCode(String ageinfCode) {
        this.ageinfCode = ageinfCode == null ? null : ageinfCode.trim();
    }

    public String getLoginname() {
        return loginname;
    }

    public void setLoginname(String loginname) {
        this.loginname = loginname == null ? null : loginname.trim();
    }

    public String getLoginpwd() {
        return loginpwd;
    }

    public void setLoginpwd(String loginpwd) {
        this.loginpwd = loginpwd == null ? null : loginpwd.trim();
    }

    public String getAgeuserstatus() {
        return ageuserstatus;
    }

    public void setAgeuserstatus(String ageuserstatus) {
        this.ageuserstatus = ageuserstatus == null ? null : ageuserstatus.trim();
    }

    public String getPwdquestion() {
        return pwdquestion;
    }

    public void setPwdquestion(String pwdquestion) {
        this.pwdquestion = pwdquestion == null ? null : pwdquestion.trim();
    }

    public String getPwdanswer() {
        return pwdanswer;
    }

    public void setPwdanswer(String pwdanswer) {
        this.pwdanswer = pwdanswer == null ? null : pwdanswer.trim();
    }

    public String getInitpwdflag() {
        return initpwdflag;
    }

    public void setInitpwdflag(String initpwdflag) {
        this.initpwdflag = initpwdflag == null ? null : initpwdflag.trim();
    }

    public String getFirstloginflag() {
        return firstloginflag;
    }

    public void setFirstloginflag(String firstloginflag) {
        this.firstloginflag = firstloginflag == null ? null : firstloginflag.trim();
    }

    public Integer getOprId() {
        return oprId;
    }

    public void setOprId(Integer oprId) {
        this.oprId = oprId;
    }

    public String getOprDate() {
        return oprDate;
    }

    public void setOprDate(String oprDate) {
        this.oprDate = oprDate;
    }
}