package com.sharedprint.system.agent.mapper;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.agent.pojo.AgentInfo;

public interface AgentInfoMapper {
    int deleteByPrimaryKey(Integer ageinfId);

    int insert(AgentInfo record);

    int insertSelective(AgentInfo record);

    AgentInfo selectByPrimaryKey(Integer ageinfId);

    int updateByPrimaryKeySelective(AgentInfo record);

    int updateByPrimaryKey(AgentInfo record);

	List<Map<String, Object>> findAgentList(Map<String, Object> searchMap);

	int findAgentCount(Map<String, Object> searchMap);
	
}