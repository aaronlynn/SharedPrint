package com.sharedprint.system.agent.mapper;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.agent.pojo.AgentUserInfo;

public interface AgentUserInfoMapper {
    int deleteByPrimaryKey(Integer ageuserId);

    int insert(AgentUserInfo record);

    int insertSelective(AgentUserInfo record);

    AgentUserInfo selectByPrimaryKey(Integer ageuserId);

    int updateByPrimaryKeySelective(AgentUserInfo record);

    int updateByPrimaryKey(AgentUserInfo record);

	List<Map<String, Object>> findAgentUserList(Map<String, Object> searchMap);

	int findAgentUserCount(Map<String, Object> searchMap);
	
	int resetPwdAgentUser(Map<String, Object> searchMap);

	int isExitfindAgentUserCount(Map<String, Object> searchMap);
	
	int deleteByAgentCode(Map<String, Object> delMap);
	
	int isExitAgentUserPwdCount(Map<String, Object> searchMap);
}