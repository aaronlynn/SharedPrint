package com.sharedprint.system.role.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sharedprint.system.menu.service.MenuInfoService;
import com.sharedprint.system.role.pojo.RoleInfo;
import com.sharedprint.system.role.service.RoleInfoService;

/**
 * 角色管理的控制器
 * 
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/api/roleInfo")
public class RoleInfoController {

	@Autowired
	private RoleInfoService roleInfoService;
	@Autowired
	private MenuInfoService menuInfoService;
	String resultJson=null;
	/**
	 * 获取角色列表数据
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/queryPageList")
	@ResponseBody
	public Map<String, Object> queryRoleList(HttpServletRequest req) {
		String roleNameLike = req.getParameter("RoleNameLike");

		String currentPage = req.getParameter("currentPage");
		String pageSize = req.getParameter("pageSize");
		String orderByValue = req.getParameter("orderByValue");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		List<Map<String, Object>> roleList = new ArrayList<Map<String, Object>>();
		int totalCount = 0;
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {
			if (currentPage == null || "".equals(currentPage.trim())) {
				currentPage = "0";
			}
			if (pageSize == null || "".equals(pageSize.trim())) {
				pageSize = "10";
			}
			Map<String, Object> searchMap = new HashMap<String, Object>();
			int currentpages = 0;
			if ((currentPage != null || !"".equals(currentPage.trim()))) {
				currentpages = Integer.parseInt(currentPage);
				currentpages = currentpages * Integer.parseInt(pageSize);
			}

			searchMap.put("currentPage", currentpages);
			searchMap.put("pageSize", Integer.parseInt(pageSize));
			searchMap.put("RoleName", roleNameLike);

			totalCount = roleInfoService.findRoleInfoCount(searchMap);// 总记录数
			if (totalCount > 0) {
				if (orderByValue == null || "".equals(orderByValue.trim())) {
					searchMap.put("orderByRoleIdDesc", "1");
				}
				roleList = roleInfoService.findRoleInfoList(searchMap);
			}
		}
		// list转json str
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("currentPage", Integer.parseInt(currentPage) + 1);
		resultMap.put("pageTotal", totalCount);
		resultMap.put("pageList", roleList);
		resultMap.put("success", "1");

		return resultMap;
	}
	// 初始化前台角色权限菜单
		@RequestMapping("/queryRoleMenu")
		@ResponseBody
		
		public Map<String, Object> initAdd(HttpServletRequest req, HttpServletResponse resp) throws IOException {
			String token = req.getHeader("token");
			String userId = req.getHeader("userId");
			System.out.println("token=" + token + ",userId=" + userId);

			Map<String, Object> resultMap = new HashMap<String, Object>();
			List<Map<String, Object>> menuList = new ArrayList<Map<String, Object>>();
			if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim()) ) {
				Map<String, Object> searchMap = new HashMap<String, Object>();
				menuList = menuInfoService.findMenuInfoList(searchMap);
			}
			resultMap.put("menuList", menuList);
			resultMap.put("success", "1");
			return resultMap;

		}
	/**
	 * 验证角色名称是否存在！
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/validateRoleName")
	@ResponseBody
	public boolean validateRole(HttpServletRequest req) {
		String addRoleName = req.getParameter("addRoleName");
		String updateRoleName = req.getParameter("updateRoleName");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		String excludeRoleId = req.getParameter("excludeRoleId");
		System.err.println("后台接受修改的id和名称"+excludeRoleId+updateRoleName);
		System.out.println("token=" + token + ",userId=" + userId + ",addRoleName=" + addRoleName);
		boolean result = false;
		Map<String, Object> searchMap = new HashMap<String, Object>();
		if (excludeRoleId != null && !"".equals(excludeRoleId.trim())) {
			searchMap.put("excludeRoleId", excludeRoleId);
			searchMap.put("updateRoleName", updateRoleName);
		}
		if (addRoleName != null && !"".equals(addRoleName.trim())) {
			searchMap.put("addRoleName", addRoleName);
		}
		int count = roleInfoService.findRoleInfoCount(searchMap);
		if (count == 0) {
			result = true;
		}
		return result;
	}

	/**
	 * 保存新增角色！
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public Map<String, Object> addRole(HttpServletRequest req) {
		String RoleName = req.getParameter("addRoleName");
		String Remark = req.getParameter("addRemark");
		String menuId = req.getParameter("menuId");
		System.err.println(menuId);
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		System.out.println(token + userId);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		Map<String, Object> searchMap = new HashMap<String, Object>();
		Map<String, Object> roleMap = new HashMap<String, Object>();
		List<Map<String, Object>> roleList = new ArrayList<Map<String, Object>>();
		resultMap.put("success", "0");
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {

			RoleInfo roleInfo = new RoleInfo();
			roleInfo.setRolename(RoleName);
			roleInfo.setRemark(Remark);
			roleInfo.setOprId(Integer.parseInt(userId));
			int success = 0;
			success = roleInfoService.insertRoleInfo(roleInfo);
			searchMap.put("RoleName", RoleName);
			roleList = roleInfoService.findRoleInfoList(searchMap);
			roleMap = roleList.get(0);
			String RoleId =String.valueOf(roleMap.get("Role_ID")) ;
			roleMap.clear();
			roleMap.put("Role_ID", RoleId);
			String[] menuIds = menuId.split(",");
			System.err.println(menuIds[0]);
			for (int i = 0; i < menuIds.length; i++) {
				roleMap.put("menuId",Integer.parseInt(menuIds[i]) );
				success = roleInfoService.insertRoleMenuInfo(roleMap);
				roleMap.remove("menuId");
			}
			if (success > 0) {
				resultMap.put("success", "1");
			} else {
				resultMap.put("msg", "保存失败");
			}
		} else {
			resultMap.put("msg", "请上传相关参数");
		}
		return resultMap;
	}

	// 初始化前台修改角色数据
	@RequestMapping("/initUpdate")
	@ResponseBody
	public Map<String, Object> initUpdate(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		String roleId = req.getParameter("roleId");
		System.out.println("token=" + token + ",userId=" + userId + ",roleId=" + roleId);

		Map<String, Object> roleMap = new HashMap<String, Object>();
		Map<String, Object> menuMap = new HashMap<String, Object>();
		Map<String, Object> roleMenuMap = new HashMap<String, Object>();
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<Map<String, Object>> roleList = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> menuList = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> roleMenuList = new ArrayList<Map<String, Object>>();
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim()) && roleId != null
				&& !"".equals(roleId.trim())) {
			Map<String, Object> searchMap = new HashMap<String, Object>();
			searchMap.put("roleId", roleId);
			System.err.println("要修改的ID"+roleId);
			roleList = roleInfoService.findRoleInfoList(searchMap);
			roleMap = roleList.get(0);
			searchMap.put("updateRoleID", roleId);
			roleMenuList = roleInfoService.findRoleMenuInfoList(searchMap);
			searchMap.clear();
			menuList = menuInfoService.findMenuInfoList(searchMap);
		}
		for (int i = 0; i < menuList.size(); i++) {
			for (int j = 0; j < roleMenuList.size(); j++) {
				menuMap = menuList.get(i);
				roleMenuMap = roleMenuList.get(j);
				if (menuMap.get("menuCode")==roleMenuMap.get("Menu_id")) {
					menuMap.put("checked", true);
				}
			}
		}
		resultMap.put("menuList", menuList);
		for (int i = 0; i <  menuList.size(); i++) {
			menuMap = menuList.get(i);
			menuMap.put("doCheck", false);
		}
		resultMap.put("detailList", menuList);
		resultMap.put("roleMap", roleMap);
		resultMap.put("success", "1");
		return resultMap;

	}

	/*
	 * 保存修改前台角色
	 */
	@RequestMapping("/update")
	@ResponseBody
	public Map<String, Object> saveUpdate(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String updateRoleName = req.getParameter("updateRoleName");
		String roleId = req.getParameter("excludeRoleId");
		System.err.println("excludeRoleId=="+roleId);
		String updateRemark = req.getParameter("updateRemark");
		String updateMenuId = req.getParameter("updatemenuId");
		System.err.println(updateMenuId);
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		System.out.println("token:"+token +"userID"+ userId+"roleID:"+roleId);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		Map<String, Object> roleMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {

			RoleInfo roleInfo = new RoleInfo();
			roleInfo.setRolename(updateRoleName);
			roleInfo.setRemark(updateRemark);
			roleInfo.setOprId(Integer.parseInt(userId));
			roleInfo.setRoleId(Integer.parseInt(roleId));
			int success = 0;
			success = roleInfoService.updateRoleInfo(roleInfo);
			String[] menuIds = updateMenuId.split(",");
			System.err.println(menuIds[0]);
			roleMap.put("Role_ID", Integer.parseInt(roleId));
			success = roleInfoService.deleteRoleMenuInfo(Integer.parseInt(roleId));
			for (int i = 0; i < menuIds.length; i++) {
				roleMap.put("menuId",Integer.parseInt(menuIds[i]) );
				success = roleInfoService.insertRoleMenuInfo(roleMap);
				roleMap.remove("menuId");
			}
			if (success > 0) {
				resultMap.put("success", "1");
			} else {
				resultMap.put("msg", "修改失败");
			}
		} else {
			resultMap.put("msg", "请上传相关参数");
		}
		return resultMap;
	}

	// 前台角色详情
	@RequestMapping("/detail")
	@ResponseBody
	public Map<String, Object> detailFront(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		return initUpdate(req,resp);
	}

	// 删除前台菜单数据
	@RequestMapping("/delete")
	@ResponseBody
	public Map<String, Object> delete(HttpServletRequest req, HttpServletResponse resp)  {
		String roleId = req.getParameter("role_id");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");

		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		if (roleId != null && !"".equals(roleId.trim()) && token != null && !"".equals(token.trim()) && userId != null
				&& !"".equals(userId.trim())) {
			int success = roleInfoService.delRoleInfo(Integer.parseInt(roleId));
			if (success > 0) {
				resultMap.put("success", "1");
			} else {
				resultMap.put("msg", "删除失败!");
			}
		}
		return resultMap;
	}
}
