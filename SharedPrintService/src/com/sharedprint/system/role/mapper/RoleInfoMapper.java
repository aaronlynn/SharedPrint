package com.sharedprint.system.role.mapper;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.role.pojo.RoleInfo;

public interface RoleInfoMapper {
    int deleteByPrimaryKey(Integer roleId);

    int insert(RoleInfo record);

    int insertSelective(RoleInfo record);

    RoleInfo selectByPrimaryKey(Integer roleId);

    int updateByPrimaryKeySelective(RoleInfo record);

    int updateByPrimaryKey(RoleInfo record);
    
    /**
	 * 获取角色列表
	 * @param searchMap
	 * @return
	 */
	public List<Map<String,Object>> findRoleList(Map<String,Object> searchMap);
	
	/**
	 * 获取角色条数
	 * @param searchMap
	 * @return
	 */
	public int findRoleCount(Map<String,Object> searchMap);
	/**
	 * 插入角色菜单关系表
	 * @param roleMap
	 * @return
	 */
	int insertRoleMenuInfo(Map<String, Object> roleMap);
	/**
	 * 加载角色菜单关系列表
	 * @param searchMap
	 * @return
	 */
	List<Map<String, Object>> findRoleMenuInfoList(Map<String, Object> searchMap);
	/**
	 * 更新角色菜单关系表
	 * @param parseInt
	 * @return
	 */
	int deleteRoleMenuInfo(Integer roleId);

}