package com.sharedprint.system.role.service;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.role.pojo.RoleInfo;

public interface RoleInfoService {

	/**
	 * 获取角色列表
	 * @param searchMap
	 * @return
	 */
	List<Map<String, Object>> findRoleInfoList(Map<String, Object> searchMap);
	
	/**
	 * 获取角色列表数量
	 * @param searchMap
	 * @return
	 */
	int findRoleInfoCount(Map<String, Object> searchMap);

	/**
	 * 新增角色
	 * @param roleInfo
	 * @return
	 */
	int insertRoleInfo(RoleInfo roleInfo);
	/**
	 * 更新角色
	 * @param roleInfo
	 * @return
	 */
	int updateRoleInfo(RoleInfo roleInfo);
	/**
	 * 删除菜单
	 * @param roleInfo
	 * @return
	 */
	int delRoleInfo(int RoleID);
	/**
	 * 插入角色菜单关系表
	 * @param roleMap
	 * @return
	 */
	int insertRoleMenuInfo(Map<String, Object> roleMap);
	/**
	 * 加载角色菜单关系列表
	 * @param searchMap
	 * @return
	 */
	List<Map<String, Object>> findRoleMenuInfoList(Map<String, Object> searchMap);
	/**
	 * 更新和删除角色菜单关系表
	 * @param roleId
	 * @return
	 */
	int deleteRoleMenuInfo(int roleId);
}
