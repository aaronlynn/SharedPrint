package com.sharedprint.system.role.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sharedprint.system.role.mapper.RoleInfoMapper;
import com.sharedprint.system.role.pojo.RoleInfo;

@Service
public class RoleInfoServiceImpl implements RoleInfoService {

	@Autowired
	private RoleInfoMapper roleInfoMapper;
	
	@Override
	public List<Map<String, Object>> findRoleInfoList(Map<String, Object> searchMap) {
		return roleInfoMapper.findRoleList(searchMap);
	}

	@Override
	public int findRoleInfoCount(Map<String, Object> searchMap) {
		
		return roleInfoMapper.findRoleCount(searchMap);
	}

	@Override
	public int insertRoleInfo(RoleInfo roleInfo) {
		return roleInfoMapper.insertSelective(roleInfo);
	}

	@Override
	public int updateRoleInfo(RoleInfo roleInfo) {
	
		return roleInfoMapper.updateByPrimaryKeySelective(roleInfo);
	}

	@Override
	public int delRoleInfo(int roleId) {
		return roleInfoMapper.deleteByPrimaryKey(roleId);
	}

	@Override
	public int insertRoleMenuInfo(Map<String, Object> roleMap) {
		
		return roleInfoMapper.insertRoleMenuInfo(roleMap);
	}

	@Override
	public List<Map<String, Object>> findRoleMenuInfoList(Map<String, Object> searchMap) {
		return roleInfoMapper.findRoleMenuInfoList(searchMap);
	}

	@Override
	public int deleteRoleMenuInfo(int roleId) {
		return roleInfoMapper.deleteRoleMenuInfo(roleId);
	}


}
