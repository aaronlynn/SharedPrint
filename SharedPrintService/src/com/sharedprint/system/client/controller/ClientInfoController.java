package com.sharedprint.system.client.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sharedprint.dics.service.DicsInfoService;
import com.sharedprint.system.agent.service.AgentUserInfoService;
import com.sharedprint.system.area.pojo.AreaInfo;
import com.sharedprint.system.client.pojo.PerInfo;
import com.sharedprint.system.client.service.ClientInfoService;

/**
 * 客户管理的控制器
 */
@Controller
@RequestMapping("/api/clientInfo")
public class ClientInfoController {

	@Autowired
	private ClientInfoService clientInfoService;
	@Autowired
	private DicsInfoService dicsInfoService;

	private Date date = new Date();
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd :HH:mm:ss");

	/**
	 * 获取地区列表数据
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/queryClientList")
	@ResponseBody
	public Map<String, Object> queryClientList(HttpServletRequest req) {
		String clientCode = req.getParameter("ClientCode");
		String clientNameLike = req.getParameter("ClientNameLike");

		String currentPage = req.getParameter("currentPage");
		String pageSize = req.getParameter("pageSize");
		String orderByValue = req.getParameter("orderByValue");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		List<Map<String, Object>> clientList = new ArrayList<Map<String, Object>>();
		int totalCount = 0;
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {
			if (currentPage == null || "".equals(currentPage.trim())) {
				currentPage = "0";
			}
			if (pageSize == null || "".equals(pageSize.trim())) {
				pageSize = "10";
			}
			Map<String, Object> searchMap = new HashMap<String, Object>();
			int currentpages = 0;
			if ((currentPage != null || !"".equals(currentPage.trim()))) {
				currentpages = Integer.parseInt(currentPage);
				currentpages = currentpages * Integer.parseInt(pageSize);
			}

			searchMap.put("currentPage", currentpages);
			searchMap.put("pageSize", Integer.parseInt(pageSize));
			searchMap.put("clientCode", clientCode);
			searchMap.put("clientName", clientNameLike);

			totalCount = clientInfoService.findClientInfoCount(searchMap);// 总记录数
			if (totalCount > 0) {
				if (orderByValue == null || "".equals(orderByValue.trim())) {
					searchMap.put("orderByAreaIdDesc", "1");
				}
				clientList = clientInfoService.findClientInfoList(searchMap);
			}
		}
		// 获取数据字典
		Map<String, Object> dicsSearchMap = new HashMap<String, Object>();
		List<Map<String, Object>> dicsList = new ArrayList<Map<String, Object>>();
		dicsSearchMap.put("dicsCode", "AgeUserStatus");
		dicsList = dicsInfoService.findDicsList(dicsSearchMap);
		for (int i = 0; i < clientList.size(); i++) {
			for (int j = 0; j < dicsList.size(); j++) {
				if (clientList.get(i).get("clientStatus").toString()
						.equals(dicsList.get(j).get("key_value").toString())) {
					clientList.get(i).put("clientStatus", dicsList.get(j).get("key_name").toString());
				}
			}
		}
		dicsSearchMap.put("dicsCode", "RealStatus");
		dicsList = dicsInfoService.findDicsList(dicsSearchMap);
		for (int i = 0; i < clientList.size(); i++) {
			for (int j = 0; j < dicsList.size(); j++) {
				if (clientList.get(i).get("realStatus").toString()
						.equals(dicsList.get(j).get("key_value").toString())) {
					clientList.get(i).put("realStatus", dicsList.get(j).get("key_name").toString());
				}
			}
		}

		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("currentPage", Integer.parseInt(currentPage) + 1);
		resultMap.put("pageTotal", totalCount);
		resultMap.put("pageList", clientList);
		resultMap.put("success", "1");

		return resultMap;
	}

	@RequestMapping("/resetPwd")
	@ResponseBody
	public Map<String, Object> resetPwd(HttpServletRequest req, HttpServletResponse resp) throws IOException {

		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		String clientID = req.getParameter("clientID");
		System.out.println("token:" + token + "userID" + userId + "clientID:" + clientID);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {

			PerInfo perInfo = new PerInfo();
			perInfo.setPeruserId(Integer.parseInt(clientID));
			perInfo.setLoginpwd("123456");
			int success = clientInfoService.updateClientInfo(perInfo);
			if (success > 0) {
				resultMap.put("success", "1");
			} else {
				resultMap.put("msg", "保存失败");
			}
		} else {
			resultMap.put("msg", "请上传相关参数");
		}
		return resultMap;
	}

	@RequestMapping("/freeze")
	@ResponseBody
	public Map<String, Object> freeze(HttpServletRequest req, HttpServletResponse resp) throws IOException {

		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		String clientID = req.getParameter("clientID");
		System.out.println("token:" + token + "userID" + userId + "clientID:" + clientID);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {

			Map<String, Object> SearchMap = new HashMap<String, Object>();
			List<Map<String, Object>> clientList = new ArrayList<Map<String, Object>>();
			SearchMap.put("clientId", clientID);
			clientList = clientInfoService.findClientInfoList(SearchMap);
			if ("2".equals(clientList.get(0).get("clientStatus"))) {
				resultMap.put("success", "2");
			} else {
				PerInfo perInfo = new PerInfo();
				perInfo.setPeruserId(Integer.parseInt(clientID));
				perInfo.setPeruserstatus("2");
				int success = clientInfoService.updateClientInfo(perInfo);
				if (success > 0) {
					resultMap.put("success", "1");
				} else {
					resultMap.put("msg", "保存失败");
				}
			}
		} else {
			resultMap.put("msg", "请上传相关参数");
		}
		return resultMap;
	}

	@RequestMapping("/unfreeze")
	@ResponseBody
	public Map<String, Object> unfreeze(HttpServletRequest req, HttpServletResponse resp) throws IOException {

		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		String clientID = req.getParameter("clientID");
		System.out.println("token:" + token + "userID" + userId + "clientID:" + clientID);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {
			Map<String, Object> SearchMap = new HashMap<String, Object>();
			List<Map<String, Object>> clientList = new ArrayList<Map<String, Object>>();
			SearchMap.put("clientId", clientID);
			clientList = clientInfoService.findClientInfoList(SearchMap);
			if (!"2".equals(clientList.get(0).get("clientStatus"))) {
				resultMap.put("success", "2");
			} else {
				PerInfo perInfo = new PerInfo();
				perInfo.setPeruserId(Integer.parseInt(clientID));
				perInfo.setPeruserstatus("1");
				int success = clientInfoService.updateClientInfo(perInfo);
				if (success > 0) {
					resultMap.put("success", "1");
				} else {
					resultMap.put("msg", "保存失败");
				}
			}
		} else {
			resultMap.put("msg", "请上传相关参数");
		}
		return resultMap;
	}

	// 客户详情
	@RequestMapping("/detail")
	@ResponseBody
	public Map<String, Object> detailFront(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		String clientId = req.getParameter("clientID");

		Map<String, Object> clientMap = new HashMap<String, Object>();
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<Map<String, Object>> clientList = new ArrayList<Map<String, Object>>();
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {
			Map<String, Object> searchMap = new HashMap<String, Object>();
			searchMap.put("clientId", clientId);
			clientList = clientInfoService.findClientInfoList(searchMap);
			clientMap = clientList.get(0);

			// 获取数据字典
			Map<String, Object> dicsSearchMap = new HashMap<String, Object>();
			List<Map<String, Object>> dicsList = new ArrayList<Map<String, Object>>();
			dicsSearchMap.put("dicsCode", "AgeUserStatus");
			dicsList = dicsInfoService.findDicsList(dicsSearchMap);
			for (int i = 0; i < dicsList.size(); i++) {
				if (clientMap.get("clientStatus").toString().equals(dicsList.get(i).get("key_value").toString())) {
					clientMap.put("clientStatus", dicsList.get(i).get("key_name").toString());
				}
			}
		}
		resultMap.put("clientMap", clientMap);
		resultMap.put("success", "1");
		return resultMap;

	}

}
