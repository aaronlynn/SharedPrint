package com.sharedprint.system.client.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sharedprint.dics.service.DicsInfoService;
import com.sharedprint.system.client.service.IntDetailService;

/**
 * 客户积分明细管理的控制器
 */
@Controller
@RequestMapping("/api/intdetail")
public class IntDetailController {

	@Autowired
	private IntDetailService intDetailService;
	
	@Autowired
	private DicsInfoService dicsInfoService;

	@RequestMapping("/queryintdetailList")
	@ResponseBody
	public Map<String, Object> queryClientList(HttpServletRequest req) {
		String clientCode = req.getParameter("ClientCode");
		String clientNameLike = req.getParameter("ClientNameLike");

		String currentPage = req.getParameter("currentPage");
		String pageSize = req.getParameter("pageSize");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		String peruserId = req.getHeader("peruserId");
		
		List<Map<String, Object>> intDetailList = new ArrayList<Map<String, Object>>();
		int totalCount = 0;
		//if ((token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim()))
			//	||(token != null && !"".equals(token.trim()) && peruserId != null && !"".equals(peruserId.trim()))) {
			if (currentPage == null || "".equals(currentPage.trim())) {
				currentPage = "0";
			}
			if (pageSize == null || "".equals(pageSize.trim())) {
				pageSize = "10";
			}
			Map<String, Object> searchMap = new HashMap<String, Object>();
			int currentpages = 0;
			if ((currentPage != null || !"".equals(currentPage.trim()))) {
				currentpages = Integer.parseInt(currentPage);
				currentpages = currentpages * Integer.parseInt(pageSize);
			}

			searchMap.put("currentPage", currentpages);
			searchMap.put("pageSize", Integer.parseInt(pageSize));
			searchMap.put("clientCode", clientCode);
			searchMap.put("clientName", clientNameLike);
			if(peruserId != null && !"".equals(peruserId.trim())&& !"undefined".equals(peruserId.trim())) {
				searchMap.put("peruserId",peruserId);
			}
			
			totalCount = intDetailService.findIntDetailCount(searchMap);// 总记录数
			if (totalCount > 0) {
				intDetailList = intDetailService.findIntDetailList(searchMap);
			}
		//}
			//获取数据字典
			Map<String, Object> dicsSearchMap = new HashMap<String, Object>();
			List<Map<String, Object>> dicsList = new ArrayList<Map<String,Object>>();
			dicsSearchMap.put("dicsCode", "IntegralType");
			dicsList = dicsInfoService.findDicsList(dicsSearchMap);
			for(int i=0;i<intDetailList.size();i++)
			{
				for (int j = 0; j < dicsList.size(); j++) {
					if (intDetailList.get(i).get("integralType").toString().equals(dicsList.get(j).get("key_value").toString())) {
						intDetailList.get(i).put("integralType", dicsList.get(j).get("key_name").toString());
					}
				}
			}
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("currentPage", Integer.parseInt(currentPage) + 1);
		resultMap.put("pageTotal", totalCount);
		resultMap.put("pageList", intDetailList);
		resultMap.put("success", "1");

		return resultMap;
	}
}
