package com.sharedprint.system.client.pojo;

import java.util.Date;

public class PerIntegral {
    private Integer perintegralId;

    private String perinfCode;

    private String peruserCode;

    private String integral;

    private String remark;

    private Integer oprId;

    private Date oprDate;

    public Integer getPerintegralId() {
        return perintegralId;
    }

    public void setPerintegralId(Integer perintegralId) {
        this.perintegralId = perintegralId;
    }

    public String getPerinfCode() {
        return perinfCode;
    }

    public void setPerinfCode(String perinfCode) {
        this.perinfCode = perinfCode == null ? null : perinfCode.trim();
    }

    public String getPeruserCode() {
        return peruserCode;
    }

    public void setPeruserCode(String peruserCode) {
        this.peruserCode = peruserCode == null ? null : peruserCode.trim();
    }

    public String getIntegral() {
        return integral;
    }

    public void setIntegral(String integral) {
        this.integral = integral == null ? null : integral.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Integer getOprId() {
        return oprId;
    }

    public void setOprId(Integer oprId) {
        this.oprId = oprId;
    }

    public Date getOprDate() {
        return oprDate;
    }

    public void setOprDate(Date oprDate) {
        this.oprDate = oprDate;
    }
}