package com.sharedprint.system.client.pojo;

import java.util.Date;

public class Intdetail {
    private Integer perintegraldetailId;

    private String perinfCode;

    private String integral;

    private Date integraldate;

    private String perintegraltype;

    private String remark;

    private Integer oprId;

    private Date oprDate;

    public Integer getPerintegraldetailId() {
        return perintegraldetailId;
    }

    public void setPerintegraldetailId(Integer perintegraldetailId) {
        this.perintegraldetailId = perintegraldetailId;
    }

    public String getPerinfCode() {
        return perinfCode;
    }

    public void setPerinfCode(String perinfCode) {
        this.perinfCode = perinfCode == null ? null : perinfCode.trim();
    }

    public String getIntegral() {
        return integral;
    }

    public void setIntegral(String integral) {
        this.integral = integral == null ? null : integral.trim();
    }

    public Date getIntegraldate() {
        return integraldate;
    }

    public void setIntegraldate(Date integraldate) {
        this.integraldate = integraldate;
    }

    public String getPerintegraltype() {
        return perintegraltype;
    }

    public void setPerintegraltype(String perintegraltype) {
        this.perintegraltype = perintegraltype == null ? null : perintegraltype.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Integer getOprId() {
        return oprId;
    }

    public void setOprId(Integer oprId) {
        this.oprId = oprId;
    }

    public Date getOprDate() {
        return oprDate;
    }

    public void setOprDate(Date oprDate) {
        this.oprDate = oprDate;
    }
}