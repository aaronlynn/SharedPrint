package com.sharedprint.system.client.mapper;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.client.pojo.PerIntegral;

public interface PerIntegralMapper {
    int deleteByPrimaryKey(Integer perintegralId);

    int insert(PerIntegral record);

    int insertSelective(PerIntegral record);

    PerIntegral selectByPrimaryKey(Integer perintegralId);

    int updateByPrimaryKeySelective(PerIntegral record);

    int updateByPrimaryKey(PerIntegral record);
    
    /**
	 * 获取客户积分列表
	 * @param searchMap
	 * @return
	 */
	public List<Map<String,Object>> findIntegralList(Map<String,Object> searchMap);
	
	/**
	 * 获取客户积分条数
	 * @param searchMap
	 * @return
	 */
	public int findIntegralCount(Map<String,Object> searchMap);
}