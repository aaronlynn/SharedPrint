package com.sharedprint.system.client.mapper;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.client.pojo.PerInfo;

public interface PerInfoMapper {
    int deleteByPrimaryKey(Integer peruserId);

    int insert(PerInfo record);

    int insertSelective(PerInfo record);

    PerInfo selectByPrimaryKey(Integer peruserId);

    int updateByPrimaryKeySelective(PerInfo record);

    int updateByPrimaryKey(PerInfo record);
    /**
 	 * 获取客户列表
 	 * @param searchMap
 	 * @return
 	 */
 	public List<Map<String,Object>> findClientList(Map<String,Object> searchMap);
 	
 	/**
 	 * 获取客户条数
 	 * @param searchMap
 	 * @return
 	 */
 	public int findClientCount(Map<String,Object> searchMap);
}