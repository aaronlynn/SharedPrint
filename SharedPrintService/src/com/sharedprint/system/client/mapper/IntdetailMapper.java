package com.sharedprint.system.client.mapper;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.client.pojo.Intdetail;

public interface IntdetailMapper {
    int deleteByPrimaryKey(Integer perintegraldetailId);

    int insert(Intdetail record);

    int insertSelective(Intdetail record);

    Intdetail selectByPrimaryKey(Integer perintegraldetailId);

    int updateByPrimaryKeySelective(Intdetail record);

    int updateByPrimaryKey(Intdetail record);
    
    /**
	 * 获取客户积分明细列表
	 * @param searchMap
	 * @return
	 */
	public List<Map<String,Object>> findIntDetailList(Map<String,Object> searchMap);
	
	/**
	 * 获取客户积分明细条数
	 * @param searchMap
	 * @return
	 */
	public int findIntDetailCount(Map<String,Object> searchMap);
}