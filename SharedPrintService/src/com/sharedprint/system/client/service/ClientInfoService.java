package com.sharedprint.system.client.service;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.client.pojo.PerInfo;

public interface ClientInfoService {
	/**
	 * 获取客户列表
	 * @param searchMap
	 * @return
	 */
	List<Map<String, Object>> findClientInfoList(Map<String, Object> searchMap);
	
	/**
	 * 获取客户列表数量
	 * @param searchMap
	 * @return
	 */
	int findClientInfoCount(Map<String, Object> searchMap);

	/**
	 * 新增客户
	 * @param perInfo
	 * @return
	 */
	int insertClientInfo(PerInfo perInfo);
	/**
	 * 更新客户
	 * @param perInfo
	 * @return
	 */
	int updateClientInfo(PerInfo perInfo);
	/**
	 * 删除客户
	 * @param perInfo
	 * @return
	 */
	int delClientInfo(int perId);

}
