package com.sharedprint.system.client.service;

import java.util.List;
import java.util.Map;

public interface IntDetailService {

	/**
	 * 获取客户积分明细列表
	 * @param searchMap
	 * @return
	 */
	List<Map<String, Object>> findIntDetailList(Map<String, Object> searchMap);
	
	/**
	 * 获取客户积分明细列表数量
	 * @param searchMap
	 * @return
	 */
	int findIntDetailCount(Map<String, Object> searchMap);
}
