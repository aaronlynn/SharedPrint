package com.sharedprint.system.client.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sharedprint.system.client.mapper.PerInfoMapper;
import com.sharedprint.system.client.pojo.PerInfo;

@Service
public class ClientInfoServiceImpl implements ClientInfoService{
	@Autowired
	private PerInfoMapper perInfoMapper;

	@Override
	public List<Map<String, Object>> findClientInfoList(Map<String, Object> searchMap) {
		return perInfoMapper.findClientList(searchMap);
	}

	@Override
	public int findClientInfoCount(Map<String, Object> searchMap) {
		return perInfoMapper.findClientCount(searchMap);
	}

	@Override
	public int insertClientInfo(PerInfo perInfo) {
		return perInfoMapper.insertSelective(perInfo);
	}

	@Override
	public int updateClientInfo(PerInfo perInfo) {
		return perInfoMapper.updateByPrimaryKeySelective(perInfo);
	}

	@Override
	public int delClientInfo(int perId) {
		return perInfoMapper.deleteByPrimaryKey(perId);
	}

}
