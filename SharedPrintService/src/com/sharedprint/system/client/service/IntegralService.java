package com.sharedprint.system.client.service;

import java.util.List;
import java.util.Map;

public interface IntegralService {

	/**
	 * 获取客户积分列表
	 * @param searchMap
	 * @return
	 */
	List<Map<String, Object>> findIntegralList(Map<String, Object> searchMap);
	
	/**
	 * 获取客户积分列表数量
	 * @param searchMap
	 * @return
	 */
	int findIntegralCount(Map<String, Object> searchMap);

}
