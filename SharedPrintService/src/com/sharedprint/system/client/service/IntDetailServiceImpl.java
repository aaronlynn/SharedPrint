package com.sharedprint.system.client.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sharedprint.system.client.mapper.IntdetailMapper;

@Service
public class IntDetailServiceImpl implements IntDetailService{

	@Autowired
	private IntdetailMapper intdetailMapper;

	@Override
	public List<Map<String, Object>> findIntDetailList(Map<String, Object> searchMap) {
		return intdetailMapper.findIntDetailList(searchMap);
	}

	@Override
	public int findIntDetailCount(Map<String, Object> searchMap) {
		return intdetailMapper.findIntDetailCount(searchMap);
	}
	
	


}
