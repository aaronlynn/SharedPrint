package com.sharedprint.system.client.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sharedprint.system.client.mapper.PerIntegralMapper;

@Service
public class IntegralServiceImpl implements IntegralService{
	
	@Autowired
	private PerIntegralMapper perIntegralMapper;

	/**
	 * 获取客户积分列表
	 * @param searchMap
	 * @return
	 */
	@Override
	public List<Map<String, Object>> findIntegralList(Map<String, Object> searchMap){
		return perIntegralMapper.findIntegralList(searchMap);
	}
	
	/**
	 * 获取客户积分列表数量
	 * @param searchMap
	 * @return
	 */
	@Override
	public int findIntegralCount(Map<String, Object> searchMap) {
		return perIntegralMapper.findIntegralCount(searchMap);
	}
}
