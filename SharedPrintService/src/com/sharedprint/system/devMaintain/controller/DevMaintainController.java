package com.sharedprint.system.devMaintain.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sharedprint.dics.service.DicsInfoService;
import com.sharedprint.merchant.merDev.service.MerDevService;
import com.sharedprint.system.area.service.AreaInfoService;
import com.sharedprint.system.devFactory.service.DevFactoryInfoService;
import com.sharedprint.system.devInfo.pojo.DevInfo;
import com.sharedprint.system.devInfo.service.DevInfoService;
import com.sharedprint.system.devMaintain.pojo.DevMaintain;
import com.sharedprint.system.devMaintain.service.DevMaintainService;
import com.sharedprint.system.devModel.pojo.DevModel;
import com.sharedprint.system.devModel.service.DevModelInfoService;
import com.sharedprint.system.merchant.service.MerInfoService;
import com.sharedprint.system.organization.service.OrgInfoService;
import com.sharedprint.util.RandomCode;

/**
 * 设备故障管理的控制器
 * 
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/api/devMaintainInfo")
public class DevMaintainController {

	@Autowired
	private DicsInfoService dicsInfoService;
	@Autowired
	private DevMaintainService devMaintainService;
	@Autowired
	private MerDevService merDevInfoService;
	@Autowired
	private AreaInfoService areaInfoService;
	@Autowired
	private MerInfoService merInfoService; 
	
	/**
	 * 获取设备故障列表数据
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/queryPageList")
	@ResponseBody
	public Map<String, Object> queryDevModelList(HttpServletRequest req) {
		String queryDevMaintainNameLike = req.getParameter("queryDevMaintainNameLike");//名称
		String queryDevInfCode = req.getParameter("queryDevInfCode");//编码
		String queryDevMaintainStatus = req.getParameter("queryDevMaintainStatus");//状态
		String currentPage = req.getParameter("currentPage");
		String pageSize = req.getParameter("pageSize");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		Map<String, Object> searchMap = new HashMap<String, Object>();
	    List<Map<String, Object>> devMaintainList = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> statusList = new ArrayList<Map<String, Object>>();
		
		int totalCount = 0;
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {
			if (currentPage == null || "".equals(currentPage.trim())) {
				currentPage = "0";
			}
			if (pageSize == null || "".equals(pageSize.trim())) {
				pageSize = "10";
			}
			int currentpages = 0;
			if ((currentPage != null || !"".equals(currentPage.trim()))) {
				currentpages = Integer.parseInt(currentPage);
				currentpages = currentpages * Integer.parseInt(pageSize);
			}

			searchMap.put("currentPage", currentpages);
			searchMap.put("pageSize", Integer.parseInt(pageSize));
			searchMap.put("queryDevMaintainNameLike", queryDevMaintainNameLike);
			searchMap.put("queryDevInfCode", queryDevInfCode);
			searchMap.put("queryDevMaintainStatus", queryDevMaintainStatus);
			searchMap.put("dicsCode", "DevMaintainStatus");
			
			statusList = dicsInfoService.findDicsList(searchMap);
			
			totalCount = devMaintainService.findDevMaintainCount(searchMap);// 总记录数
			if (totalCount > 0) {
				devMaintainList = devMaintainService.findDevMaintainList(searchMap);
			}
		}
		// list转json str
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("currentPage", Integer.parseInt(currentPage) + 1);
		resultMap.put("pageTotal", totalCount);
		
		resultMap.put("statusList", statusList);
		
		resultMap.put("searchMap", searchMap);
		resultMap.put("pageList", devMaintainList);
		resultMap.put("success", "1");

		return resultMap;
	}
	
	// 初始化添加
			@RequestMapping("/initAdd")
			@ResponseBody
			public Map<String, Object> initAdd(HttpServletRequest req, HttpServletResponse resp) throws IOException {
				String token = req.getHeader("token");
				String userId = req.getHeader("userId");
				System.out.println("token=" + token + ",userId=" + userId );

				Map<String, Object> resultMap = new HashMap<String, Object>();
				List<Map<String, Object>> areaList = new ArrayList<Map<String, Object>>();
				List<Map<String, Object>> merDevList = new ArrayList<Map<String, Object>>();
				if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {
					Map<String, Object> searchMap = new HashMap<String, Object>();
					merDevList = merDevInfoService.findMerDevInfoList(searchMap);
					areaList = areaInfoService.findAreaInfoList(searchMap);
				}
				resultMap.put("merDevList", merDevList);
				resultMap.put("areaList", areaList);
				resultMap.put("success", "1");
				return resultMap;

			}
		/**
		 * 保存添加！
		 * 
		 * @param req
		 * @return
		 */
		@RequestMapping("/add")
		@ResponseBody
		public Map<String, Object> addDevModel(HttpServletRequest req) {
			String devmaintainCode = RandomCode.getRandomCode();
			String devmaintainname = req.getParameter("addDevMaintainName");
			String merdevCode = req.getParameter("addMerDev_Code");
			String devinfCode = merDevInfoService.findDevinfCode(merdevCode);
			String areaCode = req.getParameter("addArea_Code");
			String attachpath = req.getParameter("addAttachPath");
			String contactname = req.getParameter("addContactName");
			String contacttel = req.getParameter("addContactTel");
			String faulttime = req.getParameter("addFaultTime");
			String remark = req.getParameter("addRemark");
			
			String token = req.getHeader("token");
			String userId = req.getHeader("userId");
			System.out.println(token + userId);
			Map<String, Object> searchMap = new HashMap<String, Object>();
			Map<String, Object> resultMap = new HashMap<String, Object>();
			Map<String, Object> merUserMap = new HashMap<String, Object>();
			List<Map<String, Object>> merInfoList = new ArrayList<Map<String, Object>>();
			searchMap.put("MerUser_ID", userId);
			merInfoList = merInfoService.findMerInfoList(searchMap);
			merUserMap = merInfoList.get(0);
			String orgCode = (String)merUserMap.get("Org_Code");
			String ageinfCode = (String)merUserMap.get("AgeInf_Code");
			String merinfCode = (String)merUserMap.get("MerInf_Code");
			resultMap.put("success", "0");
			if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {

				DevMaintain devMaintain = new DevMaintain();
				devMaintain.setDevmaintainCode(devmaintainCode);
				devMaintain.setDevmaintainname(devmaintainname);
				devMaintain.setMerdevCode(merdevCode);
				devMaintain.setDevinfCode(devinfCode);
				devMaintain.setOrgCode(orgCode);
				devMaintain.setAgeinfCode(ageinfCode);
				devMaintain.setMerinfCode(merinfCode);
				devMaintain.setAreaCode(areaCode);
				devMaintain.setAttachpath(attachpath);
				devMaintain.setContactname(contactname);
				devMaintain.setContacttel(contacttel);
				devMaintain.setFaulttime(faulttime);
				devMaintain.setRemark(remark);
				devMaintain.setCrtuserid(Integer.parseInt(userId));
				int success = devMaintainService.insertDevMaintain(devMaintain);
				if (success > 0) {
					resultMap.put("success", "1");
				} else {
					resultMap.put("msg", "添加失败");
				}
			} else {
				resultMap.put("msg", "请上传相关参数");
			}
			return resultMap;
		}
	// 初始化前台修改设备故障数据
	@RequestMapping("/initUpdate")
	@ResponseBody
	public Map<String, Object> initUpdate(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		String DevMaintainId = req.getParameter("DevMaintainId");
		System.out.println("token=" + token + ",userId=" + userId + ",DevInfoId=" + DevMaintainId);

		Map<String, Object> devMaintainMap = new HashMap<String, Object>();
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<Map<String, Object>> statusList = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> devMaintainList = new ArrayList<Map<String, Object>>();
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim()) && DevMaintainId != null
				&& !"".equals(DevMaintainId.trim())) {
			Map<String, Object> searchMap = new HashMap<String, Object>();
			searchMap.put("dicsCode", "DevMaintainStatus");
			statusList = dicsInfoService.findDicsList(searchMap);
			
			searchMap.put("DevMaintainId", DevMaintainId);
			System.err.println("要修改的ID"+DevMaintainId);
			devMaintainList = devMaintainService.findDevMaintainList(searchMap);
			devMaintainMap = devMaintainList.get(0);
			System.err.println("devMaintainMap="+devMaintainMap);
		}
		resultMap.put("devMaintainMap", devMaintainMap);
		resultMap.put("statusList", statusList);
		resultMap.put("success", "1");
		return resultMap;

	}

	/*
	 * 保存修改前台设备故障
	 */
	@RequestMapping("/update")
	@ResponseBody
	public Map<String, Object> saveUpdate(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String repairname = req.getParameter("updateRepairName");
		String repairbegintime = req.getParameter("updateRepairBeginTime");
		String devmaintainstatusSl = req.getParameter("updateDevMaintainStatus");
		String solveattachpath = req.getParameter("updateSolveAttachPath");
		String solveremark = req.getParameter("updateSolveRemark");
		String solvetime = req.getParameter("updateSolveTime");
		String DevMaintainId = req.getParameter("DevMaintainId");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		System.out.println("token:"+token +"userID："+ userId+"DevMaintainId:"+DevMaintainId);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {

			DevMaintain devMaintain = new DevMaintain();
			devMaintain.setDevmaintainId(Integer.parseInt(DevMaintainId));
			devMaintain.setRepairname(repairname);
			devMaintain.setRepairbegintime(repairbegintime);
			devMaintain.setDevmaintainstatusSl(devmaintainstatusSl);
			devMaintain.setSolveattachpath(solveattachpath);
			devMaintain.setSolveremark(solveremark);
			devMaintain.setSolvetime(solvetime);
			devMaintain.setUptuserid(Integer.parseInt(userId));
			int success = devMaintainService.updateDevMaintain(devMaintain);
			if (success > 0) {
				resultMap.put("success", "1");
			} else {
				resultMap.put("msg", "修改失败");
			}
		} else {
			resultMap.put("msg", "请上传相关参数");
		}
		return resultMap;
	}

	// 前台设备故障详情
	@RequestMapping("/detail")
	@ResponseBody
	public Map<String, Object> detailFront(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		return initUpdate(req,resp);
	}
	//删除故障
	@RequestMapping("/delete")
	@ResponseBody
	public Map<String, Object> delete(HttpServletRequest req, HttpServletResponse resp)  {
		String DevInfo_ID = req.getParameter("DevInfo_ID");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");

		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		if (DevInfo_ID != null && !"".equals(DevInfo_ID.trim()) && token != null && !"".equals(token.trim()) && userId != null
				&& !"".equals(userId.trim())) {
			int success = devMaintainService.delDevMaintain(Integer.parseInt(DevInfo_ID));
			if (success > 0) {
				resultMap.put("success", "1");
			} else {
				resultMap.put("msg", "删除失败!");
			}
		}
		return resultMap;
	}

}
