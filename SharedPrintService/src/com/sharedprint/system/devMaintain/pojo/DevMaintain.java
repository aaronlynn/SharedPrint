package com.sharedprint.system.devMaintain.pojo;

import java.util.Date;

public class DevMaintain {
    private Integer devmaintainId;

    private String devmaintainCode;

    private String devmaintainname;

    private String devmaintainstatusSl;

    private String merdevCode;

    private String devinfCode;

    private String orgCode;

    private String ageinfCode;

    private String merinfCode;

    private String areaCode;

    private String contactname;

    private String contacttel;

    private String faulttime;

    private String remark;

    private String attachpath;

    private String repairname;

    private String repairbegintime;

    private String solvetime;

    private String solveremark;

    private String solveattachpath;

    private Integer crtuserid;

    private Date crtdate;

    private Integer uptuserid;

    private Date lastuptdate;

    public Integer getDevmaintainId() {
        return devmaintainId;
    }

    public void setDevmaintainId(Integer devmaintainId) {
        this.devmaintainId = devmaintainId;
    }

    public String getDevmaintainCode() {
        return devmaintainCode;
    }

    public void setDevmaintainCode(String devmaintainCode) {
        this.devmaintainCode = devmaintainCode == null ? null : devmaintainCode.trim();
    }

    public String getDevmaintainname() {
        return devmaintainname;
    }

    public void setDevmaintainname(String devmaintainname) {
        this.devmaintainname = devmaintainname == null ? null : devmaintainname.trim();
    }

    public String getDevmaintainstatusSl() {
        return devmaintainstatusSl;
    }

    public void setDevmaintainstatusSl(String devmaintainstatusSl) {
        this.devmaintainstatusSl = devmaintainstatusSl == null ? null : devmaintainstatusSl.trim();
    }

    public String getMerdevCode() {
        return merdevCode;
    }

    public void setMerdevCode(String merdevCode) {
        this.merdevCode = merdevCode == null ? null : merdevCode.trim();
    }

    public String getDevinfCode() {
        return devinfCode;
    }

    public void setDevinfCode(String devinfCode) {
        this.devinfCode = devinfCode == null ? null : devinfCode.trim();
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode == null ? null : orgCode.trim();
    }

    public String getAgeinfCode() {
        return ageinfCode;
    }

    public void setAgeinfCode(String ageinfCode) {
        this.ageinfCode = ageinfCode == null ? null : ageinfCode.trim();
    }

    public String getMerinfCode() {
        return merinfCode;
    }

    public void setMerinfCode(String merinfCode) {
        this.merinfCode = merinfCode == null ? null : merinfCode.trim();
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode == null ? null : areaCode.trim();
    }

    public String getContactname() {
        return contactname;
    }

    public void setContactname(String contactname) {
        this.contactname = contactname == null ? null : contactname.trim();
    }

    public String getContacttel() {
        return contacttel;
    }

    public void setContacttel(String contacttel) {
        this.contacttel = contacttel == null ? null : contacttel.trim();
    }

    public String getFaulttime() {
        return faulttime;
    }

    public void setFaulttime(String faulttime) {
        this.faulttime = faulttime == null ? null : faulttime.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getAttachpath() {
        return attachpath;
    }

    public void setAttachpath(String attachpath) {
        this.attachpath = attachpath == null ? null : attachpath.trim();
    }

    public String getRepairname() {
        return repairname;
    }

    public void setRepairname(String repairname) {
        this.repairname = repairname == null ? null : repairname.trim();
    }

    public String getRepairbegintime() {
        return repairbegintime;
    }

    public void setRepairbegintime(String repairbegintime) {
        this.repairbegintime = repairbegintime == null ? null : repairbegintime.trim();
    }

    public String getSolvetime() {
        return solvetime;
    }

    public void setSolvetime(String solvetime) {
        this.solvetime = solvetime == null ? null : solvetime.trim();
    }

    public String getSolveremark() {
        return solveremark;
    }

    public void setSolveremark(String solveremark) {
        this.solveremark = solveremark == null ? null : solveremark.trim();
    }

    public String getSolveattachpath() {
        return solveattachpath;
    }

    public void setSolveattachpath(String solveattachpath) {
        this.solveattachpath = solveattachpath == null ? null : solveattachpath.trim();
    }

    public Integer getCrtuserid() {
        return crtuserid;
    }

    public void setCrtuserid(Integer crtuserid) {
        this.crtuserid = crtuserid;
    }

    public Date getCrtdate() {
        return crtdate;
    }

    public void setCrtdate(Date crtdate) {
        this.crtdate = crtdate;
    }

    public Integer getUptuserid() {
        return uptuserid;
    }

    public void setUptuserid(Integer uptuserid) {
        this.uptuserid = uptuserid;
    }

    public Date getLastuptdate() {
        return lastuptdate;
    }

    public void setLastuptdate(Date lastuptdate) {
        this.lastuptdate = lastuptdate;
    }
}