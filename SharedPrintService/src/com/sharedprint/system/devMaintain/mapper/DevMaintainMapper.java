package com.sharedprint.system.devMaintain.mapper;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.devMaintain.pojo.DevMaintain;

public interface DevMaintainMapper {
    int deleteByPrimaryKey(Integer devmaintainId);

    int insert(DevMaintain record);

    int insertSelective(DevMaintain record);

    DevMaintain selectByPrimaryKey(Integer devmaintainId);

    int updateByPrimaryKeySelective(DevMaintain record);

    int updateByPrimaryKey(DevMaintain record);

	List<Map<String, Object>> findDevMaintainList(Map<String, Object> searchMap);

	int findDevMaintainCount(Map<String, Object> searchMap);
}