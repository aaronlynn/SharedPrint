package com.sharedprint.system.devMaintain.service;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.devInfo.pojo.DevInfo;
import com.sharedprint.system.devMaintain.pojo.DevMaintain;

public interface DevMaintainService {

	/**
	 * 获取故障列表
	 * @param searchMap
	 * @return
	 */
	List<Map<String, Object>> findDevMaintainList(Map<String, Object> searchMap);
	
	/**
	 * 获取故障数量
	 * @param searchMap
	 * @return
	 */
	int findDevMaintainCount(Map<String, Object> searchMap);
	/**
	 * 新增设备故障
	 * @param devMaintain
	 * @return
	 */
	int insertDevMaintain(DevMaintain devMaintain);
	/**
	 * 修改故障
	 * @param devInfo
	 * @return
	 */
	int updateDevMaintain(DevMaintain devMaintain);
	/**
	 * 删除故障
	 * @param DevInfoID
	 * @return
	 */
	int delDevMaintain(int DevMaintainID);

	
}
