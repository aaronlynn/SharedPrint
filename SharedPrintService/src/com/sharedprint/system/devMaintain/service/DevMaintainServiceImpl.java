package com.sharedprint.system.devMaintain.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sharedprint.system.devInfo.mapper.DevInfoMapper;
import com.sharedprint.system.devInfo.pojo.DevInfo;
import com.sharedprint.system.devMaintain.mapper.DevMaintainMapper;
import com.sharedprint.system.devMaintain.pojo.DevMaintain;

@Service
public class DevMaintainServiceImpl implements DevMaintainService {

	@Autowired
	private DevMaintainMapper devMaintainMapper;
	
	@Override
	public List<Map<String, Object>> findDevMaintainList(Map<String, Object> searchMap) {
		return devMaintainMapper.findDevMaintainList(searchMap);
	}

	@Override
	public int findDevMaintainCount(Map<String, Object> searchMap) {
		
		return devMaintainMapper.findDevMaintainCount(searchMap);
	}


	@Override
	public int updateDevMaintain(DevMaintain devMaintain) {
	
		return devMaintainMapper.updateByPrimaryKeySelective(devMaintain);
	}

	@Override
	public int delDevMaintain(int DevMaintainId) {
		return devMaintainMapper.deleteByPrimaryKey(DevMaintainId);
	}

	@Override
	public int insertDevMaintain(DevMaintain devMaintain) {
		return devMaintainMapper.insertSelective( devMaintain);
	}



}
