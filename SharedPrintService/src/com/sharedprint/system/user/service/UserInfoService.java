package com.sharedprint.system.user.service;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.user.pojo.UserInfo;

/**
 * 用户的服务端的接口
 * 
 * @author Administrator
 *
 */
public interface UserInfoService {

	/**
	 * 获取用户数量
	 * 
	 * @param searchMap
	 * @return
	 */
	public int findCount(Map<String, Object> searchMap);

	/**
	 * 获取列表
	 * 
	 * @param searchMap
	 * @return
	 */
	public List<Map<String, Object>> findUserList(Map<String, Object> searchMap);

	/**
	 * 保存信息
	 * 
	 * @param areaInfo
	 * @return
	 */
	public int saveInfo(UserInfo userInfo);

	/**
	 * 获取分页列表
	 * 
	 * @param searchMap
	 * @return
	 */
	public List<Map<String, Object>> findPageList(Map<String, Object> searchMap);

	/**
	 * 修改用户信息（重置密码）
	 * 
	 * @param userInfo
	 * @return
	 */
	public int updateInfo(UserInfo userInfo);

	/**
	 * 删除用户信息
	 * 
	 * @param searchMap
	 * @return
	 */
	public int deleteInfo(Map<String, Object> searchMap);

	/**
	 * 重置密码
	 * 
	 * @param searchMap
	 * @return
	 */
	public int resetInfo(Map<String, Object> searchMap);
}
