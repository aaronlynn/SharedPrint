package com.sharedprint.system.user.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sharedprint.system.user.mapper.UserInfoMapper;
import com.sharedprint.system.user.pojo.UserInfo;


/**
 * 	用户的服务端的实现类
 * @author Administrator
 *
 */
@Service
@Transactional
public class UserInfoServiceImpl implements UserInfoService {
	@Autowired
	private UserInfoMapper userInfoMapper ;
	
	@Override
	public int findCount(Map<String, Object> searchMap) {
		return userInfoMapper.findCount(searchMap);
	}

	@Override
	public List<Map<String, Object>> findUserList(Map<String, Object> searchMap) {
		return userInfoMapper.findUserList(searchMap);
	}

	@Override
	public int saveInfo(UserInfo userInfo) {
		return userInfoMapper.insert(userInfo);
	}

	@Override
	public List<Map<String, Object>> findPageList(Map<String, Object> searchMap) {
		return userInfoMapper.findPageList(searchMap);
	}

	@Override
	public int updateInfo(UserInfo userInfo) {
		return userInfoMapper.updateInfo(userInfo);
	}

	@Override
	public int deleteInfo(Map<String, Object> searchMap) {
		return userInfoMapper.deleteInfo(searchMap);
	}

	@Override
	public int resetInfo(Map<String, Object> searchMap) {
		return userInfoMapper.resetInfo(searchMap);
	}

}
