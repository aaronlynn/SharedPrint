package com.sharedprint.system.user.mapper;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.user.pojo.UserInfo;

public interface UserInfoMapper {
	int deleteByPrimaryKey(Integer userId);

	int insertSelective(UserInfo record);

	UserInfo selectByPrimaryKey(Integer userId);

	int updateByPrimaryKeySelective(UserInfo record);

	int updateByPrimaryKey(UserInfo record);

	/**
	 * 获取条数
	 * 
	 * @param searchMap
	 * @return
	 */
	int findCount(Map<String, Object> searchMap);

	/**
	 * 获取列表
	 * 
	 * @param searchMap
	 * @return
	 */
	List<Map<String, Object>> findUserList(Map<String, Object> searchMap);

	/**
	 * 保存信息
	 * 
	 * @param userInfo
	 * @return
	 */
	int insert(UserInfo record);

	/**
	 * 获取分页列表
	 * 
	 * @param searchMap
	 * @return
	 */
	List<Map<String, Object>> findPageList(Map<String, Object> searchMap);

	/**
	 * 修改信息
	 * 
	 * @param userInfo
	 * @return
	 */
	int updateInfo(UserInfo userInfo);

	/**
	 * 删除信息
	 * 
	 * @param searchMap
	 * @return
	 */
	int deleteInfo(Map<String, Object> searchMap);

	/**
	 * 重置密码
	 * 
	 * @param searchMap
	 * @return
	 */
	int resetInfo(Map<String, Object> searchMap);
}