package com.sharedprint.system.area.service;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.area.pojo.AreaInfo;


public interface AreaInfoService {

	/**
	 * 获取地区列表
	 * @param searchMap
	 * @return
	 */
	List<Map<String, Object>> findAreaInfoList(Map<String, Object> searchMap);
	
	/**
	 * 获取地区列表数量
	 * @param searchMap
	 * @return
	 */
	int findAreaInfoCount(Map<String, Object> searchMap);

	/**
	 * 新增地区
	 * @param menuInfo
	 * @return
	 */
	int insertAreaInfo(AreaInfo areaInfo);
	/**
	 * 更新地区
	 * @param menuInfo
	 * @return
	 */
	int updateAreaInfo(AreaInfo areaInfo);
	/**
	 * 删除地区
	 * @param menuInfo
	 * @return
	 */
	int delAreaInfo(int areaId);
}
