package com.sharedprint.system.area.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sharedprint.system.area.mapper.AreaInfoMapper;
import com.sharedprint.system.area.pojo.AreaInfo;

@Service
public class AreaInfoServiceImpl implements AreaInfoService {

	@Autowired
	private AreaInfoMapper areaInfoMapper;
	
	@Override
	public List<Map<String, Object>> findAreaInfoList(Map<String, Object> searchMap) {
		
		return areaInfoMapper.findAreaList(searchMap);
	}

	@Override
	public int findAreaInfoCount(Map<String, Object> searchMap) {
		
		return areaInfoMapper.findAreaCount(searchMap);
	}

	@Override
	public int insertAreaInfo(AreaInfo areaInfo) {
		
		return areaInfoMapper.insertSelective(areaInfo);
	}

	@Override
	public int updateAreaInfo(AreaInfo areaInfo) {
		
		return areaInfoMapper.updateByPrimaryKeySelective(areaInfo);
	}

	@Override
	public int delAreaInfo(int areaId) {
		
		return areaInfoMapper.deleteByPrimaryKey(areaId);
	}

}
