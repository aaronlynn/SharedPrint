package com.sharedprint.system.area.mapper;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.area.pojo.AreaInfo;

public interface AreaInfoMapper {
    int deleteByPrimaryKey(Integer areaId);

    int insert(AreaInfo record);

    int insertSelective(AreaInfo record);

    AreaInfo selectByPrimaryKey(Integer areaId);

    int updateByPrimaryKeySelective(AreaInfo record);

    int updateByPrimaryKey(AreaInfo record);
    
    /**
	 * 获取地区列表
	 * @param searchMap
	 * @return
	 */
	public List<Map<String,Object>> findAreaList(Map<String,Object> searchMap);
	
	/**
	 * 获取地区条数
	 * @param searchMap
	 * @return
	 */
	public int findAreaCount(Map<String,Object> searchMap);
}