package com.sharedprint.system.area.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sharedprint.system.area.pojo.AreaInfo;
import com.sharedprint.system.area.service.AreaInfoService;

/**
 * 地区管理的控制器
 */
@Controller
@RequestMapping("/api/areaInfo")
public class AreaInfoController {

	@Autowired
	private AreaInfoService areaInfoService;

	private Date date = new Date();
	private SimpleDateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd :HH:mm:ss");
	/**
	 * 获取地区列表数据
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/queryAreaList")
	@ResponseBody
	public Map<String, Object> queryMenuList(HttpServletRequest req) {
		String areaCode = req.getParameter("AreaCode");
		String areaNameLike = req.getParameter("AreaNameLike");

		String currentPage = req.getParameter("currentPage");
		String pageSize = req.getParameter("pageSize");
		String orderByValue = req.getParameter("orderByValue");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		List<Map<String, Object>> menuList = new ArrayList<Map<String, Object>>();
		int totalCount = 0;
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {
			if (currentPage == null || "".equals(currentPage.trim())) {
				currentPage = "0";
			}
			if (pageSize == null || "".equals(pageSize.trim())) {
				pageSize = "10";
			}
			Map<String, Object> searchMap = new HashMap<String, Object>();
			int currentpages = 0;
			if ((currentPage != null || !"".equals(currentPage.trim()))) {
				currentpages = Integer.parseInt(currentPage);
				currentpages = currentpages * Integer.parseInt(pageSize);
			}

			searchMap.put("currentPage", currentpages);
			searchMap.put("pageSize", Integer.parseInt(pageSize));
			searchMap.put("areaCode", areaCode);
			searchMap.put("areaName", areaNameLike);

			totalCount = areaInfoService.findAreaInfoCount(searchMap);// 总记录数
			if (totalCount > 0) {
				if (orderByValue == null || "".equals(orderByValue.trim())) {
					searchMap.put("orderByAreaIdDesc", "1");
				}
				menuList = areaInfoService.findAreaInfoList(searchMap);
			}
		}
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("currentPage", Integer.parseInt(currentPage) + 1);
		resultMap.put("pageTotal", totalCount);
		resultMap.put("pageList", menuList);
		resultMap.put("success", "1");

		return resultMap;
	}

	/**
	 * 验证地区名称是否存在！
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/validateAreaName")
	@ResponseBody
	public boolean validateAreaName(HttpServletRequest req) {
		String addAreaName = req.getParameter("addAreaName");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		String excludeAreaId = req.getParameter("excludeAreaId");
		System.out.println("token=" + token + ",userId=" + userId + ",addAreaName=" + addAreaName);
		boolean result = false;
		if (addAreaName != null && !"".equals(addAreaName.trim())) {
			Map<String, Object> searchMap = new HashMap<String, Object>();
			searchMap.put("addAreaName", addAreaName);
			if (excludeAreaId != null && !"".equals(excludeAreaId.trim())) {
				searchMap.put("excludeAreaId", excludeAreaId);
			}
			int count = areaInfoService.findAreaInfoCount(searchMap);
			if (count == 0) {
				result = true;
			}
		}
		return result;
	}
	/**
	 * 验证地区编码是否存在！
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/validateAreaCode")
	@ResponseBody
	public boolean validateAreaCode(HttpServletRequest req) {
		String addAreaCode = req.getParameter("addAreaCode");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		String excludeAreaId = req.getParameter("excludeAreaId");
		System.out.println("token=" + token + ",userId=" + userId + ",addAreaCode=" + addAreaCode);
		boolean result = false;
		if (addAreaCode != null && !"".equals(addAreaCode.trim())) {
			Map<String, Object> searchMap = new HashMap<String, Object>();
			searchMap.put("areaCode", addAreaCode);
			if (excludeAreaId != null && !"".equals(excludeAreaId.trim())) {
				searchMap.put("excludeAreaId", excludeAreaId);
			}
			int count = areaInfoService.findAreaInfoCount(searchMap);
			if (count == 0) {
				result = true;
			}
		}
		return result;
	}

	/**
	 * 保存新增菜单！
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/saveAddArea")
	@ResponseBody
	public Map<String, Object> addMenu(HttpServletRequest req) {
		String addAreaCode = req.getParameter("addAreaCode");
		String addAreaName = req.getParameter("addAreaName");
		String addAreaParentCode = req.getParameter("addAreaParentCode");
		String sortNo = req.getParameter("addSortNo");
		String addDescription = req.getParameter("addDescription");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		System.out.println(token + userId);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {

			AreaInfo areaInfo = new AreaInfo();
			areaInfo.setAreaCode(addAreaCode);
			areaInfo.setAreaName(addAreaName);
			areaInfo.setAreaParentCode(addAreaParentCode);
			areaInfo.setSortNo(Integer.parseInt(sortNo));
			areaInfo.setDescription(addDescription);
			areaInfo.setOprDate(dateFormat.format(date));
			areaInfo.setOprId(Integer.parseInt(userId));
			int success = areaInfoService.insertAreaInfo(areaInfo);
			if (success > 0) {
				resultMap.put("success", "1");
			} else {
				resultMap.put("msg", "保存失败");
			}
		} else {
			resultMap.put("msg", "请上传相关参数");
		}
		return resultMap;
	}

	// 初始化前台修改地区数据
	@RequestMapping("/initUpdate")
	@ResponseBody
	public Map<String, Object> initUpdate(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		String areaId = req.getParameter("AreaId");
		System.out.println("token=" + token + ",userId=" + userId + ",areaId=" + areaId);

		Map<String, Object> areaMap = new HashMap<String, Object>();
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<Map<String, Object>> areaList = new ArrayList<Map<String, Object>>();
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim()) && areaId != null
				&& !"".equals(areaId.trim())) {
			Map<String, Object> searchMap = new HashMap<String, Object>();
			searchMap.put("areaId", areaId);
			areaList = areaInfoService.findAreaInfoList(searchMap);
			areaMap = areaList.get(0);
			Map<String, Object> parentMap = new HashMap<String, Object>();
			areaList = areaInfoService.findAreaInfoList(parentMap);
		}
		resultMap.put("areaParentList", areaList);
		resultMap.put("areaMap", areaMap);
		resultMap.put("success", "1");
		return resultMap;

	}

	/*
	 * 更新并保存前台地区数据
	 */
	@RequestMapping("/saveUpdate")
	@ResponseBody
	public Map<String, Object> saveUpdate(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		
		String updateAreaCode = req.getParameter("updateAreaCode");
		String updateAreaName = req.getParameter("updateAreaName");
		String updateAreaId = req.getParameter("updateAreaId");
		String updateAreaParentCode = req.getParameter("updateAreaParentCode");
		String sortNo = req.getParameter("updateSortCode");
		String updateDescription = req.getParameter("updateDescription");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		System.out.println("token:"+token +"userID"+ userId+"updateAreaId:"+updateAreaId);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {

			AreaInfo areaInfo = new AreaInfo();
			areaInfo.setAreaId(Integer.parseInt(updateAreaId));
			areaInfo.setAreaCode(updateAreaCode);
			areaInfo.setAreaName(updateAreaName);
			areaInfo.setAreaParentCode(updateAreaParentCode);
			areaInfo.setDescription(updateDescription);
			areaInfo.setSortNo(Integer.parseInt(sortNo));
			areaInfo.setOprDate(dateFormat.format(date));
			areaInfo.setOprId(Integer.parseInt(userId));
			int success = areaInfoService.updateAreaInfo(areaInfo);
			if (success > 0) {
				resultMap.put("success", "1");
			} else {
				resultMap.put("msg", "保存失败");
			}
		} else {
			resultMap.put("msg", "请上传相关参数");
		}
		return resultMap;
	}

	// 前台地区详情
	@RequestMapping("/detail")
	@ResponseBody
	public Map<String, Object> detailFront(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		String areaId = req.getParameter("areaId");

		Map<String, Object> areaMap = new HashMap<String, Object>();
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<Map<String, Object>> areaList = new ArrayList<Map<String, Object>>();
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {
			Map<String, Object> searchMap = new HashMap<String, Object>();
			searchMap.put("areaId", areaId);
			areaList = areaInfoService.findAreaInfoList(searchMap);
			areaMap = areaList.get(0);
		}
		resultMap.put("areaMap", areaMap);
		resultMap.put("success", "1");
		return resultMap;

	}

	// 删除前台菜单数据
	@RequestMapping("/delete")
	@ResponseBody
	public Map<String, Object> delete(HttpServletRequest req, HttpServletResponse resp)  {
		String areaId = req.getParameter("areaId");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");

		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		if (areaId != null && !"".equals(areaId.trim()) && token != null && !"".equals(token.trim()) && userId != null
				&& !"".equals(userId.trim())) {
			 //判断是否有下级
			int success = areaInfoService.delAreaInfo(Integer.parseInt(areaId));
			if (success > 0) {
				resultMap.put("success", "1");
			} else {
				resultMap.put("msg", "该菜单存在子菜单,不允许删除!");
			}
		}
		return resultMap;
	}
}
