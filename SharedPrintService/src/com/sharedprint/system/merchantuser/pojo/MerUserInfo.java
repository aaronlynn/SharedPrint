package com.sharedprint.system.merchantuser.pojo;

import java.util.Date;

public class MerUserInfo {
    private Integer meruserId;

    private String meruserCode;

    private String merusername;

    private String merinfCode;

    private String loginname;

    private String loginpwd;

    private String meruserstatus;

    private String pwdquestion;

    private String pwdanswer;

    private String initpwdflag;

    private String firstloginflag;

    private Integer oprId;

    private Date oprDate;

    public Integer getMeruserId() {
        return meruserId;
    }

    public void setMeruserId(Integer meruserId) {
        this.meruserId = meruserId;
    }

    public String getMeruserCode() {
        return meruserCode;
    }

    public void setMeruserCode(String meruserCode) {
        this.meruserCode = meruserCode == null ? null : meruserCode.trim();
    }

    public String getMerusername() {
        return merusername;
    }

    public void setMerusername(String merusername) {
        this.merusername = merusername == null ? null : merusername.trim();
    }

    public String getMerinfCode() {
        return merinfCode;
    }

    public void setMerinfCode(String merinfCode) {
        this.merinfCode = merinfCode == null ? null : merinfCode.trim();
    }

    public String getLoginname() {
        return loginname;
    }

    public void setLoginname(String loginname) {
        this.loginname = loginname == null ? null : loginname.trim();
    }

    public String getLoginpwd() {
        return loginpwd;
    }

    public void setLoginpwd(String loginpwd) {
        this.loginpwd = loginpwd == null ? null : loginpwd.trim();
    }

    public String getMeruserstatus() {
        return meruserstatus;
    }

    public void setMeruserstatus(String meruserstatus) {
        this.meruserstatus = meruserstatus == null ? null : meruserstatus.trim();
    }

    public String getPwdquestion() {
        return pwdquestion;
    }

    public void setPwdquestion(String pwdquestion) {
        this.pwdquestion = pwdquestion == null ? null : pwdquestion.trim();
    }

    public String getPwdanswer() {
        return pwdanswer;
    }

    public void setPwdanswer(String pwdanswer) {
        this.pwdanswer = pwdanswer == null ? null : pwdanswer.trim();
    }

    public String getInitpwdflag() {
        return initpwdflag;
    }

    public void setInitpwdflag(String initpwdflag) {
        this.initpwdflag = initpwdflag == null ? null : initpwdflag.trim();
    }

    public String getFirstloginflag() {
        return firstloginflag;
    }

    public void setFirstloginflag(String firstloginflag) {
        this.firstloginflag = firstloginflag == null ? null : firstloginflag.trim();
    }

    public Integer getOprId() {
        return oprId;
    }

    public void setOprId(Integer oprId) {
        this.oprId = oprId;
    }

    public Date getOprDate() {
        return oprDate;
    }

    public void setOprDate(Date oprDate) {
        this.oprDate = oprDate;
    }
}