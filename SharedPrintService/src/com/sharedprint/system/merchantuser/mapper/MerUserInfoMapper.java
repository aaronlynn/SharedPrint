package com.sharedprint.system.merchantuser.mapper;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.merchantuser.pojo.MerUserInfo;

public interface MerUserInfoMapper {
    int deleteByPrimaryKey(Integer meruserId);

    int insert(MerUserInfo record);

    int insertSelective(MerUserInfo record);

    MerUserInfo selectByPrimaryKey(Integer meruserId);

    int updateByPrimaryKeySelective(MerUserInfo record);

    int updateByPrimaryKey(MerUserInfo record);
    
    /**
	 * 商户用户查询
	 * 
	 * @param searchMap
	 * @return
	 */
	List<Map<String, Object>> findMerUserInfoList(Map<String, Object> searchMap);
	
	/**
	 * 商户用户数量查询
	 * 
	 * @param searchMap
	 * @return
	 */
	int findMerUserInfoCount(Map<String, Object> searchMap);
}