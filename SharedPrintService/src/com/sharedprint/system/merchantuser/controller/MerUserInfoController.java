package com.sharedprint.system.merchantuser.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sharedprint.dics.service.DicsInfoService;
import com.sharedprint.system.agent.service.AgentInfoService;
import com.sharedprint.system.merchant.pojo.MerInfo;
import com.sharedprint.system.merchant.service.MerInfoService;
import com.sharedprint.system.merchantuser.pojo.MerUserInfo;
import com.sharedprint.system.merchantuser.service.MerUserService;

@Controller
@RequestMapping("/api/merUserInfo")
public class MerUserInfoController {
	
	// 代理商服务端
	@Autowired
	private AgentInfoService agentInfoService;
	
	@Autowired
	MerUserService merUserService;
	
	//商户信息服务端
	@Autowired
	MerInfoService merInfoService;
	
	//数据字典服务端
	@Autowired
	DicsInfoService dicsInfoSercice;
	
	// 获取代理商号
	private String getAgentCode(HttpServletRequest req) {
		String loginName = req.getParameter("loginName"); // 代理商登录账号
		String agentCode = null;// 代理商号
		Map<String, Object> searchMap = new HashMap<String, Object>();
		// 获取登录的代理商编号
		if (loginName != null && loginName.trim() != "") {
			searchMap.put("loginName", loginName);
			List<Map<String, Object>> agentList = agentInfoService.findAgentInfoList(searchMap);
			agentCode = agentList.get(0).get("AgeInf_Code").toString();
		}
		return agentCode;
	}
	
	@RequestMapping("/queryPageList")
	@ResponseBody
	private Map<String, Object> findMerUserInfoList(HttpServletRequest req){		
		//查询条件
		String queryName = req.getParameter("queryName"); //用户名称或登录名查询
		String queryMerUserStatus = req.getParameter("queryMerUserStatus");
		String queryMerInfNameLike = req.getParameter("queryMerInfNameLike");
		//查询条件
		String pageSize = req.getParameter("pageSize");
		String currentPage = req.getParameter("currentPage");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		String agentCode = getAgentCode(req); //代理商号
		
		System.out.println("token=" + token + ",userId="+ userId);
		Map<String, Object> searchMap = new HashMap<String, Object>();
		if (agentCode !=null) {
			searchMap.put("AgeInfCode", agentCode);
		}
		
		List<Map<String, Object>> merInfo = null;
		if (queryMerInfNameLike !=null && queryMerInfNameLike.trim() != "") {
			searchMap.put("queryMerInfNameLike", queryMerInfNameLike);
			merInfo = merInfoService.findMerInfoList(searchMap);
		}
		List<Map<String, Object>> agentMerUserList = new ArrayList<Map<String,Object>>();	//登录的代理商所属的商户用户
		List<Map<String, Object>> merInfoList = merInfoService.findMerInfoList(searchMap);
		List<Map<String, Object>> dicsInfoList = dicsInfoSercice.findDicsList(searchMap);
		searchMap.put("queryName", queryName);
		searchMap.put("queryMerUserStatus", queryMerUserStatus);
		if (merInfo !=null) {
			searchMap.put("queryMerInfCode", merInfo.get(0).get("MerInf_Code"));
		}			
		searchMap.put("pageSize", Integer.parseInt(pageSize));
		int currentRecord = Integer.parseInt(currentPage)*Integer.parseInt(pageSize);
		searchMap.put("currentRecord", currentRecord);
		System.out.println("pageSize="+pageSize+",currentRecord="+currentRecord);
		List<Map<String, Object>> merUserList = merUserService.findMerUserInfoList(searchMap);		
		for (Map<String, Object> map : merUserList) {
			if (map.get("Opr_Date")!=null) {
				String oprDate = map.get("Opr_Date").toString();
				map.put("oprDate", oprDate);
			}
			for (Map<String, Object> map2 : merInfoList) {
				if (map.get("MerInf_Code").equals(map2.get("MerInf_Code"))) {
					map.put("merInfName", map2.get("MerInfName"));
					//System.out.println(map2.get("MerInfName"));
				}
			}
			for (Map<String, Object> map3 : dicsInfoList) {
				if (map3.get("dics_code").equals("AgeUserStatus")) {
					if (map.get("MerUserStatus").equals(map3.get("key_value"))) {
						map.put("merUserStatus", map3.get("key_name"));
						//.out.println(map3.get("merUserStatus"));
					}
				}				
			}
			for (Map<String, Object> map4 : merInfoList) {
				if (map.get("MerInf_Code").equals(map4.get("MerInf_Code"))) {
					agentMerUserList.add(map);//登录的代理商下所属的商户用户
				}
			}
		}
		int totalCount = merUserService.findMerUserInfoCount(searchMap);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		if(merUserList.size() > 0){
			resultMap.put("success", "1");			
			resultMap.put("listDataGrid", merUserList);
			resultMap.put("agentMerUserList", agentMerUserList);
			resultMap.put("pageTotal", totalCount);
		}
		System.out.println("merUserListMap="+resultMap);
		return resultMap;
	}
	
	@RequestMapping("/resetPwd")
	@ResponseBody
	private Map<String, Object> resetMerUserPwd(HttpServletRequest req){
		String merUserInfId = req.getParameter("merUserInfId");
		Map<String, Object> searchMap = new HashMap<String, Object>();
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<Map<String, Object>> dicsInfoList = dicsInfoSercice.findDicsList(searchMap);
		int success=0;
		if (merUserInfId !=null) {
			MerUserInfo merUserInfo = new MerUserInfo();
			merUserInfo.setMeruserId(Integer.parseInt(merUserInfId));
			for (Map<String, Object> map : dicsInfoList) {
				if (map.get("dics_code").equals("MerUserDefault")) {
					merUserInfo.setLoginpwd(map.get("key_value").toString());
				}
			}
			success =merUserService.updateMerUserInfo(merUserInfo);
		}
		if (success >0) {
			resultMap.put("success", "1");
		}else {
			resultMap.put("success", "0");
			resultMap.put("msg", "系统繁忙，重置密码失败！");
		}
		return resultMap;
	}
	
	@RequestMapping("/detail")
	@ResponseBody
	private Map<String, Object> detailMerUserInfo(HttpServletRequest req) {
		String merUserInfId = req.getParameter("merUserInfId");
		System.out.println("merUserInfId="+merUserInfId);
		Map<String, Object> searchMap = new HashMap<String, Object>();
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<Map<String, Object>> merInfoList = merInfoService.findMerInfoList(searchMap);
		List<Map<String, Object>> dicsInfoList = dicsInfoSercice.findDicsList(searchMap);
		MerUserInfo merUserInfo =null;
		if (merUserInfId != null) {
			merUserInfo = merUserService.findMerUserInfoDetail(Integer.parseInt(merUserInfId));
			for (Map<String, Object> map : dicsInfoList) {
				if (map.get("dics_code").equals("InitPwdFlag")) {
					if (merUserInfo.getInitpwdflag().equals(map.get("key_value").toString())) {
						resultMap.put("initPwdFlag", map.get("key_name"));
					}
				}else if (map.get("dics_code").equals("AgeUserStatus")) {
					if (merUserInfo.getMeruserstatus().equals(map.get("key_value").toString())) {
						resultMap.put("merUserStatus", map.get("key_name"));
					}
				}
			}
			for (Map<String, Object> map2 : merInfoList) {
				if (merUserInfo.getMerinfCode().equals(map2.get("MerInf_Code").toString())) {
					resultMap.put("merInfName", map2.get("MerInfName"));
					//System.out.println(map2.get("MerInfName"));
				}
			}
		}
		System.out.println("merUserInfo="+merUserInfo);
		if (merUserInfo !=null) {
			resultMap.put("success", "1");
			resultMap.put("merUserInfo", merUserInfo);
		}else {
			resultMap.put("success", "0");
			resultMap.put("msg", "系统繁忙，查询详情失败！");
		}
		return resultMap;
	}
	
	/**
	 * 检查原密码是否正确
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/checkOriPwd")
	@ResponseBody
	private boolean checkPwd(HttpServletRequest req) {
		String merUserId = req.getParameter("merUserId");
		String originalPwd = req.getParameter("originalPwd");
		System.err.println("originalPwd="+originalPwd+",merUserId="+merUserId);
		if (merUserId !=null && originalPwd !=null) {
			MerUserInfo merUserInfo = merUserService.findMerUserInfoDetail(Integer.parseInt(merUserId));
			if (originalPwd.equals(merUserInfo.getLoginpwd())) {
				System.err.println("1111");
				return true;
			}else {
				System.err.println("222");
				return false;
			}
		}else {
			System.err.println("3333");
			return false;
		}		
	}
	
	@RequestMapping("/updatePwd")
	@ResponseBody
	private Map<String, Object> updateMerUserPWd(HttpServletRequest req){
		String merUserId = req.getParameter("merUserId");
		String merUserNewPwd = req.getParameter("updateNewPwd");
		String deteminePwd = req.getParameter("determinePwd");
		Map<String, Object> resultMap = new HashMap<String, Object>();
		if (merUserNewPwd.equals(deteminePwd)) {
			MerUserInfo merUserInfo =new MerUserInfo();
			merUserInfo.setLoginpwd(merUserNewPwd);
			merUserInfo.setMeruserId(Integer.parseInt(merUserId));
			int success = merUserService.updateMerUserInfo(merUserInfo);
			if (success >0) {
				resultMap.put("success", "1");
			}else {
				resultMap.put("msg", "系统繁忙！请稍后再试");
			}
		}else {
			resultMap.put("success", "2");
			resultMap.put("msg", "两次输入的新密码不一致，请重新输入！");
		}
		return resultMap;
	}
}
