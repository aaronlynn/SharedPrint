package com.sharedprint.system.merchantuser.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sharedprint.system.merchantuser.mapper.MerUserInfoMapper;
import com.sharedprint.system.merchantuser.pojo.MerUserInfo;

@Service
public class MerUserServiceImpl implements MerUserService {

	@Autowired
	private MerUserInfoMapper merUserInfoMapper;
	
	@Override
	public int insertMerUser(MerUserInfo merUserInfo) {
		// TODO Auto-generated method stub
		return merUserInfoMapper.insertSelective(merUserInfo);
	}

	@Override
	public List<Map<String, Object>> findMerUserInfoList(Map<String, Object> searchMap) {
		// TODO Auto-generated method stub
		return merUserInfoMapper.findMerUserInfoList(searchMap);
	}

	@Override
	public int findMerUserInfoCount(Map<String, Object> searchMap) {
		// TODO Auto-generated method stub
		return merUserInfoMapper.findMerUserInfoCount(searchMap);
	}

	@Override
	public int updateMerUserInfo(MerUserInfo merUserInfo) {
		// TODO Auto-generated method stub
		return merUserInfoMapper.updateByPrimaryKeySelective(merUserInfo);
	}

	@Override
	public MerUserInfo findMerUserInfoDetail(int merUserInfId) {
		// TODO Auto-generated method stub
		return merUserInfoMapper.selectByPrimaryKey(merUserInfId);
	}


}
