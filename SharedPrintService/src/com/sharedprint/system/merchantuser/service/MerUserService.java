package com.sharedprint.system.merchantuser.service;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.merchantuser.pojo.MerUserInfo;

public interface MerUserService {
	/**
	 * 新增商户用户
	 * 
	 * @param merUserInfo
	 * @return
	 */
	int insertMerUser(MerUserInfo merUserInfo);
	
	/**
	 * 商户用户查询
	 * 
	 * @param searchMap
	 * @return
	 */
	List<Map<String, Object>> findMerUserInfoList(Map<String, Object> searchMap);
	
	/**
	 * 商户用户数量查询
	 * 
	 * @param searchMap
	 * @return
	 */
	int findMerUserInfoCount(Map<String, Object> searchMap);
	
	/**
	 * 修改商户用户信息
	 * 
	 * @param merUserInfo
	 * @return
	 */
	int updateMerUserInfo(MerUserInfo merUserInfo);
	
	/**
	 * 查看商户用户详情
	 * 
	 * @param merUserInfId
	 * @return
	 */
	MerUserInfo findMerUserInfoDetail(int merUserInfId);
}
