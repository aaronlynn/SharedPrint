package com.sharedprint.system.menu.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sharedprint.system.menu.mapper.AgentMenuInfoMapper;
import com.sharedprint.system.menu.mapper.MerchantMenuInfoMapper;
import com.sharedprint.system.menu.pojo.MenuInfo;
import com.sharedprint.system.menu.service.MenuInfoService;

/**
 * 菜单管理的控制器
 * 
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/api/menuInfo")
public class MenuInfoController {

	@Autowired
	private MenuInfoService menuInfoService;
	
	@Autowired
	private AgentMenuInfoMapper AgentMenuInfoService;

	@Autowired
	private MerchantMenuInfoMapper merchantMenuInfoService;

	
	/**
	 * 初始化商户系统左边菜单栏
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/queryMerchantLeftMenuList")
	@ResponseBody
	public Map<String, Object> queryMerchantLeftMenuList(HttpServletRequest request) {
		Map<String, Object> searchMap = new HashMap<String, Object>();
		String token = request.getHeader("token");
		String userId = request.getHeader("userId");
		System.out.println("token=" + token + ",userId=" + userId);
		List<Map<String, Object>> menuList = merchantMenuInfoService.findMenuList(searchMap);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", "1");
		resultMap.put("menuList", menuList);
		return resultMap;
	}
	/**
	 * 初始化代理商系统左边菜单栏
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/queryAgentLeftMenuList")
	@ResponseBody
	public Map<String, Object> queryAgentLeftMenuList(HttpServletRequest request) {
		Map<String, Object> searchMap = new HashMap<String, Object>();
		String token = request.getHeader("token");
		String userId = request.getHeader("userId");
		System.out.println("token=" + token + ",userId=" + userId);
		List<Map<String, Object>> menuList = AgentMenuInfoService.findMenuList(searchMap);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", "1");
		resultMap.put("menuList", menuList);
		return resultMap;
	}

	
	
	/**
	 * 初始化代理商管理系统左边菜单栏
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/queryLeftMenuList")
	@ResponseBody
	public Map<String, Object> queryLeftMenuList(HttpServletRequest request) {
		Map<String, Object> searchMap = new HashMap<String, Object>();
		String token = request.getHeader("token");
		String userId = request.getHeader("userId");
		System.out.println("token=" + token + ",userId=" + userId);
		List<Map<String, Object>> menuList = menuInfoService.findMenuInfoList(searchMap);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", "1");
		resultMap.put("menuList", menuList);
		return resultMap;
	}

	/**
	 * 获取菜单列表数据
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/queryMenuList")
	@ResponseBody
	public Map<String, Object> queryMenuList(HttpServletRequest req) {
		String menuCode = req.getParameter("menuCode");
		String menuNameLike = req.getParameter("menuNameLike");

		String currentPage = req.getParameter("currentPage");
		String pageSize = req.getParameter("pageSize");
		String orderByValue = req.getParameter("orderByValue");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		List<Map<String, Object>> menuList = new ArrayList<Map<String, Object>>();
		int totalCount = 0;
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {
			if (currentPage == null || "".equals(currentPage.trim())) {
				currentPage = "0";
			}
			if (pageSize == null || "".equals(pageSize.trim())) {
				pageSize = "10";
			}
			Map<String, Object> searchMap = new HashMap<String, Object>();
			int currentpages = 0;
			if ((currentPage != null || !"".equals(currentPage.trim()))) {
				currentpages = Integer.parseInt(currentPage);
				currentpages = currentpages * Integer.parseInt(pageSize);
			}

			searchMap.put("currentPage", currentpages);
			searchMap.put("pageSize", Integer.parseInt(pageSize));
			searchMap.put("menuId", menuCode);
			searchMap.put("menuName", menuNameLike);

			totalCount = menuInfoService.findMenuInfoCount(searchMap);// 总记录数
			if (totalCount > 0) {
				if (orderByValue == null || "".equals(orderByValue.trim())) {
					searchMap.put("orderByAreaIdDesc", "1");
				}
				menuList = menuInfoService.findMenuInfoList(searchMap);
			}
		}
		// list转json str
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("currentPage", Integer.parseInt(currentPage) + 1);
		resultMap.put("pageTotal", totalCount);
		resultMap.put("pageList", menuList);
		resultMap.put("success", "1");

		return resultMap;
	}

	/**
	 * 验证菜单名称是否存在！
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/validateMenu")
	@ResponseBody
	public boolean validateMenu(HttpServletRequest req) {
		String addMenuName = req.getParameter("addMenuName");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		String excludeAreaId = req.getParameter("excludeAreaId");
		System.out.println("token=" + token + ",userId=" + userId + ",addMenuName=" + addMenuName);
		boolean result = false;
		if (addMenuName != null && !"".equals(addMenuName.trim())) {
			Map<String, Object> searchMap = new HashMap<String, Object>();
			searchMap.put("addMenuName", addMenuName);
			if (excludeAreaId != null && !"".equals(excludeAreaId.trim())) {
				searchMap.put("excludeAreaId", excludeAreaId);
			}
			int count = menuInfoService.findMenuInfoCount(searchMap);
			if (count == 0) {
				result = true;
			}
		}
		return result;
	}

	/**
	 * 保存新增菜单！
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/saveAddMenu")
	@ResponseBody
	public Map<String, Object> addMenu(HttpServletRequest req) {
		String menuName = req.getParameter("addMenuName");
		String menuParentCode = req.getParameter("addMenuParentCode");
		String sortNo = req.getParameter("addSortNo");
		String addUrl = req.getParameter("addUrl");

		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		System.out.println(token + userId);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {

			MenuInfo menuInfo = new MenuInfo();
			menuInfo.setMenuName(menuName);
			menuInfo.setMenuParentCode(Integer.parseInt(menuParentCode));
			menuInfo.setMenuSortCode(Integer.parseInt(sortNo));
			menuInfo.setMenuAddress(addUrl);

			int success = menuInfoService.insertMenuInfo(menuInfo);
			if (success > 0) {
				resultMap.put("success", "1");
			} else {
				resultMap.put("msg", "保存失败");
			}
		} else {
			resultMap.put("msg", "请上传相关参数");
		}
		return resultMap;
	}

	// 初始化前台修改菜单数据
	@RequestMapping("/initUpdate")
	@ResponseBody
	public Map<String, Object> initUpdate(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		String menuId = req.getParameter("menuId");
		System.out.println("token=" + token + ",userId=" + userId + ",menuId=" + menuId);

		Map<String, Object> menuMap = new HashMap<String, Object>();
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<Map<String, Object>> menuList = new ArrayList<Map<String, Object>>();
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim()) && menuId != null
				&& !"".equals(menuId.trim())) {
			Map<String, Object> searchMap = new HashMap<String, Object>();
			searchMap.put("menuId", menuId);
			menuList = menuInfoService.findMenuInfoList(searchMap);
			menuMap = menuList.get(0);
			Map<String, Object> parentMap = new HashMap<String, Object>();
			parentMap.put("menuParentIdEqualsNull", "all");
			menuList = menuInfoService.findMenuInfoList(parentMap);
		}
		resultMap.put("menuList", menuList);
		resultMap.put("menuMap", menuMap);
		resultMap.put("success", "1");
		return resultMap;

	}

	/*
	 * 保存前台菜单
	 */
	@RequestMapping("/saveUpdate")
	@ResponseBody
	public Map<String, Object> saveUpdate(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String menuName = req.getParameter("updateMenuName");
		String menuId = req.getParameter("updateMenuId");
		String menuParentCode = req.getParameter("updateMenuParentCode");
		String sortNo = req.getParameter("updateSortCode");
		String addUrl = req.getParameter("updateUrl");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		System.out.println("token:"+token +"userID"+ userId+"menuID:"+menuId);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {

			MenuInfo menuInfo = new MenuInfo();
			menuInfo.setMenuName(menuName);
			if (menuParentCode != null) {
				menuInfo.setMenuParentCode(Integer.parseInt(menuParentCode));
			}
			if (sortNo != null) {
				menuInfo.setMenuSortCode(Integer.parseInt(sortNo));
			}
			if (menuId != null) {
				 menuInfo.setMenuId(Integer.parseInt(menuId));
			}
			menuInfo.setMenuAddress(addUrl);
           
			int success = menuInfoService.updateMenuInfo(menuInfo);
			if (success > 0) {
				resultMap.put("success", "1");
			} else {
				resultMap.put("msg", "保存失败");
			}
		} else {
			resultMap.put("msg", "请上传相关参数");
		}
		return resultMap;
	}

	// 前台菜单详情
	@RequestMapping("/detailMenu")
	@ResponseBody
	public Map<String, Object> detailFront(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");
		String menuId = req.getParameter("menuId");

		Map<String, Object> menuMap = new HashMap<String, Object>();
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<Map<String, Object>> menuList = new ArrayList<Map<String, Object>>();
		if (token != null && !"".equals(token.trim()) && userId != null && !"".equals(userId.trim())) {
			Map<String, Object> searchMap = new HashMap<String, Object>();
			searchMap.put("menuId", menuId);
			menuList = menuInfoService.findMenuInfoList(searchMap);
			menuMap = menuList.get(0);
		}
		resultMap.put("menuMap", menuMap);
		resultMap.put("success", "1");
		return resultMap;

	}

	// 删除前台菜单数据
	@RequestMapping("/delMenu")
	@ResponseBody
	public Map<String, Object> delete(HttpServletRequest req, HttpServletResponse resp)  {
		String menu_id = req.getParameter("menu_id");
		String token = req.getHeader("token");
		String userId = req.getHeader("userId");

		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("success", "0");
		if (menu_id != null && !"".equals(menu_id.trim()) && token != null && !"".equals(token.trim()) && userId != null
				&& !"".equals(userId.trim())) {
			 //判断是否有下级
			int success = menuInfoService.delMenuInfo(Integer.parseInt(menu_id));
			if (success > 0) {
				resultMap.put("success", "1");
			} else {
				resultMap.put("msg", "该菜单存在子菜单,不允许删除!");
			}
		}
		return resultMap;
	}
}
