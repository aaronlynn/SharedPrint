package com.sharedprint.system.menu.mapper;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.menu.pojo.MenuInfo;

public interface MenuInfoMapper {
    int deleteByPrimaryKey(Integer menuId);

    int insert(MenuInfo record);

    int insertSelective(MenuInfo record);

    MenuInfo selectByPrimaryKey(Integer menuId);

    int updateByPrimaryKeySelective(MenuInfo record);

    int updateByPrimaryKey(MenuInfo record);
    
    /**
	 * 获取菜单列表
	 * @param searchMap
	 * @return
	 */
	public List<Map<String,Object>> findMenuList(Map<String,Object> searchMap);
	
	/**
	 * 获取菜单条数
	 * @param searchMap
	 * @return
	 */
	public int findMenuCount(Map<String,Object> searchMap);
}