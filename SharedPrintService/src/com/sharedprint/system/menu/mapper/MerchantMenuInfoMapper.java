package com.sharedprint.system.menu.mapper;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.menu.pojo.MerchantMenuInfo;

public interface MerchantMenuInfoMapper {
    int deleteByPrimaryKey(Integer menuId);

    int insert(MerchantMenuInfo record);

    int insertSelective(MerchantMenuInfo record);

    MerchantMenuInfo selectByPrimaryKey(Integer menuId);

    int updateByPrimaryKeySelective(MerchantMenuInfo record);

    int updateByPrimaryKey(MerchantMenuInfo record);
    
    /**
   	 * 获取菜单列表
   	 * @param searchMap
   	 * @return
   	 */
   	public List<Map<String,Object>> findMenuList(Map<String,Object> searchMap);

}