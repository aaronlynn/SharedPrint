package com.sharedprint.system.menu.mapper;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.menu.pojo.AgentMenuInfo;

public interface AgentMenuInfoMapper {
    int deleteByPrimaryKey(Integer menuId);

    int insert(AgentMenuInfo record);

    int insertSelective(AgentMenuInfo record);

    AgentMenuInfo selectByPrimaryKey(Integer menuId);

    int updateByPrimaryKeySelective(AgentMenuInfo record);

    int updateByPrimaryKey(AgentMenuInfo record);
    
    /**
	 * 获取菜单列表
	 * @param searchMap
	 * @return
	 */
	public List<Map<String,Object>> findMenuList(Map<String,Object> searchMap);
}