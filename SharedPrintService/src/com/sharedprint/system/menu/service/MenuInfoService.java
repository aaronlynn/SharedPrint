package com.sharedprint.system.menu.service;

import java.util.List;
import java.util.Map;

import com.sharedprint.system.menu.pojo.MenuInfo;

public interface MenuInfoService {

	/**
	 * 获取菜单列表
	 * @param searchMap
	 * @return
	 */
	List<Map<String, Object>> findMenuInfoList(Map<String, Object> searchMap);
	
	/**
	 * 获取菜单列表数量
	 * @param searchMap
	 * @return
	 */
	int findMenuInfoCount(Map<String, Object> searchMap);

	/**
	 * 新增菜单
	 * @param menuInfo
	 * @return
	 */
	int insertMenuInfo(MenuInfo menuInfo);
	/**
	 * 更新菜单
	 * @param menuInfo
	 * @return
	 */
	int updateMenuInfo(MenuInfo menuInfo);
	/**
	 * 删除菜单
	 * @param menuInfo
	 * @return
	 */
	int delMenuInfo(int menuId);
}
