package com.sharedprint.system.menu.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sharedprint.system.menu.mapper.MenuInfoMapper;
import com.sharedprint.system.menu.pojo.MenuInfo;

@Service
public class MenuInfoServiceImpl implements MenuInfoService {

	@Autowired
	private MenuInfoMapper menuInfoMapper;
	
	@Override
	public List<Map<String, Object>> findMenuInfoList(Map<String, Object> searchMap) {
		return menuInfoMapper.findMenuList(searchMap);
	}

	@Override
	public int findMenuInfoCount(Map<String, Object> searchMap) {
		
		return menuInfoMapper.findMenuCount(searchMap);
	}

	@Override
	public int insertMenuInfo(MenuInfo menuInfo) {
		return menuInfoMapper.insertSelective(menuInfo);
	}

	@Override
	public int updateMenuInfo(MenuInfo menuInfo) {
	
		return menuInfoMapper.updateByPrimaryKeySelective(menuInfo);
	}

	@Override
	public int delMenuInfo(int menuId) {
		return menuInfoMapper.deleteByPrimaryKey(menuId);
	}

}
