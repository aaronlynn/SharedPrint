package com.sharedprint.system.menu.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sharedprint.system.menu.mapper.MerchantMenuInfoMapper;

@Service
public class MerchantMenuInfoServiceImpl implements MerchantMenuInfoService {
	@Autowired
	private MerchantMenuInfoMapper merchantMenuInfoMapper;
	@Override
	public List<Map<String, Object>> findMenuInfoList(Map<String, Object> searchMap) {
		
		return merchantMenuInfoMapper.findMenuList(searchMap);
	}

}
