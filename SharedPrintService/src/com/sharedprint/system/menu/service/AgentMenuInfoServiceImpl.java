package com.sharedprint.system.menu.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sharedprint.system.menu.mapper.AgentMenuInfoMapper;

@Service
public class AgentMenuInfoServiceImpl implements AgentMenuInfoService {

	@Autowired
	private AgentMenuInfoMapper agentMenuInfoMapper;
	@Override
	public List<Map<String, Object>> findMenuInfoList(Map<String, Object> searchMap) {
		
		return agentMenuInfoMapper.findMenuList(searchMap);
	}

}
