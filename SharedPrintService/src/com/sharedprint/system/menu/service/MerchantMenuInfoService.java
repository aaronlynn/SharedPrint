package com.sharedprint.system.menu.service;

import java.util.List;
import java.util.Map;

public interface MerchantMenuInfoService {
	/**
	 * 获取菜单列表
	 * @param searchMap
	 * @return
	 */
	List<Map<String, Object>> findMenuInfoList(Map<String, Object> searchMap);

}
