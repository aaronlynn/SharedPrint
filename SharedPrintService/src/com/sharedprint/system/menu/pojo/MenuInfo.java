package com.sharedprint.system.menu.pojo;

import java.util.Date;

public class MenuInfo {
    private Integer menuId;

    private String menuName;

    private Integer menuParentCode;

    private String menuAddress;

    private Integer menuSortCode;

    private Date menuOprdate;
    
    private String menuUrl;

    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName == null ? null : menuName.trim();
    }

    public Integer getMenuParentCode() {
        return menuParentCode;
    }

    public void setMenuParentCode(Integer menuParentCode) {
        this.menuParentCode = menuParentCode;
    }

    public String getMenuAddress() {
        return menuAddress;
    }

    public void setMenuAddress(String menuAddress) {
        this.menuAddress = menuAddress == null ? null : menuAddress.trim();
    }

    public Integer getMenuSortCode() {
        return menuSortCode;
    }

    public void setMenuSortCode(Integer menuSortCode) {
        this.menuSortCode = menuSortCode;
    }

    public Date getMenuOprdate() {
        return menuOprdate;
    }

    public void setMenuOprdate(Date menuOprdate) {
        this.menuOprdate = menuOprdate;
    }
    
    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl == null ? null : menuUrl.trim();
    }
}