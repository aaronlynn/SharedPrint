﻿package com.alipay.config;

import java.io.FileWriter;
import java.io.IOException;

/* *
 *类名：AlipayConfig
 *功能：基础配置类
 *详细：设置帐户有关信息及返回路径
 *修改日期：2017-04-05
 *说明：
 *以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 *该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
 */

public class AlipayConfig {
	
//↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓

	
	// 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
	public static String app_id = "2016101200669714";
	
	// 商户私钥，您的PKCS8格式RSA2私钥
    public static String merchant_private_key = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQC4wZxdsZztHfLsqUX/Y24bdF71U+qyOOSp6ur4AM58BNgxUa+lGXU4u8sd+QhnLFjguvLyL3J1QWloWDbwFJEW/Ym8r8OUNRjuQyInEDXlAG1Sl8r6GDhkfa6wMzm3kuPpDvP1fp5CxwwjF7w4lnvbLm6AUPqKs13yxjTfAJOlhgvgHYmZzdycxGb30hqbMNWZLyDNe7cJzO92mv2ZSIVdUjUKKIKB3jnQjKTJjatQ7LmSv48aZiUrcJZE6dvEWCieJkwaqv4+RCEcn5IkcJB73qGt9e4B4ZBeDJlUg4oT3AEI69jpP04SytIBT85exHA7D0jU0D41cpiY0w+3x6eDAgMBAAECggEBAKxpUOhGZTEJr7aWSJIJUakhMbQyuWPTn9/JL7h72DT3uGBd4FFrrm5hIS05RlQxmEcvbqP/DwrJHfqXx7zHFJalxzFjvFnEKx29XFOj1wd7HInkyTAWBhoRB0G6q/PZigHlSFgh7sJWScc90RHlrHNfoxLvfr6Mn/F8cb5hJWdiVMI480p/T3UERhXAbLaLase2OH0o1BqY9JAARNauY9WtZa4GF00fX0IdVl++j3/Z+jQr3Tf2fTyGy7YB4SW8WHScECv1FMN5SeDU/aM9blBEX2sELmeBNX+l5g1ETn4tMnoH4cvzX7fx9CXZTe9wTj3IWdLuwEahMxVGxUHEy/ECgYEA96IgOVrpRn8WIKaJrgSvjJ1AhzKeHpdJr1qDEKVIh2qMcodrkNIKME5XPDOS+nyiFMy3K4wrmGaPaYTKUZP/7lUZ50dL4AI1ot5E42mt/ts7qSGe645wmkWjZzXPPHw059bHqYm9EdHlL/V3O+ze+5HvjWhGwLycpdmxahVdfQ8CgYEAvv+jS0Inxz+Yi5xGZXFXnQG4mfFBsx9EkxhQdwZJgJKVjtNtEEfQkdP/iufRUhe8lUKUQicqS5c2JaxIqlisa/FPajVocPQgUb9GklThL63elmZ3vLlQRhV7u2uWyyzUlFbbvrTnda2hemYA9VQ5zXFZTEiT9UhoRWHamh76Vk0CgYEA92UvkF105IsezivTLNldkLuhtfAp7CH06Y0hl8WoujEGc307R4mvyCbcHDbiRgGx49nJvWXiP/VmnWKexfV4IOFAacDe2sMaVsZ8SD5SKGVaK1R9cpPxhw6/euNVUPY93MjE3GnRTDZIJCu1nVa+RtK7Yqik1vUYAtGD35nMtp0CgYAnNbfCJWyhHM8mKgjRnnkMBGRvRfXk3dBoGh+6r5G7UVxem8zSAMwyg8D8NSbloXXXdnmVMxEdv4cgKMQqJFYROtLAzQIJ3jQcVBYayE4KSbleSt7d4XU3TgW+rhGg2C0z3rjx55V0tRZnWDVuJ8JOCKz6WhP10M2HINWNujpOKQKBgCSMrVL6iWr8sQNpE4HCFCxi3HMeEw017luSUY2ShcbU4/iBHwNv4WbkNoYFJPs8k/r5RR8bfrAs08Bz+E+qPmPScGg6UCXwarO8iapho8/jnQ3D57tg6eZ0lLl4OF49mYNvWLiig2dVMudAdy9grUL/6bYv57M3lWGhIeBM2BQa";
	
	// 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgQQFcb1GeyL9IZJyIj+5V4JHCN2Y358yn1D4hGRiXIny9lF4+4HxYFp7gKVprYnLZ3YjqWAA98ZFG/vUpgHUaZQMALOFnKK/8EV1HFSsg2rr5TvL2yeVRaiwc9kYRbuYNniAlEVrDizFTzNM8K0IDyNN7Ht2+/ym2N10JxaBfi0IlFtxuT6xYYTL04IYkRiDp1WPRTmSeEdIcAbNvpHCDV2NJocKF7FThhyQJc3AJyXNqFUXcVH+1OfdwjiGhr/OhyfvGcjjbzwz9X0a3tga/DIDT2oxRPtk2Nc6E2M+P9eGh45Fc+eXAhX2lldOcrT6nKhcYXNGIHEZ6Of+HiH+wwIDAQAB";

	// 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	public static String notify_url = "http://localhost:8080//SharedPrintShoppingMall/notify_url.jsp";

	// 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	public static String return_url = "http://localhost:8080/SharedPrintShoppingMall/payed.jsp";

	// 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	public static String order_url = "http://localhost:8080/SharedPrintShoppingMall/payedFile.jsp";
		
	// 签名方式
	public static String sign_type = "RSA2";
	
	// 字符编码格式
	public static String charset = "utf-8";
	
	// 支付宝网关
	public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";
	
	// 支付宝网关
	public static String log_path = "D:\\";
	


//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

    /** 
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     * @param sWord 要写入日志里的文本内容
     */
    public static void logResult(String sWord) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis()+".txt");
            writer.write(sWord);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

